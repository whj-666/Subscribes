<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:set var="ctx" value="${pageContext.request.contextPath}" />
<body>

<nav class="navbar navbar-fixed-top navbar-inverse">
	<div class="container">
		<div class="navbar-header" style="width: 80%;">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${ctx }/applyIndex.jsp"><img src="${ctx }/upload/img/yuyuelogo.png" /></a>
			<a class="navbar-menu" href="${ctx }/time.jsp">预约看板</a>
			<a class="navbar-menu" href="${ctx }/applyIndex.jsp">设备预约</a>
			<a class="navbar-menu" href="${ctx }/myRooms.jsp">我的预约</a>
			<a class="navbar-menu" href="${ctx }/InvitedRecord.jsp">受邀记录</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav pull-right">
				<li id="login">
					<a href="" style="color: #fff;font-weight: 300;border-radius: 2px;margin-left: 8px;line-height: inherit;height: inherit;" data-toggle="modal" id="loginbtn" data-target="#loginPane">登录</a>
				</li>
				<li class="dropdown" id="myroom">
					<a id="username" href="#"></a>
					<ul class="dropdown-menu">
						<%-- <li><a href="${ctx }/myRooms.jsp"><span
										class="glyphicon glyphicon-home"></span> &nbsp;我的预约</a></li> --%>
						<%-- <li><a href="${ctx }/myservice/myinforms/"><span
										class="glyphicon glyphicon-bell"></span>
										&nbsp;通&nbsp;&nbsp;知&nbsp;&nbsp;<span id="unreadinf"></span></a></li> --%>

						<li>
							<a href="JavaScript:void(0)" data-url="${ctx }/user/logout" id="logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;退出登录</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- 消息提示 -->

<!--登录模态框-->
<div id="loginPane" class="modal fade"	onkeydown="keyLogin();">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
						</button>
        <h1 class="text-center text-primary" id="myModalLabel">登录</h1>
      </div>
      <div class="modal-body">
        <form class="form col-md-12 center-block">
        <p hidden="hidden" style="color: red" id="login-tip"></p>
          <div class="form-group">
            <input type="text" class="form-control input-lg" placeholder="请输入用户名或邮箱" name="username" id="login-username" >
          </div>
          <div class="alert alert-danger" id="usertip" hidden="hidden">
			<span class="glyphicon glyphicon-remove-sign"></span>&nbsp;<span id="user-cont"></span>
		  </div>
          <div class="form-group">
            <input type="password" class="form-control input-lg" name="password" placeholder="请输入登录密码" aria-describedby="pwd" id="login-password" />
          </div>
          <div class="alert alert-danger" id="passwordtip" hidden="hidden">
			<span class="glyphicon glyphicon-remove-sign"></span>&nbsp;<span id="password-cont"></span>
		  </div>
          <div class="form-group">
            <a id="login-btn" class="btn btn-primary btn-lg btn-block">登 录</a>
            <!-- <span><a href="#">找回密码</a></span>
            <span><a href="#" class="pull-right">注册</a></span> -->
          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- 只有关闭弹出框 -->
<div class="modal fade" tabindex="0" role="dialog" id="showCloseDialog_Modal" data-backdrop="false">
	<div class="modal-dialog close-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				<h4 class="modal-title">提示</h4>
			</div>
			<div class="modal-body">
				<span id="showCloseDialog_content"></span>
			</div>
			<div class="modal-footer">
				<button type="button" id="showCloseDialog_QxButton" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
	
	$(function () { 
		/* ajax登录 */
		var email = getQueryString("portalUser");
		isportal(email);
		
	});
	function getQueryString(name) { 
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
		var r = window.location.search.substr(1).match(reg); 
		if (r != null)
			return unescape(r[2]);
			return null; 
		} 
	//判断是否登录
	function isportal(email){
		if(email == null){
			isLogin();
			return false;
		}else{
			var loadInfo = '${ctx }' + "/login";
			$.ajax({
				url: loadInfo,
				type: 'GET',
				dataType: 'json',
				cache: false,
				data: {
					subscribe_user_email: email,
					
				},
				beforeSend: LoadFunction, //加载执行方法
				error: erryFunction, //错误执行方法
				success: succFunction //成功执行方法
			})

			function LoadFunction() {
				//加载执行方法
			}

			function erryFunction() {
				//错误执行方法
				isLogin();
				
			}

			function succFunction(JsonString) {
				var json = eval(JsonString);
				var emails=json.extend.success;
				var loadInfo = '${ctx }' + "/selectByPersonEmail";
				$.ajax({
					url: loadInfo,
					type: 'GET',
					dataType: 'json',
					cache: false,
					data: {
						email: emails,
					},
					beforeSend: LoadFunction, //加载执行方法
					error: erryFunction, //错误执行方法
					success: succFunction //成功执行方法
				})

				function LoadFunction() {
					//加载执行方法
				}

				function erryFunction() {
					//错误执行方法
					isLogin();
					
				}

				function succFunction(JsonString) {
					var json = eval(JsonString);
					console.log(JSON.stringify(json));
					if(json.code > 0) {

						var ss = json.extend.success[0];
						window.sessionStorage.setItem("userid", ss.userid);
						window.sessionStorage.setItem("usertruename", ss.usertruename);
						window.sessionStorage.setItem("email", ss.email);
						window.sessionStorage.setItem("mobilephone", ss.mobilephone);
						window.sessionStorage.setItem("userteam", ss.userteam);
						window.sessionStorage.setItem("organizeallname", ss.organizeallname);
						window.sessionStorage.setItem("idnumber", ss.idnumber);
						
						console.log("------------------------");
						var ok = window.sessionStorage.getItem("usertruename");

						
					
						
						$("#username").append(
							"<span class='hiuser glyphicon glyphicon-user'></span><a class='hiuser' id='username' href='#'>" + "欢迎，" + ss.usertruename + "</a>"
						);
						$('#username').show();
						$("#login").hide();
						if (json.extend.success[0].isapplication == "0"){
							setTimeout(function(){
								window.location.href="application-1.jsp";
								},1400);
						}
					} else {
						
						isLogin();

					}

				}
				

			}
			return true;
		}
		}
	//回车登录
	function keyLogin(){
		 if (event.keyCode==13)  //回车键的键值为13
		   document.getElementById("login-btn").click(); //调用登录按钮的登录事件
		}
	$("#login-btn").click(function() {
		var username = $("#login-username").val();
		var password = $("#login-password").val();
		if(username == '') {
			$("#user-cont").html('未输入用户名或邮箱！');
			$("#usertip").show(300);
			setTimeout(function(){
				$("#usertip").hide();
				},2000);
			return false;
		} else if(password == "") {
			$("#password-cont").html("未输入密码！");
			$("#passwordtip").show(300);
			setTimeout(function(){
				$("#passwordtip").hide();
				},2000);
			return false;
		}
		var loadInfo = '${ctx }' + "/selectByPersonEmailAndPassword";
		$.ajax({
			url: loadInfo,
			type: 'GET',
			dataType: 'json',
			cache: false,
			data: {
				email: username,
				password: password
			},
			beforeSend: LoadFunction, //加载执行方法
			error: erryFunction, //错误执行方法
			success: succFunction //成功执行方法
		})

		function LoadFunction() {
			//加载执行方法
		}

		function erryFunction() {
			//错误执行方法

		}

		function succFunction(JsonString) {
			var json = eval(JsonString);
			console.log(JSON.stringify(json));
			if(json.code > 0) {

				var ss = json.extend.success[0];
				window.sessionStorage.setItem("userid", ss.userid);
				window.sessionStorage.setItem("usertruename", ss.usertruename);
				window.sessionStorage.setItem("email", ss.email);
				window.sessionStorage.setItem("mobilephone", ss.mobilephone);
				window.sessionStorage.setItem("userteam", ss.userteam);
				window.sessionStorage.setItem("organizeallname", ss.organizeallname);
				window.sessionStorage.setItem("idnumber", ss.idnumber);
				
				console.log("------------------------");
				var ok = window.sessionStorage.getItem("usertruename");

				$('#showCloseDialog_content').html("登录成功");
				$('#showCloseDialog_Modal').modal('show');
				setTimeout(function(){
					$("#showCloseDialog_Modal").modal("hide"),
					$('#loginPane').modal('hide')
					},2000);
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
					$('#loginPane').modal('hide');
				});
				$("#username").append(
					"<span class='hiuser glyphicon glyphicon-user'></span><a class='hiuser' id='username' href='#'>" + "欢迎，" + ss.usertruename + "</a>"
				);
				
				$('#username').show();
				$("#login").hide();
				if (json.extend.success[0].isapplication == "0"){
					setTimeout(function(){
						window.location.href="application-1.jsp";
						},1400);
				}
				Agreement();
			} else {
				//设置提示内容
				$('#showCloseDialog_content').html(json.extend.fail);
				//显示提示框
				$('#showCloseDialog_Modal').modal('show');
				//监听关闭按钮
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
				});

			}

		}
	});
	//退出
	$("#logout").click(function logout() {
		window.sessionStorage.clear();
		document.getElementById('username').innerHTML = "";
		$('#username').hide();
		$('#login').show();
		
	});
	

	
	function isLogin() {
		var usertruename = window.sessionStorage.getItem("usertruename");
		if(usertruename == null) {
			
				$("#loginPane").modal('show');
				$('#username').hide();
				$('#login').show();
			
			return false;
		} else {
			document.getElementById('username').innerHTML = "";
			$("#username").append(
				"<span class='hiuser glyphicon glyphicon-user'></span><a class='hiuser' id='username' href='#'>" + "欢迎，" + usertruename + "</a>"
			);
			$("#login").hide();
			return true;
		}
	}
</script>
</body>