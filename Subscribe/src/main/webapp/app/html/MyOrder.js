
var SubscribecnArray;//存储用户预约记录的数组

function getSubscribeByEmailAndName(email,name) {

    var getSubscribeByEmailAndName_URL = URL_1+"getSubscribeByEmailAndNameAndTime"; //

    $.ajax({
        url: getSubscribeByEmailAndName_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data:{
          subscribe_user_email:email,
          subscribe_user_name:name
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
//$(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-预约信息列表-超时");
    }

    function succFunction(JsonString) {
        var title_color;

        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.getSubscribeByEmailAndName;
            SubscribecnArray = new Array(dataJson.length);
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
              SubscribecnArray[i]=dataJson[i];
              //解析JSON
              var id = dataJson[i]["id"];

              var subscribeStatus = dataJson[i]["subscribeStatus"];
              var subscribeStatusValue = "";
              if (subscribeStatus == 1 || subscribeStatus == 2) {
                  subscribeStatusValue = "进行中";
                  title_color = "#9edaf6";
              } else if (subscribeStatus == 3 ) {
                  subscribeStatusValue = "已结束";
                  title_color = "orange";
              } else if (subscribeStatus == 4) {
                  subscribeStatusValue = "已撤销";
                  title_color = "red";
              }else if (subscribeStatus == 5) {
                  subscribeStatusValue = "已终止";
                  title_color = "orange";
              }
              var subscribeBeginTime = fmtDate(dataJson[i]["subscribeBeginTime"]);
              var subscribeEndTime =fmtDate(dataJson[i]["subscribeEndTime"]);
              var deviceType =dataJson[i]["device"]["deviceType"];
              var deviceNumber=dataJson[i]["device"]["deviceNumber"];
              var devicePlace=dataJson[i]["device"]["devicePlace"];

                htmlString +="<div class='max_box'  onclick='subscribeOnclick(\"" + id + "\")'>"+
                "<div class='title' id='title' style='background-color:"+title_color+"'>"+
                "</div><div class='tp'><img src='../img/l1.jpg'><div class='state'><font id='state'>'"+subscribeStatusValue+
                "'</font></div></div><div class='starttime'>预约时间：<font id='StartOrderTime'>"+subscribeBeginTime+
                "'</font></div><div class='endtime'>结束时间：<font id='EndOrderTime'>"+subscribeEndTime+
                "'</font></div><div class='equipment'>预约设备：<font id='orderEquipment'>"+deviceNumber+
                "'</font></div><div class='position'>设备位置：<font id='EquipmentPoint'>"+devicePlace+
                "'</font></div></div>"

                $("title").css("background-color","#000");
            }
            //渲染数据
            document.getElementById('max_box').innerHTML=htmlString;
        } else {
            window.native.toast(json.msg);
        }
    }
};
function subscribeOnclick(id){

   var flage="MyOrderPage";//1代表 从 我的预约界面 跳转
   openOrderDetailedWin("OrderDetailed",id,flage);

}



function openOrderDetailedWin(name,id,flage) {

       window.open("OrderDetailed.html?OrderId="+id+"&Pageflage="+flage+"");
}


function fmtDate(obj) {
    var date = new Date(obj);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}
