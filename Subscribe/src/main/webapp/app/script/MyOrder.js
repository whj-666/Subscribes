﻿
var SubscribecnArray;//存储用户预约记录的数组


var aaa=0;
var aaa2=0;
var aaa3=0;
var aaa4=0;
var bbb=0;
var bbb2=0;
var bbb3=0;
var bbb4=0;


//本方法只为了给mui-badge 赋值。统计预约记录里各项的个数
function getSubscribeByEmailAndName(email,name,state) {
    //更改为可查询全部记录的链接
    var getSubscribeByEmailAndName_URL = URL_1+"getSubscribeByEmailAndName"; 

    $.ajax({
        url: getSubscribeByEmailAndName_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data:{
          subscribe_user_email:email,
          subscribe_user_name:name
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
        //$(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-预约信息列表-超时");
    }

    function succFunction(JsonString) {
        var title_color;
        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.getEmailAndName;
            SubscribecnArray = new Array(dataJson.length);
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
          SubscribecnArray[i]=dataJson[i];
          //解析JSON
          var id = dataJson[i]["id"];
      
          var subscribeStatus = dataJson[i]["subscribeStatus"];
          var subscribeStatusValue = "";
          if (subscribeStatus == 1 || subscribeStatus == 2) {
            aaa++;
            
          } else if (subscribeStatus == 3 ) {
            aaa2++;
    	  } else if (subscribeStatus == 4) {
            aaa3++;
		  }else if (subscribeStatus == 5) {
            aaa4++;
			  	}
          /* 由于状态值1或2都被判定为进行中状态
           * 则在传递过来的参数state为1，
           * 当前的状态值subscribeStatus为2时也将其判断为进行中*/
         
            }
            //alert(aaa);
            if(aaa==0){
            document.getElementById("aaa").style.cssText ="visibility:hidden;"
            }
            if(aaa2==0){
            document.getElementById("aaa2").style.cssText ="visibility:hidden;"
            }
            if(aaa3==0){
            document.getElementById("aaa3").style.cssText ="visibility:hidden;"
            }
            if(aaa4==0){
            document.getElementById("aaa4").style.cssText ="visibility:hidden;"
            }
            
            document.getElementById("aaa").innerHTML=aaa+"";
            document.getElementById("aaa2").innerHTML=aaa2+"";
            document.getElementById("aaa3").innerHTML=aaa3+"";
            document.getElementById("aaa4").innerHTML=aaa4+"";
          
        } 
    }
};
//本方法只为了给mui-badge 赋值。统计受邀记录里各项的个数
function selectSubscribeSonByEmailAndName_Invite(email,name,state) {
       //alert(email+""+name+""+state);
    //更改为可查询全部记录的链接
    var selectSubscribeSonByEmailAndName_Invite_URL = URL_1+"selectSubscribeSonByEmailAndName"; 

    $.ajax({
        url: selectSubscribeSonByEmailAndName_Invite_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data:{
          subscribe_user_email:email,
          subscribe_user_name:name
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
       // $(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-邀约记录-超时");
    }

    function succFunction(JsonString) {
        var title_color;
        json = eval(JsonString);
        if (json.code > 0) {  
            var dataJson = json.extend.selectByEmailAndName;
            
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
			  	//解析JSON
			  	var subscribeUserStatus = dataJson[i]["subscribeUserStatus"];
			  	if(subscribeUserStatus == 0){
                 bbb++;
                }else if(subscribeUserStatus == 1){
                 bbb2++;
                }else if(subscribeUserStatus == 2){
                 bbb3++;
                }else if(subscribeUserStatus == 3){
                 bbb4++;
                }
            }
            if(bbb==0){
            document.getElementById("bbb").style.cssText ="visibility:hidden;"
            }
            if(bbb2==0){
            document.getElementById("bbb2").style.cssText ="visibility:hidden;"
            }
            if(bbb3==0){
            document.getElementById("bbb3").style.cssText ="visibility:hidden;"
            }
            if(bbb4==0){
            document.getElementById("bbb4").style.cssText ="visibility:hidden;"
            }
            
            document.getElementById("bbb2").innerHTML=bbb2+"";
            document.getElementById("bbb3").innerHTML=bbb3+"";
            document.getElementById("bbb4").innerHTML=bbb4+"";
            document.getElementById("bbb").innerHTML=bbb+"";
        }
    }
};

function subscribeOnclick(id){

   var flage="MyOrderPage";//1代表 从 我的预约界面 跳转
   openOrderDetailedWin("OrderDetailed",id,flage);

}



function openOrderDetailedWin(name,id,flage) {

       window.open("OrderDetailed.html?OrderId="+id+"&Pageflage="+flage+"");
}


function fmtDate(obj) {
    var date = new Date(obj);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}
