﻿var DeviceID = ""; //设备ID
var DeviceImagesArray; //设备图片数组
var CatagpruName; //设备类别名称
var Order_peopleArray; //预约人姓名
var Order_qishishijianArray; //预约起始时间
var Order_jiezhishijianArray; //预约截止时间
var deviceType;


var UserName ;//用户的姓名
var UserEmail;//用户的邮箱 唯一标识
var UserId ; //用户的邮箱 唯一标识 后台改后删除这个字段 暂时用的和用户邮箱一样的数据
var deviceNumber;
var weekStr;
var endTime;
var startTime;
var subscribeUserName;
var Order_button;
function getPageValue() {
    //获取设备ID
    DeviceID = utils.getParam("DeviceId");
    //设备类别名称
    CatagpruName = utils.getParam("CatagpruName");
	


    //根据获得好的设备ID获取设备信息
    getDeviceDetailedByDeviceID(DeviceID);
    //根据获得好的设备ID获取设备图片
    getDeviceImagesByDeviceID(DeviceID);
    //根据获得好的设备ID获取预约信息
    getDeviceOrderDetailedByDeviceID(DeviceID);
	
};


function getDeviceDetailedByDeviceID(id) {
//alert(id);
        var getDeviceDetailedByDeviceID_URL = URL_1+"getDeviceDataById"; //查询设备详情根据设备ID

    $.ajax({
        url: getDeviceDetailedByDeviceID_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: {
            DeviceId: id
        },
    
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })




    function erryFunction() {
        alert("获取-设备列表-超时");
    }
    function succFunction(json) {
	

            if (json.code > 0) {
            var dataJson = eval(json.extend.deviceId);
            var id = dataJson.id; // 主键ID 根据主键ID查出该设备的全部信息
            var deviceNo = dataJson.deviceNo; //设备序号
            deviceNumber = dataJson.deviceNumber; // //设备编号
            var deviceBrand = dataJson.deviceBrand; // 品牌
            deviceType = dataJson.deviceType; // 型号名称
            var deviceClass = dataJson.deviceClass; // 类别
            var devicePhoto = dataJson.devicePhoto; // 图片url地址id
            var devicePlace = dataJson.devicePlace; // 位置描述
            var deviceStatus = dataJson.deviceStatus; // 设备状态（1正常2使用中3维修4下线）
            var DeviceStatusValue = "";
            if (deviceStatus == 1 || deviceStatus == 2) {
                DeviceStatusValue = "可预约";
                document.getElementById("reserve_button_appearance").style.background ="#29b6f6";
                //alert("不合理");
                document.getElementById("reserve_button_appearance").removeAttribute("disabled");
            } else if (deviceStatus == 3) {
                DeviceStatusValue = "维修中";
                document.getElementById("reserve_button_appearance").style.background ="#666666";
                //alert("不合理");
                document.getElementById("reserve_button_appearance").setAttribute('disabled','disabled');
            } else if (deviceStatus == 4) {
                DeviceStatusValue = "已下线";
                document.getElementById("reserve_button_appearance").style.background ="#666666";
                //alert("不合理");
                document.getElementById("reserve_button_appearance").setAttribute('disabled','disabled');
            }
            var flag = dataJson.flag; //状态值判断（0删除1没删除）
            var deviceRemark = dataJson.deviceRemark; //备注
            //获取设备信息 渲染数据
            document.getElementById('deviceNo').innerHTML = deviceNumber; //设备序号
            document.getElementById('deviceBrand').innerHTML = deviceBrand; // 品牌
            document.getElementById('deviceType').innerHTML = deviceType; // 型号名称
            document.getElementById('DeviceStatusValue').innerHTML = DeviceStatusValue; // 设备状态
            document.getElementById('devicePlace').innerHTML = devicePlace; // 位置描述
            document.getElementById('CatagpruName').innerHTML = CatagpruName; // 类别
        } else {
            alert(json.msg);
        }
    }
};

//根据获得好的设备ID获取设备图片
function getDeviceImagesByDeviceID(id) {
        
    var getDeviceandImgByDeviceID_URL = URL_1+"getImgListByDeviceId"; //查询设备图片根据设备ID

    $.ajax({
        url: getDeviceandImgByDeviceID_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: {
            DeviceId: id
        },
       
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

  

    function erryFunction() {
        alert("获取-设备图片-超时");
    }

    function succFunction(JsonString) {
        // $(".middle_EquipmentDisplay_div_name").html('');
        // $(".middle_EquipmentDisplay_div_orderState").html('');
        json = eval(JsonString);
        if (json.code > 0) {
            var orderJson = json.extend.deviceImg;
            // DeviceImagesArray = new Array(orderJson.length);
            var htmlString_img = "";
            if (orderJson.length != 0) {
                for (var i = 0; i < orderJson.length; i++) {
                    htmlString_img += "<div class='mui-slider-item'><a href='#'><img src='"+URL_img+orderJson[i]["fileurl"]+"' /></a></div>";
                }
            } else {
                htmlString_img = "<div class='mui-slider-item' ><div class='title'><img src='../img/nopic.jpg '></div></div>";
            }

            document.getElementById('swiper-wrapper1').innerHTML = htmlString_img;
               imageScoll();
        } else {
            alert(json.msg);
        }
        // $(".middle_EquipmentDisplay_div_freeTime").html('');


    }
};

function imageScoll (){
//获得slider插件对象
var gallery = mui('.mui-slider');
gallery.slider({
  interval:5000//自动轮播周期，若为0则不自动播放，默认为0；
});
}


function getDeviceOrderDetailedByDeviceID(id) {
    var getSubscribeByDeviceID_URL = URL_1+"getSubscribeByIdAndStatus"; //查询设备预约列表根据设备ID

    $.ajax({
        url: getSubscribeByDeviceID_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: {
            subscribe_device_id: id
        },
       
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    

    function erryFunction() {
        alert("获取-预约信息-超时");
    }

    function succFunction(JsonString) {

        json = eval(JsonString);
        //alert(JSON.stringify(json));
        if (json.code > 0) {
            var dataJson = json.extend.getSubscribeByIdAndStatus;
            var htmlString_order = "";

            for (var i = 0; i < dataJson.length; i++) {

                if(i == 0){
                    htmlString_order += "<ul class='mui-table-view'>";
                }
                //获取预约时间的星期
                var end = new Date(dataJson[i]["subscribeEndTime"]);
                weekStr = "星期" + "日一二三四五六".charAt(end.getDay());
                /*2017-12-10 10:00裁切之后的日期格式文字：10:00*/
                endTime = fmtDate(dataJson[i]["subscribeEndTime"]).substring(11,16);
                startTime = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(11,16);
                var month = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(5,7);
                var day = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(8,10);
                subscribeUserName = dataJson[i]["subscribeUserName"];
                    
                Order_button = "<div class='col-xs-3 ' style='text-align:center;line-height:34.3px;color:#42A5F5;'" + " onclick='detailerButtonClick(\"" + dataJson[i].id + "\")'>详情</div>" ;//预约截至时间
               
                htmlString_order += '<li class="mui-table-view-cell"  style="padding: 3px 15px;"><div class="col-xs-3" style="text-align:center;line-height:34.3px;font-size:12px;">' + subscribeUserName + '</div>' +
                    '<div class="col-xs-6" style="font-size:12px;line-height:35px;text-align: center;font-size:12px;word-break:keep-all;white-space:nowrap; ">' +month+"-"+day+" "+weekStr+" "+ startTime + "~" + endTime + '</div>'
                    + Order_button + '</li>';
                 
                if(i == dataJson.length-1){
                    htmlString_order += "</ul>";
                }
            }
            //渲染
            document.getElementById('reserve_information_content_div').innerHTML = htmlString_order;

        } else {
           document.getElementById('reserve_information_content_div').innerHTML = "";
        }

    }
};

function detailerButtonClick(id) {
    //  alert("单击穿过来的id"+id);
    var flage = "EquipmentDetailedPage"; //1代表 从 我的预约界面 跳转
    openOrderDetailedWin("OrderDetailed", id, flage);
}

function openOrderDetailedWin(name, id, flage) {

    window.open("OrderDetailed.html?OrderId="+id+"&Pageflage=EquipmentDetailedPage");
}


function openWin() {
   // window.location.href ="oa_note.html?DeviceId="+DeviceID+"&deviceName="+deviceType;
	//获取用户信息
	window.native.webGetUserinfo("userCallBlack");
}

function userCallBlack(user){

var UserTel ="";
         json = JSON.parse(user);
		 UserName = json.user_name;
		 UserEmail = json.user_email;
		 UserId = json.user_id;
		 UserTel = json.user_mobile_phone;
		 UserTeamId =json.user_team;
		  //alert("返回的    UserName ="+UserName+"    UserEmail="+UserEmail+"    UserId="+UserId);
		
		window.location.href="orderApply.html?deviceNumber="+deviceNumber+"&DeviceId="+DeviceID+"&deviceName="+deviceType+"&orderUserID="+UserId+"&orderUserName="+UserName+"&UserEmail="+UserEmail+"&UserTel="+UserTel+"&UserTeamId="+UserTeamId+"&CatagpruName="+CatagpruName+"";

}

function closeWin() {

    window.close();
}

function fmtDate(obj) {
    var date = new Date(obj);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}
