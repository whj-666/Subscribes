﻿var equipmentNameArray; //设备类别数组
var CatagpruId; //设备类别ID
var CatagpruName;//设备类别名称
var deviceListArray; //设备列表数组
// var deviceListArray_1; //设备列表数组
var DeviceDataJson = "";
//查询设备类别列表
function getDeviceCatagory() {

    var device_catagory_URL =URL_1+"device_catagoryService";

    $.ajax({
        url: device_catagory_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
        //$(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-设备类别列表-超时");
    }

    function succFunction(JsonString) {
        // $(".middle_EquipmentDisplay_div_name").html('');
        // $(".middle_EquipmentDisplay_div_orderState").html('');
        // $(".middle_EquipmentDisplay_div_freeTime").html('');
        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.device;
          //  alert(dataJson.length);
          //  equipmentNameArray = new Array(dataJson.length+1);
            equipmentNameArray = new Array(dataJson.length);
            for (var i = 0; i < dataJson.length; i++) {

                var id = dataJson[i]["id"]; // 主键ID
                var equipmentNumber = dataJson[i]["equipmentNumber"]; // 设备类别编号
                var equipmentName = dataJson[i]["equipmentName"]; // 设备类别名称
                var equipmentSpell = dataJson[i]["equipmentSpell"]; // 设备类别简拼
                var equipmentFirstSpell = dataJson[i]["equipmentFirstSpell"]; // 设备类别首拼

                // var obj = {
                //     "id": id,
                //     "value": equipmentName
                // };
                var obj = {
                    "id": id,
                    "value": equipmentName
                };
                var obj_1 = {
                    "id": 0,
                    "value": "全部"
                };
                if(i==0){
                    equipmentNameArray[0] = obj_1;
                      equipmentNameArray[1] = obj;
                }else{
                    equipmentNameArray[i+1] = obj;
              }
                // equipmentNameArray[i] = obj;
            }
            //获取完设备类别后 准备获取所有设备信息 默认显示设备类别编号为1的设备

           //getDeviceListByID(1);
            getDeviceListByID_all();
        } else {
            window.native.toast(json.msg);
        }
    }
};



//查询所有类别的设备
function getDeviceListByID_all() {
    //URL拼接
        var getDeviceListById_URL = URL_1+"getAllDeviceByFlagQ";
    $.ajax({
        url: getDeviceListById_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
       // $(".header1").html('加载中...');

    }

    function erryFunction() {
        window.native.toast("获取-设备列表-超时");
    }

    function succFunction(JsonString) {

        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.DeviceFlag;
            deviceListArray = new Array(dataJson.length);
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
                //记录从JSON解析出的对象放入数组里面
                deviceListArray[i]=dataJson[i];
                var id = dataJson[i]["id"]; // 主键ID 根据主键ID查出该设备的全部信息
                var deviceNo = dataJson[i]["deviceNo"]; //设备序号
                var deviceNumber = dataJson[i]["deviceNumber"]; // //设备编号
                var deviceBrand = dataJson[i]["deviceBrand"]; // 品牌
                var deviceType = dataJson[i]["deviceType"]; // 型号名称
                var deviceClass = dataJson[i]["deviceClass"]; // 类别
                var devicePhoto = URL_img+dataJson[i]["devicePhoto"]; // 图片url地址id
                 //alert(devicePhoto);
                var devicePlace = dataJson[i]["devicePlace"]; // 位置描述
                var deviceStatus = dataJson[i]["deviceStatus"]; // 设备状态（1正常2使用中3维修4下线）
                
                var equipmentName = dataJson[i]["equipmentName"];
                var DeviceStatusValue = "";
                var zi_color;
                if (deviceStatus == 1 || deviceStatus == 2) {
                    DeviceStatusValue = "可预约";
                    zi_color=" border: 1px solid #46bff7 ;color:#46bff7";
                } else if (deviceStatus == 3) {
                    DeviceStatusValue = "维修中";
                      zi_color=" border: 1px solid #fd971f ;color:#fd971f";
                } else if (deviceStatus == 4) {
                    DeviceStatusValue = "已下线";
                      zi_color=" border: 1px solid #ea2000 ;color:#ea2000";
                }
                var flag = dataJson[i]["flag"]; //状态值判断（0删除1没删除）
                var deviceRemark = dataJson[i]["deviceRemark"]; //备注
                  //style=\"  border: 1px solid #46bff7\"
                htmlString += "<div class='middle_EquipmentDisplay_div' id='middle_EquipmentDisplay_div_" + i + " ' onclick='middle_EquipmentDisplay_div_img_onclick(\""+id+"\",\""+equipmentName+"\")'>" + //外层显示DIV
                    "<div class='middle_EquipmentDisplay_div_img'  id='middle_EquipmentDisplay_div_img_" + i + "'><img src='" +devicePhoto + "'>" + //图片显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_name' id='middle_EquipmentDisplay_div_name_" + i + "'>" + deviceType + //名称显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_no'  id='middle_EquipmentDisplay_div_no_" + i + "'>" + DeviceStatusValue + //预约状态显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_orderState' style=\"  "+zi_color+"\"    id='middle_EquipmentDisplay_div_orderState_" + i + "'>" + deviceNumber + //设备序号显示DIV
                    "</div></div>"
            }
          
            document.getElementById('middle_div').innerHTML=htmlString;

        } else {
            window.native.toast(json.msg);
        }
    }
};

//根据id查询设备列表
function getDeviceListByID(id) {
//alert(id);

    var getDeviceListById_URL = URL_1+"getDeviceListByCatagoryId";

    $.ajax({
        url: getDeviceListById_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data:{
        //  CatagoryId:id+1
        CatagoryId:id
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
       // $(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-设备列表-超时");
    }

    function succFunction(JsonString) {
        // $(".middle_EquipmentDisplay_div_name").html('');
        // $(".middle_EquipmentDisplay_div_orderState").html('');
        // $(".middle_EquipmentDisplay_div_freeTime").html('');
        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.deviceClass;
            deviceListArray = new Array(dataJson.length);
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
                //记录从JSON解析出的对象放入数组里面
                deviceListArray[i]=dataJson[i];
                var id = dataJson[i]["id"]; // 主键ID 根据主键ID查出该设备的全部信息
                var deviceNo = dataJson[i]["deviceNo"]; //设备序号
                var deviceNumber = dataJson[i]["deviceNumber"]; // //设备编号
                var deviceBrand = dataJson[i]["deviceBrand"]; // 品牌
                var deviceType = dataJson[i]["deviceType"]; // 型号名称
                var deviceClass = dataJson[i]["deviceClass"]; // 类别
                var devicePhoto = URL_img+dataJson[i]["devicePhoto"]; // 图片url地址id 
                var devicePlace = dataJson[i]["devicePlace"]; // 位置描述
                var deviceStatus = dataJson[i]["deviceStatus"]; // 设备状态（1正常2使用中3维修4下线）
                var equipmentName = dataJson[i]["equipmentName"]; 
                var DeviceStatusValue = "";
                var zi_color;
                if (deviceStatus == 1 || deviceStatus == 2) {
                    DeviceStatusValue = "可预约";
                    zi_color=" border: 1px solid #46bff7 ;color:#46bff7";
                } else if (deviceStatus == 3) {
                    DeviceStatusValue = "维修中";
                      zi_color=" border: 1px solid #fd971f ;color:#fd971f";
                } else if (deviceStatus == 4) {
                    DeviceStatusValue = "已下线";
                      zi_color=" border: 1px solid #ea2000 ;color:#ea2000";
                }
                var flag = dataJson[i]["flag"]; //状态值判断（0删除1没删除）
                var deviceRemark = dataJson[i]["deviceRemark"]; //备注
                  //style=\"  border: 1px solid #46bff7\"
                  //alert(id+"----"+equipmentName);
                htmlString += "<div class='middle_EquipmentDisplay_div' id='middle_EquipmentDisplay_div_" + i + " ' onclick='middle_EquipmentDisplay_div_img_onclick(\""+id+"\",\""+equipmentName+"\")'>" + //外层显示DIV
                    "<div class='middle_EquipmentDisplay_div_img'  id='middle_EquipmentDisplay_div_img_" + i + "'><img src='" + devicePhoto + "'>" + //图片显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_name' id='middle_EquipmentDisplay_div_name_" + i + "'>" + deviceType + //名称显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_no'  id='middle_EquipmentDisplay_div_no_" + i + "'>" + DeviceStatusValue + //预约状态显示DIV
                    "</div><div class='middle_EquipmentDisplay_div_orderState' style=\"  "+zi_color+"\"    id='middle_EquipmentDisplay_div_orderState_" + i + "'>" + deviceNumber + //设备序号显示DIV
                    "</div></div>"
            }
           document.getElementById('middle_div').innerHTML=htmlString;

		
        } else {
            window.native.toast(json.msg);
        }
    }
};




function top_nav_leftDiv_onClick() {
    var top_nav_leftDiv_title_titleDom = document.querySelector('#top_nav_leftDiv_title_title');
    var top_nav_leftDiv_title_idDom = document.querySelector('#top_nav_leftDiv_title_id');
    var top_nav_leftDiv_title_id = top_nav_leftDiv_title_titleDom.dataset['id'];
    var top_nav_leftDiv_title = top_nav_leftDiv_title_titleDom.dataset['value'];
    var bankSelect = new IosSelect(1, [equipmentNameArray], {
        container: '.container',
        title: '设备类别',
        itemHeight: 50,
        itemShowCount: 3,
        oneLevelId: top_nav_leftDiv_title_id,
        callback: function(selectOneObj) {
            top_nav_leftDiv_title_idDom.value = selectOneObj.id;
            top_nav_leftDiv_title_titleDom.innerHTML = selectOneObj.value;
            top_nav_leftDiv_title_titleDom.dataset['id'] = selectOneObj.id;
            top_nav_leftDiv_title_titleDom.dataset['value'] = selectOneObj.value;
            //类别id
            CatagpruId = top_nav_leftDiv_title_idDom.value;
            // CatagpruName=top_nav_leftDiv_title_titleDom.value;
            if(CatagpruId==0){
              //alert("全部");
              getDeviceListByID_all();
            }else{
            //  alert(CatagpruId);
              getDeviceListByID(CatagpruId);
            }


        }
    });

}


function top_nav_rightDiv_onClick() {
    var top_nav_rightDiv_title_titleDom = document.querySelector('#top_nav_rightDiv_title_title');
    var top_nav_rightDiv_title_idDom = document.querySelector('#top_nav_rightDiv_title_id');
    var top_nav_rightDiv_title_id = top_nav_rightDiv_title_titleDom.dataset['id'];
    var top_nav_rightDiv_title = top_nav_rightDiv_title_titleDom.dataset['value'];
    var bankSelect = new IosSelect(1, [data], {
        container: '.container',
        title: '可用状态',
        itemHeight: 50,
        itemShowCount: 3,
        oneLevelId: top_nav_rightDiv_title_id,
        callback: function(selectOneObj) {
            top_nav_rightDiv_title_idDom.value = selectOneObj.id;
            top_nav_rightDiv_title_titleDom.innerHTML = selectOneObj.value;
            top_nav_rightDiv_title_titleDom.dataset['id'] = selectOneObj.id;
            top_nav_rightDiv_title_titleDom.dataset['value'] = selectOneObj.value;
            //  alert(top_nav_rightDiv_title_idDom.value);
             getDeviceListBydeviceStatus(top_nav_rightDiv_title_idDom.value); // 1和2 代表可预约 3 代表维修中 4代表已下线
        }
    });

}
//根据可用状态来查询设备
function getDeviceListBydeviceStatus(deviceStatus) {


      var htmlString_deviceStatus= "";
    for (var i = 0; i < deviceListArray.length; i++) {
        var device = deviceListArray[i];
        
        if (device.deviceStatus == deviceStatus) {
        var id = device.id; // 主键ID 根据主键ID查出该设备的全部信息
          var deviceStatus = device.deviceStatus; // 设备状态（1正常2使用中3维修4下线）
          var DeviceStatusValue = "";
          var zi_color;
          if (deviceStatus == 1 || deviceStatus == 2) {
              DeviceStatusValue = "可预约";
              zi_color=" border: 1px solid #46bff7 ;color:#46bff7";
          } else if (deviceStatus == 3) {
              DeviceStatusValue = "维修中";
                zi_color=" border: 1px solid #fd971f ;color:#fd971f";
          } else if (deviceStatus == 4) {
              DeviceStatusValue = "已下线";
                zi_color=" border: 1px solid #ea2000 ;color:#ea2000";
          }
         //  alert(device.deviceType + "    "+device.deviceNumber);
         //alert( URL_img+device.devicePhoto);
          htmlString_deviceStatus +="<div class='middle_EquipmentDisplay_div' id='middle_EquipmentDisplay_div_" + i + " ' onclick='middle_EquipmentDisplay_div_img_onclick(\""+id+"\",\""+device.equipmentName+"\")'>" + //外层显示DIV
              "<div class='middle_EquipmentDisplay_div_img'  id='middle_EquipmentDisplay_div_img_" + i + "'><img src='" + URL_img+device.devicePhoto + "'>" + //图片显示DIV
              "</div><div class='middle_EquipmentDisplay_div_name' id='middle_EquipmentDisplay_div_name_" + i + "'>" + device.deviceType + //名称显示DIV
              "</div><div class='middle_EquipmentDisplay_div_no' id='middle_EquipmentDisplay_div_no_" + i + "'>" + DeviceStatusValue + //预约状态显示DIV
              "</div><div class='middle_EquipmentDisplay_div_orderState'  style=\"  "+zi_color+"\"     id='middle_EquipmentDisplay_div_orderState_" + i + "'>" + device.deviceNumber + //设备序号显示DIV
              "</div></div>"

        }
   document.getElementById('middle_div').innerHTML=htmlString_deviceStatus;

    }
}

function middle_EquipmentDisplay_div_img_onclick(id,CatagpruName) {
    //alert(id+"-------"+CatagpruName)
 // alert(CatagpruName);
   window.location.href="EquipmentDisplayDetailed.html?DeviceId="+id+"&CatagpruName="+CatagpruName+"";
//     window.location.href = "./EquipmentDisplayDetailed.html?id=" + id;

}
