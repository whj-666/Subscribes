﻿
var SubscribecnArray;//存储用户预约记录的数组

function getSubscribeByEmailAndName(email,name,state) {
    
    var getSubscribeByEmailAndName_URL = URL_1+"getSubscribeByEmailAndName"; 

    $.ajax({
        url: getSubscribeByEmailAndName_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data:{
          subscribe_user_email:email,
          subscribe_user_name:name
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
      //  $(".header1").html('加载中...');
    }

    function erryFunction() {
        window.native.toast("获取-预约信息列表-超时");
    }

    function succFunction(JsonString) {
        var title_color;
        json = eval(JsonString);

        if (json.code > 0) {
            var dataJson = json.extend.getEmailAndName;
            SubscribecnArray = new Array(dataJson.length);
            var htmlString = "";
            for (var i = 0; i < dataJson.length; i++) {
			  	SubscribecnArray[i]=dataJson[i];
			  	//解析JSON
			  	var id = dataJson[i]["id"];
			
			  	var subscribeStatus = dataJson[i]["subscribeStatus"];
				var subscribeRemark="无";
				subscribeRemark=dataJson[i]["subscribeRemark"];
			  	var subscribeStatusValue = "";
			  	if (subscribeStatus == 1 || subscribeStatus == 2) {
			      	subscribeStatusValue = "进行中";
			  		title_color = "#9edaf6";
			  	} else if (subscribeStatus == 3 ) {
			      	subscribeStatusValue = "已结束";
			  		title_color = "orange";
			  	} else if (subscribeStatus == 4) {
			     	subscribeStatusValue = "已撤销";
			  		title_color = "red";
			  	}else if (subscribeStatus == 5) {
			      	subscribeStatusValue = "已终止";
			  		title_color = "#333";
			  	}
			  	/* 由于状态值1或2都被判定为进行中状态
			  	 * 则在传递过来的参数state为1，
			  	 * 当前的状态值subscribeStatus为2时也将其判断为进行中*/
			  	if((state == 1 && subscribeStatus == 2) || subscribeStatus == state){
			  	  var end = new Date(dataJson[i]["subscribeEndTime"]);
                var weekStr = "星期" + "日一二三四五六".charAt(end.getDay());
				  	var deviceType = dataJson[i]["device"]["deviceType"];
				  	var deviceNumber = dataJson[i]["device"]["deviceNumber"];
				  	var devicePlace = dataJson[i]["device"]["devicePlace"];
				  	 endTime = fmtDate(dataJson[i]["subscribeEndTime"]).substring(11,16);
                startTime = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(11,16);
                var month = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(5,7);
                var day = fmtDate(dataJson[i]["subscribeBeginTime"]).substring(8,10);
                       
	                htmlString +="<div class='max_box'  onclick='subscribeOnclick(\"" + id + "\")'>"+
	                "<div class='title' id='title' style='background-color:"+title_color+"'>"+
	                "</div><div class='tp'><img src='../img/l1.jpg'><div class='state'><font id='state'>"+subscribeStatusValue+
	                "</font></div></div><div class='starttime'>预约时间：<font id='StartOrderTime'>"+month+"-"+day+" "+weekStr+" "+ startTime + "~" + endTime +
	                "</font></div><div class='endtime'>设备名称：<font id='EndOrderTime'>"+deviceType+
	                "</font></div><div class='equipment'>设备编号：<font id='orderEquipment'>"+deviceNumber+
	                "</font></div><div class='position'>设备位置：<font id='EquipmentPoint'>"+devicePlace+
	                "</font></div><div class='position'>备注：<font id='EquipmentPoint'>"+subscribeRemark+"</font></div></div>"
			  	}else{
			  		/*Do Nothing*/
			  	}
            }
            if(htmlString.toString().length <= 0){
            	htmlString = "<div class='nodata col-xs-12 col-sm-12'>暂 无 数 据</div>";	
            }
            //渲染数据
            document.getElementById('max_box').innerHTML=htmlString;
        } else {
            window.native.toast(json.msg);
        }
    }
};

// function selectSubscribeSonByEmailAndName(email,name,state) {
    
//     var selectSubscribeSonByEmailAndName_URL = URL_1+"selectSubscribeSonByEmailAndName"; 

//     $.ajax({
//         url: selectSubscribeSonByEmailAndName_URL,
//         type: 'GET',
//         dataType: 'json',
//         cache: false,
//         data:{
//           subscribe_user_email:email,
//           subscribe_user_name:name
//         },
//         beforeSend: LoadFunction, //加载执行方法
//         error: erryFunction, //错误执行方法
//         success: succFunction //成功执行方法
//     })

//     function LoadFunction() {
//         $(".header1").html('加载中...');
//     }

//     function erryFunction() {
//         window.native.toast("获取-邀约记录-超时");
//     }

//     function succFunction(JsonString) {
//         var title_color;
//         json = eval(JsonString);
//         if (json.code > 0) {
//             alert("访问成功");
//         } else {
//             window.native.toast(json.msg);
//         }
//     }
// };





function subscribeOnclick(id){

   var flage="MyOrderPage";//1代表 从 我的预约界面 跳转
   openOrderDetailedWin("OrderDetailed",id,flage);

}



function openOrderDetailedWin(name,id,flage) {
       window.open("OrderDetailed.html?OrderId="+id+"&Pageflage="+flage+"");
}
function fmtDateym(stime,etime) {
    var startTime = new Date(stime);
    var endTime = new Date(etime);
    var weekStr = "星期" + "日一二三四五六".charAt(startTime.getDay());
//  var y = date.getFullYear();
    var m = startTime.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = startTime.getDate();
    d = d < 10 ? ('0' + d) : d;
    var startHour = startTime.getHours();
    startHour = startHour < 10 ? ('0' + startHour) : startHour;
    var startminute = startTime.getMinutes();
    startminute = startminute < 10 ? ('0' + startminute) : startminute;
    
    var endHour = endTime.getHours();
    endHour = endHour < 10 ? ('0' + endHour) : endHour;
    var endminute = endTime.getMinutes();
    endminute = endminute < 10 ? ('0' + endminute) : endminute;
    
    var result = m + '月' + d + '日 ' +weekStr +" "+ startHour + ':' + startminute + '~'
    + endHour +":"+ endminute;
    return result;
};


function fmtDate(obj) {
    var date = new Date(obj);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}
