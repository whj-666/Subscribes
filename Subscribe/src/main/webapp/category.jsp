<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>
<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
	<table id="category"></table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="category_tool.add();">添加</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit-new" onclick="category_tool.edit();">修改</a><a href="#"
				class="easyui-linkbutton" plain="true" iconCls="icon-delete-new"
				onclick="category_tool.remove();">删除</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-reload" plain="true"
				onclick="category_tool.reload();">刷新</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="category_tool.redo();">取消选择</a>
		</div>
	</div>
	<form action="${pageContext.request.contextPath}/UploadHandleServlet" enctype="multipart/form-data" method="post">
        
        上传图片：<input type="file" class="easyui-filebox" name="file1"><input type="submit" value="提交"><br/>
    </form>
	<div id="page_nav_area"
		style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
	<!-- 新增窗口 -->
	<form id="category_add" action="${APP_PATH }/UploadHandleServlet" enctype="multipart/form-data" method="post"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			类别名称：<input type="text" name="category_add" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			设备简拼：<input type="text" name="category_spell_add" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			设备首拼：<input type="text" name="category_first_add" class="textbox"
				style="width: 150px;">
		</p>
        	图片上传：<input class="easyui-filebox"   name="file2"><button onclick="${APP_PATH }/UploadHandleServlet" id="upload_btn" style="background-color:#666666; display:none;">上传</button><br/>
	</form>
	<!-- 修改窗口 -->
	<form id="category_edit"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			类别名称：<input type="text" name="category_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			设备简拼：<input type="text" name="category_spell_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			设备首拼：<input type="text" name="category_first_edit" class="textbox"
				style="width: 150px;">
		</p>
		  	图片上传：<input class="easyui-filebox"   name="file3"><button onclick="${APP_PATH }/UploadHandleServlet" id="upload_btn1" style="background-color:#666666; display:none;">上传</button><br/>
	</form>
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		var js_message = '<%=request.getAttribute("message")%>'
		$(function() {
			function getData(pagenum) {
				$.ajax({
					url : "${APP_PATH }/device_catagoryFlag?pn=" + pagenum,
					type : "GET",
					dataType : "json",
					cache: false,
					success : function(data) {
						var pageinfo = data.extend.pageInfo;
						build_page_nav(data);
						//加载分页数据
						$("#category").datagrid("loadData", pageinfo.list); //动态取数据
					}
				});
			}
			//查询数据
			$("#category").datagrid({
				fit : true,
				iconCls : "icon-manager",
				rownumbers : true,
				striped : true,
				fitColumns : true,
				columns : [ [ {
					field : 'id',
					title : 'id',
					sortName : 'id', //排序字段
					idField : 'id', //标识字段,主键
					checkbox : true,
					width : 100,
				}, {
					field : "equipmentName",
					title : '类别名称',
					halign : "center",
					width : 100,
				}, {
					field : "equipmentSpell",
					title : '设备简拼',
					halign : "center",
					width : 100,
				}, {
					field : "equipmentFirstSpell",
					title : '设备首拼',
					halign : "center",
					width : 100,
				},{
					field : "images",
					title : '图片路径',
					halign : "center",
					width : 100,
				},] ],

				toolbar : "#tb",
			});
			//loadData easyui类

			/*    $("#category").datagrid("loadData", data.extend.pageInfo.list);  //动态取数据
			   
			}
			}); */
			getData(1);

			// 分页条
			function build_page_nav(result) {
				// 清空分页条
				$("#page_nav_area").empty();
				// page_nav_area
				var ul = $("<ul></ul>").addClass("pagination");

				var firstPageLi = $("<li></li>").append(
						$("<a></a>").append("首页").attr("href", "#"));
				var prePageLi = $("<li></li>").append(
						$("<a></a>").append("&laquo;"));
				// 判断是否有前一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasPreviousPage == false) {
					firstPageLi.addClass("disabled");
					prePageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件
					firstPageLi.click(function() {
						getData(1);
					});
					prePageLi.click(function() {
						getData(result.extend.pageInfo.pageNum - 1);
					});
				}

				var nextPageLi = $("<li></li>").append(
						$("<a></a>").append("&raquo;"));
				var lastPageLi = $("<li></li>").append(
						$("<a></a>").append("末页").attr("href", "#"));
				// 判断是否有下一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasNextPage == false) {
					nextPageLi.addClass("disabled");
					lastPageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件

					nextPageLi.click(function() {
						getData(result.extend.pageInfo.pageNum + 1);
					});
					lastPageLi.click(function() {
						getData(result.extend.pageInfo.pages);
					});
				}

				// 添加首页和前一页 的提示
				ul.append(firstPageLi).append(prePageLi);
				// 1.2.3.4.5 遍历给ul中添加页码提示
				$.each(result.extend.pageInfo.navigatepageNums, function(index,
						item) {

					var numLi = $("<li></li>")
							.append($("<a></a>").append(item));
					// 显示当前页
					if (result.extend.pageInfo.pageNum == item) {
						numLi.addClass("active");
					}
					// 绑定单机事件
					numLi.click(function() {
						getData(item);
					});
					ul.append(numLi);
				});
				// 添加下一页和末页 的提示
				ul.append(nextPageLi).append(lastPageLi);

				// 把ul加入到nav
				var navEle = $("<nav></nav>").append(ul);
				navEle.appendTo("#page_nav_area");
			}
			
			
			
			//新增管理
			$("#category_add")
					.dialog(
							{
								width : 300,
								title : "新增管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#category_add").form(
														"validate")) {
													var strFileName = $('input[name="file2"]').val();
													//转义符
													var i =strFileName.lastIndexOf('\\');
													var str =strFileName.substring(i+1,strFileName.length);
													document.getElementById("upload_btn").click();
													$
															.ajax({
																url : "${APP_PATH }/deviceADD",
																type : "GET",
																data : {

																	equipmentName : $(
																			'input[name="category_add"]')
																			.val(),
																	equipmentSpell : $(
																			'input[name="category_spell_add"]')
																			.val(),
																	equipmentFirstSpell : $(
																			'input[name="category_first_add"]')
																			.val(),
																	images:str,
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在新增中...",
																			});
																	
																},

																success : function(data,response,status) {
																	$.messager.progress("close");
																	if (data) {
																		console.log(data);
																		$.messager.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$("#category_add").dialog("close").form("reset");
																		$("#category").datagrid("reload");
																		
																	} else {
																		$.messager
																				.alert(
																						"新增失败!",
																						"未知错误导致失败,请重试!",
																						"warning");
																	}
																	getData(1);
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$("#category_add").dialog(
														"close").form("reset");
											},
										} ],
							});

			//修改管理
			$("#category_edit")
					.dialog(
							{
								width : 350,
								title : "修改管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",

											handler : function() {
												if ($("#category_edit").form(
														"validate")) {
													var strFileName = $('input[name="file3"]').val();
													//转义符
													var i =strFileName.lastIndexOf('\\');
													var str =strFileName.substring(i+1,strFileName.length);
													if(str == null ||str=="" ){
														$
																.ajax({
																	url : "${APP_PATH }/decUpdate",
																	type : "GET",
																	data : {
																		id : id1,
																		equipmentName : $(
																				'input[name="category_edit"]')
																				.val(),
																		equipmentSpell : $(
																				'input[name="category_spell_edit"]')
																				.val(),
																		equipmentFirstSpell : $(
																				'input[name="category_first_edit"]')
																				.val(),
																		
																	},
																	beforeSend : function() {
																		$.messager
																				.progress({
																					text : "正在修改中...",
																				});
																	},
																	success : function(
																			data,
																			response,
																			status) {
																		$.messager
																				.progress("close");
																		if (data) {
																			$.messager
																					.show({
																						title : "提示",
																						msg : data.msg,
																					});
																			$(
																					"#category_edit")
																					.dialog(
																							"close")
																					.form(
																							"reset");
																			$(
																					"#category")
																					.datagrid(
																							"reload");
																		} else {
																			$.messager
																					.alert(
																							"修改失败!",
																							"未知错误或没有任何修改,请重试!",
																							"warning");
																		}
																		getData(1);
																	}
																});
													}else{
														document.getElementById("upload_btn1").click();
														$
																.ajax({
																	url : "${APP_PATH }/decUpdate",
																	type : "GET",
																	data : {
																		id : id1,
																		equipmentName : $(
																				'input[name="category_edit"]')
																				.val(),
																		equipmentSpell : $(
																				'input[name="category_spell_edit"]')
																				.val(),
																		equipmentFirstSpell : $(
																				'input[name="category_first_edit"]')
																				.val(),
																		images : str,
																	},
																	beforeSend : function() {
																		$.messager
																				.progress({
																					text : "正在修改中...",
																				});
																	},
																	success : function(
																			data,
																			response,
																			status) {
																		$.messager
																				.progress("close");
																		if (data) {
																			$.messager
																					.show({
																						title : "提示",
																						msg : data.msg,
																					});
																			$(
																					"#category_edit")
																					.dialog(
																							"close")
																					.form(
																							"reset");
																			$(
																					"#category")
																					.datagrid(
																							"reload");
																		} else {
																			$.messager
																					.alert(
																							"修改失败!",
																							"未知错误或没有任何修改,请重试!",
																							"warning");
																		}
																		getData(1);
																	}
																});
													}
													/* document.getElementById("upload_btn1").click();
													$
															.ajax({
																url : "${APP_PATH }/decUpdate",
																type : "POST",
																data : {
																	id : id1,
																	equipmentName : $(
																			'input[name="category_edit"]')
																			.val(),
																	equipmentSpell : $(
																			'input[name="category_spell_edit"]')
																			.val(),
																	equipmentFirstSpell : $(
																			'input[name="category_first_edit"]')
																			.val(),
																	images : str,
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在修改中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$(
																				"#category_edit")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#category")
																				.datagrid(
																						"reload");
																	} else {
																		$.messager
																				.alert(
																						"修改失败!",
																						"未知错误或没有任何修改,请重试!",
																						"warning");
																	}
																	getData(1);
																}
															}); */
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$('#category_edit').dialog(
														'close').form('reset');
											},
										} ],
							});

			//类别名称
			$("input[name='category_add']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell_add']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first_add']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});

			//类别名称
			$("input[name='category_edit']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell_edit']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first_edit']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});
			category_tool = {
				reload : function() {
					$("#category").datagrid("reload");
					//刷新目前解决办法
					getData(1);
				},
				redo : function() {
					$("#category").datagrid("unselectAll");
				},
				add : function() {
					$("#category_add").dialog("open");
					$("input[name='category_equipmentNumber_add']").focus();
				},
				edit : function() {
					var rows = $("#category").datagrid("getSelections");
					if (rows.length > 1) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) {
						/* console.log(rows[0]); */
						$
								.ajax({
									url : "${APP_PATH }/device",
									type : "GET",
									data : {
										id : rows[0].id
									/* $('input[name="category_ID"]').val() */
									},
									beforeSend : function() {
										$.messager.progress({
											text : "正在获取中...",
										});
									},
									success : function(data, response, status) {
										$.messager.progress("close");
										if (data) {
											id1 = data.extend.devices.id;
											$('#category_edit')
													.form(
															'load',
															{
																category_edit : data.extend.devices.equipmentName,
																category_spell_edit : data.extend.devices.equipmentSpell,
																category_first_edit : data.extend.devices.equipmentFirstSpell,

															}).dialog('open');
										} else {
											$.messager.alert("获取失败!",
													"未知错误导致失败,请重试!", "warning");
										}
									}
								});
					} else if (rows.length == 0) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					}
				},
				remove : function() {
					var rows = $("#category").datagrid("getSelections");
					var temID="";
		            for (i = 0; i < rows.length;i++) {
		                if (temID =="") {
		                    temID = rows[i].id;
		                } else {
		                    temID = rows[i].id + "," + temID;
		                }               
		            }
					if (rows.length < 1) {
						$.messager.alert("警告操作!", "请选择要删除的数据!", "warning");
					}else{
						
						console.log(rows[0].id);
						$.ajax({
							type : "POST",
							url : "${APP_PATH }/dev",
							data : {
								id : temID,
								flag : "0",

							},

							beforeSend : function() {
								$("#category").datagrid("loading");
							},
							success : function(data) {
								if (data) {
									getData(1);
									$("#category").datagrid("loaded");
									//刷新当前页
									$("#category").datagrid("load");
									//取消所有行选定
									$("#category").datagrid("unselectAll");
									if(data.code=="0"){
										$.messager.show({
											title : "提示",
											msg : data.msg+":  "+data.extend.fail,
										});
									}else{
										$.messager.show({
											title : "提示",
											msg : data.msg,
										});
									}
								} else {
									$.messager.alert("获取失败!", "未知错误导致失败,请重试!",
											"warning");
								}
							},
						});
					}
				},
			};
		});
	</script>
</body>
</html>