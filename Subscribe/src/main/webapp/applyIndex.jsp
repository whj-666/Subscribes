<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!--<link rel="icon" href="../../favicon.ico">-->

		<title>预约管理系统</title>

		<script src="${ctx }/easyui/jquery.min.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script src="${ctx }/My97DatePicker/My97DatePicker/WdatePicker.js"></script>
		<link href="${ctx }/css/base.css" rel="stylesheet">
		<link href="${ctx }/css/apply-index.css" rel="stylesheet">
		<link href="${ctx }/css/apply-form.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- Required Stylesheets -->
		<link href="${ctx }/css/time.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap-treeview.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap-treeview.min.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/default.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/normalize.css" rel="stylesheet">
		<link href="${ctx }/css/base.css" rel="stylesheet">
		<link href="${ctx }/css/apply-index.css" rel="stylesheet">
		<link href="${ctx }/css/apply-form.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/pgwmenu.css" rel="stylesheet">

		<!-- Required Javascript -->
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap-treeview.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap-treeview.min.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/applyIndex.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/pgwmenu.min.js"></script>

	</head>

	<body>
		<!-- 使用动态include指令导入头部 -->
		<jsp:include page="header.jsp" />

		<section id="main">
			<!-- <div class="navbar2">
       			<div class="navbar-inner">
          			<ul class="nav topul" id="topul">
            			<li><a href="#" class="allType" onclick="RefreshAll()">所有设备</a></li>
            			<li><a href="#">Menu2</a></li>
            			<li class="dropdown"> <a href="#menu3">Menu3</a>
              				<ul class="dropdown-menu">
                				<li><a href="#menu7">Menu7</a></li>
								<li><a href="#menu8">Menu8</a></li>
              				</ul>
            			</li>
            			<li><a href="#">Menu4</a></li>
            			<li><a href="#">Menu5</a></li>
            			<li><a href="#">Menu6</a></li>
          			</ul>
        		</div>
      		</div> -->
      		
      		<div class="navbar2">
	        	<div class="">
					<ul class="pgwMenu na" id="topul">
						<li><a class="selected" href="" onclick="RefreshAll()">所有设备</a></li>
							<!-- <li><a href="#">代码</a></li>
							<li><a href="#">素材</a></li>
							<li><a href="#">模板</a></li>
						
							<li><a href="javascript:">关于</a></li>
							<li><a href="javascript:">服务</a></li>
							<li><a href="#">联系</a></li>
							<li><a href="#">代码</a></li>
							<li><a href="#">素材</a></li>
							<li><a href="#">模板</a></li>
						
							<li><a href="javascript:">关于</a></li>
							<li><a href="javascript:">服务</a></li>
							<li><a href="#">联系</a></li>
							<li><a href="#">代码</a></li>
							<li><a href="#">素材</a></li>
							<li><a href="#">模板</a></li>
						
							<li><a href="javascript:">关于</a></li>
							<li><a href="javascript:">服务</a></li>
							<li><a href="#">联系</a></li> -->
					</ul>
				</div>
			</div>
		
			<div>
				<div class="col-sm-10 " id="space-list" style="width: 100%;maigin: 0 auto;">
					<p class="apply-tip"></p>
					<!-- 已登录 -->
					<div class="row">
						<!-- <div class="b-title">教学楼</div> -->

						<div id="device_Category" class="building-div clearfix">
							<img class="imgshebei" src="${ctx }/upload/img/shebei.png" />
							<a class="target-fix"></a> <span id="classTitle"></span>
						</div>
						<div id="device" class="row">
						</div>
					</div>
				</div>
			</div>
		</section>
		<br>

		<!-- 使用动态include指令导入底部版权栏 -->
		<jsp:include page="footer.jsp" />

		<!-- 消息提示框 -->
		<div class="modal fade" id="MsgDialog" tabindex="1" style="z-index:9999" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
						<h2 class="modal-title">温馨提示</h2>
					</div>
					<div class="modal-body" id="MsgMessage">
						<h3>请求失败,请您重新预约</h3>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>

		<!-- 表单弹出框 -->
		<div class="modal fade " id="applyform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
						<h3 class="modal-title" id="myModalLabel">预约申请</h3>

					</div>
					<div class="modal-body">
						<div id="form-div">
							<p class="form-time" id="form-time"></p>
							<form method="post">
								<fieldset id="data_person" style="width: 52%;float: left;margin-right: 3%;margin-top: -10px;">
									<legend>申请者信息</legend>
									<p>
										<label>预&nbsp;&nbsp;&nbsp;&nbsp;约人：</label>
										<span name="username"></span>
									</p>

									<p>
										<label>手&nbsp;&nbsp;&nbsp;&nbsp;机号：</label> <span name="phone"></span>
									</p>

								</fieldset>
								<fieldset id="data_device" style="width: 44%;">
									<legend>设备信息</legend>
								</fieldset>
								<fieldset>
									<legend>邀约伙伴</legend>
									<p id="submite-btn" class="addperson-btn">
										<label>选择人员：</label>
										<a id="app-btn" onclick="Addperson();">添加人员</a>
									</p>
									<fieldset id="searchperson" style="display:none;">
										<legend>您邀约的伙伴</legend>
									</fieldset>
								</fieldset>

								<fieldset>
									<legend>填写表单</legend>

									<p>
										<label>选择日期：</label>
										<input name="FINHOSPDATE_BEGIN" style="width: 190px;" id="DATE"  onclick="WdatePicker({minDate:'%y-%M-{%d}',maxDate:'%y-%M-{%d+6}',disabledDays:[0],dchanged:dayChanged,onpicked:function(){MINDATE.click();}})" class="Wdate" readonly/><br>
									</p>
									<p>
										<label>预约时间：</label>
										<div id="SelectOrderData">
											<input name="FINHOSPDATE_BEGIN" style="width: 190px;" id="MINDATE" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'MAXDATE\')}',qsEnabled:true,onpicked:function(){MAXDATE.click();},dateFmt:'HH:mm',minTime:'08:30',maxTime:'20:30',Hchanged:hourChanged,qsEnabled:false});" class="Wdate" readonly/> 至
											<input name="FINHOSPDATE_END" style="width: 190px;" id="MAXDATE" onclick="WdatePicker({minDate:'#F{$dp.$D(\'MINDATE\')}',dateFmt:'HH:mm',minTime:'08:30',mchanged:minuteChanged ,Hchanged:hourChanged1 ,maxTime:'20:30'});" class="Wdate" readonly/>
										</div>
									</p>
									<p>
										<label>备注：</label>
										<input class="input_Remark" maxlength="30" placeholder="备注(30个字以内)" name="reason" id="form-reason"></input>
									</p>
								</fieldset>

								<p id="submite-btn">
									<a id="app-btn" onclick="isSubmit();">提交申请</a>
								</p>
							</form>
							<div class="modal-footer">
								<button type="button"  id="closebtn" onclick="CloseBtn()" class="btn btn-default" data-dismiss="modal">关闭</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 申请场所时信息提示 -->
		<div class="modal fade" id="applyMSG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
						<h2 class="modal-title">温馨提示</h2>
					</div>
					<div class="modal-body">
						<h3 id="apply-message"></h3>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!-- 协议页 -->
		<div class="modal fade" id="agreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
			<div class="modal-dialog" role="document" style="width: 700px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
						<h2 class="modal-title">职工健身中心入场须知</h2>
					</div>
					<div class="modal-body">
					一、	集团职工首次进入健身中心需提前填写手机端电子版入会申请登记表，  
						每次健身员工须用手机在程序中预约入场健身，方可进入健身中心。
						严禁冒名顶替，严禁本公司工作人员私自带家属及朋友进场。<br/>
					二、	每次使用场地内有氧设备、乒乓球、羽毛球、台球需要预约使用。<br/>
					三、	进入健身中心需穿运动服、运动鞋（室内专用），请勿穿拖鞋、高跟鞋、    
						钉子鞋、硬底鞋在健身场所内走动或健身。<br/>
					四、	严禁在健身区和前台公共区域赤背、赤脚、打闹。<br/>
					五、	请自觉保持训练场地、更衣室及淋浴间的环境卫生，严禁在训练场地内 
						进食、喝饮料、吸烟、随地吐痰及吐口香糖、乱扔杂物，禁止将碳酸、果汁饮料带进场地内饮用。<br/>
					六、	酒后、过饱、过饥者请不要健身。<br/>
					七、	患有疾病、传染病者请勿入健身中心。<br/>
					八、	如首次健身，使用健身器材前请教练示范后使用，自由力量区要有人保  
						护的情况下使用。<br/>
					九、	当使用健身设备出现问题时，请第一时间告知教练或工作人员。<br/>
					十、	发现故意损坏健身设备和器材者将追究责任，并按价赔偿。<br/>
					十一、健身器材要轻拿轻放，用后请放回原处，方便他人使用。<br/>
					十二、场地内空调、电源开关、由健身中心工作人员操作，其他人员不得擅自
						变更。<br/>
					十三、健身中心西侧门为紧急安全出口，只有在发生火灾、地震等紧急情况下
						使用，平时禁止出入，如违反规定造成不良后果将追究当事人责任。<br/>
					十四、请妥善保管好私人物品，如有丢失健身中心概不负责。<br/>
					十五、对健身中心工作人员服务不满意或不认可，请向值班经理或工会相关人  
						员投诉。投诉电话：15043426222郭经理<br/>



                                              		<div style="float:right;">中国第一汽车集团公司总工会宣</div><br/>
                                              		
					
					</div>
					<div class="modal-footer">
							<table class="fenye-table">
								<tr>
									<td>
										
									</td>
								</tr>
							</table>
						<button type="button" class="btn btn-primary" id="agree" disabled="disabled">同意</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="disagree">不同意</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 未来一周申请情况的提示框 -->
		<div class="modal fade" id="app-week" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 700px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
						<h2 class="modal-title">未来一周预约情况</h2>
					</div>
					<div class="modal-body">
					<table class="table table-striped">
						<th class="app-week-list-no-th" style="width: 240px !important;">序号</th><th class="app-week-list-name-th" style="width: 350px !important;">预约人</th><th class="app-week-list-time-th" style="width: 350px !important;">预约时间</th>
					</table>
						<table class="table table-striped" id="app-week-list">
							
						</table>
					</div>
					<div class="modal-footer">
							<table class="fenye-table">
								<tr>
									<td>
										<div id="barcon" name="barcon"></div>
									</td>
								</tr>
							</table>
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>

		<!-- 添加人员提示框 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog addperson-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title" id="myModalLabel">添加人员</h4>
					</div>

					<div class="row">
						<hr>
						<h3 class="h3_addperson">人员组织架构</h3>
						<div class="search_div btnView">
							<h4><span class="glyphicon glyphicon-search"></span> 搜索</h4>
							<div class="form-group">
							<input style="width: auto;float: left;" type="input" class="form-control dropdown" id="input-check-node" placeholder="输入要搜索人的真实姓名..." value="">
									<ul id="selectdiv" class="na nav" style="float: left;">
				            			<li class="dropdown" style="margin-top: 10px;margin-left: 6px;"> 
				            			<a style="display: initial;">搜索结果&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down"></span></a>
				              				<ul class="dropdown-menu y_height" id="personul" style="margin: -18px 110px;">
				              				</ul> 
				            			</li>
				          			</ul>
				    <div class=" clear"></div>
			          			<!-- <div class="checkbox">
								<label>
             						<input type="checkbox" class="checkbox" id="chk-check-silent" value="false">沉默（无事件）
            					</label>
								</div>
								<div class="form-group row search_div_btn">
									<button type="button" class="btn btn-success check-node" id="btn-check-node">选中节点</button>
									<button type="button" class="btn btn-danger check-node" id="btn-uncheck-node">取消选中</button>
								</div>
								<div class="form-group search_div_btn">
									<button type="button" class="btn btn-primary check-node" id="btn-toggle-checked">反选节点</button>
								</div>
								<hr>
								<div class="form-group row search_div_btn">
									<div class="col-sm-6">
										<button type="button" class="btn btn-success" id="btn-check-all">选中所有</button>
									</div>
								</div>
								<button type="button" class="btn btn-danger" id="btn-uncheck-all">取消选中所有</button> -->
							</div>
							
						</div>
						<div class="col-sm-4 treeView">
							<h4>组织列表</h4>
							<div id="treeview-checkable" class=""></div>
						</div>
						<div class="col-sm-4 choiseView">
							<h4>部门下所有人员列表</h4>
							<div id="checkable-choise" class="choise">
								
							</div>
						</div>
						<div class="col-sm-4 thingView">
							<h4>确认所选</h4>
							<div id="checkable-output">
								
							</div>
		<!-- 					<button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok" aria-hidden="true" onclick="queren();"> </span>确认选择</button> -->
						</div>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>
								<button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal" onclick="savePerson();"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true" ></span>保存</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>

	<script>
		var device;
		var device_id="";
		var timeMsg="";
		//点击人名后保存选中人员的信息
		var selectedPersonDataArray =new Array();
		var failPersonJsonStr="";
		
		window.onload = function(){
			loadTopUl();
			loadDeviceInfoAll();
			Agreement();
			$('.pgwMenu').pgwMenu({
				dropDownLabel: '所有设备',
				viewMoreLabel: '更多<span class="icon"></span>'
			});
			$(".pgwMenu li").hover(function(){
				$(this).find("ul").slideDown("slow");	
			},function(){
				$(this).find("ul").slideUp("fast");	
			});
			}
		//滑轮禁用
		function disabledMouseWheel() {  
			  if (document.addEventListener) {  
			    document.addEventListener('DOMMouseScroll', scrollFunc, false);  
			  }//W3C  
			  window.onmousewheel = document.onmousewheel = scrollFunc;//IE/Opera/Chrome  
			}
			function scrollFunc(evt) {  
			  evt = evt || window.event;  
			    if(evt.preventDefault) {  
			    // Firefox  
			      evt.preventDefault();  
			      evt.stopPropagation();  
			    } else {  
			      // IE  
			      evt.cancelBubble=true;  
			      evt.returnValue = false;  
			  }  
			  return false;  
			}  
			//滑轮启用
			function abledMouseWheel() {  
				  if (document.addEventListener) {  
				    document.addEventListener('DOMMouseScroll', scrollFunc1, false);  
				  }//W3C  
				  window.onmousewheel = document.onmousewheel = scrollFunc1;//IE/Opera/Chrome  
				}
				function scrollFunc1(evt) {  
				  evt = evt || window.event;  
				   
				  return true;  
				} 
				
		function savePerson(){
			if(selectedPersonDataArray.length>0){
				failPersonJsonStr=JSON.stringify(selectedPersonDataArray);
				//console.log(selectedPersonDataArray[0].username);
				document.getElementById('searchperson').innerHTML = "";
				for(var m=0;m<selectedPersonDataArray.length;m++){
					$("#searchperson").prepend(
							"<p class='p_searchperson'><span class='glyphicon glyphicon-user'> </span>"+selectedPersonDataArray[m].username+"</p>"
					);
				}
			}else{
				alert("你还没有选择人员");
				failPersonJsonStr="";
				document.getElementById('searchperson').innerHTML = "";
			}
			console.log(failPersonJsonStr);
		}
		
		function shanchu(usertruename,userid,email,phone){
			document.getElementById('checkable-output').innerHTML = "";
			if(selectedPersonDataArray.length>0){
				for (var i = 0 ; i < selectedPersonDataArray.length ; i++){
					var obj=selectedPersonDataArray[i];
					if(obj.userid == userid){
						selectedPersonDataArray.remove(i);
						for (var j = 0 ; j < selectedPersonDataArray.length ; j++){
							var obj_j=selectedPersonDataArray[j];
							
						$('#checkable-output').prepend(
								"<p id='p"+obj_j.userid+"'><span class='glyphicon glyphicon-user'> </span>"+obj_j.username+"<span onclick='shanchu(\"" + obj_j.username + "\",\""  + obj_j.userid + "\",\""  + obj_j.email + "\",\""  + obj_j.phone + "\")' class='glyphicon glyphicon-remove'> </span></p>"
								);
						}
					}else{
						console.log("没有此人");
					}
				}
			}
			return selectedPersonDataArray;
		}
		//全部设备刷新方法
		function RefreshAll(){
			window.location.reload();
		}

		//加载设备数据
		function loadDeviceInfo(classId, className) {
			document.getElementById('device').innerHTML = "";
			var loadInfo = '${ctx }' + "/getDeviceListByCatagoryId?CatagoryId=" + classId;
			$.getJSON(loadInfo,{random:Math.random()}, function(data) {
				$.each(data.extend.deviceClass, function(i, item) {
					var zi_color;
					var deviceStatu = "";
					var bt_color;
					var bt_class
					var loadInfos = '${ctx }' + "/getSubscribeByIdAndStatus";
					console.log(i);
					console.log(item.id);
					$.ajax({
				        url: loadInfos,
				        type: 'GET',
				        dataType: 'json',
				        cache: false,
				        async:false,
				        data:{
				        	subscribe_device_id : item.id,
				        	},
				        beforeSend: LoadFunction, //加载执行方法
				        error: erryFunction, //错误执行方法
				        success: succFunction //成功执行方法
				    })

				    function LoadFunction() {
						//加载执行方法
				    }
				    function erryFunction() {
				    	//错误执行方法
				    }
				    function succFunction(JsonString) {
				        json = eval(JsonString);
				      	//清空值
				        if (json.code==1){
				      		var i= json.extend.getSubscribeByIdAndStatus.length;
				      		if(i>=0&&i<=5){
					    		//bt_color="background-color:yellow";
					    		bt_class="btn btn-success active";
							}else if(i>5&&i<=10){
								//bt_color="background-color:red";
								bt_class="btn btn-warning";
							}else{
								//bt_color="background-color:green";
								bt_class="btn btn-danger";
							} 
				      	}else if(json.code==0){
				      		//bt_color="background-color:yellow";
				      		bt_class="btn btn-success active";
						}
				    }
					if(item.deviceStatus == 1 || item.deviceStatus == 2) {
						deviceStatu = "可预约";
						zi_color = "color:#46bff7";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='btn btn-sm btn-info applyform'onclick='SelectDevice(\"" + item.id + "\")'>预约设备" +
								"</a>&nbsp;&nbsp;<a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					} else if(item.deviceStatus == 3) {
						deviceStatu = "维修中(不可预约)";
						zi_color = "color:#fd971f";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					} else if(item.deviceStatus == 4) {
						deviceStatu = "已下线(不可预约)";
						zi_color = "color:#ea2000";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					}
					

				});
				document.getElementById('classTitle').innerHTML = className;

			});

		}
		//加载全部设备数据
		function loadDeviceInfoAll() {
			document.getElementById('device').innerHTML = "";
			var loadInfo = '${ctx }' + "/getAllDeviceByFlagQ";
			$.getJSON(loadInfo,{random:Math.random()}, function(data) {
				$.each(data.extend.DeviceFlag, function(i, item) {
					var zi_color;
					var deviceStatu = "";
					var bt_color;
					var bt_class
					var loadInfos = '${ctx }' + "/getSubscribeByIdAndStatus";
					console.log(i);
					console.log(item.id);
					$.ajax({
				        url: loadInfos,
				        type: 'GET',
				        dataType: 'json',
				        cache: false,
				        async:false,
				        data:{
				        	subscribe_device_id : item.id,
				        	},
				        beforeSend: LoadFunction, //加载执行方法
				        error: erryFunction, //错误执行方法
				        success: succFunction //成功执行方法
				    })

				    function LoadFunction() {
						//加载执行方法
				    }
				    function erryFunction() {
				    	//错误执行方法
				    }
				    function succFunction(JsonString) {
				        json = eval(JsonString);
				      	//清空值
				        if (json.code==1){
				      		var i= json.extend.getSubscribeByIdAndStatus.length;
				      		if(i>=0&&i<=5){
					    		//bt_color="background-color:yellow";
					    		bt_class="btn btn-success active";
							}else if(i>5&&i<=10){
								//bt_color="background-color:red";
								bt_class="btn btn-warning";
							}else{
								//bt_color="background-color:green";
								bt_class="btn btn-danger";
							}
				      	}else if(json.code==0){
				      		//bt_color="background-color:yellow";
				      		bt_class="btn btn-success active";
						}
				    }
					if(item.deviceStatus == 1 || item.deviceStatus == 2) {
						deviceStatu = "可预约";
						zi_color = "color:#46bff7";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='btn btn-sm btn-info applyform'onclick='SelectDevice(\"" + item.id + "\")'>预约设备" +
								"</a>&nbsp;&nbsp;<a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					} else if(item.deviceStatus == 3) {
						deviceStatu = "维修中(不可预约)";
						zi_color = "color:#fd971f";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					} else if(item.deviceStatus == 4) {
						deviceStatu = "已下线(不可预约)";
						zi_color = "color:#ea2000";
						$("#device").append("<div class='col-sm-4'><div class='panel panel-info'><div class='panel-heading'><span class='bui-name'>"+item.deviceType +":"+ item.deviceNumber +
								"</span></div><div class='panel-body'><p><label>设备品牌：</label><span>" + item.deviceBrand +
								"</span></p><p><label>设备状态：</label><span style='" + zi_color + "'>" + deviceStatu +
								"</span></p><p><label>设备位置：</label><span>" + item.devicePlace +
								"</span></p><p><a class='"+bt_class+"' style='" + bt_color + "' id='weekUse" + item.id + "' data-id='" + item.id + "' onclick='SelectByWillOneWeek(\"" + item.id + "\")'>未来一周的使用情况</a></p></div></div></div>");
					}
					

				});
				

			});

		}
		//一周预约详情
		function SelectByWillOneWeek(id){
			var ids = id;
			
			var loadInfo = '${ctx }' + "/getSubscribeByIdAndStatus";
			$.ajax({
		        url: loadInfo,
		        type: 'GET',
		        dataType: 'json',
		        cache: false,
		        async:false,
		        data:{
		        	subscribe_device_id : ids,
		        	},
		        beforeSend: LoadFunction, //加载执行方法
		        error: erryFunction, //错误执行方法
		        success: succFunction //成功执行方法
		    })

		    function LoadFunction() {
				//加载执行方法
			
				
		    }

		    function erryFunction() {
		    	//错误执行方法
		    	
		    }

		    function succFunction(JsonString) {
		    	
		    	document.getElementById('app-week-list').innerHTML = "";
		        json = eval(JsonString);
		      	//清空值
		      
		    	if(json.code==1){
		    		
		    		/* $("#app-week-list").append(
							
							
							"<th class='app-week-list-no-th'>序号</th><th class='app-week-list-name-th'>姓名</th><th class='app-week-list-time-th'>预约时间</th>"
						); */
		    		for(var i = 0;i<json.extend.getSubscribeByIdAndStatus.length;i++){
						var item= json.extend.getSubscribeByIdAndStatus[i];
						var a = i + 1;
						$("#app-week-list").append(
							"<tr><td class='app-week-list-no'>" + a + "</td><td class='app-week-list-name'>" + item.subscribeUserName + "</td><td class='app-week-list-time'>" + fmtDate(item.subscribeBeginTime,item.subscribeEndTime) + "</td></tr>"
						);
					}
					goPage(1, 8);
		    		$('#app-week').modal({
						backdrop: false
					}) 
					
				}else{
					document.getElementById('MsgMessage').innerHTML = "";  
					$("#MsgMessage").append(
							
							"<h3>"+"该设备未来一周没有预约记录"+"</h3>"
						);
					$("#MsgDialog").modal('show');
					//5s后自动关闭
					setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000); 
					
				}
				
				
				
		    }

			
		}
		//分页
		/**
		 * 分页函数
		 * pno--页数
		 * psize--每页显示记录数
		 * 分页部分是从真实数据行开始，因而存在加减某个常数，以确定真正的记录数
		 * 纯js分页实质是数据行全部加载，通过是否显示属性完成分页功能
		 **/
		function goPage(pno, psize) {
			var itable = document.getElementById("app-week-list");
			var num = itable.rows.length; //表格所有行数(所有记录数)
			//console.log(num);
			var totalPage = 0; //总页数
			var pageSize = psize; //每页显示行数
			//总共分几页 
			if(num / pageSize > parseInt(num / pageSize)) {
				totalPage = parseInt(num / pageSize) + 1;
			} else {
				totalPage = parseInt(num / pageSize);
			}
			var currentPage = pno; //当前页数
			var startRow = (currentPage - 1) * pageSize + 1; //开始显示的行  31 
			var endRow = currentPage * pageSize; //结束显示的行   40
			endRow = (endRow > num) ? num : endRow;
			40
			//console.log(endRow);
			//遍历显示数据实现分页
			for(var i = 1; i < (num + 1); i++) {
				var irow = itable.rows[i - 1];
				if(i >= startRow && i <= endRow) {
					irow.style.display = "block";
				} else {
					irow.style.display = "none";
				}
			}
			var finalnum = num;
			var pageEnd = document.getElementById("pageEnd");
			var tempStr = "共&nbsp;" + finalnum + "&nbsp;条记录&nbsp;&nbsp;分&nbsp;" + totalPage + "&nbsp;页&nbsp;&nbsp;当前第&nbsp;" + currentPage + "&nbsp;页&nbsp;";
			if(currentPage > 1) {
				tempStr += "<a href=\"javascript:goPage(" + (1) + "," + psize + ")\" >&nbsp;首页&nbsp;</a>";
				tempStr += "<a href=\"javascript:goPage(" + (currentPage - 1) + "," + psize + ")\" >&nbsp;&nbsp;<上一页&nbsp;&nbsp;</a>"
			} else {
				tempStr += "&nbsp;&nbsp;首页&nbsp;&nbsp;";
				tempStr += "&nbsp;&nbsp;<上一页&nbsp;&nbsp;";
			}

			if(currentPage < totalPage) {
				tempStr += "<a href=\"javascript:goPage(" + (currentPage + 1) + "," + psize + ")\" >&nbsp;&nbsp;下一页>&nbsp;&nbsp;</a>";
				tempStr += "<a href=\"javascript:goPage(" + (totalPage) + "," + psize + ")\" >&nbsp;&nbsp;尾页&nbsp;&nbsp;</a>";
			} else {
				tempStr += "&nbsp;&nbsp;下一页>&nbsp;&nbsp;";
				tempStr += "&nbsp;&nbsp;尾页&nbsp;&nbsp;";
			}

			document.getElementById("barcon").innerHTML = tempStr;

		}
		/*毫秒转换成正常时间格式*/
		/*只用于预约详情显示*/
		/*12月1日 星期五  17:00~18:00*/
		function fmtDate(stime, etime) {
			var startTime = new Date(stime);
			var endTime = new Date(etime);
			var weekStr = "星期" + "日一二三四五六".charAt(startTime.getDay());
			//  var y = date.getFullYear();
			var m = startTime.getMonth() + 1;
			m = m < 10 ? ('0' + m) : m;
			var d = startTime.getDate();
			d = d < 10 ? ('0' + d) : d;
			var startHour = startTime.getHours();
			startHour = startHour < 10 ? ('0' + startHour) : startHour;
			var startminute = startTime.getMinutes();
			startminute = startminute < 10 ? ('0' + startminute) : startminute;

			var endHour = endTime.getHours();
			endHour = endHour < 10 ? ('0' + endHour) : endHour;
			var endminute = endTime.getMinutes();
			endminute = endminute < 10 ? ('0' + endminute) : endminute;

			var result = m + '-' + d + ' ' + weekStr + " " + startHour + ':'
					+ startminute + '~' + endHour + ":" + endminute;
			return result;
		};
		//预约设备关闭按钮事件
		function CloseBtn(){
			abledMouseWheel();
		}
		
		//加载预约表单
		function SelectDevice(id) {
			
			document.getElementById('searchperson').innerHTML = "";
			document.getElementById('form-reason').innerHTML = "";
			
			var usertruename =window.sessionStorage.getItem("usertruename");
			if(isLogin(usertruename)==false){
				$("#applyform").modal("hide");
			}else{
				$("#applyform").modal({backdrop: 'static', keyboard: false});
			}
			//Agreement();
			getNowFormatDate();
			SelectPerson();
			disabledMouseWheel();
			var ids = id;
			
			var loadInfo = '${ctx }' + "/getDeviceDataById?DeviceId=" + ids;
			//loadDeviceInfoOfAddPerson();
			$.getJSON(loadInfo,{random:Math.random()}, function(data) {
				
				//console.log(JSON.stringify(data));
				var device = data.extend.deviceId;
				device_id=device.id
				document.getElementById('data_device').innerHTML = "";
				$("#data_device").append(
					"<p><label>设备编号</label><span id='form-room-id'>" + device.deviceNumber + "</span></p>" +
					"<p><label>设备名称</label><span id='form-room-type'>" + device.deviceType + "</span></p>"
				);
			});
		}
		 $("#agree").click(function() {
			$("#agreement").modal('hide');
			var usertruename =window.sessionStorage.getItem("usertruename");
			document.getElementById("agree").disabled=true;
			window.sessionStorage.setItem("isagree", "agree"+usertruename+"");
			abledMouseWheel();
		});
		$("#disagree").click(function() {
			window.sessionStorage.clear();
			window.location.reload();
			document.getElementById("agree").disabled=true;
			
		});
		
		
		
		//加载协议页
		 function Agreement(){
			var usertruename =window.sessionStorage.getItem("usertruename");
			var agree=window.sessionStorage.getItem("isagree");
			if(usertruename==null || agree=="agree"+usertruename+""){
				
			}else {
				$("#agreement").modal({backdrop: 'static', keyboard: false});
				disabledMouseWheel();
				setTimeout(function() { 
					document.getElementById("agree").disabled=false;
				},3000);
			}
			
		} 
		//加载预约人信息
		function SelectPerson(){
			var usertruename =window.sessionStorage.getItem("usertruename");
			var email =window.sessionStorage.getItem("email");
			var mobilephone =window.sessionStorage.getItem("mobilephone");
			document.getElementById('data_person').innerHTML = "";
			$("#data_person").append(
					"<p><label>姓&nbsp;&nbsp;&nbsp;&nbsp;名:</label><span id='form-room-id'>" + usertruename + "</span></p>" +
					"<p><label>邮&nbsp;&nbsp;&nbsp;&nbsp;箱:</label><span id='form-room-id'>" + email + "</span></p>" +
					"<p><label>手&nbsp;&nbsp;&nbsp;&nbsp;机：</label><span id='form-room-type'>" + mobilephone + "</span></p>"
				);
			
		}
		//获取当前日期方法
		 function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        setTimeout(function(){
        	var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	        var myDate = new Date(Date.parse(currentdate.replace(/-/g, "/")));  
	        var weekResult =weekDay[myDate.getDay()];
	        if(weekResult =="星期日" || weekResult == "星期六"){
	        	/* document.getElementById('MsgMessage').innerHTML = "";  
	        	$("#MsgMessage").append(
						"<h3>" +"周六预约时间为"+"<br>"+" 08：00~17：00" + "</h3>"
					);
				$("#MsgDialog").modal('show');
				//2s后自动关闭
				setTimeout(function(){
					$("#MsgDialog").modal("hide")
					},3000); */
	        	
	        }else{
	        	/* document.getElementById('MsgMessage').innerHTML = "";  
				$("#MsgMessage").append(
						"<h3>"+"今天是"+weekResult+"</h3>"+
						"<h3>" + "工作日预约时间为 "+"<br>"+"06：30~08：00"+"<br>"+"12：00~13：00 "+"<br>"+"17：00~19：00"+"<br></h3>"
					);
				$("#MsgDialog").modal('show');
				//2s后自动关闭
				setTimeout(function(){
					$("#MsgDialog").modal("hide")
					},3500); */
	        }
			},500);
        
    	} 

		//转换时间戳方法
		Date.prototype.format = function(format) {
			var o = {
				"M+": this.getMonth() + 1, // month  
				"d+": this.getDate(), // day  
				"h+": this.getHours(), // hour  
				"m+": this.getMinutes(), // minute  
				"s+": this.getSeconds(), // second  
				/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   */ //没用上所以注释了
				"S": this.getMilliseconds()
				// millisecond  
			}
			if(/(y+)/.test(format))
				format = format.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
			for(var k in o)
				if(new RegExp("(" + k + ")").test(format))
					format = format.replace(RegExp.$1,
						RegExp.$1.length == 1 ? o[k] : ("00" + o[k])
						.substr(("" + o[k]).length));
			return format;
		}
		
		//时间改变事件
		function dayChanged(){
			var month =$dp.cal.getP('y')+"-"+$dp.cal.getP('M')+"-"+$dp.cal.getP('d');
			var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	    	var dateStr = month;
	        var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/")));  
	        var weekResult =weekDay[myDate.getDay()];
	        if(weekResult == "星期日" || weekResult == "星期六"){
	        	document.getElementById('MsgMessage').innerHTML = "";  
				$("#MsgMessage").append(
						"<h3>" +"周六预约时间为"+"<br>"+" 08：30~17：30" + "</h3>"
					);
				$("#MsgDialog").modal('show');
				setTimeout(function(){
					$("#MsgDialog").modal("hide")
					},3000); 
	        }else{
	        	document.getElementById('MsgMessage').innerHTML = "";  
				$("#MsgMessage").append(
						"<h3>" +"周一至周五预约时间为 "+"<br>"+"12：00~13：00 "+"<br>"+"17：30~20：30"+ "</h3>"
					);
				$("#MsgDialog").modal('show');
				
				//2s后自动关闭
				 setTimeout(function(){
					$("#MsgDialog").modal("hide")
					},3000); 
	        }
		}
		function hourChanged(){
			var month =$('input[name="FINHOSPDATE_BEGIN"]').val();
			var hour = $dp.cal.getP('H');
			var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	    	var dateStr = month;
	        var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/")));  
	        var weekResult =weekDay[myDate.getDay()];
	        if(weekResult == "星期日" || weekResult == "星期六"){
	        	
	        	if(hour>=8 &&hour<=17){
	        		if(hour==17){
	        			var min=$('input[id="MINDATE"]').val();
			        	
			        	var mins=min.substring(3,5);
			        	if(mins<30){
			        		return true;
			        	}else{
			        		document.getElementById('MsgMessage').innerHTML = "";  
							$("#MsgMessage").append(
									"<h3>"+"选择的时间不在范围内"+"</h3>"+
									"<h3>" +"周六预约时间为"+"<br>"+" 08：30~17：30" + "</h3>"
								);
							$("#MsgDialog").modal('show');
							
							//2s后自动关闭
							 setTimeout(function(){
								$("#MsgDialog").modal("hide")
								},3000); 
			        		return false;
			        	}
	        		}else{
	        			return true;
	        		}
	        	}else{
	        		document.getElementById('MsgMessage').innerHTML = "";  
					$("#MsgMessage").append(
							"<h3>"+"选择的时间不在范围内"+"</h3>"+
							"<h3>" +"周六预约时间为"+"<br>"+" 08：30~17：30" + "</h3>"
						);
					$("#MsgDialog").modal('show');
					
					//2s后自动关闭
					 setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000); 
					return false;
	        	}
	        }else{
	        	if(hour==12 || hour==17 || hour==18 || hour==19 || hour==20){
	        		if(hour==17){
						var min=$('input[id="MINDATE"]').val();
			        	
			        	var mins=min.substring(3,5);
			        	if(mins>=30){
			        		return true;
			        	}else{
			        		document.getElementById('MsgMessage').innerHTML = "";  
							$("#MsgMessage").append(
									"<h3>"+"选择的时间不在范围内"+"</h3>"+
									"<h3>" +"工作日预约时间为 "+"<br>"+"12：00~13：00 "+"<br>"+"17：30~20：30"+ "</h3>"
								);
							$("#MsgDialog").modal('show');
							
							//2s后自动关闭
							 setTimeout(function(){
								$("#MsgDialog").modal("hide")
								},3000); 
			        		return false;
			        	}
	        		}else{
	        			return true;
	        		}
	        		
	        		
	        	}else{
	        		$('#MINDATE').val("");
	        		document.getElementById('MsgMessage').innerHTML = "";  
					$("#MsgMessage").append(
							"<h3>" + "选择的时间不在范围内" +"</h3>"+
							"<h3>" +"工作日预约时间为 "+"<br>"+"12：00~13：00 "+"<br>"+"17：30~20：30"+ "</h3>"
						);
					$("#MsgDialog").modal('show');
					
					//2s后自动关闭
					 setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000); 
					 return false;
	        	}
	        	
	        	
	        	
	        }
	        
	        
			
		}
		function hourChanged1(){
			var month =$('input[name="FINHOSPDATE_BEGIN"]').val();
			var hour = $dp.cal.getP('H');
			var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	    	var dateStr = month;
	        var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/")));  
	        var weekResult =weekDay[myDate.getDay()];
	        if(weekResult == "星期日" || weekResult == "星期六"){
	        	
	        	if(hour>=8 &&hour<=17){
	        		if(hour==17){
	        			var max=$('input[id="MAXDATE"]').val();
			        	var maxs=max.substring(3,5);
		        		if(maxs<=30){
			        		return true;
			        	}else{
			        		document.getElementById('MsgMessage').innerHTML = "";  
							$("#MsgMessage").append(
									"<h3>"+"选择的时间不在范围内"+"</h3>"+
									"<h3>" +"周六预约时间为"+"<br>"+" 08：30~17：30" + "</h3>"
								);
							$("#MsgDialog").modal('show');
							
							//2s后自动关闭
							 setTimeout(function(){
								$("#MsgDialog").modal("hide")
								},3000); 
			        		return false;
			        	}
	        		}else{
	        			return true;
	        		}
	        		
	        		
	        		
	        	}else{
	        		document.getElementById('MsgMessage').innerHTML = "";  
					$("#MsgMessage").append(
							"<h3>"+"选择的时间不在范围内"+"</h3>"+
							"<h3>" +"周六预约时间为"+"<br>"+" 08：30~17：30" + "</h3>"
						);
					$("#MsgDialog").modal('show');
					
					//2s后自动关闭
					 setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000); 
					return false;
	        	}
	        }else{
	        	if(hour==12 || hour==13 || hour==17 || hour==18 || hour==19 || hour==20){
	        		
	        		
	        		if(hour==17){
	        			var max=$('input[id="MAXDATE"]').val();
			        	var maxs=max.substring(3,5);
		        		if(maxs>30){
			        		return true;
			        	}else{
			        		document.getElementById('MsgMessage').innerHTML = "";  
							$("#MsgMessage").append(
									"<h3>"+"选择的时间不在范围内"+"</h3>"+
									"<h3>" +"工作日预约时间为 "+"<br>"+"12：00~13：00 "+"<br>"+"17：30~20：30"+ "</h3>"
								);
							$("#MsgDialog").modal('show');
							
							//2s后自动关闭
							 setTimeout(function(){
								$("#MsgDialog").modal("hide")
								},3000); 
			        		return false;
			        	}
	        		}else{
	        			return true;
	        		}
	        		
	        	}else{
	        		$('#MINDATE').val("");
	        		document.getElementById('MsgMessage').innerHTML = "";  
					$("#MsgMessage").append(
							"<h3>" + "选择的时间不在范围内" +"</h3>"+
							"<h3>" +"工作日预约时间为 "+"<br>"+"12：00~13：00 "+"<br>"+"17：30~20：30"+ "</h3>"
						);
					$("#MsgDialog").modal('show');
					
					//2s后自动关闭
					 setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000); 
					 return false;
	        	}
	        	
	        	
	        	
	        }
	        
	        
			
		}
		function minuteChanged(){
			var month_user =$('input[name="FINHOSPDATE_BEGIN"]').val();
			var min=$('input[id="MINDATE"]').val();
        	var max=$('input[id="MAXDATE"]').val();
			if(month_user=="" || min=="" || max==""){
				
				timeMsg="请选择要预约的日期";
				return false;
			}else{
				var aa =month_user+" "+$('input[id="MINDATE"]').val()+":00";
				var bb =month_user+" "+$('input[id="MAXDATE"]').val()+":00";
				aa=aa.replace(new RegExp(/-/gm) ,"/");
				bb=bb.replace(new RegExp(/-/gm) ,"/");
				
				date1=new Date(aa);
				date2=new Date(bb);
				
				var time1=Date.parse(date1);
				var time2=Date.parse(date2);
				var times= time2-time1;
			
				
				if(times>=600000 && times<=7200000){
					var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
			    	var dateStr = month_user;
			    	
			        var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/")));  
			        var weekResult =weekDay[myDate.getDay()];
			        if(weekResult == "星期日" || weekResult == "星期六"){
			        	var min=$('input[id="MINDATE"]').val();
			        	var max=$('input[id="MAXDATE"]').val();
			        	var mins=min.substring(0,2);
			        	var maxs=max.substring(0,2);
			        	var maxs_minute=max.substring(3,5);
			        	if(mins>=8 && mins<=maxs && maxs<=17){
			        		if(maxs==17){
			        			if(maxs_minute<=30){
			        				return true;
			        			}else{
			        				timeMsg ="时间不符合周六规定时间";
			        				return false;
			        			}
			        		}else{
			        			return true;
			        		}
			        		
			        		
			        		
			        	}else{
			        		
							timeMsg ="时间不符合周六规定时间";
			        		return false;
			        		
			        	}
			        }else{
			        	
			        	
			        	var min=$('input[id="MINDATE"]').val();
			        	var max=$('input[id="MAXDATE"]').val();
			        	var mins=min.substring(0,2);
			        	var maxs=max.substring(0,2);
			        	var maxs1=max.substring(0,5);
			        	var mins_minute=min.substring(3,5);
			        	var maxs_minute=max.substring(3,5);
			        	
			        	if(mins=="12" || mins=="17" || mins=="18" || mins=="19" || mins=="20"){
			        		if(mins=="12"){
				        		if(maxs=="12" || maxs1=="13:00"){
				        			return true;
				        		}else{
				        			
									timeMsg="时间不符合工作日规定时间";	
				        			return false;
				        		}
				        	}else if(mins=="17"){
				        		if(mins_minute>=30){
				        			return true;
				        		}else{
				        			timeMsg="时间不符合工作日规定时间";	
				        			return false;
				        		}
				        		
				        		
				        	}else{
				        		return true;
				        	}
			        	}else{
			        		timeMsg="时间不符合工作日规定时间";	
			        		return false;
			        	}
			        	
			        	
			        	
			        	
			        	
			        	/* if(mins=="06"){
			        		if(maxs=="06" || maxs=="07" || maxs1=="08:00"){
			        			return true;
			        		}else{
			        		
								timeMsg="时间不符合工作日规定时间";
			        			return false;
			        		}
			        		
			        	}else if(mins=="07"){
			        		if(maxs=="07" || maxs1=="08:00"){
			        			return true;
			        		}else{
			        			
								timeMsg="时间不符合工作日规定时间";
			        			return false;
			        		}
			        	}else if(mins=="12"){
			        		if(maxs=="12" || maxs1=="13:00"){
			        			return true;
			        		}else{
			        			
								timeMsg="时间不符合工作日规定时间";	
			        			return false;
			        		}
			        	}else if(mins=="17"){
			        		if(maxs=="17" || maxs=="18" || maxs1=="19:00"){
			        			return true;
			        		}else{
								timeMsg="时间不符合工作日规定时间";
			        			return false;
			        		}
			        	}else if(mins=="18"){
			        		if(maxs=="18" || maxs1=="19:00"){
			        			return true;
			        		}else{
								timeMsg="时间不符合工作日规定时间";
			        			return false;
			        		}
			        	}else{
			        		timeMsg="时间不符合工作日规定时间";
		        			return false;
			        	} */  	
			        }
						
					
				}else if(times>0 && times<600000){
					document.getElementById('MsgMessage').innerHTML = "";  
					timeMsg="时间长度小于10分钟";
					return false;
				}else if(times==0){
			 		timeMsg="开始时间等于结束时间";
			 		return false;
				}else if(times<0){
					timeMsg="结束时间小于初始时间";
					return false;
				}else if(times>7200000){
					timeMsg="最长预约时间2小时";
					return false;
				}
				
			}
			
			
			
		}
		//提交预约前判断
		function isSubmit(){
			
			var a=minuteChanged();
			if(a==true){
				Subscribe();
				$('#MINDATE').val("");
				$('#MAXDATE').val("");
				$('#form-reason').val("");
				$('#DATE').val("");
			}else if(a==false){
				document.getElementById('MsgMessage').innerHTML = "";
				
				$("#MsgMessage").append(
						"<h3>" +"预约失败！"+timeMsg+ "</h3>"
					);
				$("#MsgDialog").modal('show');
				//2s后自动关闭
				 setTimeout(function(){
					$("#MsgDialog").modal("hide")
					},3000);
				 $('#MINDATE').val("");
				 $('#MAXDATE').val("");
				 $('#form-reason').val("");
				 abledMouseWheel();
			}
			
		}


		function formatDatebox(value) {
			if(value == null || value == '') {
				return '';
			}
			var dt;
			if(value instanceof Date) {
				dt = value;
			} else {
				dt = new Date(value);
			}

			return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
		}
		//清除弹窗原数据
		$("#applyform").on("hidden.bs.modal", function() {
		    $(this).removeData("bs.modal");
		});
		//提交预约表单
		function Subscribe() {
			
			var usertruename =window.sessionStorage.getItem("usertruename");
			var email =window.sessionStorage.getItem("email");
			var mobilephone =window.sessionStorage.getItem("mobilephone");
			var userid = window.sessionStorage.getItem("userid");
			var userteam = window.sessionStorage.getItem("userteam");
			
			var starttime =$('input[name="FINHOSPDATE_BEGIN"]').val()+" "+$('input[id="MINDATE"]').val()+":59";
			var endtime =$('input[name="FINHOSPDATE_BEGIN"]').val()+" "+$('input[id="MAXDATE"]').val()+":00";
			$.ajax({
				url: "${ctx }/insertSubscribeForApp",
				type: "GET",
				data: {
					subscribe_user_id: userid,
					subscribe_user_email: email,
					subscribe_device_id: device_id,
					subscribe_begin_time:starttime,
					subscribe_end_time: endtime,
					subscribe_user_name: usertruename,
					subscribe_user_mobile: mobilephone,
					subscribe_remark: $('input[name="reason"]').val(),
					subscribe_sons:failPersonJsonStr,
					subscribe_organize_id : userteam
						},
					
				beforeSend: function() {
					//请求前
					
					
				},
				success: function(result) {
					//请求成功时
					$("#applyform").modal('hide');
					document.getElementById('MsgMessage').innerHTML = "";
					if(result.code=="1"){
						$("#MsgMessage").append(
								"<h3>" + result.extend.successMsg + "</h3>"
							);
					}else{
						$("#MsgMessage").append(
								"<h3>" + result.extend.failMsg + "</h3>"
							);
					}
					$("#MsgDialog").modal('show');
					//2s后自动关闭
					setTimeout(function(){
						$("#MsgDialog").modal("hide")
						},3000);
					abledMouseWheel();
				},
				complete: function() {
					//请求结束时
					abledMouseWheel();
					//$("#MsgDialog").modal('hide');
				},
				error: function() {
					//请求失败时
					//$("#MsgDialog").modal('show');
					abledMouseWheel
				}
			});

		}

		function Addperson() {
			
			document.getElementById('searchperson').style.display = "block";
		}
		
		
		/**
		*删除数组指定下标或指定对象
		*/
		Array.prototype.remove=function(obj){
		for(var i =0;i <this.length;i++){
		var temp = this[i];
		if(!isNaN(obj)){
		temp=i;
		}
		if(temp == obj){
		for(var j = i;j <this.length;j++){
		this[j]=this[j+1];
		}
		this.length = this.length-1;
		}
		}
		}
		
		//头部菜单加载数据

		
		function loadTopUl() {
		
			var htmlStr = "全部类别";
			var loadInfo = '${ctx }' + "/device_catagoryService"
			$.getJSON(loadInfo,{random:Math.random()}, function(data) {
			
				$.each(data.extend.device, function(i, item) {
					$("#topul").append(
						"<li class='dropdown'>" +
							"<a href='javascript:loadDeviceInfo(\"" + item.id + "\",\"" + item.equipmentName + "\");'>"+ item.equipmentName + "</a>" +
						"</li>"
					);
					document.getElementById('classTitle').innerHTML = "全部设备";
				});
			});

		}
			

	//人员搜索功能判断
        var flag = true;
        $('#input-check-node').on('compositionstart',function(){
            flag = false;
        })
        $('#input-check-node').on('compositionend',function(){
            flag = true;
        })
        $('#input-check-node').on('input',function(){
            var _this = this;
            setTimeout(function(){
                if(flag){
                	//console.log($(_this).val());
                	loadSearchPerson($(_this).val());
                	var select = document.getElementById('selectdiv');
                	select.style.display="block";
                }
            },0)
        })
        
        
		var acid = document.getElementById('app-btn');
		$(acid).click(function() {
			$("#myModalLabel").text("添加人员");
			//selectedPersonDataArray=[];
			//document.getElementById('checkable-output').innerHTML = "";
			$('#myModal').modal();
			
		});
	</script>

</html>