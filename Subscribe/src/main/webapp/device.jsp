<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
 <script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script> 

<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>

<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

	

</head>
<body>
	<table id="device">
	</table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="device_tool.add();">添加</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit-new" onclick="device_tool.edit();">修改</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-delete-new" onclick="device_tool.remove();">删除</a> <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="device_tool.reload();">全部</a> <a href="#"
				class="easyui-linkbutton" plain="true" iconCls="icon-add-new"
				onclick="device_tool.adds();">批量添加</a><a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="device_tool.redo();">取消选择</a>
		</div>
		<div style="padding: 0 0 0 7px; color: #333;">
			查询类别：<input id="lb2" type="text"
				name="selectCategory" class="combobox" style="width: 150px;"> <a
				href="#" class="easyui-linkbutton" iconCls="icon-search"
				onclick="selectbutton(1);">查询</a>

		</div>
		<div id="page_nav_area"
			style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
	<!-- 修改窗口 --><!-- 修改和增加一个窗口 -->
	<form id="device_edit"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;" enctype="multipart/form-data">
		<p>
			设备编号&nbsp：<input type="text" name="deviceNumber_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			品&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp牌：<input type="text"
				name="deviceBrand_edit" class="textbox" style="width: 150px;">
		</p>
		<p>
			型号类型&nbsp：<input type="text" name="deviceType_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			类&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp别：<input type="text" id="lb3"
				name="deviceClass_edit" class="easyui-combobox"
				data-options="url:'${APP_PATH }/device_catagoryService1?r='+Math.random(),
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'equipmentName',  
                                           
                                           
                                         onLoadSuccess: function () { //加载完成后,设置选中第一项  
                                var val = $(this).combobox('getData');  
                                for (var item in val[0]) {  
                                    if (item == 'id') {  
                                        $(this).combobox('select', val[0][item]);  
                                    }  
                                }  
                            }
                                         "
				style="width: 150px;">
		</p>
		<p>
			位置描述&nbsp：<input type="text" name="devicePlace_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			设备状态&nbsp：<select id="cc" class="easyui-combobox"
				name="deviceStatus_edit" style="width: 150px;" >
				<option value="1">正常</option>
				<!-- <option value="2">使用中</option> -->
				<option value="3">维修</option>
				<option value="4">下线</option>
			</select>
		</p>
		<p>
			备&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp注：<input type="text"
				name="deviceRemark_edit" class="textbox" style="width: 150px;">
		</p>
	</form>
	<!-- 批量添加 -->
	<form id="device_adds"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;" enctype="multipart/form-data">
		<p>
			数&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp量：<input type="text"
				name="number" class="textbox" style="width: 150px;">
		</p>
		<p>
			品&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp牌：<input type="text"
				name="deviceBrand1" class="textbox" style="width: 150px;">
		</p>
		<p>
			型号类型&nbsp：<input type="text" name="deviceType1" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			类&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp别：<input id="lb1" type="text"
				name="deviceClass1" class="easyui-combobox"
				style="width: 150px;">
		</p>
		<p>
			位置描述&nbsp：<input type="text" name="devicePlace1" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			备&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp注：<input type="text"
				name="deviceRemark1" class="textbox" style="width: 150px;">
		</p>

	</form>
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		var type;
		$(function() {
			function getData(pagenum) {
				$.ajax({
					url : "${APP_PATH }/selectByDeviceClassIdOrNull?pn=" + pagenum,
					type : "POST",
					dataType : "json",
					success : function(data) {
						var pageinfo = data.extend.pageInfo;
						build_page_nav(data);
						//加载分页数据
						$("#device").datagrid("loadData", pageinfo.list); //动态取数据
					}
				});
			}
			//解析分页表格
			$("#device").datagrid({
				fit : true,
				iconCls : "icon-manager",
				width : 'auto',
				height : '100',
				rownumbers : true,
				striped : true,
				fitColumns : true,
				columns : [ [ {
					field : 'id',
					title : '编号',
					sortName : 'id', //排序字段
					idField : 'id', //标识字段,主键
					width : 100,
					checkbox : true,
				}, {
					field : "deviceNumber",
					title : '设备编号',
					halign : "center",
					width : 100,
				}, {
					field : "deviceBrand",
					title : '品牌',
					halign : "center",
					width : 100,
				}, {
					field : "deviceType",
					title : '型号类型',
					halign : "center",
					width : 100,
				}, {
					field : "deviceType",
					title : '类别',
					halign : "center",
					width : 100,
				}, {
					field : "devicePhoto",
					title : '图片',
					halign : "center",
					width : 100,
				}, {
					field : "devicePlace",
					title : '位置描述',
					halign : "center",
					width : 100,
				}, {
					field : "deviceStatusName",
					title : '设备状态',
					halign : "center",
					width : 100,
				}, {
					field : "deviceRemark",
					title : '备注',
					halign : "center",
					width : 100,
				}, ] ],
				toolbar : "#tb"
			});
			getData(1);

			// 分页条
			function build_page_nav(result) {
				// 清空分页条
				$("#page_nav_area").empty();
				// page_nav_area
				var ul = $("<ul></ul>").addClass("pagination");

				var firstPageLi = $("<li></li>").append(
						$("<a></a>").append("首页").attr("href", "#"));
				var prePageLi = $("<li></li>").append(
						$("<a></a>").append("&laquo;"));
				// 判断是否有前一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasPreviousPage == false) {
					firstPageLi.addClass("disabled");
					prePageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件
					firstPageLi.click(function() {
						getData(1);
					});
					prePageLi.click(function() {
						getData(result.extend.pageInfo.pageNum - 1);
					});
				}

				var nextPageLi = $("<li></li>").append(
						$("<a></a>").append("&raquo;"));
				var lastPageLi = $("<li></li>").append(
						$("<a></a>").append("末页").attr("href", "#"));
				// 判断是否有下一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasNextPage == false) {
					nextPageLi.addClass("disabled");
					lastPageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件

					nextPageLi.click(function() {
						getData(result.extend.pageInfo.pageNum + 1);
					});
					lastPageLi.click(function() {
						getData(result.extend.pageInfo.pages);
					});
				}

				// 添加首页和前一页 的提示
				ul.append(firstPageLi).append(prePageLi);
				// 1.2.3.4.5 遍历给ul中添加页码提示
				$.each(result.extend.pageInfo.navigatepageNums, function(index,
						item) {

					var numLi = $("<li></li>")
							.append($("<a></a>").append(item));
					// 显示当前页
					if (result.extend.pageInfo.pageNum == item) {
						numLi.addClass("active");
					}
					// 绑定单机事件
					numLi.click(function() {
						getData(item);
					});
					ul.append(numLi);
				});
				// 添加下一页和末页 的提示
				ul.append(nextPageLi).append(lastPageLi);

				// 把ul加入到nav
				var navEle = $("<nav></nav>").append(ul);
				navEle.appendTo("#page_nav_area");
			}
			$('#lb2').combobox({
				url : '${APP_PATH }/device_catagoryService1?r='+Math.random(),
				valueField : 'id',
				textField : 'equipmentName',
				onLoadSuccess: function(){
			        //设置默认值
			        $('#lb2').combobox('setText','全部');
			    } 

			});
			
			
			//条件查询
			
			selectbutton = function (pagenum) {
				/* $('#subscribe').datagrid('load', {
					subscribecn_code : $('input[name="usercode"]').val(),
					subscribecn_name : $('input[name="username"]').val(),
				}); */
				var CatagoryId = $('#lb2').combobox('getValue');
				
				{
		        $.ajax({
		            url: "${APP_PATH }/selectByDeviceClassIdOrNull?pn=" + pagenum,
		            type: "GET",
		            cache: false,
		            data: {
		            	id:CatagoryId,
		                
						

		            },
		            beforeSend: function () {
		                    $.messager.progress({
		                        text: "正在查询中...",
		                    });
		                },
		                success: function (data, response, status) {
		                    $.messager.progress("close");
		                    var pageinfo = data.extend.pageInfo;
		    				build_page_nav(data);
		    				//加载分页数据
		    				$("#device").datagrid("loadData", pageinfo.list); //动态取数据
		                    if (data) {
		                    	
		                        $.messager.show({
		                            title: "提示",
		                            msg: data.msg,
		                        });

		                        $("#device").datagrid("loadData",data.extend.deviceClass);
		                      //解析分页表格
		            			$("#device").datagrid({
		            				fit : true,
		            				iconCls : "icon-manager",
		            				width : 'auto',
		            				height : '100',
		            				rownumbers : true,
		            				striped : true,
		            				columns : [ [ {
		            					field : 'id',
		            					title : '编号',
		            					sortName : 'id', //排序字段
		            					idField : 'id', //标识字段,主键
		            					width : 100,
		            					checkbox : true,
		            				}, {
		            					field : "deviceNumber",
		            					title : '设备编号',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "deviceBrand",
		            					title : '品牌',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "deviceType",
		            					title : '型号类型',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "deviceType",
		            					title : '类别',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "devicePhoto",
		            					title : '图片',
		            					halign : "center",
		            					width : 100,
		            				/* formatter:imgFormatter, */
		            				}, {
		            					field : "devicePlace",
		            					title : '位置描述',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "deviceStatusName",
		            					title : '设备状态',
		            					halign : "center",
		            					width : 100,
		            				}, {
		            					field : "deviceRemark",
		            					title : '备注',
		            					halign : "center",
		            					width : 100,
		            				}, ] ],
		            				toolbar : "#tb"
		            			});
		            			
		                    } else {
		                        $.messager.alert("查询失败!", "未知错误导致失败,请重试!", "warning");
		                    }
		                }
		        });
		    } 
			}

			//新增管理
			$('#lb').combobox({
				url : '${APP_PATH }/device_catagoryService1?r='+Math.random(),
				valueField : 'id',
				textField : 'equipmentName',
			});
			

			$("#device_edit").dialog({
			    width: 300,
			    title: "设备",
			    modal: true,
			    closed: true,
			    iconCls: "icon-user-add",
			    processData: false,
			    contentType: false,
			    buttons: [{
			        text: "提交",
			        iconCls: "icon-ok",
			        handler: function() {
			        		if ($("#device_edit").form(
							"validate")) {
						var Cc = $('#lb3').combobox('getValue');
						$.ajax({
		                    url: "${APP_PATH }/SaveDeviceById1",
		                    type: "POST",
		                    data : {
								id : id1,
								deviceNumber : $(
										'input[name="deviceNumber_edit"]')
										.val(),
								deviceBrand : $(
										'input[name="deviceBrand_edit"]')
										.val(),
								deviceType : $(
										'input[name="deviceType_edit"]')
										.val(),
								deviceClass : Cc,
								devicePhoto : $(
										'input[name="devicePhoto_edit"]')
										.val(),
								devicePlace : $(
										'input[name="devicePlace_edit"]')
										.val(),
								deviceStatus : $(
										'input[name="deviceStatus_edit"]')
										.val(),
								deviceRemark : $(
										'input[name="deviceRemark_edit"]')
										.val()
							},
		                   
		                      beforeSend: function() {
		                        $.messager.progress({
		                            text: "正在进行中...",
		                        })
		                    }, 
		                    success : function(data, response, status){
		                        $.messager.progress("close");
		                        if (data) {
		                            $.messager.show({
		                                title: "提示",
		                                msg: data.msg,
		                            });
		                            $("#device_edit").dialog("close").form("reset");
		                            $("#device").datagrid("reload")
		                        } else {
		                            $.messager.alert("失败!", "未知错误导致失败,请重试!", "warning");
		                        } 
		                        getData(1);
		                    } 

		                });
					}
			        }
			    },
			    {
			        text: "取消",
			        iconCls: "icon-redo",
			        handler: function() {
			            $("#device_edit").dialog("close").form("reset");
			        },
			    
			}],
			});
			//批量添加管理
			
		$('#lb1').combobox({
				url : '${APP_PATH }/device_catagoryService1?r='+Math.random(),
				method : 'get',
				valueField : 'id',
				textField : 'equipmentName',

				onLoadSuccess : function() { //加载完成后,设置选中第一项  
					var val = $(this).combobox('getData');
					for ( var item in val[0]) {
						if (item == 'id') {
							$(this).combobox('select', val[0][item]);
						}
					}
				}
			});
			$("#device_adds")
					.dialog(
							{
								width : 300,
								title : "批量添加",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#device_adds").form(
														"validate")) {

													$
															.ajax({
																url : "${APP_PATH }/AddDeviceMany",
																type : "POST",
																data : {
																	number : $(
																			'input[name="number"]')
																			.val(),
																	deviceBrand : $(
																			'input[name="deviceBrand1"]')
																			.val(),
																	deviceType : $(
																			'input[name="deviceType1"]')
																			.val(),
																	deviceClass : $(
																			'input[name="deviceClass1"]')
																			.val(),
																	devicePlace : $(
																			'input[name="devicePlace1"]')
																			.val(),
																	deviceRemark : $(
																			'input[name="deviceRemark1"]')
																			.val()
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在新增中...",
																			})
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$(
																				"#device_adds")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#device")
																				.datagrid(
																						"reload")
																	} else {
																		$.messager
																				.alert(
																						"新增失败!",
																						"未知错误导致失败,请重试!",
																						"warning")
																	}
																	getData(1);
																}
															})
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$("#device_adds").dialog(
														"close").form("reset")
											},
										} ],
							});
			//类别名称
			$("input[name='category']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});
			//备注
			$("input[name='deviceRemark1']").validatebox({
				required : false,
				validType : "length[0,30]",
				missingMessage : "请输入备注",
				invalidMessage : "限制30字以内",
			});
			//备注
			$("input[name='deviceRemark_edit']").validatebox({
				required : false,
				validType : "length[0,30]",
				missingMessage : "请输入备注",
				invalidMessage : "限制30字以内",
			});
			device_tool = {
				reload : function() {
					$("#device").datagrid("reload");
					//刷新目前解决办法
					location.reload();
					getData(1);
					$('#lb2').combobox('setText','全部');
				},
				redo : function() {
					$("#device").datagrid("unselectAll");
				},
				add : function() {
					id1 = "";
					$("#device_edit").dialog("open");
					$("input[name='category']").focus();
				},
				edit : function() {
					var rows = $("#device").datagrid("getSelections");
					if (rows.length > 1) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) {
						/* console.log(rows[0]); */
						$
								.ajax({
									url : "${APP_PATH }/getDeviceDataById",
									type : "POST",
									data : {
										DeviceId : rows[0].id
									},
									beforeSend : function() {
										$.messager.progress({
											text : "正在获取中...",
										});
									},
									success : function(data, response, status) {
										$.messager.progress("close");
										if (data) {
											id1 = data.extend.deviceId.id;
											$('#device_edit')
													.form(
															'load',
															{
																deviceNumber_edit : data.extend.deviceId.deviceNumber,
																deviceBrand_edit : data.extend.deviceId.deviceBrand,
																deviceType_edit : data.extend.deviceId.deviceType,
																deviceClass_edit : data.extend.deviceId.deviceClass,
																devicePhoto_edit : data.extend.deviceId.devicePhoto,
																devicePlace_edit : data.extend.deviceId.devicePlace,
																deviceStatus_edit : data.extend.deviceId.deviceStatus,
																deviceRemark_edit : data.extend.deviceId.deviceRemark,
															}).dialog('open');
										} else {
											$.messager.alert("获取失败!",
													"未知错误导致失败,请重试!", "warning");
										}
									},
								});
					} else if (rows.length == 0) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					}
				},
				remove : function() {
					var rows = $("#device").datagrid("getSelections");
					var temID="";
			            for (i = 0; i < rows.length;i++) {
			                if (temID =="") {
			                    temID = rows[i].id;
			                } else {
			                    temID = rows[i].id + "," + temID;
			                }               
			            }
					/* if (rows.length > 1) {
						$.messager.alert("警告操作!", "删除记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) { */
						if (rows.length == 0) {
							$.messager.alert("警告操作!", "请选定您要删除的选项!", "warning");
						}else{
							console.log(rows[0]);
							$.ajax({
								type : "POST",
								url : "${APP_PATH }/deleteDeviceByIds",
								data : {
									ids : temID,
									flag : "0",
								},

								beforeSend : function() {
									$("#device").datagrid("loading");
								},
								success : function(data) {
									if (data) {
										getData(1);
										$("#device").datagrid("loaded");
										//刷新当前页
										$("#device").datagrid("load");
										//取消所有行选定
										$("#device").datagrid("unselectAll");
										if(data.code=="0"){
											$.messager.show({
												title : "提示",
												msg : data.msg+":  "+data.extend.fail,
											});
										}else{
											$.messager.show({
												title : "提示",
												msg : data.msg,
											});
										}
										
									} else {
										$.messager.alert("获取失败!", "未知错误导致失败,请重试!",
												"warning");
									}
								},
							});
						}
						
						/*}  else if (rows.length == 0) {
						$.messager.alert("警告操作!", "删除只能选定一条数据!", "warning");
					} */
				},
				adds : function() {
					$("#device_adds").dialog("open");
					$("input[name='category']").focus();
				},

			};
		});
		$('#cc').combobox({  
			    onChange:function(newValue,oldValue){  
			          
			        var rows = $("#device").datagrid("getSelections");
			        $.ajax({
			            url: "${APP_PATH }/getSubscribeByIdAndStatus",
			            type: "GET",
			            cache: false,
			            data: {
			            	subscribe_device_id:rows[0].id,
							

			            },
			            
			         
			            beforeSend: function () {
			            	
			                    $.messager.progress({
			                        text: "正在查询中...",
			                    });
			                    
			                },
			                success: function (data, response, status) {
			                    $.messager.progress("close");
			                    console.log(data);
			                    if (data.code=="1") {
			                    	$.messager.confirm('提示','设备有正在进行的预约记录，是否强制改状态？',function(r){   
									      if (r){   
									            
									      }else{
									    	  $("#device_edit").dialog("close").form("reset");
									      }   
									  });  
			                       
									
									
			                      	
			            			
			            			
			                    } else {
			                       //$.messager.alert("查询失败!", "未知错误导致失败,请重试!", "warning");
			                    }
			                }
			        });
			    }  
			});
		
		
	</script>
</body>
</html>