<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="icon" href="../../favicon.ico">-->


<title>预约管理系统</title>

<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
	<link href="${ctx }/bootstrap-3.3.7-dist/css/default.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/normalize.css" rel="stylesheet">
<script src="${ctx }/easyui/jquery.min.js"></script>
<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<link href="${ctx }/css/base.css" rel="stylesheet">
<style type="text/css">
/*按钮样式  */
.btn-warning {
	color: #fff;
	float: right;
	background-color: #f0ad4e;
	border-color: #eea236
}

.btn-warning.focus, .btn-warning:focus {
	color: #fff;
	background-color: #ec971f;
	border-color: #985f0d
}

.btn-warning:hover {
	color: #fff;
	background-color: #ec971f;
	border-color: #d58512
}

.btn-warning.active, .btn-warning:active, .open>.dropdown-toggle.btn-warning
	{
	color: #fff;
	background-color: #ec971f;
	border-color: #d58512
}

.btn-warning.active.focus, .btn-warning.active:focus, .btn-warning.active:hover,
	.btn-warning:active.focus, .btn-warning:active:focus, .btn-warning:active:hover,
	.open>.dropdown-toggle.btn-warning.focus, .open>.dropdown-toggle.btn-warning:focus,
	.open>.dropdown-toggle.btn-warning:hover {
	color: #fff;
	background-color: #d58512;
	border-color: #985f0d
}

.btn-warning.active, .btn-warning:active, .open>.dropdown-toggle.btn-warning
	{
	background-image: none
}

.btn-warning.disabled.focus, .btn-warning.disabled:focus, .btn-warning.disabled:hover,
	.btn-warning[disabled].focus, .btn-warning[disabled]:focus,
	.btn-warning[disabled]:hover, fieldset[disabled] .btn-warning.focus,
	fieldset[disabled] .btn-warning:focus, fieldset[disabled] .btn-warning:hover
	{
	background-color: #f0ad4e;
	border-color: #eea236
}

.btn-warning .badge {
	color: #f0ad4e;
	background-color: #fff
}
</style>
</head>

<body>
	<!-- 使用动态include指令导入头部 -->
	<jsp:include page="header.jsp" />


	<section id="main" class="container">

		<h3>我的预约</h3>
		<hr>

		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#pass" data-toggle="tab">进行中 <span id="pass_num"></span></a></li>
			<li><a href="#noPass" data-toggle="tab">已结束 <span id="noPass_num"></span></a></li>
			<li><a href="#noDeal" data-toggle="tab">已撤销 <span id="noDeal_num"></span></a></li>
			<li><a href="#noWay" data-toggle="tab">已终止 <span id="noWay_num"></span></a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade in active" id="pass">
				<h3 id ="pass_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="noPass">
					<h3 id ="noPass_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="noDeal">
					<h3 id ="noDeal_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="noWay">
				<h3 id ="noWay_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
		</div>


	</section>

	<jsp:include page="footer.jsp" />

	
	
	
	<!-- 预约详情弹出框 -->
	<div class="modal fade" tabindex="-1" role="dialog" id="showOrderDetailDialog_Modal" data-backdrop="false" style="z-index:1;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">预约详情</h4>
				</div>
				<div class="modal-body">
					<span>预约状态：</span><span id="orderState" style="color:red;" ></span>
					<table class="table">
						<tr>
							<td>预约人：</td>
							<td><span id="orderUserName"></span></td>

						</tr>
						<tr>
							<td>预约时间：</td>
							<td><span id="startTime"></td>
						</tr>
						<tr>
							<td>设备名称：</td>
							<td><span id="orderEquipment"></span></td>
						</tr>
						<tr>
							<td>设备序号：</td>
							<td><span id="deviceNumber"></span></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px solid #ddd;">设备位置：</td>
							<td style="border-bottom: 1px solid #ddd;"><span id="equipmentPoint"></span></td>
						</tr>
						<tr>
							<td>备注：</td>
							<td><span id="deviceRemark"></span></td>
						</tr>
						<tr id="CauseOfRevocation" style="display:none;">
							<td style="border-top: none;">撤销原因：</td>
							<td style="border-top: none;" colspan="2"><span id="CauseOfRevocationSpan"></span></td>
						</tr>
						<tr>
							<td colspan="2">
							<button type="button" id="ReserveButton" style="display:none; width: 30%;margin-left: auto;margin-right: auto;float: none;" class="btn btn-warning"onclick="ButtonOnClick(2);">撤   销</button><button type="button" id="StopButton" style="display:none; width: 30%;margin-left: auto;margin-right: auto;float: none;" class="btn btn-warning " onclick="ButtonOnClick(1);">终止</button>
							</td>
						</tr>
						
					</table>
					
					<span>邀约的伙伴：</span>
				<table class="table table-striped">
				  <thead>
			        <tr>
			          <th>姓名</th>
			          <th>联系方式</th>
			          <th>状态</th>
			          <th>操作</th>
			        </tr>
			      </thead>
			      <tbody id="invitePosContent">
			        <!-- <tr>
			          <th>Mark</th>
			          <td></td>
			          <td></td>
			          <td></td>
			        </tr>
			        <tr>
			          <th scope="row">Jacob</th>
			          <td></td>
			          <td></td>
			          <td></td>
			        </tr>
			        <tr>
			          <th scope="row">Jacob</th>
			          <td></td>
			          <td>the Bird</td>
			          <td>@twitter</td>
			        </tr> -->
			      </tbody>
				</table>
				</div>
				<div class="modal-footer">
					<button type="button" id="showOrderDetailModal_QxButton" class="btn btn-default"
						data-dismiss="modal">取消</button>
					<button type="button" id="showOrderDetailModal_QdButton" class="btn btn-primary">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	
		<!-- 撤销确认弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showRescindDialog_Modal" data-backdrop="false" style="z-index:2;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showRescindDialog_content"></span>
					<div class="form-group" style="color: #EA2000 !important;">
						<input type="text" class="form-control" id="showRescindDialog_ReserveReasonText" maxlength="30"
							placeholder="请输入您的撤销原因  *最长输入30个字符">
						<span id="showStopDialog_tishi"></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="showRescindDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
					<button type="button" id="showRescindDialog_QdButton" class="btn btn-primary">确定</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	
		<!-- 终止确认弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showStopDialog_Modal" data-backdrop="false" style="z-index:2;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showStopDialog_content"></span>
					<div class="form-group">
						<input type="text" class="form-control" id="showStopDialog_ReserveReasonText"
							placeholder="请输入您的撤销原因">
					</div><br>
				</div>
				<div class="modal-footer">
					<button type="button" id="showStopDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
					<button type="button" id="showStopDialog_QdButton" class="btn btn-primary">确定</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	
		<!-- 删除确认弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showDeletDialog_Modal" data-backdrop="false" style="z-index:2;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showDeletDialog_content"></span>
					<div class="form-group">
						<input type="text" class="form-control" id="showDeletDialog_ReserveReasonText"
							placeholder="请输入您的删除原因">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="showDeletDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
					<button type="button" id="showDeletDialog_QdButton" class="btn btn-primary">确定</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
		<!-- 只有关闭弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showCloseDialog_Modal" data-backdrop="false" style="z-index:999;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showCloseDialog_content"></span>
				</div>
				<div class="modal-footer">
					<button type="button" id="showCloseDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
		<!-- 撤销 只有关闭弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showCheXiaoDialog_Modal" data-backdrop="false" style="z-index:999;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showCheXiaoDialog_content"></span>
				</div>
				<div class="modal-footer">
					<button type="button" id="showCheXiaoDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
		<!-- 撤销 只有关闭弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showZhongZhiDialog_Modal" data-backdrop="false" style="z-index:999;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showZhongZhiDialog_content"></span>
				</div>
				<div class="modal-footer">
					<button type="button" id="showZhongZhiDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
</body>
<script>
var userName;
var userEmail;
var ai="0";
var ai1="0";
var ai2="0";
var ai3="0";
window.onload = function(){ 
	trace("初始化方法进入");
	
	 userName = window.sessionStorage.getItem("usertruename");
	userEmail = window.sessionStorage.getItem("email");
/* 	alert(userName+"--------------------------userName")
	alert(userEmail+"--------------------------userEmail") */
 if(userName!=null&&userEmail!=null){
	person();
}else{
	window.location.replace("applyIndex.jsp");
} 
/* 	person1();
	person2();
	person3(); */
	}; 
function trace(obj){ 
	console.log(obj); 
	} 

function person(){
	var loadInfo = '${ctx }' + "/getSubscribeByEmailAndName";
	 $.ajax({
	        url: loadInfo,
	        type: 'GET',
	        dataType: 'json',
	        cache: false,
	        data:{
	        	subscribe_user_email:userEmail,
	        	subscribe_user_name:userName
	        },
	        beforeSend: LoadFunction, //加载执行方法
	        error: erryFunction, //错误执行方法
	        success: succFunction //成功执行方法
	    })
	    function LoadFunction() {
		 //加载执行方法
	    }
	    function erryFunction() {
	    	//错误执行方法
	    }
	    function succFunction(JsonString) {

	        json = eval(JsonString);
	       // console.log(JSON.stringify(json));
	        if (json.code > 0) {
	        	//清空值
	    		//document.getElementById('pass').innerHTML="";
	    		$.each(json.extend.getEmailAndName, function(i, item) {
	    			
	    			if(item.subscribeStatus ==1 || item.subscribeStatus==2)
					{	
	    				ai++;
	    				//alert("进行中    ："+ai);
	    				$("#pass_NoRecord").hide();
	    			/* $("#pass").append(
	    					"<div class='row myyuyue'><br><div class='panel panel-success'><div class='panel-heading'>"+"进行中："+"</div><div class='panel-body'><p>"
	    					+"<label>设备编号：</label>"+item.deviceNumber+"</p><p><label>设备名称：</label>"+item.name+"</p><p><label>开始时间：</label>"+formatDatebox(item.subscribeBeginTime)
	    					+"</p><p><label>结束时间：</label>"+formatDatebox(item.subscribeEndTime)+"</p><p><label>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</label>"+item.subscribeRemark+"</p><p><button type='button' class='btn btn-warning'id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\")'>预约详情</button></p></div></div></div>"
	    			); */
	    				$("#pass").append("<div class='plan'><h3 class='plan-title-Run'>"
	    	    				+"进行中"+"</h3><ul class='plan-features'><li class='plan-feature'>"
	    	    				+"设备编号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
	    	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.name+"</span>"
	    	    				+"</li><li class='plan-feature'>开始时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeBeginTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>结束时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeEndTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
	    	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\")'>预约详情</a></div>"
	    						);
	    			if(ai > 99 ){
	    				$("#pass_num").html("99+");
	    			}else{
	    				$("#pass_num").html("("+ai+")");
	    			}
	    			
					}
	    			if(item.subscribeStatus ==3)
					{	
	    				ai1++;
	    				//alert("已结束      ："+ai1);
	    				$("#noPass_NoRecord").hide();
	    				$("#noPass").append("<div class='plan'><h3 class='plan-title-End'>"
	    	    				+"已结束"+"</h3><ul class='plan-features'><li class='plan-feature'>"
	    	    				+"设备编号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
	    	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.name+"</span>"
	    	    				+"</li><li class='plan-feature'>开始时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeBeginTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>结束时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeEndTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
	    	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\")'>预约详情</a></div>"
	    						);
	    				$("#noPass_num").html(ai1);
	    				if(ai1 > 99 ){
		    				$("#noPass_num").html("99+");
		    			}else{
		    				$("#noPass_num").html("("+ai1+")");
		    			}
					}
	    			if(item.subscribeStatus ==4)
					{	
	    				ai2++;
	    				//alert("已撤销   ："+ai2);
	    				$("#noDeal_NoRecord").hide();
	    				$("#noDeal").append("<div class='plan'><h3 class='plan-title-Revoke'>"
	    	    				+"已撤销"+"</h3><ul class='plan-features'><li class='plan-feature'>"
	    	    				+"设备编号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
	    	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.name+"</span>"
	    	    				+"</li><li class='plan-feature'>开始时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeBeginTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>结束时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeEndTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
	    	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\")'>预约详情</a></div>"
	    						);
	    				if(ai2 > 99 ){
		    				$("#noDeal_num").html("99+");
		    			}else{
		    				$("#noDeal_num").html("("+ai2+")");
		    			}
					}
	    			if(item.subscribeStatus ==5)
					{	
	    				ai3++;
	    				//alert("已终止      ："+ai3);
	    				$("#noWay_NoRecord").hide();
	    				$("#noWay").append("<div class='plan'><h3 class='plan-title-Stop'>"
	    	    				+"已终止"+"</h3><ul class='plan-features'><li class='plan-feature'>"
	    	    				+"设备编号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
	    	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.name+"</span>"
	    	    				+"</li><li class='plan-feature'>开始时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeBeginTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>结束时间：<span class='plan-feature-name'>"+formatDatebox(item.subscribeEndTime)+"</span>"
	    	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
	    	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\")'>预约详情</a></div>"
	    						);
	    				if(ai3 > 99 ){
		    				$("#noWay_num").html("99+");
		    			}else{
		    				$("#noWay_num").html("("+ai3+")");
		    			}
					}
	    			
	    		});
	    		
	    		
	        }else{
			
			}
	    }

}
 
var OrderIdByOnclickOrderDetailButton= "";
var nowOrderState;
//查看预约详情按钮
function OrderDetailButtonOnClick(orderID){
	OrderIdByOnclickOrderDetailButton=orderID;
	
	$('#showOrderDetailModal_QxButton').hide();
	$('#showOrderDetailDialog_Modal').modal('show');
	$('#showOrderDetailModal_QdButton').click(function() {
		$('#showOrderDetailDialog_Modal').modal('hide');
	});
	
	 var getSubscribeDetailedWithDeviceDataBySubscribeId_URL ='${ctx }' + "/getSubscribeDetailedWithDeviceDataBySubscribeId"; //
	    
	    $.ajax({
	        url: getSubscribeDetailedWithDeviceDataBySubscribeId_URL,
	        type: 'GET',
	        dataType: 'json', 
	        cache: false,
	        data: {
	            subscribe_id: orderID
	        },
	        beforeSend: LoadFunction, //加载执行方法
	        error: erryFunction, //错误执行方法
	        success: succFunction //成功执行方法
	    })

	    function LoadFunction() {
	    	//加载执行方法
	    }

	    function erryFunction() {
	    	//错误执行方法
	    }

	    function succFunction(JsonString) {
	 
	        json = eval(JsonString);
	       console.log(JSON.stringify(json));
	        if (json.code > 0) {

	            var dataJson = json.extend.getSubscribeDetailedWithDeviceDataBySubscribeId;
	            var dataJson_huoban =json.extend.subscribeSon;
	            for (var i = 0; i < dataJson.length; i++) {
	                //解析JSON
	                var id = dataJson[i]["id"];
	               
	                var subscribeStatus = dataJson[i]["subscribeStatus"];
	                nowOrderState= subscribeStatus;
	                var subscribeStatusValue = "";
	                
	                var subscribeBeginTime = dataJson[i]["subscribeBeginTime"];
	                var subscribeEndTime = dataJson[i]["subscribeEndTime"];
	                var deviceType = dataJson[i]["device"]["deviceType"];
	                var deviceNumber = dataJson[i]["device"]["deviceNumber"];
	                var devicePlace = dataJson[i]["device"]["devicePlace"];
	                var subscribeUserName= dataJson[i]["subscribeUserName"];
	                var  causeOfRevocation= dataJson[i]["subscribeResult"];
	                var deviceRemark = dataJson[i]["subscribeRemark"];
	                var oderfriends;////////////////////////////////////////////////////////////////////////////////
	                var stopButton_obj = document.getElementById("StopButton");  
                    var reserveButton_obj = document.getElementById("ReserveButton");
                   
	                if (subscribeStatus == 1 || subscribeStatus == 2) {
	                    subscribeStatusValue = "进行中";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    var nowTime = new Date(); //系统当前时间
		                //经过方法转换为毫秒
		                nowTime = fmtMillisecond(nowTime);
		                //后面的数据直接用毫秒
		                var startTime = subscribeBeginTime;
                        var endTime = subscribeEndTime;
		                var stopButton  = document.getElementById("StopButton");  
	                    var reserveButton = document.getElementById("ReserveButton");  
	                    if ((startTime < nowTime) && (endTime > nowTime)) { //判断 预约开始时间小于系统当前时间
	                    	reserveButton_obj.style.cssText ="display:none;"
                            stopButton_obj.style.cssText ="display:block; width: 30%;margin-left: auto;margin-right: auto;float: none;" //显示终止按钮
                            
                        } else if (startTime > nowTime ||startTime == nowTime) { //判断 预约开始时间大于等于系统当前时间
                        	stopButton_obj.style.cssText ="display:none;"
                            reserveButton_obj.style.cssText ="display:block; width: 30%;margin-left: auto;margin-right: auto;float: none;"//显示撤销按钮
                        
                        } else if (endTime < nowTime ){//结束时间小于系统当前时间
                            stopButton_obj.style.cssText ="display:none;"
                            reserveButton_obj.style.cssText ="display:none;"
                        } 
	                } else if (subscribeStatus == 3) {
	                    subscribeStatusValue = "已结束";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    stopButton_obj.style.cssText ="display:none;"
                        reserveButton_obj.style.cssText ="display:none;"
	                } else if (subscribeStatus == 4 ) {
	                    subscribeStatusValue = "已撤销";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:table-row;"             
	                    stopButton_obj.style.cssText ="display:none;"
                        reserveButton_obj.style.cssText ="display:none;"
	                }else if (subscribeStatus == 5 ) {
	                    subscribeStatusValue = "已终止";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    stopButton_obj.style.cssText ="display:none;"
                        reserveButton_obj.style.cssText ="display:none;"
	                }
	            }

	            //渲染数据
	            
	            document.getElementById('orderUserName').innerHTML = subscribeUserName;
	            document.getElementById('orderState').innerHTML = subscribeStatusValue;
	            document.getElementById('startTime').innerHTML = fmtDate(subscribeBeginTime,subscribeEndTime);
	            document.getElementById('orderEquipment').innerHTML = deviceType;
	            document.getElementById('deviceNumber').innerHTML = deviceNumber;
	            document.getElementById('equipmentPoint').innerHTML = devicePlace;
	            document.getElementById("CauseOfRevocationSpan").innerHTML = causeOfRevocation;
	            
	            document.getElementById("deviceRemark").innerHTML = deviceRemark;
	            
	            var htmlString = "";
	            if(dataJson_huoban.length>0){
	                for (var i = 0; i < dataJson_huoban.length; i++) {
	                   var id = dataJson_huoban[i].id;
	                   var subscribeUserStatus = dataJson_huoban[i].subscribeUserStatus;
	                   var state="";

	                  if(dataJson_huoban[i].subscribeUserStatus!=3){
					   htmlString += "<tr><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserName
								+ "</td><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserMobile
								+ "</td><td style='color:red;line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserStatusName
								+ "</td><td><button type='button' class='btn btn-warning' id='' onclick='DeleteInvitePerson(\""+id+"\","+subscribeUserStatus+")'>删除</button></td></tr>"
					
	                }else{
	                	  htmlString += "<tr><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserName
								+ "</td><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserMobile
								+ "</td><td style='color:red;line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserStatusName
								+ "</td><td style='color:blue;line-height:34px;'>已删除</td></tr>"
					
	                }
	                }
					//alert(htmlString.toString().length);
					if (htmlString.toString().length <= 0) {
						htmlString = "<tr><td colspan='4' style='text-align:center;color:blue;'>无邀约的伙伴</div></td></tr>";
					}

				} else {
					htmlString = "<tr><td colspan='4' style='text-align:center;color:blue;' >无邀约的伙伴</div></td></tr>";
					//document.getElementById('huobanhuoban').innerHTML=htmlString;	
				}
	            document.getElementById('invitePosContent').innerHTML=htmlString;
			}
		}
	}
	
	
//var new_InviteId ="";
function DeleteInvitePerson(InviteId,subscribeUserStatus){
	var new_InviteId = InviteId;
	if(nowOrderState==3 ||nowOrderState==4||nowOrderState==5){
	        $('#showCloseDialog_content').html("当前预约已结束");
			$('#showCloseDialog_Modal').modal('show');
			$('#showCloseDialog_QxButton').click(function() {
				$('#showCloseDialog_Modal').modal('hide');
			});
	       }else{
	        if(subscribeUserStatus ==3){
	       	  $('#showCloseDialog_content').html("已删除");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
				});	      
				
	        }else if(subscribeUserStatus ==2){
	         $('#showCloseDialog_content').html("对方已拒绝邀请，不可删除");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
				});	 
	        }else{
	        	$('#showDeletDialog_content').html("是否确认删除该邀请人？");
				/* $('#QxButton').hide(); */
				$('#showDeletDialog_Modal').modal('show');
				$('#showDeletDialog_QdButton').click(function() {
					$('#showDeletDialog_Modal').modal('hide');
				
					 updateSubscribeSonByIdForUserStatus_click(new_InviteId,subscribeUserStatus,$('#showDeletDialog_ReserveReasonText').val());
					 new_InviteId = "";
				});
				$('#showDeletDialog_QxButton').click(function() {
					$('#showDeletDialog_Modal').modal('hide');
				//	location.reload();
				//	OrderDetailButtonOnClick(id);
					new_InviteId = "";
				});
	        }
	}
}
	
	//根据信息id，原因删除受邀人员
function updateSubscribeSonByIdForUserStatus_click(InviteId,subscribeUserStatus,yuanyin){
	
		
	 //URL拼接
	       if(nowOrderState==3 ||nowOrderState==4||nowOrderState==5){
	        $('#showCloseDialog_content').html("当前预约已结束");
			$('#showCloseDialog_Modal').modal('show');
			$('#showCloseDialog_QxButton').click(function() {
				$('#showCloseDialog_Modal').modal('hide');
			});	 
	       }else{
	        if(subscribeUserStatus ==3){
	         $('#showCloseDialog_content').html("已删除");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
				});	 
	        }else{
	        
	   		 if(InviteId =="" || InviteId ==null){
	    	
		
	        }else{
	         	
	    	    var updateSubscribeSonByIdForUserStatus_URL = '${ctx }' +"/updateSubscribeSonByIdForUserStatus";
	    		
	    	
	    	    $.ajax({
	    	        url: updateSubscribeSonByIdForUserStatus_URL,
	    	        type: 'GET',
	    	        dataType: 'json',
	    	        cache: false,
	    	        data:{   
	    	            id:InviteId,
	    	            subscribe_user_status:3,
	    	            subscribe_result:yuanyin
	    	        },
	    	        beforeSend: LoadFunction, //加载执行方法
	    	        error: erryFunction, //错误执行方法
	    	        success: succFunction //成功执行方法
	    	    })

	    	    function LoadFunction() {
	    	        $(".header1").html('加载中...');
	    	    }

	    	    function erryFunction() {
	    	    	//错误执行方法
	    	    }

	    	    function succFunction(JsonString) {
	    	        json = eval(JsonString);
	    	        if (json.code > 0) {
	    	        	   $('#showCloseDialog_content').html("删除成功");
	    					$('#showCloseDialog_Modal').modal('show');
	    					$('#showCloseDialog_QxButton').click(function() {
	    						$('#showCloseDialog_Modal').modal('hide');
	    						
	    						OrderDetailButtonOnClick(OrderIdByOnclickOrderDetailButton);
	    					//	location.reload();
	    					});	 
	    	        } else {
	    	        	  $('#showCloseDialog_content').html(json.msg);
	    					$('#showCloseDialog_Modal').modal('show');
	    					$('#showCloseDialog_QxButton').click(function() {
	    						$('#showCloseDialog_Modal').modal('hide');
	    					});	 
	    	        }
	    	    }
	        }
	    
		}         
	  }
		
   }


	
	

	function ButtonOnClick(flage) {
		if (flage == 1) { //终止按钮
			$('#showStopDialog_content').html("是否确认终止？");
			/*   $('#QxButton').hide();
			  $('#QdButton').hide(); */
			$('#showStopDialog_Modal').modal('show');
			$('#showStopDialog_ReserveReasonText').hide();
			$('#showStopDialog_QdButton').click(function() {
				$('#showStopDialog_Modal').modal('hide');
				UpdateOrderState("1", "");
			});
			$('#showStopDialog_QxButton').click(function() {
				$('#showStopDialog_Modal').modal('hide');
			});
		} else if (flage == 2) { //撤销按钮
			$('#showRescindDialog_content').html("是否确认撤销？");

			$('#showRescindDialog_ReserveReasonText').show();
			$('#showRescindDialog_Modal').modal('show');
			$('#showRescindDialog_QdButton').click(function() {
				//$('#showRescindDialog_Modal').modal('hide');
			//	if($('#showRescindDialog_ReserveReasonText').val()!=null && $('#showRescindDialog_ReserveReasonText').val()!=""){
				UpdateOrderState("2", $('#showRescindDialog_ReserveReasonText').val());
			//	}else{
					//$('#showStopDialog_tishi').replaceWith(" * 原因不能为空，请填写撤销原因！");
			//		$('#showRescindDialog_Modal').modal('hide');
			//	}
			});
			$('#showRescindDialog_QxButton').click(function() {
				$('#showRescindDialog_Modal').modal('hide');
				
			});
		}
	}

	function UpdateOrderState(flage, Reason) {

		var state;
		if (flage == 1) {
			state = 5; //代表终止状态
		} else if (flage == 2) {
			state = 4; //代表撤销状态
		}
		var UpdateOrderState_URL = '${ctx }' + "/updateSubscribeStatusById";

		$.ajax({
			url : UpdateOrderState_URL,
			type : 'GET',
			dataType : 'json',
			cache : false,
			data : {
				subscribe_id : OrderIdByOnclickOrderDetailButton,
				subscribe_status : state,
				subscribe_result : Reason
			},
			beforeSend : LoadFunction, //加载执行方法
			error : erryFunction, //错误执行方法
			success : succFunction
		//成功执行方法
		})

		function LoadFunction() {
			$(".header1").html('加载中...');
		}

		function erryFunction() {
			//错误执行方法
		}

		function succFunction(JsonString) {

			json = eval(JsonString);

			if (json.code > 0) {
				if (flage == 1) {
					$('#showZhongZhiDialog_content').html("终止成功");
					$('#showZhongZhiDialog_Modal').modal('show');
					$('#showZhongZhiDialog_QxButton').click(function() {
						$('#showZhongZhiDialog_Modal').modal('hide');
						window.location.reload();
					});	 
				} else if (flage == 2) {
					$('#showCheXiaoDialog_content').html("撤销成功");
					$('#showCheXiaoDialog_Modal').modal('show');
					$('#showCheXiaoDialog_QxButton').click(function() {
						$('#showCheXiaoDialog_Modal').modal('hide');
						window.location.reload();
					});	 
				}
			} else {
				$('#showCloseDialog_content').html("操作失败");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
				});	
			}
		}

	}

	//转换时间戳方法
	Date.prototype.format = function(format) {
		var o = {
			"M+" : this.getMonth() + 1, // month  
			"d+" : this.getDate(), // day  
			"h+" : this.getHours(), // hour  
			"m+" : this.getMinutes(), // minute  
			"s+" : this.getSeconds(), // second  
			/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   *///没用上所以注释了
			"S" : this.getMilliseconds()
		// millisecond  
		}
		if (/(y+)/.test(format))
			format = format.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(format))
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
						: ("00" + o[k]).substr(("" + o[k]).length));
		return format;
	}

	function formatDatebox(value) {
		if (value == null || value == '') {
			return '';
		}
		var dt;
		if (value instanceof Date) {
			dt = value;
		} else {
			dt = new Date(value);
		}

		return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
	}
	/*将时间字符串格式转换为毫秒直接与json中的参数进行比较判断*/
	/*用于时间数据校验*/
	function fmtMillisecond(date) {
		//将时间字符串格式转换为毫秒
		var y = date.getFullYear();
		//从日期字符串中获取的月份数据再加1
		var m = date.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		var h = date.getHours();
		h = h < 10 ? ('0' + h) : h;
		var minute = date.getMinutes();
		var second = date.getSeconds();
		var time = y + '/' + m + '/' + d + ' ' + h + ':' + minute + ':'
				+ second;
		var t = new Date(time).getTime();
		return t;
	}
	/*毫秒转换成正常时间格式*/
	/*只用于预约详情显示*/
	/*12月1日 星期五  17:00~18:00*/
	function fmtDate(stime, etime) {
		var startTime = new Date(stime);
		var endTime = new Date(etime);
		var weekStr = "星期" + "日一二三四五六".charAt(startTime.getDay());
		//  var y = date.getFullYear();
		var m = startTime.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = startTime.getDate();
		d = d < 10 ? ('0' + d) : d;
		var startHour = startTime.getHours();
		startHour = startHour < 10 ? ('0' + startHour) : startHour;
		var startminute = startTime.getMinutes();
		startminute = startminute < 10 ? ('0' + startminute) : startminute;

		var endHour = endTime.getHours();
		endHour = endHour < 10 ? ('0' + endHour) : endHour;
		var endminute = endTime.getMinutes();
		endminute = endminute < 10 ? ('0' + endminute) : endminute;

		var result = m + '-' + d + ' ' + weekStr + " " + startHour + ':'
				+ startminute + '~' + endHour + ":" + endminute;
		return result;
	};
</script>
</html>