<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="icon" href="../../favicon.ico">-->

<title>三级联动</title>

<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="${ctx }/easyui/jquery.min.js"></script>
<script src="${ctx }/app/script/cmbProvince.js"></script>
<link href="${ctx }/css/apply-index.css" rel="stylesheet">
<link href="${ctx }/css/apply-form.css" rel="stylesheet">
</head>
<body>  
<select id="cmbProvince" name="cmbProvince"></select>  
                        <select id="cmbCity" name="cmbCity"></select>  
                        <select id="cmbArea" name="cmbArea"></select>  
  
  
               <script type="text/javascript">  
                    addressInit('cmbProvince', 'cmbCity', 'cmbArea');  
               </script>  
</body>  

</html>