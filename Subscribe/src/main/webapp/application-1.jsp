<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<meta name="description" content="">
		<meta name="author" content="">
<title>申请表</title>
<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
		<script src="${ctx }/easyui/jquery.min.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	

	<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">入场须知</h4>
	      </div>
	      <div class="modal-body">
	       <h1 align="center">中国第一汽车集团公司职工健身中心入场须知</h1>
		        一、 &nbsp;&nbsp;集团职工首次进入健身中心需提前填写手机端电子版入会申请登记表，每次健身员工须用手机在程序中预约入场健身，方可进入健身中心。
			严禁冒名顶替，严禁本公司工作人员私自带家属及朋友进场。<br/>
			二、每次使用场地内有氧设备、乒乓球、羽毛球、台球需要预约使用。<br/>
			三、进入健身中心需穿运动服、运动鞋（室内专用），请勿穿拖鞋、高跟鞋、钉子鞋、硬底鞋在健身场所内走动或健身。<br/>
			四、严禁在健身区和前台公共区域赤背、赤脚、打闹。<br/>
			五、请自觉保持训练场地、更衣室及淋浴间的环境卫生，严禁在训练场地内进食、喝饮料、吸烟、随地吐痰及吐口香糖、乱扔杂物，禁止将碳酸、果汁饮料带进场地内引用。<br/>
			六、酒后、过饱、过饥者请不要健身。<br/>
			七、患有疾病、传染病者请勿入健身中心。<br/>
			八、如首次健身，使用健身器材前请教练示范后使用，自由力量区要有人保护的情况下使用。<br/>
			九、当时用健身设备出现问题时，请第一时间告知教练或工作人员。<br/>
			十、发现故意损坏健身设备和器材者追究责任，并按价赔偿。<br/>
			十一、健身器材要轻拿轻放，用后请放回原处，方便他人使用。<br/>
			十二、场地内空调、电源开关、由健身中心工作人员操作，其他啊人员不得擅自变更。<br/>
			十三、健身中心西侧门为紧急安全出口，只有在发生火灾、地震等紧急情况下使用，平时禁止出入，如违反规定造成不良后果将追究当事人责任。<br/>
			十四、请妥善保管好私人物品，如有丢失健身中心概不负责。<br/>
			十五、对健身中心工作人员服务不满意或不认可，请向值班经理或工会相关人员投诉。投诉电话：15043426222 郭经理<br/>
			<div class="row">
			<div class="col-md-5 col-md-offset-7">
					中国第一汽车集团公司总工会宣
			</div>	
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
	        <button type="button" class="btn btn-primary" onclick="isSubmit();">确定</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<div class="container">
		<!-- 标题 -->
		<div class="row">
		  <div class="col-md-12">
				<h1 align="center"><b>中国第一汽车集团公司职工健身中心入会申请表</b></h1>
				<hr>
		 </div>
		</div>
		<!-- 内容 -->
		
		<div class="row">
		  <div class="col-md-2">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">姓名</span>
			  <input readonly="readonly" type="text" class="form-control" id="name" aria-describedby="basic-addon1"/>
			</div>
		  </div>	
		  <div class="col-md-3">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">性别</span>
			  <input readonly="readonly" type="text" class="form-control" name="gender" id="gender" />
			</div>
		  </div>
		  <div class="col-md-3">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">证件类型</span>
			  <input readonly="readonly" type="text" class="form-control" name="cardtype" id="cardtype" />
			</div>
		  </div>
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">证件号码</span>
			  <input readonly="readonly" type="text" class="form-control" id="cardno" aria-describedby="basic-addon1">
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">工作证号</span>
			  <input type="text" class="form-control" id="staff_no" aria-describedby="basic-addon1">
			</div>
		  </div>	
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">国籍</span>
			  <input type="text" class="form-control" id="nation" aria-describedby="basic-addon1">
			</div>
		  </div>
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">生日</span>
			  <input readonly="readonly" type="text" class="form-control" id="birthday" aria-describedby="basic-addon1">
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">QQ/微信号码</span>
			  <input type="text" class="form-control" id="qv" aria-describedby="basic-addon1">
			</div>
		  </div>	
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">居住地址</span>
			  <input type="text" class="form-control" id="address" aria-describedby="basic-addon1">
			</div>
		  </div>
		  <div class="col-md-4">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">邮箱</span>
			  <input readonly="readonly" type="text" class="form-control" id="email" aria-describedby="basic-addon1">
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-md-6">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">本人联系电话</span>
			  <input type="text" class="form-control" id="cell" aria-describedby="basic-addon1">
			 </div>
		  </div>
		  <div class="col-md-6">
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">单位所属部门</span>
			  <input readonly="readonly" type="text" class="form-control" id="department" aria-describedby="basic-addon1">
			 </div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-md-12">
		    <div class="input-group ">
			  <span class="input-group-addon" id="basic-addon1">紧急情况下联系方式</span>
			  <input type="text" class="form-control" id="emer" aria-describedby="basic-addon1">
			  <input type="text" class="form-control" id="emer2" aria-describedby="basic-addon2">
			 </div>
		  </div>
		 </div>
		 
		 <div class="row">
            <div class="col-lg-6">
                <div class="input-group" id="radiolist">
                    <span class="input-group-addon">是否有疾病史：</span>
                        无<input type="radio" name="disease" id="disease" value="无">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有<input type="radio" name="disease" id="disease" value="无">
          
                </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-lg-6">
                <div class="input-group" id="radiolist2">
                    <span class="input-group-addon">是否有伤痛史：</span>
                        无<input type="radio" name="injury" id="injury" value="无">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有<input type="radio" name="injury" id="injury" value="无">
          
                </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-lg-6">
                <div class="input-group" id="radiolist3">
                    <span class="input-group-addon">是否有运动史：</span>
                        无<input type="radio" name="sport" id="sport" value="无">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有<input type="radio" name="sport" id="sport" value="有">
          
                </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-lg-12">
                <div class="input-group" id="checkboxlist">
                    <span class="input-group-addon">健身目标：</span>
                    <input value="改善形体" type="checkbox" name="goal" id="goal">改善形体&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input value="增肌" type="checkbox" name="goal" id="goal">增肌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   	<input value="改善体质" type="checkbox" name="goal" id="goal">改善体质&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   	<input value="减缓工作或学习压力" type="checkbox" name="goal" id="goal">减缓工作或学习压力&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input value="减脂" type="checkbox" name="goal" id="goal">减脂&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input value="塑形" type="checkbox" name="goal" id="goal">塑形&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input value="病体恢复" type="checkbox" name="goal" id="goal">病体恢复&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-6">
              <p>
             		入会规定<br/>
					1、&nbsp;职工入会申请需工会批准，工会有权拒绝申请人的申请。<br/>
					2、&nbsp;在场地训练中因个人原因呢造成身体伤害或突发疾病出现意外事故，由职工自行承担一切经济责任。<br/>
					3、&nbsp;有既往病史的职工要填写附表不得隐瞒。<br/>
					4、&nbsp;发现故意损坏健身设备者，工会有权终止健身资格，损坏物品按价赔偿。<br/>
					5、&nbsp;协助健身中心工作人员填写意见调查表。<br/>
              </p>
          
             </div>
             <div class="col-lg-6">
              <p>
             		自愿声明<br/>
					1、&nbsp;本人声明本申请表及随附文件所载一切资料，依本人所知均属正确，并无遗漏。<br/>
					2、&nbsp;本人自愿参与健身训练，以改善身体健康水平。<br/>
					3、&nbsp;本人明白在健身过程中如有不适应立即停止。<br/>
					4、&nbsp;本人在健身过程中因不遵循中心规定，没按健身教练员指导或因自身原因造成的一切不良后果由自己承担责任。<br/>
					5、&nbsp;使用更衣箱时箱内不存放贵重物品，更衣箱物品不能隔夜存放。<br/>
					6、&nbsp;本人已阅读并理解，当遵守健身中心规定。<br/>
					
              </p>
          
             </div>       
         </div>
         
         <div class="row">
         	<div class="col-lg-6">
         		<b>以上内容已详细阅读</b><input type="radio" name="read" id="read" />
         	</div>
         </div>
         
        <div class="row"> 
			<div class="col-md-4 col-md-offset-8">
				<button type="button" data-toggle="modal" data-target="#confirmModal" id="confirm" onclick="isread();">确定</button>
			</div>
		</div> 
	</div>
	
	<script type="text/javascript">
	//页面加载事件
	/* window.onload = function(){
		var email = window.sessionStorage.getItem("email");
		console.log(email);
		$.ajax({
			url: '${ctx }' + "/selectByPersonEmail",
			type: "GET",
			data: {
				email: email
				},
			success: function(result) {
				if (result.extend.success[0].isapplication == "1"){
					alert("你已经填过了，不需要再来填写");
					setTimeout(function(){
						window.location.href="applyIndex.jsp";
						},500);
				}
			},
		});
		} */
		window.onload = function(){
			var email = window.sessionStorage.getItem("email"); 
			//var email = "tangwenwen@faw.com.cn";
			url = '${ctx }' + "/selectByPersonEmail?email=" + email;
				$.getJSON(url, function(data) {
					var idnumber = data.extend.success[0].idnumber;
					$("#name").val(data.extend.success[0].usertruename);
					$("#cardno").val(idnumber);
					$("#email").val(email);
					$("#department").val(data.extend.success[0].organizeallname);
					$("#cell").val(data.extend.success[0].mobilephone);
					var id1 = idnumber.substr(6, 4);
					var id2 = idnumber.substr(10, 2);
					var id3 = idnumber.substr(12, 2);
					$("#birthday").val(id1 + "-" + id2 + "-"+ id3);
					var sex = "";
					if(data.extend.success[0].usersex == "0"){
						sex = "女";
					}else if(data.extend.success[0].usersex == "1"){
						sex = "男";
					}
					$("#gender").val(sex);
					if(idnumber != null){
						$("#cardtype").val("身份证");
					}else{
						$("#cardtype").val("其他");
					}
					$("#emer").val(data.extend.success[0].officephone);
				});
			}
	 //表单验证
	function isread(){
	} 
		
	//提交表单
	function isSubmit(){
		var checkboxstr = '';
		 $("#checkboxlist > [type=checkbox]").each(function(){
		  if($(this)[0].checked) {
			  checkboxstr += $(this).val()+',';
		  }
		 });
		 
		 var radiostr = '';
		 $("#radiolist > [type=radio]").each(function(){
		  if($(this)[0].checked) {
			  radiostr += $(this).val();
		  }
		 });
		 var radiostr2 = '';
		 $("#radiolist2 > [type=radio]").each(function(){
		  if($(this)[0].checked) {
			  radiostr2 += $(this).val();
		  }
		 });
		 var radiostr3 = '';
		 $("#radiolist3 > [type=radio]").each(function(){
		  if($(this)[0].checked) {
			  radiostr3 += $(this).val();
		  }
		 });
		
		var usertruename =window.sessionStorage.getItem("usertruename");
		var email =window.sessionStorage.getItem("email");
		var mobilephone =window.sessionStorage.getItem("mobilephone");
		var userid = window.sessionStorage.getItem("userid");
		var organizeallname = window.sessionStorage.getItem("organizeallname");
		var idnumber = window.sessionStorage.getItem("idnumber");
		
		$.ajax({
			url: "${ctx }/insertApplication",
			type: "GET",
			data: {
				staffidno: $('input[id="staff_no"]').val(),
				name: usertruename,
				gender: $('input[id="gender"]').val(),
				cardtype: $('input[id="cardtype"]').val(),
				cardno: idnumber,
				national: $('input[id="nation"]').val(),
				birthday: $('input[id="birthday"]').val(),
				qqwechat: $('input[id="qv"]').val(),
				address: $('input[id="address"]').val(),
				email : email,
				phone : mobilephone,
				phone1 : $('input[id="emer"]').val(),
				phone2 : $('input[id="emer2"]').val(),
				department : organizeallname,
				isdisease : radiostr,
				ishurt : radiostr2,
				issport : radiostr3,
				goal : checkboxstr
					},
				
			beforeSend: function() {
				//请求前
				
			},
			success: function(result) {
				//请求成功时
					alert("提交成功");
					setTimeout(function(){
						window.location.href="/Subscribe/applyIndex.jsp";
						},1000);
			},
			complete: function() {
				//请求结束时
				
				//$("#MsgDialog").modal('hide');
			},
			error: function() {
				//请求失败时
				//$("#MsgDialog").modal('show');
			}
		});
	}

	</script>
</body>
</html>