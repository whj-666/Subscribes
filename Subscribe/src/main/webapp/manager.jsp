<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
	<table id="manager"></table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="manager_tool.add();">添加</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit-new" onclick="manager_tool.edit();">修改</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-delete-new" onclick="manager_tool.remove();">删除</a> <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="manager_tool.reload();">刷新</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="manager_tool.redo();">取消选择</a>
		</div>
		<!-- <div style="padding:0 0 0 7px;color:#333;">
			查询账号：<input type="text" name="user" class="textbox">
			<a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="obj.search();">查询</a>
		</div> -->
	</div>
	<!-- 新增窗口 -->
	<form id="manager_add"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			管理帐号：<input type="text" name="manager" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			管理密码：<input type="password" name="password" class="textbox"
				style="width: 150px;">
		</p>
	</form>
	<!-- 修改窗口 -->
	<form id="manager_edit"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			管理员ID：<input type="text" name="id_edit" class="textbox"
				disabled="disabled" style="width: 150px;">
		</p>
		<p>
			管理帐号：<input type="text" name="manager_edit" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			管理密码：<input type="text" name="password_edit" class="textbox"
				style="width: 150px;">
		</p>
	</form>
	<!-- js方式加载 -->
	<script type="text/javascript">
		//时间戳功能
		Date.prototype.format = function(format) {
			var o = {
				"M+" : this.getMonth() + 1, // month  
				"d+" : this.getDate(), // day  
				"h+" : this.getHours(), // hour  
				"m+" : this.getMinutes(), // minute  
				"s+" : this.getSeconds(), // second  
				"q+": Math.floor((this.getMonth()+3 ) / 3), 
				"S" : this.getMilliseconds()
			// millisecond  
			}
			if (/(y+)/.test(format))
				format = format.replace(RegExp.$1, (this.getFullYear() + "")
						.substr(4 - RegExp.$1.length));
			for ( var k in o)
				if (new RegExp("(" + k + ")").test(format))
					format = format.replace(RegExp.$1,
							RegExp.$1.length == 1 ? o[k] : ("00" + o[k])
									.substr(("" + o[k]).length));
			return format;
		}

		function formatDatebox(value) {
			if (value == null || value == '') {
				return '';
			}
			var dt;
			if (value instanceof Date) {
				dt = value;
			} else {
				dt = new Date(value);
			}

			return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
		}
		$(function() {
			//管理员列表
			$("#manager").datagrid({
				fit : true,
				url : "${APP_PATH }/manager/datagrid",
				iconCls : "icon-manager",
				rownumbers : true,
				striped : true,
				columns : [ [ {
					field : 'id',
					title : '编号',
					width : 100,
					checkbox : true,
				}, {
					field : "manager",
					title : '管理员',
					halign : "center",
					width : 100,
				},
				/* {
					field:"password",
					title:'密码',
					halign:"center",
					width:100,
				}, */
				/* {
					field : "createTime",
					title : '创建时间',
					halign : "center",
					width : 200,
					formatter : formatDatebox,
				}, */

				] ],
				toolbar : "#tb",
				pagination : true,
				pageSize : 10,
				pageList : [ 5, 10, 100 ],
			});

			//新增管理
			$("#manager_add")
					.dialog(
							{
								width : 300,
								title : "新增管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#manager_add").form(
														"validate")) {
													$
															.ajax({
																url : "${APP_PATH }/manager/add_manager",
																type : "POST",
																data : {
																	manager : $(
																			'input[name="manager"]')
																			.val(),
																	password : $(
																			'input[name="password"]')
																			.val(),
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在新增中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.message,
																				});
																		$(
																				"#manager_add")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#manager")
																				.datagrid(
																						"reload");
																	} else {
																		$.messager
																				.alert(
																						"新增失败!",
																						"未知错误导致失败,请重试!",
																						"warning");
																	}
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$("#manager_add").dialog(
														"close").form("reset");
											},
										} ],
							});

			//修改管理
			$("#manager_edit")
					.dialog(
							{
								width : 350,
								title : "修改管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#manager_edit").form(
														"validate")) {
													$
															.ajax({
																url : "${APP_PATH }/manager/update_manager",
																type : "POST",
																data : {
																	id : $(
																			"input[name='id_edit']")
																			.val(),
																	manager : $(
																			"input[name='manager_edit']")
																			.val(),
																	password : $(
																			"input[name='password_edit']")
																			.val(),
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在修改中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.message,
																				});
																		$(
																				"#manager_edit")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#manager")
																				.datagrid(
																						"reload");
																	} else {
																		$.messager
																				.alert(
																						"修改失败!",
																						"未知错误或没有任何修改,请重试!",
																						"warning");
																	}
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$('#manager_edit').dialog(
														'close').form('reset');
											},
										} ],
							});

			//管理帐号
			$("input[name='manager']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入管理账号",
				invalidMessage : "管理名称在 2-20 位",
			});

			//管理密码
			$("input[name='password']").validatebox({
				required : true,
				validType : "length[6,30]",
				missingMessage : "请输入管理密码",
				invalidMessage : "管理密码在 6-30 位",
			});

			//修改管理帐号
			$("input[name='manager_edit']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入管理账号",
				invalidMessage : "管理名称在 2-20 位",
			});

			//修改管理密码
			$("input[name='password_edit']").validatebox({
				required : true,
				validType : "length[6,30]",
				missingMessage : "请输入管理密码",
				invalidMessage : "管理密码在 6-30 位",
			});

			manager_tool = {
				reload : function() {
					$("#manager").datagrid("reload");
				},
				redo : function() {
					$("#manager").datagrid("unselectAll");
				},
				add : function() {
					$("#manager_add").dialog("open");
					$("input[name='manager']").focus();
				},
				//更改时获取数据
				edit : function() {
					var rows = $("#manager").datagrid("getSelections");
					if (rows.length > 1) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) {
						$.ajax({
							url : "${APP_PATH }/manager/query_managerbyid",
							type : "POST",
							data : {
								id : rows[0].id,
							},
							beforeSend : function() {
								$.messager.progress({
									text : "正在获取中...",
								});
							},
							success : function(data, response, status) {
								$.messager.progress("close");
								if (data) {
									$('#manager_edit').form('load', {
										id_edit : data[0].id,
										manager_edit : data[0].manager,
										password_edit : data[0].password1,//data[0].password正确的
									}).dialog('open');
								} else {
									$.messager.alert("获取失败!", "未知错误导致失败,请重试!",
											"warning");
								}
							},
						});
					} else if (rows.length == 0) {
						$.messager.alert("警告操作!", "编辑记录只能选定一条数据!", "warning");
					}
				},
				remove : function() {
					var rows = $("#manager").datagrid("getSelections");
					if (rows.length > 0) {
						$.messager.confirm("确定操作", "您正在要删除所选的记录吗?", function(
								flag) {
							if (flag) {
								var ids = [];
								for (var i = 0; i < rows.length; i++) {
									ids.push(rows[i].id);
								}
								//发送删除请求
								$.ajax({
									type : "POST",
									url : "${APP_PATH }/manager/del_manager",
									data : {
										ids : ids.join(","),
									},
									beforeSend : function() {
										$("#manager").datagrid("loading");
									},
									success : function(data) {
										if (data) {
											$("#manager").datagrid("loaded");
											//刷新当前页
											$("#manager").datagrid("load");
											//取消所有行选定
											$("#manager").datagrid(
													"unselectAll");
											$.messager.show({
												title : "提示",
												msg : data + "个用户被删除成功!",
											});
										}
									},
								});
							}
						});
					} else {
						$.messager.alert("提示", "请选择要删除的记录!", "info");
					}
				},
			};
		});
	</script>
</body>
</html>