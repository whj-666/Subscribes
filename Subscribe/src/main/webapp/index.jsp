<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11;IE=10;IE=9;IE=8;" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>index.jsp</title>
<script src="${ctx }/easyui/jquery.min.js"></script>
</head>
<body>
<script type="text/javascript">
		
		window.onload = function() {
			var email = getQueryString("portalUser");
			var type  = getQueryString("type");
			//判断浏览器内核版本
			var a = getBroswer();
			if(a.broswer=="IE"&&a.version=="10.0" || a.version=="9.0" || a.version=="8.0" || a.version=="7.0"){
				location.href = "UpBrowser.jsp";
			}else{
				login(email,type);
			}
			
			
		}
		function getQueryString(name) { 
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
			var r = window.location.search.substr(1).match(reg); 
			if (r != null)
				return unescape(r[2]);
				return null; 
			} 
		
		function getBroswer() {
		    var Sys = {};
		    var ua = navigator.userAgent.toLowerCase();
		    var s; (s = ua.match(/edge\/([\d.]+)/)) ? Sys.edge = s[1] : (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] : (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] : (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] : (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] : (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] : (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;

		    if (Sys.edge) return {
		        broswer: "Edge",
		        version: Sys.edge
		    };
		    if (Sys.ie) return {
		        broswer: "IE",
		        version: Sys.ie
		    };
		    if (Sys.firefox) return {
		        broswer: "Firefox",
		        version: Sys.firefox
		    };
		    if (Sys.chrome) return {
		        broswer: "Chrome",
		        version: Sys.chrome
		    };
		    if (Sys.opera) return {
		        broswer: "Opera",
		        version: Sys.opera
		    };
		    if (Sys.safari) return {
		        broswer: "Safari",
		        version: Sys.safari
		    };

		    return {
		        broswer: "",
		        version: "0"
		    };
		}
		
		
		
		
		function login(email,type){
			
			var loadInfo = '${ctx }' + "/login";
			$.ajax({
				url: loadInfo,
				type: 'GET',
				dataType: 'json',
				cache: false,
				async:false,
				data: {
					subscribe_user_email: email,
					
				},
				beforeSend: LoadFunction, //加载执行方法
				error: erryFunction, //错误执行方法
				success: succFunction //成功执行方法
			})

			function LoadFunction() {
				//加载执行方法
			}

			function erryFunction() {
				//错误执行方法
				window.location.href = "applyIndex.jsp";
				
			}

			function succFunction(JsonString) {
				
				var json = eval(JsonString);
				var emails=json.extend.success;
				var loadInfo = '${ctx }' + "/selectByPersonEmail";
				$.ajax({
					url: loadInfo,
					type: 'GET',
					dataType: 'json',
					cache: false,
					data: {
						email: emails,
					},
					beforeSend: LoadFunction, //加载执行方法
					error: erryFunction, //错误执行方法
					success: succFunction //成功执行方法
				})

				function LoadFunction() {
					//加载执行方法
				}

				function erryFunction() {
					//错误执行方法
					window.location.href = "applyIndex.jsp";
					
				}

				function succFunction(JsonString) {
					var json = eval(JsonString);
					
					if(json.code > 0) {

						var ss = json.extend.success[0];
						window.sessionStorage.setItem("userid", ss.userid);
						window.sessionStorage.setItem("usertruename", ss.usertruename);
						window.sessionStorage.setItem("email", ss.email);
						window.sessionStorage.setItem("mobilephone", ss.mobilephone);
						window.sessionStorage.setItem("userteam", ss.userteam);
						window.sessionStorage.setItem("organizeallname", ss.organizeallname);
						
						
						
						var ok = window.sessionStorage.getItem("usertruename");
						
						
					
						
						$("#username").append(
							"<span class='hiuser glyphicon glyphicon-user'></span><a class='hiuser' id='username' href='#'>" + "欢迎，" + ss.usertruename + "</a>"
						);
						$('#username').show();
						$("#login").hide();
						if (json.extend.success[0].isapplication == "0"){
							setTimeout(function(){
								window.location.href="application-1.jsp";
								},1400);
						}else {
							
							if(type == "1"){
								
								window.location.href = "applyIndex.jsp";
								
							}else if (type == "2"){
								
								location.href = "myRooms.jsp";
								
							}else if(type == "3"){
								
								location.href = "InvitedRecord.jsp";
								
							}else if(type == "4"){
								location.href = "myRooms.jsp";
							}
						}
					} else {
						
						window.location.href = "applyIndex.jsp";

					}

				}
				

			}
			return true;
		}
</script>
</body>
</html>