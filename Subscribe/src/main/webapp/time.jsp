<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" /> -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<meta content="always" name="referrer">

		<link href="${ctx }/css/apply-index.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap-table.css" rel="stylesheet">
		<link href="${ctx }/css/base.css" rel="stylesheet">
		<link href="${ctx }/css/time.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/default.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/normalize.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/pgwmenu.css" rel="stylesheet">

		<script src="${ctx }/easyui/jquery-1.10.2.js"></script>
		<script src="${ctx }/easyui/jquery.min.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap-table.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/time.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/jquery.treemenu.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/pgwmenu.min.js"></script>

		<title>健身预约系统</title>
		<%
			pageContext.setAttribute("APP_PATH", request.getContextPath()); 
		%>

	</head>

	<body>
		<jsp:include page="header.jsp" />
		<div class="main" id="main">
		 
			<!-- <div class="navbar2">
       			<div class="navbar-inner">
          			<ul class="nav topul" id="topul">
            			<li><a href="#" class="allType" onclick="RefreshAll()">所有设备</a></li>
            			<li><a href="#">Menu2</a></li>
            			<li class="dropdown"> <a href="#menu3">Menu3</a>
              				<ul class="dropdown-menu">
                				<li><a href="#menu7">Menu7</a></li>
								<li><a href="#menu8">Menu8</a></li>
              				</ul>
            			</li>
            			<li><a href="#">Menu4</a></li>
            			<li><a href="#">Menu5</a></li>
            			<li><a href="#">Menu6</a></li>
          			</ul>
        		</div>
      		</div> -->
        	<div class="navbar2">
	        	<div class="">
					<ul class="pgwMenu na" id="topul">
						<li><a class="selected" href="" onclick="RefreshAll()">所有设备</a></li>
					</ul>
				</div>
			</div>
      		
			
			<div id="device_Category" class="building-div clearfix">
				<img class="imgshebei" src="${ctx }/upload/img/shebei.png" />
				<a class="target-fix"></a> <span id="classTitle">全部设备</span>
			</div>
			
			<table class="month" id="month" summary="Calendar for 2007.10">
				<caption><span id="date" class="date">看板加载中...</span></caption>
				<tr>
					<th id="thday" class="thday"></th>
					<th scope="col"><span>6:00-7:00</span></th>
					<th scope="col"><span>7:00-8:00</span></th>
					<th scope="col"><span>8:00-9:00</span></th>
					<th scope="col"><span>9:00-10:00</span></th>
					<th scope="col"><span>10:00-11:00</span></th>
					<th scope="col"><span>11:00-12:00</span></th>
					<th scope="col"><span>12:00-13:00</span></th>
					<th scope="col"><span>13:00-14:00</span></th>
					<th scope="col"><span>14:00-15:00</span></th>
					<th scope="col"><span>15:00-16:00</span></th>
					<th scope="col"><span>16:00-17:00</span></th>
					<th scope="col"><span>17:00-18:00</span></th>
					<th scope="col"><span>18:00-19:00</span></th>
					<th scope="col"><span>19:00-20:00</span></th>
				</tr>
				<tr id="Line1">
					<td class="thday" id="1-1"><span>01-01<br> 星期一  </span></td>
					<td class="disactive" id="td-1" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-2" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-3" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-4" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-5" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-6" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-7" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-8" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-9" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-10" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-11" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-12" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-13" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-14" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line2">
					<td class="thday" id="2-1"><span>01-02<br> 星期二  </span></td>
					<td class="disactive" id="td-15" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-16" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-17" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-18" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-19" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-20" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-21" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-22" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-23" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-24" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-25" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-26" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-27" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-28" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line3">
					<td class="thday" id="3-1"><span>01-03<br> 星期三 </span></td>
					<td class="disactive" id="td-29" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-30" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-31" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-32" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-33" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-34" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-35" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-36" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-37" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-38" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-39" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-40" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-41" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-42" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line4">
					<td class="thday" id="4-1"><span>01-04<br> 星期四  </span></td>
					<td class="disactive" id="td-43" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-44" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-45" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-46" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-47" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-48" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-49" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-50" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-51" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-52" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-53" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-54" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-55" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-56" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line5">
					<td class="thday" id="5-1"><span>01-05<br> 星期五  </span></td>
					<td class="disactive" id="td-57" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-58" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-59" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-60" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-61" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-62" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-63" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-64" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-65" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-66" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-67" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-68" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-69" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-70" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line6">
					<td class="thday" id="6-1"><span>01-06<br> 星期六  </span></td>
					<td class="disactive" id="td-71" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-72" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-73" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-74" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-75" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-76" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-77" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-78" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-79" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-80" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-81" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-82" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-83" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-84" title="该时间段还没有预约信息"></td>
				</tr>
				<tr id="Line7">
					<td class="thday" id="7-1"><span>01-07<br> 星期日  </span></td>
					<td class="disactive" id="td-85" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-86" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-87" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-88" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-89" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-90" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-91" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-92" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-93" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-94" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-95" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-96" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-97" title="该时间段还没有预约信息"></td>
					<td class="disactive" id="td-98" title="该时间段还没有预约信息"></td>
				</tr>
			</table>
			
			<!-- 看板预约详情 -->
			<div class="modal fade" id="app-week" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog kanban-dialog" role="document">
					<div class="modal-content"> 
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
							<h4 class="modal-title">预约详情</h4>
						</div>
						<div class="modal-body">
							<table class="table table-striped kanban-dialog-table">
								<tr>
									<th class="th_No">序号</th>
									<th class="th_deviceType">设备</th>
									<th class="th_UserName">预约人</th>
									<th class="th_UserMobile">预约人电话</th>
									<th class="th_UserEmail">预约人邮箱</th>
									<th class="th_BeginTime">预约时间</th>
									<th class="th_Remark">备注</th>
								</tr>
							</table>
							<table class="table table-striped kanban-dialog-table" id="yuyuexiangqing-list">
								
							</table>

						</div>
						<div class="modal-footer">
							<table class="fenye-table">
								<tr>
									<td>
										<div id="barcon" name="barcon"></div>
									</td>
								</tr>
							</table>

							<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			
			<div>
				<img class="loadingimg" id="loadingimg" src="${ctx }/upload/img/loading.gif"/>
			</div>
			
		</div>
		
		<jsp:include page="footer.jsp" />

		<script>
		//页面加载事件
		window.onload = function(){
			loadTopUl();
			loadDeviceInfo();
			$('.pgwMenu').pgwMenu({
				dropDownLabel: '所有设备',
				viewMoreLabel: '更多<span class="icon"></span>'
			});
			}
		</script>

	</body>

</html>