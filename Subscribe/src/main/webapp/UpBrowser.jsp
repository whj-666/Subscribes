<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="content-type" content="text/html;">
		<meta content="always" name="referrer">
		
		<script src="${ctx }/easyui/jquery-1.10.2.js"></script>
		<script src="${ctx }/easyui/jquery.min.js"></script>
		<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
		
		<link href="${ctx }/css/base.css" rel="stylesheet">

<title>升级您的浏览器</title>

<style>
*{margin:0;padding:0;font-family:'Microsoft Yahei';}
.ie-a{color:#eee;font-size:14px;text-decoration:none;}
img{border:0;}
.ie-a p{line-height:35px;}
.ie-1-4-box{width:20%;margin:0;padding:0;text-align:center;float:left;}
</style>

</head>

<body>
<div id="ie-box" style="width:100%;background:url('upload/img/ie-bg.png') repeat-x;margin:0;padding:0 0 0 0;min-height: 635px;min-width: 925px;">
<center>
	<div style="width:814px;background:url('upload/img/bg.png') no-repeat;margin:0 auto;">
		<div style="padding:270px 100px 0 100px;display:inline-block;margin-top: 30px;">
			<div class="ie-1-4-box">
				<a class="ie-a" href="https://www.baidu.com/link?url=9Otxa5RHv5mr_tWtDCgdx4f4VafMiTGkeBQnFHnGD0rzB2gBqd_kOP7Emi-oapkXhUS9go5fRv45mraBpEu24yyAhmlQUywJl0BJIkr8F43&wd=&eqid=d37e4c7f000414480000000358185df6">
					<img src="upload/img/chrome.png" style="width:50%">
					<p>Google Chrome</p>
					<p>谷歌浏览器</p>
				</a>
			</div>
			<div class="ie-1-4-box">
				<a class="ie-a" href="https://www.baidu.com/link?url=e4BMeblS8EUx2-Bxuk6EBeoNGKfLVmw-8WD9lQQQo9xU1O6yV9sGTQ4zktypi0VmPx9QdChTGD4PlL9-CWqLzoaPlwt8E4B8uoF0Ijmj5c7&wd=&eqid=d8dab14200000bce000000045a780aec">
					<img src="upload/img/firefox.png" style="width:50%">
					<p>Mozilla FireFox</p>
					<p>火狐浏览器</p>
				</a>
			</div>
			<div class="ie-1-4-box">
				<a class="ie-a" href="https://www.baidu.com/link?url=_Ds0SEtaVTjbQd7_G7-Fe9NxDNbtprfvMDWyUPobLM98sC-4NpHfGP4mkjJci6ElLLZChloZPiw89XsUZ5ugZzJn-sSOVy_MeGoZBIYy1BS&wd=&eqid=d7c5b61c00008215000000035a715c47">
					<img src="upload/img/opera.png" style="width:50%">
					<p>Opera</p>
					<p>Opera浏览器</p>
				</a>
			</div>
			<div class="ie-1-4-box">
				<a class="ie-a" href="https://www.baidu.com/link?url=aGpQ7Vs_AdS-zKozhlshJZcn_2jVZt2g7UfBjFO6saV1hGMTFqBf-UyfHtsZkrZ8xXsq6RfC7PBqISFNYGcBx872lmDYpgDyCFGYx3d5Qxy&wd=&eqid=ca96d96b000088b8000000035a715c5c">
					<img src="upload/img/safari.png" style="width:50%">
					<p>Apple Safari</p>
					<p>Apple 浏览器</p>
				</a>
			</div>
			<div class="ie-1-4-box">
				<a class="ie-a" href="https://www.baidu.com/link?url=4ReiqFgPUWzn6RV9D9VDsRox4AQpj0vuHzBglkNjEHxhuTKZUUEOWhMpszQy4uknnc0Zybcvc_1cp0OIVAa2znJZIpJ6SccYXVPtELybN_G&wd=&eqid=d71b966100002698000000045a780c26">
					<img src="upload/img/ie.png" style="width:50%">
					<p>IE 11</p>
					<p>IE11 浏览器</p>
				</a>
			</div>
		</div>
		<div style="clear:both"></div>
	</div>
	</center>
</div>
<jsp:include page="footer.jsp" />

<script type="text/javascript">
    window.onload = windowHeight; //页面载入完毕执行函数
    function windowHeight() {
      var h = document.documentElement.clientHeight; //获取当前窗口可视操作区域高度
      var bodyHeight = document.getElementById("ie-box"); //寻找ID为content的对象
      bodyHeight.style.height = (h - 41) + "px"; //你想要自适应高度的对象
    }
</script>

</body>
</html>