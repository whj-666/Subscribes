<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
 <script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script> 

<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>

<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

	

<script type="text/javascript" src="${APP_PATH }/layer/layer.js"></script>  

</head>
<body>

	<table id="device">

	</table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="device_tool.add();">添加和替换</a>  <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="openAddView();">图片上传</a> <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="device_tool.reload();">刷新</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="device_tool.redo();">取消选择</a>
		</div>
		
		<div id="page_nav_area"
			style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
<!-- 新增 -->
	<form id="device_edit"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;" enctype="multipart/form-data">
		<p>
			设&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp备：<input type="text" id="lb"
				name="deviceClass_edit" class="easyui-combobox"
				data-options="url:'${APP_PATH }/device_catagoryService1?r='+Math.random(),
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'equipmentName',  
                                           
                                           
                                         onLoadSuccess: function () { //加载完成后,设置选中第一项  
                                var val = $(this).combobox('getData');  
                                for (var item in val[0]) {  
                                    if (item == 'id') {  
                                        $(this).combobox('select', val[0][item]);  
                                    }  
                                }  
                            }
                                         "
				style="width: 150px;">
		</p>
		<p>
			类别图片&nbsp：<input name="file" id="file"  type="text" class="easyui-filebox" style="width: 150px;">
		</p>
		
		
		
	</form>
	
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		var type;
		function openAddView() {
			layer.open({
				type: 2,
				title: '上传',
				shadeClose: true,
				shade: false,
				maxmin: true, 
				area: ['893px', '600px'],
				content: 'upload/openAddView.do'
			});
		}
		$(function() {
			/* var option1=$('#device').pagination("options");//使用easyUI提供的options方法获得分页选项对象 */
			function getData(pagenum) {
				$.ajax({
					url : "${APP_PATH }/getAllDeviceByFlag?pn=" + pagenum,
					type : "POST",
					dataType : "json",
					cache : false,
					success : function(data) {
						var pageinfo = data.extend.pageInfo;
						build_page_nav(data);
						//加载分页数据
						$("#device").datagrid("loadData", pageinfo.list); //动态取数据
					}
				});
			}
			//解析分页表格
			$("#device").datagrid({
				fit : true,
				iconCls : "icon-manager",
				width : 'auto',
				height : '100',
				rownumbers : true,
				striped : true,
				fitColumns : true,
				columns : [ [ {
					field : 'id',
					title : '编号',
					sortName : 'id', //排序字段
					idField : 'id', //标识字段,主键
					width : 100,
					checkbox : true,
				}, {
					field : "deviceNumber",
					title : '设备编号',
					halign : "center",
					width : 100,
				}, {
					field : "deviceBrand",
					title : '品牌',
					halign : "center",
					width : 100,
				}, {
					field : "deviceType",
					title : '型号类型',
					halign : "center",
					width : 100,
				}, {
					field : "equipmentName",
					title : '类别',
					halign : "center",
					width : 100,
				}, {
					field : "devicePhoto",
					title : '图片',
					halign : "center",
					width : 100,
				}, {
					field : "devicePlace",
					title : '位置描述',
					halign : "center",
					width : 100,
				}, {
					field : "name",
					title : '设备状态',
					halign : "center",
					width : 100,
				}, {
					field : "deviceRemark",
					title : '备注',
					halign : "center",
					width : 100,
				}, ] ],
				toolbar : "#tb"
			});
			getData(1);

			// 分页条
			function build_page_nav(result) {
				// 清空分页条
				$("#page_nav_area").empty();
				// page_nav_area
				var ul = $("<ul></ul>").addClass("pagination");

				var firstPageLi = $("<li></li>").append(
						$("<a></a>").append("首页").attr("href", "#"));
				var prePageLi = $("<li></li>").append(
						$("<a></a>").append("&laquo;"));
				// 判断是否有前一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasPreviousPage == false) {
					firstPageLi.addClass("disabled");
					prePageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件
					firstPageLi.click(function() {
						getData(1);
					});
					prePageLi.click(function() {
						getData(result.extend.pageInfo.pageNum - 1);
					});
				}

				var nextPageLi = $("<li></li>").append(
						$("<a></a>").append("&raquo;"));
				var lastPageLi = $("<li></li>").append(
						$("<a></a>").append("末页").attr("href", "#"));
				// 判断是否有下一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasNextPage == false) {
					nextPageLi.addClass("disabled");
					lastPageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件

					nextPageLi.click(function() {
						getData(result.extend.pageInfo.pageNum + 1);
					});
					lastPageLi.click(function() {
						getData(result.extend.pageInfo.pages);
					});
				}

				// 添加首页和前一页 的提示
				ul.append(firstPageLi).append(prePageLi);
				// 1.2.3.4.5 遍历给ul中添加页码提示
				$.each(result.extend.pageInfo.navigatepageNums, function(index,
						item) {

					var numLi = $("<li></li>")
							.append($("<a></a>").append(item));
					// 显示当前页
					if (result.extend.pageInfo.pageNum == item) {
						numLi.addClass("active");
					}
					// 绑定单机事件
					numLi.click(function() {
						getData(item);
					});
					ul.append(numLi);
				});
				// 添加下一页和末页 的提示
				ul.append(nextPageLi).append(lastPageLi);

				// 把ul加入到nav
				var navEle = $("<nav></nav>").append(ul);
				navEle.appendTo("#page_nav_area");
			}
			
			
			

			$("#device_edit").dialog({
			    width: 300,
			    title: "图片",
			    modal: true,
			    closed: true,
			    iconCls: "icon-user-add",
			    enctype: 'multipart/form-data',
			    processData: false,
			    contentType: false,

			    buttons: [{
			        text: "提交",
			        iconCls: "icon-ok",
			        handler: function() {
			        		if ($("#device_edit").form(
							"validate")) {
			        			//获取设备id
						var Cc = $('#lb').combobox('getValue');
						
						var strFileName = $('input[name="file"]').val();
						//转义符
						var i =strFileName.lastIndexOf('\\');
						var str =strFileName.substring(i+1,strFileName.length);
						
						
						$.ajax({
		                    url: "${APP_PATH }/InsertCategoryImage",
		                    type : "POST",
		                    cache : false,
		                    data:{
		                    	deviceClass : Cc,
		                    	devicePhoto : str,
							},
		               
		                    beforeSend: function() {
		                        $.messager.progress({
		                            text: "正在新增中...",
		                        })
		                    },
		                    success : function(data, response, status){
		                        $.messager.progress("close");
		                        if (data) {
		                            $.messager.show({
		                                title: "提示",
		                                msg: data.msg,
		                            });
		                            $("#device_edit").dialog("close").form("reset");
		                            $("#device").datagrid("reload")
		                        } else {
		                            $.messager.alert("新增失败!", "未知错误导致失败,请重试!", "warning");
		                        }
		                        getData(1);
		                    }

		                });
						
						
					}
			        	
			           

			        }
			    },
			    {
			        text: "取消",
			        iconCls: "icon-redo",
			        handler: function() {
			            $("#device_edit").dialog("close").form("reset");
			        },
			    
			}],
			});
			

			//类别名称
			$("input[name='category']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});
			device_tool = {
				reload : function() {
					//刷新目前解决办法
					getData(1);
				},
				redo : function() {
					$("#device").datagrid("unselectAll");
				},
				add : function() {
					$("#device_edit").dialog("open");
					$("input[name='category']").focus();
				},
				
				

			};
		});
		
		
		
	</script>
</body>
</html>