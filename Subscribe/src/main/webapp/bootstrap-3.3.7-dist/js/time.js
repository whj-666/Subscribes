/*var ipUrl = "http://10.10.4.170:8088/Subscribe/";

var URL_1 = "http://10.10.4.170:8088/Subscribe/";
var URL_img = "http://10.10.4.170:8088/";
*/
var URL_1 = getRootPath();

function RefreshAll(){ 
	window.location.reload();
}
//根据日期 得到是星期几
function getWeekByDay(dayValue){ //dayValue=“2014-01-01”
 var day = new Date(Date.parse(dayValue.replace(/-/g, '/'))); //将日期值格式化 
    var today = new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六"); //创建星期数组
    return today[day.getDay()];  //返一个星期中的某一天，其中0为星期日 
}

//得到某一日期的前一天 后一天 n可以正负数
/*function dateAdd(dd,n){
 var strs= new Array(); 
 strs = dd.split("-");
 var y = strs[0];
 var m = strs[1];
 var d = strs[2];
 var t = new Date(y,m-1,d);
 var str = t.getTime()+n*(1000*60*60*24);
 var newdate = new Date();
 newdate.setTime(str);
 var strYear=newdate.getFullYear();   
    var strDay=newdate.getDate();
    if(strDay < 10){
     strDay = "0"+strDay;
    }   
    var strMonth=newdate.getMonth()+1;   
    if(strMonth < 10){   
        strMonth = "0"+strMonth;   
    }   
    var strdate=strYear+"-"+strMonth+"-"+strDay;   
    return strdate;
}*/

//转换时间戳方法
Date.prototype.format = function(format) {
	var o = {
		"M+": this.getMonth() + 1, // month  
		"d+": this.getDate(), // day  
		"h+": this.getHours(), // hour  
		"m+": this.getMinutes(), // minute  
		"s+": this.getSeconds(), // second  
		/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   */ //没用上所以注释了
		"S": this.getMilliseconds()
		// millisecond  
	}
	if(/(y+)/.test(format))
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
			.substr(4 - RegExp.$1.length));
	for(var k in o)
		if(new RegExp("(" + k + ")").test(format))
			format = format.replace(RegExp.$1,
				RegExp.$1.length == 1 ? o[k] : ("00" + o[k])
				.substr(("" + o[k]).length));
	return format;
}

function formatDatebox(value) {
	if(value == null || value == '') {
		return '';
	}
	var dt;
	if(value instanceof Date) {
		dt = value;
	} else {
		dt = new Date(value);
	}

	return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
}
function formatDatebox_YY_MM_DD(value) {
	if(value == null || value == '') {
		return '';
	}
	var dt;
	if(value instanceof Date) {
		dt = value;
	} else {
		dt = new Date(value);
	}

	return dt.format("yyyy-MM-dd"); //扩展的Date的format方法(上述插件实现)  
}
function formatDatebox_MM_DD(value) {
	if(value == null || value == '') {
		return '';
	}
	var dt;
	if(value instanceof Date) {
		dt = value;
	} else {
		dt = new Date(value);
	}

	return dt.format("MM-dd"); //扩展的Date的format方法(上述插件实现)  
}

//加载图片(加载中)
function loadgif(){
	var table_month = document.getElementById('month');
	var img_loading = document.getElementById('loadingimg');
	table_month.style.display="none";
	img_loading.style.display="block";
	
	setTimeout("setimg()",250);
	setTimeout("settable()",250);
}

function settable(){
	var table_month = document.getElementById('month');
	table_month.style.display="block";
}
function setimg(){
	var img_loading = document.getElementById('loadingimg');
	img_loading.style.display="none";
}


//加载预约详情看板数据
function loadDeviceInfo() {
	myDate = new Date();
	document.getElementById('date').innerHTML = "";
	var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * 0));
	$("#date").append("今天是"+time);

	for(var i=0 ;i<7;i++){
		var selectStartTime = formatDatebox_YY_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
		var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
		var h = (i + 1);
		
		var mydate=new Date();
		var myddy=mydate.getDay();//获取存储当前日期
		var weekday=["星期日","星期一","星期二","星期三","星期四","星期五","星期六","星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
		var biaotou = "<td id='"+h+"-1' class='thday'><span>" +time+ "<br>" +  weekday[myddy+i] + "</span></td>";	
		$("#"+h+"-1").replaceWith(biaotou);
	}
	
	var loadInfo=URL_1 + "selectSeeBoard?r="+Math.random();
	$.ajax({
        url: loadInfo,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{},
        beforeSend: LoadFunction, //加载执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
		//加载执行方法
		loadgif();
    }

    function succFunction(JsonString) {
        json = eval(JsonString);
        var co = 0;
        if(json.code == 1) {
        $.each(json.extend.subscribe, function(i, item) {
        	console.log(i);
        	var j = i + 1;
        	if(item.sfyy == "有") {
        		if((item.ids).length <= 33 * 10){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1;
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='active' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}else if((item.ids).length <= 33 * 20){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1;
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='yellowactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}else if((item.ids).length > 33 * 20){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1;
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='redactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}
        	}else if(item.sfyy == "无") {
        		$("#td-" + j).replaceWith("<td class='noactive' id='td-" + item.id + "' title='该时间段还没有预约信息'></td></tr>");
        	}
        });
    	}
    }
}

//头部菜单加载数据
function loadTopUl() {
	
	var loadInfo=URL_1 + "device_catagoryService";
	$.ajax({
        url: loadInfo,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{flag:1},
        success: succFunction //成功执行方法
    })
    function succFunction(JsonString) {

        json = eval(JsonString);
        $.each(json.extend.device, function(i, item) {
			a = i + 1;
			$("#topul").append(
					"<li onclick='loadDeviceInfoByType(\"" + item.id + "\",\"" + item.equipmentName + "\")' class='dropdown'>" +
						"<a href='#'>"+ item.equipmentName + "</a>" +
							"<ul class='dropdown-menu y_height' id='topul_li_ul" + a + "'></ul>" +
					"</li>"
			);
			loadAllDeviceByClassID(a,item.id);
        });
        $(".pgwMenu li").bind("click",function(){
			$(this).find("ul").slideDown(330,function(){
				$(document).bind("click",function(){
						$(".pgwMenu li").find("ul").slideUp(200,function(){
								$(document).unbind("click");
						});	
				});
		});	
        });
    }
}
	
function loadDeviceInfoByType(id,equipmentName){
	 document.getElementById('classTitle').innerHTML = "全部 " + equipmentName + " 设备信息"; 

     myDate = new Date();
 	document.getElementById('date').innerHTML = "";
 	var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * 0));
 	$("#date").append("今天是"+time);

 	for(var i=0 ;i<7;i++){
 		var selectStartTime = formatDatebox_YY_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
 		var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
 		var h = (i + 1);
 		
 		var mydate=new Date();
 		var myddy=mydate.getDay();//获取存储当前日期
 		var weekday=["星期日","星期一","星期二","星期三","星期四","星期五","星期六","星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
 		var biaotou = "<td id='"+h+"-1' class='thday'><span>" +time+ "<br>" +  weekday[myddy+i] + "</span></td>";	
 		$("#"+h+"-1").replaceWith(biaotou);
 	}
 	
 	var loadInfo=URL_1 + "selectSeeBoard";
 	$.ajax({
         url: loadInfo,
         type: 'GET',
         dataType: 'json',
         cache: false,
         async:false,
         data:{catagoryId:id},
         beforeSend: LoadFunction, //加载执行方法
         success: succFunction //成功执行方法
     })

     function LoadFunction() {
 		//加载执行方法
 		loadgif();
     }

     function succFunction(JsonString) {
         json = eval(JsonString);
         var co = 0;
         if(json.code == 1) {
         $.each(json.extend.subscribe, function(i, item) {
         	var j = i + 1;
         	if(item.sfyy == "有") {
         		if((item.ids).length <= 33 * 5){
 			        var arr ="";
 					arr+=item.ids+",";
 					var al = arr.length - 1;
 					//console.log(arr.length);
 					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='active' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
 		        	//console.log(item.id);
         		}else if((item.ids).length <= 33 * 10){
 			        var arr ="";
 					arr+=item.ids+",";
 					var al = arr.length - 1;
 					//console.log(arr.length);
 					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='yellowactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
 		        	//console.log(item.id);
         		}else if((item.ids).length > 33 * 10){
 			        var arr ="";
 					arr+=item.ids+",";
 					var al = arr.length - 1;
 					//console.log(arr.length);
 					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='redactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
 		        	//console.log(item.id);
         		}
         	}else if(item.sfyy == "无") {
         		$("#td-" + j).replaceWith("<td class='noactive' id='td-" + item.id + "' title='该时间段还没有预约信息'></td></tr>");
         	}
         });
     	}
     }
}
	

//根据设备类别id查出 该类别下所有的设备信息
function loadAllDeviceByClassID(a,classId){

	var loadInfo_1=URL_1 + "getDeviceListByCatagoryId";

	$.ajax({
        url: loadInfo_1,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{
        	CatagoryId:classId
        	},
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
		//加载执行方法
		loadgif();
    }

    function erryFunction() {
    	//错误执行方法
    }

    function succFunction(JsonString) {
        json = eval(JsonString);
        $("#topul_li_ul" + a).html("");
        $.each(json.extend.deviceClass, function(i, item) {
				$("#topul_li_ul" + a).append(
						"<li><a href='javascript:loadDeviceInfoTree(\"" + item.id + "\",\"" + item.deviceType + "\",\"" + item.deviceNumber + "\");'>"+item.deviceBrand+" "+item.deviceNumber+"</a></li>"
				);
				
		});
    	//console.log("呵呵ヘ");
    }
}

//加载设备数据
function loadDeviceInfoTree(devicedId,deviceType,deviceNumber) {
    //console.log(deviceType);
    document.getElementById('classTitle').innerHTML = deviceType + ":" + deviceNumber;

    myDate = new Date();
	document.getElementById('date').innerHTML = "";
	var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * 0));
	$("#date").append("今天是"+time);

	for(var i=0 ;i<7;i++){
		var selectStartTime = formatDatebox_YY_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
		var time= formatDatebox_MM_DD(myDate.getTime()+(1000 * 60 * 60 * 24 * i));
		var h = (i + 1);
		
		var mydate=new Date();
		var myddy=mydate.getDay();//获取存储当前日期
		var weekday=["星期日","星期一","星期二","星期三","星期四","星期五","星期六","星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
		var biaotou = "<td id='"+h+"-1' class='thday'><span>" +time+ "<br>" +  weekday[myddy+i] + "</span></td>";	
		$("#"+h+"-1").replaceWith(biaotou);
	}
	
	var loadInfo=URL_1 + "selectSeeBoard";
	$.ajax({
        url: loadInfo,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{deviceId:devicedId},
        beforeSend: LoadFunction, //加载执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
		//加载执行方法
		loadgif();
    }

    function succFunction(JsonString) {
        json = eval(JsonString);
        var co = 0;
        if(json.code == 1) {
        $.each(json.extend.subscribe, function(i, item) {
        	var j = i + 1;
        	if(item.sfyy == "有") {
        		if((item.ids).length <= 33 * 10){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1; 
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='active' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}else if((item.ids).length <= 33 * 20){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1;
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='yellowactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}else if((item.ids).length > 33 * 20){
			        var arr ="";
					arr+=item.ids+",";
					var al = arr.length - 1;
					//console.log(arr.length);
					$("#td-" + j).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='redactive' id='td-" + item.id + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
		        	//console.log(item.id);
        		}
        	}else if(item.sfyy == "无") {
        		$("#td-" + j).replaceWith("<td class='noactive' id='td-" + item.id + "' title='该时间段还没有预约信息'></td></tr>");
        	}
        });
    	}
    }
}



/* function setTdId(line,h){
		$("#"+line).append("<td class='noactive' id='tdNo" + h + "' title='该时间段还没有预约信息'></td></tr>");
 }
 
 //显示周末看板信息
 function getLineData(url,h,flage,startTime_HH){
	 if(flage == "getAllOrderRecord"){
		 	$.getJSON(url,{random:Math.random()}, function(data) {
		 		var arr ="";
				if(data.code != 0) {				
					$.each(data.extend.success, function(j,item) {
						arr+=item.id+",";
						var al = arr.length - 1;
						$("#tdNo"+h).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='active' id='tdNo" + h + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
						console.log(data);
					});
				} else {
					$.each(data.extend, function(j,item) {
						$("#tdNo"+h).replaceWith("<td class='noactive' id='tdNo" + h + "' title='该时间段还没有预约信息'></td></tr>");
					});
				}
				if(data.code == 0 && startTime_HH==6 || startTime_HH==7 || startTime_HH==17 || startTime_HH==18 || startTime_HH==19) {
					//console.log("哈哈哈哈哈哈哈哈哈哈");
					$("#tdNo"+h).replaceWith("<td class='disactive' id='tdNo" + h + "' title='该时间段无法预约'></td></tr>");
				}
			});
	 }else if(flage == "getOrderRecordByDeviceId"){
		 
		  * 根据设备ID查询所有预约记录
		  * 
		 	$.getJSON(url,{random:Math.random()}, function(data) {
		 		var arr ="";
		 		//console.log(data.extend.getSubscribeByIdAndTime);
		 		
				if(data.code == 1) {	
					console.log(data);
			 		var datalength = (data.extend.getSubscribeByIdAndStatus).length;
			 		console.log(datalength);
			 		//console.log(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeBeginTime);
			 		var starttime = formatDatebox(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeBeginTime);
			 		var endtime = formatDatebox(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeEndTime);
			 		//console.log(starttime);
			 			//if(starttime.toString().substr(11,2) == "13"){
				 			$.each(data.extend.getSubscribeByIdAndStatus, function(j,item) {
								arr+=item.id+","; 
								$("#tdNo"+h).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,arr.length-1) + "\")' class='redactive' id='tdNo" + h + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
							});
			 			//}
				} else {
					$.each(data.extend, function(j,item) {
						$("#tdNo"+h).replaceWith("<td class='noactive' id='tdNo" + h + "' title='该设备还没有预约信息'></td></tr>");
					});
				}
				if(data.code == 0 && startTime_HH==6 || startTime_HH==7 || startTime_HH==17 || startTime_HH==18 || startTime_HH==19) {
					//console.log("哈哈哈哈哈哈哈哈哈哈");
					$("#tdNo"+h).replaceWith("<td class='disactive' id='tdNo" + h + "' title='该时间段无法预约'></td></tr>");
				}

			});
	 }else if(flage == "getOrderRecordByDeviceClassId"){
		 
		  * 根据设备类别ID查询所有预约记录
		  * 
	 }
 }

 //显示工作日看板信息
 function getLineData_Monday_Friday(url,h,flage,startTime_HH){
	 if(flage == "getAllOrderRecord"){
		 	$.getJSON(url,{random:Math.random()}, function(data) {
		 		var arr ="";
				if(data.code == 1) {				
					$.each(data.extend.success, function(j,item) {
						arr+=item.id+",";
						var al = arr.length - 1;
						$("#tdNo"+h).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,al) + "\")' class='active' id='tdNo" + h + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
						console.log(data);
					});
				} else if(data.code == 0) {
						$.each(data.extend, function(j,item) {
							$("#tdNo"+h).replaceWith("<td class='noactive' id='tdNo" + h + "' title='该时间段还没有预约信息'></td></tr>");
						});
				//} else if(8 < startTime_HH < 12 || 13 < startTime_HH < 15 ) {
				}
				if(data.code == 0 && startTime_HH==8 || startTime_HH==9 || startTime_HH==10 || startTime_HH==11 || startTime_HH==13 || startTime_HH==14 || startTime_HH==15 || startTime_HH==16 || startTime_HH==19) {
					//console.log("哈哈哈哈哈哈哈哈哈哈");
					$("#tdNo"+h).replaceWith("<td class='disactive' id='tdNo" + h + "' title='该时间段无法预约'></td></tr>");
				}
			});
	 }else if(flage == "getOrderRecordByDeviceId"){
		 
		  * 根据设备ID查询所有预约记录
		  * 
		 $.getJSON(url,{random:Math.random()}, function(data) {
		 		var arr ="";
		 		//console.log(data.extend.getSubscribeByIdAndTime);
		 		
				if(data.code == 1) {	
					console.log(data);
			 		var datalength = (data.extend.getSubscribeByIdAndStatus).length;
			 		console.log(datalength);
			 		//console.log(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeBeginTime);
			 		var starttime = formatDatebox(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeBeginTime);
			 		var endtime = formatDatebox(data.extend.getSubscribeByIdAndStatus[datalength-1].subscribeEndTime);
			 		//console.log(starttime);
			 			if(starttime.toString().substr(11,2) == "13"){
				 			$.each(data.extend.getSubscribeByIdAndStatus, function(j,item) {
								arr+=item.id+","; 
								$("#tdNo"+h).replaceWith("<td onclick='loadInfo(\"" + arr.substring(0,arr.length-1) + "\")' class='redactive' id='tdNo" + h + "' title='点击查看预约详情'><ul><li class='important'>有预约</li></ul></td></tr>");
							});
			 		}
				} else {
					$.each(data.extend, function(j,item) {
						$("#tdNo"+h).replaceWith("<td class='noactive' id='tdNo" + h + "' title='该设备还没有预约信息'></td></tr>");
					});
				}
				if(data.code != 0 && startTime_HH==8 || startTime_HH==9 || startTime_HH==10 || startTime_HH==11 || startTime_HH==13 || startTime_HH==14 || startTime_HH==15 || startTime_HH==16 || startTime_HH==19) {
					//console.log("哈哈哈哈哈哈哈哈哈哈");
					$("#tdNo"+h).replaceWith("<td class='disactive' id='tdNo" + h + "' title='该时间段无法预约'></td></tr>");
				}

			});
	 }else if(flage == "getOrderRecordByDeviceClassId"){
		 
		  * 根据设备类别ID查询所有预约记录
		  * 
	 }
 }
*/
 
//看板预约详情
function loadInfo(IDString) {
	document.getElementById('yuyuexiangqing-list').innerHTML = "";
	//console.log(IDArray + "-----IDArray ------");
	var IDArray = IDString.split(",");
	console.log(IDArray + "-----IDArray ------");
	for(var j=0;IDArray.length>j;j++){ 
		var loadInfo = URL_1 + "getSubscribeDetailedWithDeviceDataBySubscribeId?subscribe_id=" + IDArray[j];
		getDataByID(loadInfo,j+1);
	}
}
function getDataByID(url,flage){

	$.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{
        	},
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
		//加载执行方法
    }

    function erryFunction() {
    	//错误执行方法
    }

    function succFunction(JsonString) {
        json = eval(JsonString);
      //清空值
		for(var i = 0;i<json.extend.getSubscribeDetailedWithDeviceDataBySubscribeId.length;i++){
			var item= json.extend.getSubscribeDetailedWithDeviceDataBySubscribeId[i];
	
			$("#yuyuexiangqing-list").append(
				"<tr><td class='td_No'><span>" +flage+ "</span></td>" +
				"<td class='td_deviceType'><span>" + item.deviceType + "(" + item.deviceNumber + ")" + "</span></td>" +
				"<td class='td_UserName'><span>" + item.subscribeUserName + "</span></td>" +
				"<td class='td_UserMobile'><span>" + item.subscribeUserMobile + "</span></td>" +
				"<td class='td_UserEmail'><span>" + item.subscribeUserEmail + "</span></td>" +
				"<td class='td_BeginTime'><span>" + fmtDate(item.subscribeBeginTime,item.subscribeEndTime) + "</span></td>" +
				//"<td class='td_EndTime'><span>" + formatDatebox(item.subscribeEndTime) + "</span></td>" +
				"<td class='td_Remark'><span>" + item.subscribeRemark + "</span></td></tr>"
			);
		}
		goPage(1, 10);
    }

	$('#app-week').modal({
		backdrop: false
	})
}
//分页
/**
 * 分页函数
 * pno--页数
 * psize--每页显示记录数
 * 分页部分是从真实数据行开始，因而存在加减某个常数，以确定真正的记录数
 * 纯js分页实质是数据行全部加载，通过是否显示属性完成分页功能
 **/
function goPage(pno, psize) {
	var itable = document.getElementById("yuyuexiangqing-list");
	var num = itable.rows.length; //表格所有行数(所有记录数)
	//console.log(num);
	var totalPage = 0; //总页数
	var pageSize = psize; //每页显示行数
	//总共分几页 
	if(num / pageSize > parseInt(num / pageSize)) {
		totalPage = parseInt(num / pageSize) + 1;
	} else {
		totalPage = parseInt(num / pageSize);
	}
	var currentPage = pno; //当前页数
	var startRow = (currentPage - 1) * pageSize + 1; //开始显示的行  31 
	var endRow = currentPage * pageSize; //结束显示的行   40
	endRow = (endRow > num) ? num : endRow;
	40
	//console.log(endRow);
	//遍历显示数据实现分页
	for(var i = 1; i < (num + 1); i++) {
		var irow = itable.rows[i - 1];
		if(i >= startRow && i <= endRow) {
			irow.style.display = "block";
		} else {
			irow.style.display = "none";
		}
	}
	var finalnum = num;
	var pageEnd = document.getElementById("pageEnd");
	var tempStr = "共&nbsp;" + finalnum + "&nbsp;条记录&nbsp;&nbsp;分&nbsp;" + totalPage + "&nbsp;页&nbsp;&nbsp;当前第&nbsp;" + currentPage + "&nbsp;页&nbsp;";
	if(currentPage > 1) {
		tempStr += "<a href=\"#\" onClick=\"goPage(" + (1) + "," + psize + ")\">&nbsp;首页&nbsp;</a>";
		tempStr += "<a href=\"#\" onClick=\"goPage(" + (currentPage - 1) + "," + psize + ")\">&nbsp;&nbsp;<上一页&nbsp;&nbsp;</a>"
	} else {
		tempStr += "&nbsp;&nbsp;首页&nbsp;&nbsp;";
		tempStr += "&nbsp;&nbsp;<上一页&nbsp;&nbsp;";
	}

	if(currentPage < totalPage) {
		tempStr += "<a href=\"#\" onClick=\"goPage(" + (currentPage + 1) + "," + psize + ")\">&nbsp;&nbsp;下一页>&nbsp;&nbsp;</a>";
		tempStr += "<a href=\"#\" onClick=\"goPage(" + (totalPage) + "," + psize + ")\">&nbsp;&nbsp;尾页&nbsp;&nbsp;</a>";
	} else {
		tempStr += "&nbsp;&nbsp;下一页>&nbsp;&nbsp;";
		tempStr += "&nbsp;&nbsp;尾页&nbsp;&nbsp;";
	}

	document.getElementById("barcon").innerHTML = tempStr;

}

/*毫秒转换成正常时间格式*/
/*只用于预约详情显示*/
/*12月1日 星期五  17:00~18:00*/
function fmtDate(stime, etime) {
	var startTime = new Date(stime);
	var endTime = new Date(etime);
	var weekStr = "星期" + "日一二三四五六".charAt(startTime.getDay());
	//  var y = date.getFullYear();
	var m = startTime.getMonth() + 1;
	m = m < 10 ? ('0' + m) : m;
	var d = startTime.getDate();
	d = d < 10 ? ('0' + d) : d;
	var startHour = startTime.getHours();
	startHour = startHour < 10 ? ('0' + startHour) : startHour;
	var startminute = startTime.getMinutes();
	startminute = startminute < 10 ? ('0' + startminute) : startminute;

	var endHour = endTime.getHours();
	endHour = endHour < 10 ? ('0' + endHour) : endHour;
	var endminute = endTime.getMinutes();
	endminute = endminute < 10 ? ('0' + endminute) : endminute;

	var result = m + '-' + d + ' ' + weekStr + " " + startHour + ':'
			+ startminute + '~' + endHour + ":" + endminute;
	return result;
};

function getRootPath(){
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath=window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName=window.document.location.pathname;
    var pos=curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht=curWwwPath.substring(0,pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return(localhostPaht + "/" +projectName + "/");
}