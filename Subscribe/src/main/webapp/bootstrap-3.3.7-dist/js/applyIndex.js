var departmentArray = [];
var array = [];
var flage = 1;
var defaultData;
var getALLPersonByID = "";
var jsonStr = "";

//var URL_1 = "http://10.10.4.170:8088/Subscribe/";
var URL_1 = getRootPath();

function selectPerson(pid) {
	var result = "";
	var list = URL_1 + "selectOrganizeAllThree";
	var departmentArray = [];
	$.ajax({
		url: list,
		type: 'GET',
		dataType: 'json',
		cache: false,
		async: false,
		data: {},
		beforeSend: LoadFunction, //加载执行方法
		error: erryFunction, //错误执行方法
		success: succFunction //成功执行方法
	})

	function LoadFunction() {
		//加载执行方法

	}

	function erryFunction() {
		//错误执行方法
	}

	function succFunction(JsonString) {
		jsonstring = eval(JsonString);
		// console.log(JSON.stringify(jsonstring.extend.success));
		// console.log(getTrees(jsonstring.extend.success,pid));

		result = getTrees(jsonstring.extend.success, pid);
		defaultData = result;
		loadTree(result);
	}
}

function choicePerson(userTeam) {
	var peopleUrl = URL_1 + "selectByUserTeam";
	var departmentArray = [];
	$.ajax({
		url: peopleUrl,
		type: 'GET',
		dataType: 'json',
		cache: false,
		async: false,
		data: {
			userTeam: userTeam
		},
		beforeSend: LoadFunction, //加载执行方法
		error: erryFunction, //错误执行方法
		success: succFunction //成功执行方法
	})

	function LoadFunction() {
		//加载执行方法

	}

	function erryFunction() {
		//错误执行方法
	}

	function succFunction(JsonString) {
		getALLPersonByID = eval(JsonString);
		//console.log(JSON.stringify(JsonString));
	}
}

$(function() {
	selectPerson(-1);
});

function loadTree(json) {
	//console.log(json);
	$('#treeview-checkable').empty();
	var $checkableTree = $('#treeview-checkable').treeview({
		data: json,
		showIcon: false,
		showCheckbox: false,
		onhoverColor: "#FCFFC6",
		levels: 1,
		//这里是选择元素后的事件
		onNodeChecked: function(event, node) {
			console.log('<p>选择了' + node.text + '</p>');
		},
		//这里是取消选择后的事件
		onNodeUnchecked: function(event, node) {
			console.log('<p>取消了' + node.text + '</p>');
		},
		//这里是展开节点后的事件
		onNodeExpanded: function(event, node) {
			console.log('<p>展开了' + node.text + '</p>');
		},
		//这里是折叠节点后的事件
		onNodeCollapsed: function(event, node) {
			console.log('<p>折叠了' + node.text + '</p>');
		},
		//这里是选中节点后的事件
		onNodeSelected: function(event, node) {
			choicePerson(node.id);

			var Person = getALLPersonByID.extend.success;
			if(getALLPersonByID.code == 1) {
				document.getElementById('checkable-choise').innerHTML = "";
				for(var i = 0; i < Person.length; i++) {
					console.log('<p>' + node.text + '下面有</p>' + Person[i].usertruename);
					$('#checkable-choise').prepend('<a id="choisePerson_a_id' + i + '" class="choisePerson_a" href="javascript:choisePerson(\'' + Person[i].usertruename + '\',\'' + Person[i].userid + '\',\'' + Person[i].email + '\',\'' + Person[i].mobilephone + '\',\'' + Person[i].userteam + '\')" id="personul_a' + i + '" href="#">' + Person[i].usertruename + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-arrow-right">&nbsp;&nbsp;&nbsp;&nbsp;</span></a>');
				}
			} else {
				document.getElementById('checkable-choise').innerHTML = "";
				$('#checkable-choise').prepend("<span>该部门下没有人！</span>");
			}
		}

	});

	/*var search = function(e) {
	    var pattern = $('#input-search').val();
	    var options = {
	      ignoreCase: $('#chk-ignore-case').is(':checked'),
	      exactMatch: $('#chk-exact-match').is(':checked'),
	      revealResults: $('#chk-reveal-results').is(':checked')
	    };
	    var results = $searchableTree.treeview('search', [ pattern, options ]);

	    var output = '<p>' + results.length + ' matches found</p>';
	    $.each(results, function (index, result) {
	      output += '<p>- ' + result.text + '</p>';
	    });
	    $('#search-output').html(output);
	  }

	  $('#btn-search').on('click', search);
	  $('#input-search').on('keyup', search);

	  $('#btn-clear-search').on('click', function (e) {
	    $searchableTree.treeview('clearSearch');
	    $('#input-search').val('');
	    $('#search-output').html('');
	  });


	  var findCheckableNodess = function() {
	    return $checkableTree.treeview('search', [ $('#input-check-node').val(), { ignoreCase: false, exactMatch: false } ]);
	  };
	  var checkableNodes = findCheckableNodess();

	  // Check/uncheck/toggle nodes
	  $('#input-check-node').on('keyup', function (e) {
	    checkableNodes = findCheckableNodess();
	    $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
	  });

	  $('#btn-check-node.check-node').on('click', function (e) {
	    $checkableTree.treeview('checkNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':selected') }]);
	  });

	  $('#btn-uncheck-node.check-node').on('click', function (e) {
	    $checkableTree.treeview('uncheckNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
	  });

	  $('#btn-toggle-checked.check-node').on('click', function (e) {
	    $checkableTree.treeview('toggleNodeChecked', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
	  });

	  // Check/uncheck all
	  $('#btn-check-all').on('click', function (e) {
	    $checkableTree.treeview('checkAll', { silent: $('#chk-check-silent').is(':checked') });
	  });

	  $('#btn-uncheck-all').on('click', function (e) {
	    $checkableTree.treeview('uncheckAll', { silent: $('#chk-check-silent').is(':checked') });
	  });*/
}

function choisePerson(usertruename, userid, email, phone, userteam) {
	var obj = {
		"username": usertruename,
		"userid": userid,
		"email": email,
		"mobile": phone,
		"userteamid": userteam
	}

	//console.log(userid+"------------userid");
	console.log(userid+"------------userid");
	objinArrar(userid, usertruename, email, phone, obj);
	var email = window.sessionStorage.getItem("email");
}

//判断对象是否在数组中
function objinArrar(userid, usertruename, email, phone, obj) {
	var isExisted = false;
	var index = -1;
	var Sessionemail = window.sessionStorage.getItem("email");
	if(obj.email == Sessionemail) {
		alert("你不能添加你自己！");
		return i;
	}else{
		for(var i = 0; i < selectedPersonDataArray.length; i++) {
			console.log(selectedPersonDataArray[i].userid);
			console.log(obj.userid);
			if(selectedPersonDataArray[i].userid == obj.userid) {
				isExisted = true;
				alert("不能重复添加同一人！");
				index = i;
				return i;
			}
		}
	}
	if(!isExisted) {
		selectedPersonDataArray.push(obj);
		$("#checkable-output").append(
			"<p id='p" + userid + "'><span class='glyphicon glyphicon-user'> </span>" + usertruename + "<span onclick='shanchu(\"" + usertruename + "\",\"" + userid + "\",\"" + email + "\",\"" + phone + "\")' class='glyphicon glyphicon-remove'> </span></p>"
		);
		return -1;
	} else {

		return index;
	}
}
/*
function choisePerson(usertruename,userid){
	
	console.log(userid+"------------userid");
	$("#checkable-output").append(
			"<p id='p"+userid+"'>"+usertruename+"&nbsp;&nbsp;<a href='javascript:p_disnone(\""+userid+"\");'><span class='glyphicon glyphicon-remove'></span></a></p>"
	);
}

function p_disnone(userid){

   var p_ui = document.getElementById('p'+userid);
	p_ui.style.display="none";
}*/

/**
 * 树状的算法
 * @params list     代转化数组
 * @params parentId 起始节点
 */
function getTrees(list, parentId) {
	let items = {};

	// 获取每个节点的直属子节点，*记住是直属，不是所有子节点
	for(let i = 0; i < list.length; i++) {
		let key = list[i].pid;
		if(items[key]) {
			items[key].push(list[i]);
		} else {
			items[key] = [];
			items[key].push(list[i]);
		}
	}
	return formatTree(items, parentId);
}

/**
 * 利用递归格式化每个节点
 */
/*function formatTree(items, parentId) {
    let result = [];
    if (!items[parentId]) {
        return result;
    }
    for (let t of items[parentId]) {
        t.nodes = formatTree(items, t.id)
        result.push(t);
    }
   // console.log(result);
  return result;
}*/
//items为全部人员部门数据，parentId默认值为-1
function formatTree(items, parentId) {
	var result = [];
	var t = items[parentId];
	if(!items[parentId]) {
		return result;
	}
	//console.log(items);
	//console.log(eval(items[parentId]));
	for(let i = 0; i < items[parentId].length; i++) {
		items[parentId][i].nodes = formatTree(items, items[parentId][i].id);
		result.push(items[parentId][i])
	}
	//console.log(result);
	return result;
}

//人员搜索加载数据
function loadSearchPerson(trueName) {

	var loadInfo = URL_1 + "selectByPersonTrueName";
	$.ajax({
		url: loadInfo,
		type: 'GET',
		dataType: 'json',
		cache: false,
		async: false,
		data: {
			trueName: trueName
		},
		beforeSend: LoadFunction, //加载执行方法
		error: erryFunction, //错误执行方法
		success: succFunction //成功执行方法
	})

	function LoadFunction() {
		//加载执行方法

	}

	function erryFunction() {
		//错误执行方法
	}

	function succFunction(data) {
		console.log(data);
		document.getElementById('personul').innerHTML = "";
		if(data.code == 1) {
			$.each(data.extend.success, function(i, item) {
				a = i + 1;
				$('#personul').append('<li class="dropdown"><a href="javascript:choisePerson(\'' + item.usertruename + '\',\'' + item.userid + '\',\'' + item.email + '\',\'' + item.mobilephone + '\',\'' + item.userteam + '\')" id="personul_a' + a + '" href="#">' + item.usertruename + ' —— ' + item.organizeallname + '</a></li>');
				//console.log(item.usertruename);
				//loadAllDeviceByClassID(a,item.id);
			});
		} else {
			$("#personul").append(
				"<li class='dropdown'>" +
				"<span>暂无记录</span>" +
				"</li>"
			);
		}
	}
}


function getRootPath(){
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath=window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName=window.document.location.pathname;
    var pos=curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht=curWwwPath.substring(0,pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return(localhostPaht + "/" +projectName + "/");
}