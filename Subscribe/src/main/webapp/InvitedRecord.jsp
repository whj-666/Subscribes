<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="icon" href="../../favicon.ico">-->


<title>预约管理系统</title>

<link href="${ctx }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
	<link href="${ctx }/bootstrap-3.3.7-dist/css/default.css" rel="stylesheet">
		<link href="${ctx }/bootstrap-3.3.7-dist/css/normalize.css" rel="stylesheet">
<script src="${ctx }/easyui/jquery.min.js"></script>
<script src="${ctx }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<link href="${ctx }/css/base.css" rel="stylesheet">
<style type="text/css">
/*按钮样式  */
.btn-warning {
	color: #fff;
	float: right;
	background-color: #f0ad4e;
	border-color: #eea236
}

.btn-warning.focus, .btn-warning:focus {
	color: #fff;
	background-color: #ec971f;
	border-color: #985f0d
}

.btn-warning:hover {
	color: #fff;
	background-color: #ec971f;
	border-color: #d58512
}

.btn-warning.active, .btn-warning:active, .open>.dropdown-toggle.btn-warning
	{
	color: #fff;
	background-color: #ec971f;
	border-color: #d58512
}

.btn-warning.active.focus, .btn-warning.active:focus, .btn-warning.active:hover,
	.btn-warning:active.focus, .btn-warning:active:focus, .btn-warning:active:hover,
	.open>.dropdown-toggle.btn-warning.focus, .open>.dropdown-toggle.btn-warning:focus,
	.open>.dropdown-toggle.btn-warning:hover {
	color: #fff;
	background-color: #d58512;
	border-color: #985f0d
}

.btn-warning.active, .btn-warning:active, .open>.dropdown-toggle.btn-warning
	{
	background-image: none
}

.btn-warning.disabled.focus, .btn-warning.disabled:focus, .btn-warning.disabled:hover,
	.btn-warning[disabled].focus, .btn-warning[disabled]:focus,
	.btn-warning[disabled]:hover, fieldset[disabled] .btn-warning.focus,
	fieldset[disabled] .btn-warning:focus, fieldset[disabled] .btn-warning:hover
	{
	background-color: #f0ad4e;
	border-color: #eea236
}

.btn-warning .badge {
	color: #f0ad4e;
	background-color: #fff
}
</style>
</head>

<body>
	<!-- 使用动态include指令导入头部 -->
	<jsp:include page="header.jsp" />


	<section id="main" class="container">

		<h3>受邀记录</h3>
		<hr>

		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#Pending" data-toggle="tab">待处理<span id="Pending_num"></span></a></li>
			<li><a href="#Accepted" data-toggle="tab">已接受<span id="Accepted_num"></span></a></li>
			<li><a href="#Denied" data-toggle="tab">已拒绝<span id="Denied_num"></span></a></li>
			<li><a href="#NichtAnfechtbar" data-toggle="tab">被撤销<span id="NichtAnfechtbar_num"></span></a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade in active" id="Pending">
				<h3 id ="Pending_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="Accepted">
			<h3 id ="Accepted_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="Denied">
			<h3 id ="Denied_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
			<div class="tab-pane fade in" id="NichtAnfechtbar">
			<h3 id ="NichtAnfechtbar_NoRecord" style="text-align: center; margin-top: 95px;">暂无记录</h3>
			</div>
		</div>


	</section>

	<jsp:include page="footer.jsp" />

	
	
	
	<!-- 预约详情弹出框 -->
	<div class="modal fade" tabindex="-1" role="dialog" id="showOrderDetailDialog_Modal" data-backdrop="false" style="z-index:1;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">预约详情</h4>
				</div>
				<div class="modal-body">
					<span>预约状态：</span><span id="orderState" style="color:red;" ></span>
					<table class="table">
						<tr>
							<td>主约人：</td>
							<td><span id="orderUserName"></span></td>
						</tr>
						<tr>
							<td>预约时间：</td>
							<td><span id="startTime"></td>
						</tr>
						<tr>
							<td>设备名称：</td>
							<td><span id="orderEquipment"></span></td>
						</tr>
						<tr>
							<td>设备序号：</td>
							<td><span id="deviceNumber"></span></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px solid #ddd;">设备位置：</td>
							<td style="border-bottom: 1px solid #ddd;"><span id="equipmentPoint"></span></td>
						</tr>
						<tr id="CauseOfRevocation" style="display:none;">
							<td style="border-top: none;">撤销原因：</td>
							<td style="border-top: none;" colspan="2"><span id="CauseOfRevocationSpan"></span></td>
						</tr>
						<tr>
							<td colspan="2">
							<button type="button" id="ReserveButton" style="display:none; width: 30%;margin-left: auto;margin-right: auto;float: none;" class="btn btn-warning"onclick="ButtonOnClick(2);">确认邀请</button><button type="button" id="StopButton" style="display:none; width: 30%;margin-left: auto;margin-right: auto;float: none;" class="btn btn-warning " onclick="ButtonOnClick(1);">拒绝邀请</button>
							</td>
						</tr>
						
					</table>
					
					<span>邀约的伙伴：</span>
				<table class="table table-striped">
				  <thead>
			        <tr>
			          <th>姓名</th>
			          <th>联系方式</th>
			          <th>状态</th>
			        </tr>
			      </thead>
			      <tbody id="invitePosContent">
			        <!-- <tr>
			          <th>Mark</th>
			          <td></td>
			          <td></td>
			          <td></td>
			        </tr>
			        <tr>
			          <th scope="row">Jacob</th>
			          <td></td>
			          <td></td>
			          <td></td>
			        </tr>
			        <tr>
			          <th scope="row">Jacob</th>
			          <td></td>
			          <td>the Bird</td>
			          <td>@twitter</td>
			        </tr> -->
			      </tbody>
				</table>
				</div>
				<div class="modal-footer">
					<button type="button" id="showOrderDetailModal_QxButton" class="btn btn-default"
						data-dismiss="modal">取消</button>
					<button type="button" id="showOrderDetailModal_QdButton" class="btn btn-primary">确定</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	
	
	
		<!-- 只有关闭弹出框 -->
	<div class="modal fade" tabindex="2" role="dialog" id="showCloseDialog_Modal" data-backdrop="false" style="z-index:3;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showCloseDialog_content"></span>
				</div>
				<div class="modal-footer">
					<button type="button" id="showCloseDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	
	
		<!-- 确认弹出框 -->
	<div class="modal fade" tabindex="0" role="dialog" id="showDialog_Modal" data-backdrop="false" style="z-index:2;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">提示</h4>
				</div>
				<div class="modal-body">
					<span id="showDialog_content"></span>
				</div>
				<div class="modal-footer">
					<button type="button" id="showDialog_QxButton" class="btn btn-default"
						data-dismiss="modal">关闭</button>
					<button type="button" id="showDialog_QdButton" class="btn btn-primary">确定</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
</body>
<script>
$(document).ready(function(){ 
	trace("初始化方法进入"); 
	 userName = window.sessionStorage.getItem("usertruename");
		userEmail = window.sessionStorage.getItem("email");
	/* 	alert(userName+"--------------------------userName")
		alert(userEmail+"--------------------------userEmail") */
	 if(userName!=null&&userEmail!=null){
			getData();
	}else{
		window.location.replace("applyIndex.jsp");
	} 

/* 	person1();
	person2();
	person3(); */
	}); 
function trace(obj){ 
	console.log(obj); 
	} 

function getData(){
	var loadInfo = '${ctx }' + "/selectSubscribeSonByEmailAndName";
	 $.ajax({
	        url: loadInfo,
	        type: 'GET',
	        dataType: 'json',
	        cache: false,
	        async:false,
	        data:{
	        	subscribe_user_email:userEmail,
	        	subscribe_user_name:userName
	        },
	        beforeSend: LoadFunction, //加载执行方法
	        error: erryFunction, //错误执行方法
	        success: succFunction //成功执行方法
	    })
	    function LoadFunction() {
		 //加载执行方法
	    }
	    function erryFunction() {
	    	//错误执行方法
	    }
	    function succFunction(JsonString) {

	        json = eval(JsonString);
	        //console.log(JSON.stringify(json));
	        if (json.code > 0) {
	        	//清空值
	    		
	    		$.each(json.extend.selectByEmailAndName, function(i, item) {
	    			selectBySubscribeSonId(item.id);
	    		});
	        }else{
			
			}
	    }
}

var invitedState;//受邀状态
var result;
var num="0";
var num1="0";
var num2="0";
var num3="0";
//根据获得的受邀信息ID 查询这条受邀信息
function selectBySubscribeSonId(Invite_id){
    var selectBySubscribeSonId_Invite_URL ='${ctx }' +  "/selectBySubscribeSonId"; 
	
    $.ajax({
        url: selectBySubscribeSonId_Invite_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        async:false,
        data:{
          id:Invite_id
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })

    function LoadFunction() {
    	 //加载执行方法
    }

    function erryFunction() {
    	 //错误执行方法
    }

    function succFunction(JsonString) {
        json = eval(JsonString);
      
        if (json.code > 0) {  
        	$.each(json.extend.selectBySubscribeSonId, function(i, item) {
        		//subscribeStatusName = item.subscribeStatusName;
        		invitedState  =  item.subscribeUserStatus;
                result = item.subscribeResult;
                var SubscribeSonId = item.id;
                if(result=="" || result == null){
                	result = "已被主约人撤销";
                }
                $.each(json.extend.selectByPrimaryKeyWithDevice, function(i, item) {
                	subscribeStatusName = item.subscribeStatusName;
                  	if(invitedState ==0)
      				{	$("#Pending_NoRecord").hide();
                  		num++;
                  		if(num > 99 ){
		    				$("#Pending_num").html("99+");
		    			}else{
		    				$("#Pending_num").html("("+num+")");
		    			}
          			/* $("#Pending").append(
          					"<div class='row'><br><div class='panel panel-success'><div class='panel-heading'>"+"待处理："+"</div><div class='panel-body'><p>"
          					+"<label>主约人：</label>"+item.subscribeUserName+"</p><p><label>预约时间：</label>"+fmtDate(item.subscribeBeginTime,item.subscribeEndTime)+"</p><p><label>设备名称：</label>"+item.deviceType
          					+"</p><p><label>设备序号：</label>"+item.deviceNumber+"</p><p><label>设备位置：</label>"+item.devicePlace+"</p><p><button type='button' class='btn btn-warning'id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\",0,\""+SubscribeSonId+"\")'>预约详情</button></p></div></div></div>"
          			); */
          			$("#Pending").append("<div class='plan2'><h3 class='plan-title-End'>"
    	    				+"待处理</h3><ul class='plan-features'><li class='plan-feature'>"
    	    				+"主约人：<span class='plan-feature-name'>"+item.subscribeUserName+"</span>"
    	    				+"</li><li class='plan-feature'>预约时间：<span class='plan-feature-name'>"+fmtDate(item.subscribeBeginTime,item.subscribeEndTime)+"</span>"
    	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.deviceType+"</span>"
    	    				+"</li><li class='plan-feature'>设备序号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
    	    				+"</li><li class='plan-feature'>设备位置：<span class='plan-feature-name'>"+item.devicePlace+"</span>"
    	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
    	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\",0,\""+SubscribeSonId+"\")'>预约详情</a></div>"
    						);
      				}
          			if(invitedState ==1)
      				{$("#Accepted_NoRecord").hide();
      					console.log(item.subscribeStatusName);
	      				num1++;
	              		if(num1 > 99 ){
		    				$("#Accepted_num").html("99+");
		    			}else{
		    				$("#Accepted_num").html("("+num1+")");
		    			}
          				$("#Accepted").append("<div class='plan2'><h3 class='plan-title-Run'>"
        	    				+"已接受("+subscribeStatusName+")</h3><ul class='plan-features'><li class='plan-feature'>"
        	    				+"主约人：<span class='plan-feature-name'>"+item.subscribeUserName+"</span>"
        	    				+"</li><li class='plan-feature'>预约时间：<span class='plan-feature-name'>"+fmtDate(item.subscribeBeginTime,item.subscribeEndTime)+"</span>"
        	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.deviceType+"</span>"
        	    				+"</li><li class='plan-feature'>设备序号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
        	    				+"</li><li class='plan-feature'>设备位置：<span class='plan-feature-name'>"+item.devicePlace+"</span>"
        	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
        	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\",1,\""+SubscribeSonId+"\")'>预约详情</a></div>"
        						);
          				}
          			if(invitedState ==2)
      				{	$("#Denied_NoRecord").hide();
          				//alert("Denied");
	      				num2++;
	              		if(num2 > 99 ){
		    				$("#Denied_num").html("99+");
		    			}else{
		    				$("#Denied_num").html("("+num2+")");
		    			}
          				$("#Denied").append("<div class='plan2'><h3 class='plan-title-Stop'>"
        	    				+"已拒绝</h3><ul class='plan-features'><li class='plan-feature'>"
        	    				+"主约人：<span class='plan-feature-name'>"+item.subscribeUserName+"</span>"
        	    				+"</li><li class='plan-feature'>预约时间：<span class='plan-feature-name'>"+fmtDate(item.subscribeBeginTime,item.subscribeEndTime)+"</span>"
        	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.deviceType+"</span>"
        	    				+"</li><li class='plan-feature'>设备序号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
        	    				+"</li><li class='plan-feature'>设备位置：<span class='plan-feature-name'>"+item.devicePlace+"</span>"
        	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
        	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\",2,\""+SubscribeSonId+"\")'>预约详情</a></div>"
        						);
          				}
          			if(invitedState ==3)
      				{	$("#NichtAnfechtbar_NoRecord").hide();
          				//alert("NichtAnfechtbar");
	      				num3++;
	              		if(num3 > 99 ){
		    				$("#NichtAnfechtbar_num").html("99+");
		    			}else{
		    				$("#NichtAnfechtbar_num").html("("+num3+")");
		    			}
          				$("#NichtAnfechtbar").append("<div class='plan2'><h3 class='plan-title-Revoke'>"
        	    				+"被撤销</h3><ul class='plan-features'><li class='plan-feature'>"
        	    				+"主约人：<span class='plan-feature-name'>"+item.subscribeUserName+"</span>"
        	    				+"</li><li class='plan-feature'>预约时间：<span class='plan-feature-name'>"+fmtDate(item.subscribeBeginTime,item.subscribeEndTime)+"</span>"
        	    				+"</li><li class='plan-feature'>设备名称：<span class='plan-feature-name'>"+item.deviceType+"</span>"
        	    				+"</li><li class='plan-feature'>设备序号：<span class='plan-feature-name'>"+item.deviceNumber+"</span>"
        	    				+"</li><li class='plan-feature'>设备位置：<span class='plan-feature-name'>"+item.devicePlace+"</span>"
        	    				+"</li><li class='plan-feature'>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：<span class='plan-feature-name'>"+item.subscribeRemark+"</span>"
        	    				+"</li></ul><a href='#' class='plan-button' id='orderDetailButton' onclick='OrderDetailButtonOnClick(\""+item.id+"\",3,\""+SubscribeSonId+"\")'>预约详情</a></div>"
        						);
          				}
                  });
    		});
        } else {
            $('#showCloseDialog_content').html(json.msg);
			$('#showCloseDialog_Modal').modal('show');
			$('#showCloseDialog_QxButton').click(function() {
				$('#showCloseDialog_Modal').modal('hide');
			});	 
        }
    }
};
var inviteIDByOrderDetailButtonOnClick;
var nowOrderState;
//查看预约详情按钮
function OrderDetailButtonOnClick(orderID,state,inviteId){
	
	inviteIDByOrderDetailButtonOnClick=inviteId;
	$('#showOrderDetailModal_QxButton').hide();
	$('#showOrderDetailDialog_Modal').modal('show');
	$('#showOrderDetailModal_QdButton').click(function() {
		$('#showOrderDetailDialog_Modal').modal('hide');
	});
	
	 var getSubscribeDetailedWithDeviceDataBySubscribeId_URL ='${ctx }' + "/getSubscribeDetailedWithDeviceDataBySubscribeId"; //
	    
	    $.ajax({
	        url: getSubscribeDetailedWithDeviceDataBySubscribeId_URL,
	        type: 'GET',
	        dataType: 'json', 
	        cache: false,
	        data: {
	            subscribe_id: orderID
	        },
	        beforeSend: LoadFunction, //加载执行方法
	        error: erryFunction, //错误执行方法
	        success: succFunction //成功执行方法
	    })

	    function LoadFunction() {
	    	//加载执行方法
	    }

	    function erryFunction() {
	    	//错误执行方法
	    }

	    function succFunction(JsonString) {
	 
	        json = eval(JsonString);
	       //console.log(JSON.stringify(json));
	        if (json.code > 0) {

	            var dataJson = json.extend.getSubscribeDetailedWithDeviceDataBySubscribeId;
	            var dataJson_huoban =json.extend.subscribeSon;
	            for (var i = 0; i < dataJson.length; i++) {
	                //解析JSON
	                var id = dataJson[i]["id"];
	               
	                var subscribeStatus = dataJson[i]["subscribeStatus"];
	                nowOrderState= subscribeStatus;
	                var subscribeStatusValue = "";
	                
	                var subscribeBeginTime = dataJson[i]["subscribeBeginTime"];
	                var subscribeEndTime = dataJson[i]["subscribeEndTime"];
	                var deviceType = dataJson[i]["device"]["deviceType"];
	                var deviceNumber = dataJson[i]["device"]["deviceNumber"];
	                var devicePlace = dataJson[i]["device"]["devicePlace"];
	                var subscribeUserName= dataJson[i]["subscribeUserName"];
	                var  causeOfRevocation= dataJson[i]["subscribeResult"];
	                var oderfriends;////////////////////////////////////////////////////////////////////////////////
	                var stopButton_obj = document.getElementById("StopButton");  
                    var reserveButton_obj = document.getElementById("ReserveButton");  
	                if (subscribeStatus == 1 || subscribeStatus == 2) {
	                    subscribeStatusValue = "进行中";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    var nowTime = new Date(); //系统当前时间
		                //经过方法转换为毫秒
		                nowTime = fmtMillisecond(nowTime);
		                //后面的数据直接用毫秒
		                var startTime = subscribeBeginTime;
                        var endTime = subscribeEndTime;
		                var stopButton  = document.getElementById("StopButton");  
	                    var reserveButton = document.getElementById("ReserveButton");  
	                  
	                    if(state == 0){
                            stopButton_obj.style.cssText ="display:block; width: 30%;margin-left: auto;margin-right: 15%;float: right;" //显示拒绝邀请按钮
                            	 reserveButton_obj.style.cssText ="display:block; width: 30%;margin-left: 15%;margin-right: auto;float: left;"//显示确认邀请按钮
	                    }else{
	                    	 stopButton_obj.style.cssText ="display:none;"
		                          reserveButton_obj.style.cssText ="display:none;"
	                    }
	                } else if (subscribeStatus == 3) {
	                    subscribeStatusValue = "已结束";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    stopButton_obj.style.cssText ="display:none;"
                        reserveButton_obj.style.cssText ="display:none;"
	                } else if (subscribeStatus == 4 ) {
	                    subscribeStatusValue = "被撤销";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:table-row;"   
	                    stopButton_obj.style.cssText ="display:none;"
                            reserveButton_obj.style.cssText ="display:none;"
	                }else if (subscribeStatus == 5 ) {
	                    subscribeStatusValue = "已终止";
	                    document.getElementById("CauseOfRevocation").style.cssText ="display:none;"
	                    stopButton_obj.style.cssText ="display:none;"
                        reserveButton_obj.style.cssText ="display:none;"
	                }
	            }

	            //渲染数据
	            
	            document.getElementById('orderUserName').innerHTML = subscribeUserName;
	            document.getElementById('orderState').innerHTML = subscribeStatusValue;
	            document.getElementById('startTime').innerHTML = fmtDate(subscribeBeginTime,subscribeEndTime);
	            document.getElementById('orderEquipment').innerHTML = deviceType;
	            document.getElementById('deviceNumber').innerHTML = deviceNumber;
	            document.getElementById('equipmentPoint').innerHTML = devicePlace;
	            document.getElementById("CauseOfRevocationSpan").innerHTML = causeOfRevocation;
	            
	            var htmlString = "";
	            if(dataJson_huoban.length>0){
	                for (var i = 0; i < dataJson_huoban.length; i++) {
	                   var id = dataJson_huoban[i].id;
	                   var subscribeUserStatus = dataJson_huoban[i].subscribeUserStatus;
					   htmlString += "<tr><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserName
								+ "</td><td style='line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserMobile
								+ "</td><td style='color:red;line-height:34px;'>"
								+ dataJson_huoban[i].subscribeUserStatusName
								+ "</td></tr>"
					}
					//alert(htmlString.toString().length);
					if (htmlString.toString().length <= 0) {
						htmlString = "<tr><td colspan='4' style='text-align:center;color:blue;'>无邀约的伙伴</div></td></tr>";
					}

				} else {
					htmlString = "<tr><td colspan='4' style='text-align:center;color:blue;' >无邀约的伙伴</div></td></tr>";
					//document.getElementById('huobanhuoban').innerHTML=htmlString;	
				}
	            document.getElementById('invitePosContent').innerHTML=htmlString;
			}
		}
	}

function ButtonOnClick(flage) {
    if (flage == 2) { //接受按钮
    	$('#showDialog_content').html("是否确认同意此次邀请？");
		/* $('#QxButton').hide(); */
		$('#showDialog_Modal').modal('show');
		$('#showDialog_QdButton').click(function() {
			$('#showDialog_Modal').modal('hide');
			chuliyaoqing(1);
		});
		$('#showDialog_QxButton').click(function() {
			$('#showDialog_Modal').modal('hide');
			
		});

    } else if (flage == 1) { //拒绝按钮
       	$('#showDialog_content').html("是否确认拒绝此次邀请？");
		/* $('#QxButton').hide(); */
		$('#showDialog_Modal').modal('show');
		$('#showDialog_QdButton').click(function() {
			$('#showDialog_Modal').modal('hide');
			chuliyaoqing(2);
		});
		$('#showDialog_QxButton').click(function() {
			$('#showDialog_Modal').modal('hide');
			
		});
    }
}

//接受，拒绝邀请
function chuliyaoqing(flage) {
    var updateSubscribeSonByIdForUserStatus_URL = '${ctx }' + "/updateSubscribeSonByIdForUserStatus";

    $.ajax({
        url: updateSubscribeSonByIdForUserStatus_URL,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: {
            id: inviteIDByOrderDetailButtonOnClick,
            subscribe_user_status: flage,
            subscribe_result:""
        },
        beforeSend: LoadFunction, //加载执行方法
        error: erryFunction, //错误执行方法
        success: succFunction //成功执行方法
    })
     function LoadFunction() {
    	//加载执行方法
    }
     function erryFunction() {
    	//错误执行方法
    }
     function succFunction(JsonString) {
        json = eval(JsonString);
        console.log(JSON.stringify(json));
        if (json.code > 0) {
          if (flage == 1) {
        	  $('#showCloseDialog_content').html("已接受邀请");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
					window.location.reload();
				});	 
          } else if (flage == 2) {
        	    $('#showCloseDialog_content').html("已拒绝邀请");
				$('#showCloseDialog_Modal').modal('show');
				$('#showCloseDialog_QxButton').click(function() {
					$('#showCloseDialog_Modal').modal('hide');
					window.location.reload();
				});	 
          }
        } else {
            alert(json.msg);
        }
    }

}

	//转换时间戳方法
	Date.prototype.format = function(format) {
		var o = {
			"M+" : this.getMonth() + 1, // month  
			"d+" : this.getDate(), // day  
			"h+" : this.getHours(), // hour  
			"m+" : this.getMinutes(), // minute  
			"s+" : this.getSeconds(), // second  
			/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   *///没用上所以注释了
			"S" : this.getMilliseconds()
		// millisecond  
		}
		if (/(y+)/.test(format))
			format = format.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(format))
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
						: ("00" + o[k]).substr(("" + o[k]).length));
		return format;
	}

	function formatDatebox(value) {
		if (value == null || value == '') {
			return '';
		}
		var dt;
		if (value instanceof Date) {
			dt = value;
		} else {
			dt = new Date(value);
		}

		return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
	}
	/*将时间字符串格式转换为毫秒直接与json中的参数进行比较判断*/
	/*用于时间数据校验*/
	function fmtMillisecond(date) {
		//将时间字符串格式转换为毫秒
		var y = date.getFullYear();
		//从日期字符串中获取的月份数据再加1
		var m = date.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		var h = date.getHours();
		h = h < 10 ? ('0' + h) : h;
		var minute = date.getMinutes();
		var second = date.getSeconds();
		var time = y + '/' + m + '/' + d + ' ' + h + ':' + minute + ':'
				+ second;
		var t = new Date(time).getTime();
		return t;
	}
	/*毫秒转换成正常时间格式*/
	/*只用于预约详情显示*/
	/*12月1日 星期五  17:00~18:00*/
	function fmtDate(stime, etime) {
		var startTime = new Date(stime);
		var endTime = new Date(etime);
		var weekStr = "星期" + "日一二三四五六".charAt(startTime.getDay());
		//  var y = date.getFullYear();
		var m = startTime.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = startTime.getDate();
		d = d < 10 ? ('0' + d) : d;
		var startHour = startTime.getHours();
		startHour = startHour < 10 ? ('0' + startHour) : startHour;
		var startminute = startTime.getMinutes();
		startminute = startminute < 10 ? ('0' + startminute) : startminute;

		var endHour = endTime.getHours();
		endHour = endHour < 10 ? ('0' + endHour) : endHour;
		var endminute = endTime.getMinutes();
		endminute = endminute < 10 ? ('0' + endminute) : endminute;

		var result = m + '-' + d + ' ' + weekStr + " " + startHour + ':'
				+ startminute + '~' + endHour + ":" + endminute;
		return result;
	};
</script>
</html>