<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11;IE=10;IE=9;IE=8;" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工健身管理系统后台登录</title> <% pageContext.setAttribute("APP_PATH",
request.getContextPath()); %>
<link rel="stylesheet" type="text/css" href="css/login1.css" />
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css" href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${APP_PATH }/easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${APP_PATH }/css/login.css">
<!-- 引入脚本 -->
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>
<body>
	<div class="login_top_box">
		<img  style="float:left;height:85px;width:auto;" src="images/logo1.png" /> 
		<span style="font-size:40px; font-weight:bold; float:left; line-height:85px;">员工健身管理系统</span>
	</div>
	<div class="login_main_box">
		<div class="login_wrapper">
			<div class="login_form_box">
				<div class="user_name_box">
					<input class="" type="text" name="" pvalue="" id="username"
						value="" />
				</div>
				<div class="password_box">
					<input class="" type="password" name="" pvalue="" id="password"
						value="" />
				</div>
				
				<a href="javascript:" class="login_btn">确定</a>
				
				<p class="form_error"></p>
			</div>
			<div id="wordImg"></div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			
			//管理员账号验证
			$("#username").validatebox({
				required:true,
				missingMessage:"请输入管理员账号",
				invalidMessage:"管理员账号不得为空",
			});
			
			//管理员密码验证
			$("#password").validatebox({
				required:true,
				validType:"length[6,30]",
				missingMessage:"请输入管理员密码",
				invalidMessage:"管理员密码长度为6-30位",
			});
			
			//加载时验证
			if(!$("#username").validatebox("isValid")){
				$("#username").focus();
			}else if(!$("#password").validatebox("isValid")){
				$("#password").focus();
			}
		});
		$(".login_btn").click(function(){
			if(!$("#username").validatebox("isValid")){
				$("#username").focus();
			}else if(!$("#password").validatebox("isValid")){
				$("#password").focus();
			}else{
				$.ajax({
					url:"${APP_PATH }/manager/islogin",
					type:"POST",
					dataType:'json',  
					data:{
						manager:$("#username").val(),
						password:$("#password").val()
					},
					beforeSend : function () {
						$.messager.progress({
							text : '正在登录中...',
						});
					}, 
					success:function(data,response,status){
						$.messager.progress("close"); 
						if(data>0){
							location.href="main.jsp";
						}
					},
					error:function(){
						$.messager.progress("close"); 
						$.messager.alert("登录失败", "用户名或密码错误", "warning", function () {
							$("#password").select();
						});
					}  
				});
			}
		});
	</script>
</head>


</html>
