<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml"><head><title>
	浏览器兼容性提示
</title>

	<style type="text/css">
		body{
			text-align: center;
			line-height: 400%;
			margin-top: 10%;
			background-color: #F5F5F5;
		}
		ul li{
			list-style: none;
		}
		strong.red
		{
			color: Red;
		}
	</style>
</head>
<body>
	<form method="post" action="Subscribe/applyIndex.jsp" id="frmBrowserFailNotify">
<div class="aspNetHidden">
<input name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJOTkwMTE2ODY3ZGQpMuo7vAill/xxKGybQrliY6aKs/LbJUeLGt6iIZ+jSQ==" type="hidden">
</div>

<div class="aspNetHidden">

	<input name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E970BCEE" type="hidden">
	<input name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWAwKs3NG6DAKYm8bUBwLqmKmKDet9PY9RfLWFzXYk2svl3/IqfCKCYALLs9ErhakPD+RR" type="hidden">
</div>
	<div>
		<p>
			<strong class="red">提示</strong>: 您在使用低版本的IE浏览器，使用不兼容的浏览器可能导致浏览异常。
		</p>
		<strong>解决方法</strong>（任选其一）：
		<ul>
			<li>升级至<a href="http://windows.microsoft.com/zh-CN/internet-explorer/downloads/ie-9" target="_blank">IE9</a>或更高版本(如<a href="http://windows.microsoft.com/zh-CN/internet-explorer/downloads/ie" target="_blank">IE10</a>)；</li>
			<li>安装<a href="http://www.google.cn/chrome/intl/zh-CN/landing_chrome.html" target="_blank">Chrome</a>(谷歌浏览器）或<a href="http://firefox.com.cn/download/" target="_blank">Firefox</a>（火狐）浏览器来使用本系统；</li>
			
		</ul>
		<p>
			</p><hr>
			<!-- 我已了解浏览器兼容性问题。<input name="btnContinue" value="仍然继续 »" id="btnContinue" type="submit"> -->
			
		<p></p>
	</div>
	</form>


</body>
</html>
