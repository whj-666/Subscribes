<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	response.setHeader("Cache-Control","no-store"); 
	response.setHeader("Pragrma","no-cache"); 
	response.setDateHeader("Expires",0);
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>
<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>

<body>
	<table id="subscribe"></table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="subscribe_tool.add();">添加</a> <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit-new" onclick="subscribe_tool.edit();">撤销</a><a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit-new" onclick="subscribe_tool.stop();">终止</a> <!-- <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-delete-new" onclick="subscribe_tool.remove();">删除</a> -->
			<a href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="subscribe_tool.reload();">刷新</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="subscribe_tool.redo();">取消选择</a>
				<a href="#"
				class="easyui-linkbutton" iconCls="icon-print" plain="true"
				onclick="print();">导出</a>
		</div>
		<div style="padding: 0 0 0 7px; color: #333;">
			邮箱： <input type="text" id="usercode" name="usercode" class="textbox">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp姓名：
			<input type="text" id="username" name="username" class="textbox">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			<a class="easyui-linkbutton" iconCls="icon-search"
				onclick="selectbutton(1);">查询</a>
		</div>
		<div id="page_nav_area"
			style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
	</div>
	<!-- 新增窗口 -->
	<form id="subscribe_add"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			员工邮箱&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp： <input type="text"
				name="subscribeUserCode" class="textbox" style="width: 150px;">
		</p>
		<%-- <p>
			设备类别&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp：
			<!-- <input type="text" id="lb" name="subscribeDeviceCategory" class="combobox" style="width: 150px;"> -->
			<input type="text" id="lb" name="subscribeDeviceCategory"
				class="easyui-combobox"
				data-options="url:'${APP_PATH }/device_catagoryService1',
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'equipmentName',  
                                           onSelect:function(){  
                                           QueryCourseByCollege();  
                                         },
                                         onLoadSuccess: function () { //加载完成后,设置选中第一项  
                                var val = $(this).combobox('getData');  
                                for (var item in val[0]) {  
                                    if (item == 'id') {  
                                        $(this).combobox('select', val[0][item]);  
                                    }  
                                }  
                            }
                                         "
				style="width: 150px;"> --%>

			<%-- <select  class="easyui-combobox" id="college" name="college"   
                            data-options="url:'${APP_PATH }/device_catagoryService1',  
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'equipmentName',  
                                           onSelect:function(){  
                                           QueryCourseByCollege();  
                                         } 
                                         
                                          
                            "  
        style="width: 150px; margin-right: 100px; font-size: 16px;"> --%>
		</p>
		<p>
			设备编号&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp： <input type="text"
				id="deviceID" name="subscribeDeviceId" class="easyui-combobox"
				data-options="url:'${APP_PATH }/getAllDeviceByFlag1?r='+Math.random(),
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'deviceNumber',  
                                           onSelect:function(){  
                                           
                                         },
                                         onLoadSuccess: function () { //加载完成后,设置选中第一项  
                                var val = $(this).combobox('getData');  
                                for (var item in val[0]) {  
                                    if (item == 'id') {  
                                        $(this).combobox('select', val[0][item]);  
                                    }  
                                }  
                            }"
				style="width: 150px; float: 20px;">
		</p>
		<p>
			开始时间&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp： <input type="text"
				name="subscribeBeginTime" class="easyui-datetimebox"
				style="width: 150px;">
		</p>
		<p>
			结束时间&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp： <input type="text"
				name="subscribeEndTime" class="easyui-datetimebox"
				style="width: 150px;">
		</p>
		<p>
			预约人&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp： <input
				type="text" name="subscribeUserName" class="textbox"
				style="width: 150px;">
		</p>
		<p>
			预约人手机号&nbsp： <input type="text" name="subscribeUserMobile"
				class="textbox" style="width: 150px;">
		</p>
		<p>
			备注&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp：
			<input type="text" name="subscribeRemark" class="textbox"
				style="width: 150px;">
		</p>

	</form>
	<!-- 撤销原因窗口 -->
	<form id="subscribe_edit"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			撤销原因： <input type="text" name="subscribe_result" class="textbox"
				style="width: 150px;">
		</p>
	</form>
	<!-- 撤销原因窗口 -->
	<form id="subscribe_stop"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;">
		<p>
			终止原因： <input type="text" name="subscribe_results" class="textbox"
				style="width: 150px;">
		</p>
	</form>
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		/* var CategoryId; */
		//时间戳功能
		Date.prototype.format = function(format) {
			var o = {
				"M+" : this.getMonth() + 1, // month  
				"d+" : this.getDate(), // day  
				"h+" : this.getHours(), // hour  
				"m+" : this.getMinutes(), // minute  
				"s+" : this.getSeconds(), // second  
				/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   *///没用上所以注释了
				"S" : this.getMilliseconds()
			// millisecond  
			}
			if (/(y+)/.test(format))
				format = format.replace(RegExp.$1, (this.getFullYear() + "")
						.substr(4 - RegExp.$1.length));
			for ( var k in o)
				if (new RegExp("(" + k + ")").test(format))
					format = format.replace(RegExp.$1,
							RegExp.$1.length == 1 ? o[k] : ("00" + o[k])
									.substr(("" + o[k]).length));
			return format;
		}

		function formatDatebox(value) {
			if (value == null || value == '') {
				return '';
			}
			var dt;
			if (value instanceof Date) {
				dt = value;
			} else {
				dt = new Date(value);
			}

			return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
		}
		//条件查询
		function selectbutton(pagenum) {
			/* $('#subscribe').datagrid('load', {
				subscribecn_code : $('input[name="usercode"]').val(),
				subscribecn_name : $('input[name="username"]').val(),
			}); */
			
			{
	        $.ajax({
	            url: "${APP_PATH }/getSubscribeByEmailOrNameOrBTimeOrETime?pn=" + pagenum,
	            type: "GET",
	            cache: false,
	            data: {
	            	subscribe_user_email: $('input[name="usercode"]').val(),
	            	subscribe_user_name: $('input[name="username"]').val(),
					a:Math.random(),

	            },
	            
	         
	            beforeSend: function () {
	            	
	                    $.messager.progress({
	                        text: "正在查询中...",
	                    });
	                    
	                },
	                success: function (data, response, status) {
	                    $.messager.progress("close");
	                    
	                    if (data) {
	                        $.messager.show({
	                            title: "提示",
	                            msg: data.msg,
	                        });
	                       
							//加载分页数据
							$("#subscribe").datagrid("loadData", data.extend.getSubscribeByEmailOrNameOrBTimeOrETime); //动态取数据

	                      //分页表格
			$("#subscribe").datagrid({
				fit : true,
				iconCls : "icon-manager",
				rownumbers : true,
				striped : true,
				fitColumns :true,
				columns : [ [ {
					field : 'id',
					title : 'id',
					width : 100,
					checkbox : true,
				}, {
					field : "subscribeUserEmail",
					title : '邮箱',
					halign : "center",
					width : 100,
				}, {
					field : "subscribeUserName",
					title : '预约人',
					halign : "center",
					width : 100,
				}, {
					field : "subscribeUserMobile",
					title : '预约人手机号',
					halign : "center",
					width : 100,
				}, {
					field : "deviceNumber",
					title : '设备编号',
					halign : "center",
					width : 100,
				}, {
					field : "name",
					title : '类别名称',
					halign : "center",
					width : 100,
				}, {
					field : "subscribeStatusName",
					title : '预约状态',
					halign : "center",
					width : 100,
				}, {
					field : "subscribeRemark",
					title : '备注',
					halign : "center",
					width : 100,
				}, {
					field : "subscribeBeginTime",
					title : '开始时间',
					halign : "center",
					width : 100,
					formatter : formatDatebox,
				}, {
					field : "subscribeEndTime",
					title : '结束时间',
					halign : "center",
					width : 100,
					formatter : formatDatebox,
				}, ] ],

				 toolbar : "#tb", 
			});
	                      	
	            			
	            			
	                    } else {
	                        $.messager.alert("查询失败!", "未知错误导致失败,请重试!", "warning");
	                    }
	                }
	        });
	    } 
		}

		
		$(function() { 
			getData(1);
		 });
		function getData(pagenum) {
			$.ajax({
				url : "${APP_PATH }/getSubscribeAllByFlag?pn=" + pagenum+"&r="+Math.random(),
				type : "GET",
				dataType : "json",
				cache: false,
				success : function(data) {
					var pageinfo = data.extend.pageInfo;
					build_page_nav(data);
					//加载分页数据
					$("#subscribe").datagrid("loadData", pageinfo.list); //动态取数据
				}
			});
		}

		//分页表格
		$("#subscribe").datagrid({
			fit : true,
			iconCls : "icon-manager",
			rownumbers : true,
			striped : true,
			fitColumns :true,
			columns : [ [ {
				field : 'id',
				title : 'id',
				width : 100,
				checkbox : true,
			}, {
				field : "subscribeUserEmail",
				title : '邮箱',
				halign : "center",
				width : 100,
			}, {
				field : "subscribeUserName",
				title : '预约人',
				halign : "center",
				width : 100,
			}, {
				field : "subscribeUserMobile",
				title : '预约人手机号',
				halign : "center",
				width : 100,
			}, {
				field : "deviceNumber",
				title : '设备编号',
				halign : "center",
				width : 100,
			}, {
				field : "name",
				title : '类别名称',
				halign : "center",
				width : 100,
			}, {
				field : "subscribeStatusName",
				title : '预约状态',
				halign : "center",
				width : 100,
			}, {
				field : "subscribeRemark",
				title : '备注',
				halign : "center",
				width : 100,
			}, {
				field : "subscribeBeginTime",
				title : '开始时间',
				halign : "center",
				width : 100,
				formatter : formatDatebox,
			}, {
				field : "subscribeEndTime",
				title : '结束时间',
				halign : "center",
				width : 100,
				formatter : formatDatebox,
			}, ] ],

			 toolbar : "#tb", 
		});
			// 分页条
			function build_page_nav(result) {
				// 清空分页条
				$("#page_nav_area").empty();
				// page_nav_area
				var ul = $("<ul></ul>").addClass("pagination");

				var firstPageLi = $("<li></li>").append(
						$("<a></a>").append("首页").attr("href", "#"));
				var prePageLi = $("<li></li>").append(
						$("<a></a>").append("&laquo;"));
				// 判断是否有前一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasPreviousPage == false) {
					firstPageLi.addClass("disabled");
					prePageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件
					firstPageLi.click(function() {
						getData(1);
					});
					prePageLi.click(function() {
						getData(result.extend.pageInfo[0].pageNum - 1);
					});
				}

				var nextPageLi = $("<li></li>").append(
						$("<a></a>").append("&raquo;"));
				var lastPageLi = $("<li></li>").append(
						$("<a></a>").append("末页").attr("href", "#"));
				// 判断是否有下一页，没有，就不能点击《disabled》
				if (result.extend.pageInfo.hasNextPage == false) {
					nextPageLi.addClass("disabled");
					lastPageLi.addClass("disabled");
				} else {
					// 为元素添加点击翻页事件

					nextPageLi.click(function() {
						getData(result.extend.pageInfo.pageNum + 1);
					});
					lastPageLi.click(function() {
						getData(result.extend.pageInfo.pages);
					});
				}

				// 添加首页和前一页 的提示
				ul.append(firstPageLi).append(prePageLi);
				// 1.2.3.4.5 遍历给ul中添加页码提示
				$.each(result.extend.pageInfo.navigatepageNums, function(index,
						item) {

					var numLi = $("<li></li>")
							.append($("<a></a>").append(item));
					// 显示当前页
					if (result.extend.pageInfo.pageNum == item) {
						numLi.addClass("active");
					}
					// 绑定单机事件
					numLi.click(function() {
						getData(item);
					});
					ul.append(numLi);
				});
				// 添加下一页和末页 的提示
				ul.append(nextPageLi).append(lastPageLi);

				// 把ul加入到nav
				var navEle = $("<nav></nav>").append(ul);
				navEle.appendTo("#page_nav_area");
			}
		
		
		//导出预约信息
		function print(){
			self.location.href="${APP_PATH }/excelSubscribeCountData";
		}
		
			

			$("#subscribe_add")
					.dialog(
							{
								width : 300,
								title : "新增管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#subscribe_add").form(
														"validate")) {
													
													var deviced = $('#deviceID').combobox('getValue');
													
													$
															.ajax({
																url : "${APP_PATH }/insertSubscribe",
																type : "GET",
																cache: false,
																data : {
																	
																	subscribe_user_email : $(
																			'input[name="subscribeUserCode"]')
																			.val(),
																	subscribe_device_id : deviced,
																	subscribe_begin_time : $(
																			'input[name="subscribeBeginTime"]')
																			.val(),
																	subscribe_end_time : $(
																			'input[name="subscribeEndTime"]')
																			.val(),
																	
																	subscribe_user_mobile : $(
																			'input[name="subscribeUserMobile"]')
																			.val(),
																	subscribe_remark : $(
																			'input[name="subscribeRemark"]')
																			.val(),
																	subscribe_sons : "",
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在新增中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	
																	if (data.code==1) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$(
																				"#subscribe_add")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#subscribe")
																				.datagrid(
																						"reload");
																		subscribe_tool.reload();
																	} else {
																		$.messager.show({
																			title : "提示",
																			msg : data.extend.failMsg,
																		});
																	}
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$("#subscribe_add").dialog(
														"close").form("reset");
											},
										} ],
							});

			//撤销管理
			$("#subscribe_edit")
					.dialog(
							{
								width : 350,
								title : "撤销管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#subscribe_edit").form(
														"validate")) {
													$
															.ajax({
																url : "${APP_PATH }/updateSubscribeStatusById",
																type : "GET",
																cache: false,
																data : {
																	subscribe_id : id1,
																	subscribe_status : 4,
																	subscribe_result : $(
																			"input[name='subscribe_result']")
																			.val(),
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在修改中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$(
																				"#subscribe_edit")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#subscribe")
																				.datagrid(
																						"reload");
																		subscribe_tool.reload();
																	} else {
																		$.messager
																				.alert(
																						"修改失败!",
																						"未知错误或没有任何修改,请重试!",
																						"warning");
																	}
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$('#subscribe_edit').dialog(
														'close').form('reset');
											},
										} ],
							});
			//终止管理
			$("#subscribe_stop")
					.dialog(
							{
								width : 350,
								title : "终止管理",
								modal : true,
								closed : true,
								iconCls : "icon-user-add",
								buttons : [
										{
											text : "提交",
											iconCls : "icon-ok",
											handler : function() {
												if ($("#subscribe_stop").form(
														"validate")) {
													$
															.ajax({
																url : "${APP_PATH }/updateSubscribeStatusById",
																type : "GET",
																cache: false,
																data : {
																	subscribe_id : id1,
																	subscribe_status : 5,
																	subscribe_result : $(
																			"input[name='subscribe_results']")
																			.val(),
																},
																beforeSend : function() {
																	$.messager
																			.progress({
																				text : "正在修改中...",
																			});
																},
																success : function(
																		data,
																		response,
																		status) {
																	$.messager
																			.progress("close");
																	if (data) {
																		$.messager
																				.show({
																					title : "提示",
																					msg : data.msg,
																				});
																		$(
																				"#subscribe_stop")
																				.dialog(
																						"close")
																				.form(
																						"reset");
																		$(
																				"#subscribe")
																				.datagrid(
																						"reload");
																		subscribe_tool.reload();
																	} else {
																		$.messager
																				.alert(
																						"修改失败!",
																						"未知错误或没有任何修改,请重试!",
																						"warning");
																	}
																}
															});
												}
											},
										},
										{
											text : "取消",
											iconCls : "icon-redo",
											handler : function() {
												$('#subscribe_stop').dialog(
														'close').form('reset');
											},
										} ],
							});

			//类别名称
			$("input[name='category']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});
			//备注
			$("input[name='subscribeRemark']").validatebox({
				required : false,
				validType : "length[0,30]",
				missingMessage : "请输入备注，限制30字以内",
				invalidMessage : "请输入备注，限制30字以内",
			});
			
			subscribe_tool = {
				reload : function() {
					$("#subscribe").datagrid("reload");
					//刷新目前解决办法
					getData(1);
					
				},
				redo : function() {
					$("#subscribe").datagrid("unselectAll");
				},
				add : function() {
					$("#subscribe_add").dialog("open");
					$("input[name='category']").focus();
				},
				edit : function() {
					var rows = $("#subscribe").datagrid("getSelections");
					if (rows.length > 1) {
						$.messager.alert("警告操作!", "撤销记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) {
						$
								.ajax({
									url : "${APP_PATH }/getSubscribeDetailedWithDeviceDataBySubscribeId",
									type : "GET",
									cache: false,
									data : {
										subscribe_id : rows[0].id,
									},
									beforeSend : function() {
										$.messager.progress({
											text : "正在获取中...",
										});
									},
									success : function(data, response, status) {
										$.messager.progress("close");
										if (data) {
											id1 = rows[0].id;

											$('#subscribe_edit').form('load', {

												subscribe_result : "",
											}).dialog('open');
										} else {
											$.messager.alert("获取失败!",
													"未知错误导致失败,请重试!", "warning");
										}
									},
								});
					} else if (rows.length == 0) {
						$.messager.alert("警告操作!", "撤销记录只能选定一条数据!", "warning");
					}
				},
				stop : function() {
					var rows = $("#subscribe").datagrid("getSelections");
					if (rows.length > 1) {
						$.messager.alert("警告操作!", "终止记录只能选定一条数据!", "warning");
					} else if (rows.length == 1) {
						$
								.ajax({
									url : "${APP_PATH }/getSubscribeDetailedWithDeviceDataBySubscribeId",
									type : "GET",
									cache: false,
									data : {
										subscribe_id : rows[0].id,
									},
									beforeSend : function() {
										$.messager.progress({
											text : "正在获取中...",
										});
									},
									success : function(data, response, status) {
										$.messager.progress("close");
										if (data) {
											id1 = rows[0].id;

											$('#subscribe_stop').form('load', {

												subscribe_result : "",
											}).dialog('open');
										} else {
											$.messager.alert("获取失败!",
													"未知错误导致失败,请重试!", "warning");
										}
									},
								});
					} else if (rows.length == 0) {
						$.messager.alert("警告操作!", "终止记录只能选定一条数据!", "warning");
					}
				},
				remove : function() {
					var rows = $("#subscribe").datagrid("getSelections");
					var temID="";
		            for (i = 0; i < rows.length;i++) {
		                if (temID =="") {
		                    temID = rows[i].id;
		                } else {
		                    temID = rows[i].id + "," + temID;
		                }               
		            }
					if (rows.length == 0) {
						$.messager.alert("警告操作!", "请选择删除数据!", "warning");
					} else{
						$.ajax({
							type : "POST",
							url : "${APP_PATH }/updateSubscribeFlagById",
							cache: false,
							data : {
								subscribe_id : temID,
								subscribe_flag : "0",
								subscribe_result : ""
							},

							beforeSend : function() {
								$("#subscribe").datagrid("loading");
							},
							success : function(data) {
								if (data) {
									$("#subscribe").datagrid("loaded");
									//刷新当前页
									$("#subscribe").datagrid("load");
									//取消所有行选定
									$("#subscribe").datagrid("unselectAll");
									$.messager.show({
										title : "提示",
										msg : data.msg,
									});
									subscribe_tool.reload();
								} else {
									$.messager.alert("获取失败!", "未知错误导致失败,请重试!",
											"warning");
								}
							},
						});
					}
				},

			};
		
		
	</script>
</body>

</html>