<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
<script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>
<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>

<body>
	<table id="subscribe"></table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
		</div>
		<div style="padding: 0 0 0 7px; color: #333;">
			<!-- 邮箱： <input type="text" id="usercode" name="usercode" class="textbox">
			<a class="easyui-linkbutton" iconCls="icon-search"
				onclick="selectbutton();">查询</a> -->
		</div>
		<div id="page_nav_area"
			style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
	</div>
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		/* var CategoryId; */
		//时间戳功能
		Date.prototype.format = function(format) {
			var o = {
				"M+" : this.getMonth() + 1, // month  
				"d+" : this.getDate(), // day  
				"h+" : this.getHours(), // hour  
				"m+" : this.getMinutes(), // minute  
				"s+" : this.getSeconds(), // second  
				/* "q+": Math.floor((this.getMonth()+3 ) / 3), // quarter   *///没用上所以注释了
				"S" : this.getMilliseconds()
			// millisecond  
			}
			if (/(y+)/.test(format))
				format = format.replace(RegExp.$1, (this.getFullYear() + "")
						.substr(4 - RegExp.$1.length));
			for ( var k in o)
				if (new RegExp("(" + k + ")").test(format))
					format = format.replace(RegExp.$1,
							RegExp.$1.length == 1 ? o[k] : ("00" + o[k])
									.substr(("" + o[k]).length));
			return format;
		}

		function formatDatebox(value) {
			if (value == null || value == '') {
				return '';
			}
			var dt;
			if (value instanceof Date) {
				dt = value;
			} else {
				dt = new Date(value);
			}

			return dt.format("yyyy-MM-dd hh:mm"); //扩展的Date的format方法(上述插件实现)  
		}
		
		/* 
		//查询数据
		$(function() { */
			function getData() {
				$.ajax({
					url : "${APP_PATH }/selectApplicationAll",
					type : "GET",
					dataType : "json",
					success : function(data) {
						var pageinfo = data.extend;
						/* build_page_nav(data); */
						//加载分页数据
						$("#subscribe").datagrid("loadData", pageinfo.success); //动态取数据
					}
				});
			}
		
		//分页表格
		$("#subscribe").datagrid({
			fit : true,
			iconCls : "icon-manager",
			rownumbers : true,
			striped : true,
			fitColumns :true,
			columns : [ [ {
				field : 'id',
				title : 'id',
				width : 100,
				checkbox : true,
			}, {
				field : "email",
				title : '邮箱',
				halign : "center",
				width : 100,
			}, {
				field : "name",
				title : '申请人',
				halign : "center",
				width : 100,
			}, {
				field : "phone",
				title : '申请人手机号',
				halign : "center",
				width : 100,
			}, {
				field : "createtime",
				title : '申请时间',
				halign : "center",
				width : 100,
				formatter : formatDatebox,
			}, ] ],

			 toolbar : "#tb", 
		});
		getData(1);
	/* }); */
		// 分页条
		function build_page_nav(result) {
			// 清空分页条
			$("#page_nav_area").empty();
			// page_nav_area
			var ul = $("<ul></ul>").addClass("pagination");

			var firstPageLi = $("<li></li>").append(
					$("<a></a>").append("首页").attr("href", "#"));
			var prePageLi = $("<li></li>").append(
					$("<a></a>").append("&laquo;"));
			// 判断是否有前一页，没有，就不能点击《disabled》
			if (result.extend.pageInfo.hasPreviousPage == false) {
				firstPageLi.addClass("disabled");
				prePageLi.addClass("disabled");
			} else {
				// 为元素添加点击翻页事件
				firstPageLi.click(function() {
					getData(1);
				});
				prePageLi.click(function() {
					getData(result.extend.pageInfo[0].pageNum - 1);
				});
			}

			var nextPageLi = $("<li></li>").append(
					$("<a></a>").append("&raquo;"));
			var lastPageLi = $("<li></li>").append(
					$("<a></a>").append("末页").attr("href", "#"));
			// 判断是否有下一页，没有，就不能点击《disabled》
			if (result.extend.pageInfo.hasNextPage == false) {
				nextPageLi.addClass("disabled");
				lastPageLi.addClass("disabled");
			} else {
				// 为元素添加点击翻页事件

				nextPageLi.click(function() {
					getData(result.extend.pageInfo.pageNum + 1);
				});
				lastPageLi.click(function() {
					getData(result.extend.pageInfo.pages);
				});
			}

			// 添加首页和前一页 的提示
			ul.append(firstPageLi).append(prePageLi);
			// 1.2.3.4.5 遍历给ul中添加页码提示
			$.each(result.extend.pageInfo.navigatepageNums, function(index,
					item) {

				var numLi = $("<li></li>")
						.append($("<a></a>").append(item));
				// 显示当前页
				if (result.extend.pageInfo.pageNum == item) {
					numLi.addClass("active");
				}
				// 绑定单机事件
				numLi.click(function() {
					getData(item);
				});
				ul.append(numLi);
			});
			// 添加下一页和末页 的提示
			ul.append(nextPageLi).append(lastPageLi);

			// 把ul加入到nav
			var navEle = $("<nav></nav>").append(ul);
			navEle.appendTo("#page_nav_area");
		}

		

		
			subscribe_tool = {
				reload : function() {
					$("#subscribe").datagrid("reload");
					//刷新目前解决办法
					getData(1);
				},
				

			};
		
		
	</script>
</body>

</html>