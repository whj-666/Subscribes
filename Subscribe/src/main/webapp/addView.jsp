<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!DOCTYPE html>
<html>
	<base href="<%=basePath%>">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="${APP_PATH }/upload/layui/css/layui.css" />
		<link rel="stylesheet" href="${APP_PATH }/upload/control/css/zyUpload.css" />
		<title></title>
	</head>

	<body>
		<form class="layui-form layui-form-pane" action="">
			<div class="layui-form-item">
				<label class="layui-form-label">图片路径</label>
				<div class="layui-input-block">
					<input type="text" name="imagePath"  autocomplete="off" class="layui-input" readonly="readonly" onclick="showImageUploadDiv()">
				</div>
			</div>
			<div id="demo" class="demo"></div>
		</form>

	</body>
	<script type="text/javascript" src="${APP_PATH }/upload/js/jquery-1.8.3.min.js" ></script>
	<script type="text/javascript" src="${APP_PATH }/upload/control/js/jq22.js" ></script>
	<script type="text/javascript" src="${APP_PATH }/upload/control/js/zyFile.js" ></script>
	<script type="text/javascript" src="${APP_PATH }/upload/control/js/zyUpload.js" ></script>
	<script type="text/javascript" src="${APP_PATH }/upload/layui/layui.js" ></script>
	<script>
		//Demo
		layui.use('form', function() {
			var form = layui.form();

			//çå¬æäº¤
			form.on('submit(formDemo)', function(data) {
				layer.msg(JSON.stringify(data.field));
				return false;
			});
		});
		function showImageUploadDiv(){
			
		}
	</script>

</html>