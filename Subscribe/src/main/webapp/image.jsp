<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!-- 引入样式 -->
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${APP_PATH }/easyui/themes/icon.css" />
<link href="${APP_PATH }/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
.textbox {
	height: 20px;
	margin: 0;
	padding: 0 2px;
	box-sizing: content-box;
}
</style>
<!-- 引入脚本 -->
 <script type="text/javascript" src="${APP_PATH }/easyui/jquery.min.js"></script> 

<script type="text/javascript"
	src="${APP_PATH }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${APP_PATH }/easyui/locale/easyui-lang-zh_CN.js"></script>

<script src="${APP_PATH }/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

	

<script type="text/javascript" src="${APP_PATH }/layer/layer.js"></script>  

</head>
<body>

	<table id="device">

	</table>
	<div id="tb" style="padding: 5px;">
		<div style="margin-bottom: 5px;">
			<a href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-add-new" onclick="device_tool.add();">添加</a>  <a
				href="#" class="easyui-linkbutton" plain="true"
				iconCls="icon-delete-new" onclick="device_tool.remove();">删除</a><!-- <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="openAddView();">图片上传</a> --> <a
				href="#" class="easyui-linkbutton" iconCls="icon-reload"
				plain="true" onclick="device_tool.reload();">刷新</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-redo" plain="true"
				onclick="device_tool.redo();">取消选择</a>
		</div>
		<div style="padding: 0 0 0 7px; color: #333;">
			查询类别：<input id="lb2" type="text"
				name="selectCategory" class="combobox" style="width: 150px;"> <a
				href="#" class="easyui-linkbutton" iconCls="icon-search"
				onclick="selectbutton(1);">查询</a>
		
		<div id="page_nav_area"
			style="margin-left: 60px; position: fixed; bottom: 0; left: 30%; z-index: 100;"></div>
	</div>
<!-- 新增 -->
	<form id="device_edit" action="${APP_PATH }/UploadHandleServlet1" enctype="multipart/form-data"  method="post"
		style="margin: 0; padding: 5px 0 0 25px; color: #333;" >
		<p>
			设&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp备：<input type="text" id="lb"
				name="deviceClass_edit" class="easyui-combobox"
				data-options="url:'${APP_PATH }/getAllDeviceByFlag1?r='+Math.random(),
                                           method:'get',  
                                           valueField:'id',  
                                           textField:'deviceNumber',  
                                           
                                           
                                         onLoadSuccess: function () { //加载完成后,设置选中第一项  
                                var val = $(this).combobox('getData');  
                                for (var item in val[0]) {  
                                    if (item == 'id') {  
                                        $(this).combobox('select', val[0][item]);  
                                    }  
                                }  
                            }
                                         "
				style="width: 150px;">
		</p>
		<p>
			设备图片&nbsp：<input class="easyui-filebox"  style="width: 132px"  name="file2"><button onclick="${APP_PATH }/UploadHandleServlet1" id="upload_btn2" style="background-color:#666666; display:none;">上传</button><br/>
		
		
		
		
	</form>
	
	<!-- js方式加载 -->
	<script type="text/javascript">
		var id1;
		var type;
		function openAddView() {
			layer.open({
				type: 2,
				title: '上传',
				shadeClose: true,
				shade: false,
				maxmin: true, 
				area: ['893px', '600px'],
				content: 'upload/openAddView.do'
			});
		}
		$(function() {
			
			/* var option1=$('#device').pagination("options");//使用easyUI提供的options方法获得分页选项对象 */
			getData(1);
			

			//类别名称
			$("input[name='category']").validatebox({
				required : true,
				validType : "length[2,20]",
				missingMessage : "请输入类别名称",
				invalidMessage : "类别名称在 2-20 位",
			});

			//类别简拼
			$("input[name='category_spell']").validatebox({
				required : true,
				validType : "length[1,30]",
				missingMessage : "请输入大写简拼",
				invalidMessage : "管理密码在 1-30 位",
			});

			//类别首拼
			$("input[name='category_first']").validatebox({
				required : true,
				validType : "length[0,1]",
				missingMessage : "请输入大写设备首拼",
				invalidMessage : "设备首拼为一位大写字母",
			});
			device_tool = {
				reload : function() {
					//刷新目前解决办法
					location.reload();
					$('#lb2').combobox('setText','全部');
					getData(1);
					 
				},
				redo : function() {
					$("#device").datagrid("unselectAll");
				},
				add : function() {
					$("#device_edit").dialog("open");
					$("input[name='category']").focus();
				},
				remove : function() {
					var rows = $("#device").datagrid("getSelections");
					var temID="";
		            for (i = 0; i < rows.length;i++) {
		                if (temID =="") {
		                    temID = rows[i].id;
		                } else {
		                    temID = rows[i].id + "," + temID;
		                }               
		            }
					if (rows.length == 0) {
						$.messager.alert("警告操作!", "请选择要删除的数据!", "warning");
					} else {
						
						$.ajax({
							type : "GET",
							url : "${APP_PATH }/deleteByDeviceId",
							cache : false,
							data : {
								device_id : temID,
							},

							beforeSend : function() {
								$("#device").datagrid("loading");
							},
							success : function(data) {
								if (data) {
									$("#device").datagrid("loaded");
									//刷新当前页
									$("#device").datagrid("load");
									//取消所有行选定
									$("#device").datagrid("unselectAll");
									$.messager.show({
										title : "提示",
										msg : data.msg,
									});
								} else {
									$.messager.alert("获取失败!", "未知错误导致失败,请重试!",
											"warning");
								}
								getData(1);
							},
						});
					}
				},
				

			};
		});
		
		$('#lb2').combobox({
			url : '${APP_PATH }/getAllDeviceByFlag1?r='+Math.random(),
			valueField : 'id',
			textField : 'deviceNumber',
			onLoadSuccess: function(){
		        //设置默认值
		        $('#lb2').combobox('setText','全部');
		    } 

		});
		
		selectbutton = function (pagenum) {
			/* $('#subscribe').datagrid('load', {
				subscribecn_code : $('input[name="usercode"]').val(),
				subscribecn_name : $('input[name="username"]').val(),
			}); */
			var CatagoryId = $('#lb2').combobox('getValue');
			
			{
	        $.ajax({
	            url: "${APP_PATH }/selectDeviceImage?pn=" + pagenum,
	            type: "GET",
	            data: {
	            	deviceId:CatagoryId,
	                r:Math.random(),
					

	            },
	            beforeSend: function () {
	                    $.messager.progress({
	                        text: "正在查询中...",
	                    });
	                },
	                success: function (data, response, status) {
	                    $.messager.progress("close");
	                    var pageinfo = data.extend.pageInfo;
	    				build_page_nav(data);
	    				//加载分页数据
	    				$("#device").datagrid("loadData", pageinfo.list); //动态取数据
	                    if (data) {
	                    	
	                        $.messager.show({
	                            title: "提示",
	                            msg: data.msg,
	                        });

	                        $("#device").datagrid("loadData", pageinfo.list);
	                      //解析分页表格
	                        $("#device").datagrid({
	            				fit : true,
	            				iconCls : "icon-manager",
	            				width : 'auto',
	            				height : '100',
	            				rownumbers : true,
	            				striped : true,
	            				fitColumns : true,
	            				columns : [ [ {
	            					field : 'id',
	            					title : '编号',
	            					sortName : 'id', //排序字段
	            					idField : 'id', //标识字段,主键
	            					width : 100,
	            					checkbox : true,
	            				}, {
	            					field : "deviceNumber",
	            					title : '设备编号',
	            					halign : "center",
	            					width : 100,
	            				}, {
	            					field : "fileurl",
	            					title : '路径',
	            					halign : "center",
	            					width : 100,
	            				},] ],
	            				toolbar : "#tb"
	            			});
	            			
	                    } else {
	                        $.messager.alert("查询失败!", "未知错误导致失败,请重试!", "warning");
	                    }
	                }
	        });
	    } 
		}
		// 分页条
		function build_page_nav(result) {
			// 清空分页条
			$("#page_nav_area").empty();
			// page_nav_area
			var ul = $("<ul></ul>").addClass("pagination");

			var firstPageLi = $("<li></li>").append(
					$("<a></a>").append("首页").attr("href", "#"));
			var prePageLi = $("<li></li>").append(
					$("<a></a>").append("&laquo;"));
			// 判断是否有前一页，没有，就不能点击《disabled》
			if (result.extend.pageInfo.hasPreviousPage == false) {
				firstPageLi.addClass("disabled");
				prePageLi.addClass("disabled");
			} else {
				// 为元素添加点击翻页事件
				firstPageLi.click(function() {
					getData(1);
				});
				prePageLi.click(function() {
					getData(result.extend.pageInfo.pageNum - 1);
				});
			}

			var nextPageLi = $("<li></li>").append(
					$("<a></a>").append("&raquo;"));
			var lastPageLi = $("<li></li>").append(
					$("<a></a>").append("末页").attr("href", "#"));
			// 判断是否有下一页，没有，就不能点击《disabled》
			if (result.extend.pageInfo.hasNextPage == false) {
				nextPageLi.addClass("disabled");
				lastPageLi.addClass("disabled");
			} else {
				// 为元素添加点击翻页事件

				nextPageLi.click(function() {
					getData(result.extend.pageInfo.pageNum + 1);
				});
				lastPageLi.click(function() {
					getData(result.extend.pageInfo.pages);
				});
			}

			// 添加首页和前一页 的提示
			ul.append(firstPageLi).append(prePageLi);
			// 1.2.3.4.5 遍历给ul中添加页码提示
			$.each(result.extend.pageInfo.navigatepageNums, function(index,
					item) {

				var numLi = $("<li></li>")
						.append($("<a></a>").append(item));
				// 显示当前页
				if (result.extend.pageInfo.pageNum == item) {
					numLi.addClass("active");
				}
				// 绑定单机事件
				numLi.click(function() {
					getData(item);
				});
				ul.append(numLi);
			});
			// 添加下一页和末页 的提示
			ul.append(nextPageLi).append(lastPageLi);

			// 把ul加入到nav
			var navEle = $("<nav></nav>").append(ul);
			navEle.appendTo("#page_nav_area");
		}
		
		function getData(pagenum) {
			$.ajax({
				url : "${APP_PATH }/selectDeviceImage?pn=" + pagenum,
				type : "POST",
				dataType : "json",
				cache : false,
				success : function(data) {
					var pageinfo = data.extend.pageInfo;
					build_page_nav(data);
					//加载分页数据
					$("#device").datagrid("loadData", pageinfo.list); //动态取数据
				}
			});
		}
		//解析分页表格
		$("#device").datagrid({
			fit : true,
			iconCls : "icon-manager",
			width : 'auto',
			height : '100',
			rownumbers : true,
			striped : true,
			fitColumns : true,
			columns : [ [ {
				field : 'id',
				title : '编号',
				sortName : 'id', //排序字段
				idField : 'id', //标识字段,主键
				width : 100,
				checkbox : true,
			}, {
				field : "deviceNumber",
				title : '设备编号',
				halign : "center",
				width : 100,
			}, {
				field : "fileurl",
				title : '路径',
				halign : "center",
				width : 100,
			},] ],
			toolbar : "#tb"
		});
		getData(1);

		
		
		

		$("#device_edit").dialog({
		    width: 300,
		    title: "图片",
		    modal: true,
		    closed: true,
		    iconCls: "icon-user-add",
		    enctype: 'multipart/form-data',
		    processData: false,
		    contentType: false,

		    buttons: [{
		        text: "提交",
		        iconCls: "icon-ok",
		        handler: function() {
		        		if ($("#device_edit").form(
						"validate")) {
		        			//获取设备id
					var Cc = $('#lb').combobox('getValue');
					var strFileName = $('input[name="file2"]').val();
					//转义符
					var i =strFileName.lastIndexOf('\\');
					var str1 =strFileName.substring(i+1,strFileName.length);
					document.getElementById("upload_btn2").click();
					
					$.ajax({
	                    url: "${APP_PATH }/insertDeviceImage",
	                    secureuri: false,
	                    type : "POST",
	                    cache : false,
	                    data:{
	                    	device_id : Cc,
	                    	fileurl : str1,
						},
	               
	                    beforeSend: function() {
	                        $.messager.progress({
	                            text: "正在新增中...",
	                        })
	                    },
	                    success : function(data, response, status){
	                        $.messager.progress("close");
	                        if (data) {
	                            $.messager.show({
	                                title: "提示",
	                                msg: data.msg,
	                            });
	                            $("#device_edit").dialog("close").form("reset");
	                            $("#device").datagrid("reload");
	                        } else {
	                            $.messager.alert("新增失败!", "未知错误导致失败,请重试!", "warning");
	                        }
	                        getData(1);
	                    }

	                });
					
					
				}
		        	
		           

		        }
		    },
		    {
		        text: "取消",
		        iconCls: "icon-redo",
		        handler: function() {
		            $("#device_edit").dialog("close").form("reset");
		        },
		    
		}],
		});
		
		
	</script>
</body>
</html>