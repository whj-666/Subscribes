<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=11;IE=10;IE=9;IE=8;" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>员工健身后台管理系统</title>

<!-- 引入样式 -->
<link rel="stylesheet" type="text/css" href="${ctx }/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${ctx }/easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${ctx }/css/main.css">


<!-- 引入脚本 -->
<script type="text/javascript" src="${ctx }/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx }/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx }/easyui/echarts.min.js"></script>

<script type="text/javascript">

	 $(function () {
		//判断是否登录
		 var manager="${sessionScope.manager}";
			if(manager==null || manager==""){
				location.href="login.jsp";
		}
		//起始页
		$("#tabs").tabs({
			fit:true,
			border:false,
		}); 
		//导航栏
		$("#nav").tree({
			url:"/Subscribe/tree/gettree",
			animate : true,			lines:true,
			onBeforeExpand:function(node){
				if(node){
					$("#nav").tree("options").url="/Subscribe/tree/gettree?id="+node.id
				}
			},
			onLoadSuccess:function(node,data){
				if(data){
					$(data).each(function(index,value){
						if(this.state=="closed"){
							$("#nav").tree("expandAll");
						}
					})
				}
			},
			onClick:function(node1){
				if(node1.url){
					if($("#tabs").tabs("exists",node1.text)){
						$("#tabs").tabs("select",node1.text);
					}else{
						$("#tabs").tabs("add",{
							title:node1.text,
							iconCls:node1.iconCls,
							closable:true,
							content:'<iframe name="indextab" scrolling="auto" src="'+node1.url+".jsp"+'" frameborder="0" style="width:100%;height:100%;"></iframe>',
						})
					}
				}
			},
		});
		 var names=[];    //类别数组（实际用来盛放X轴坐标值）
         var nums=[];    //使用次数数组（实际用来盛放Y坐标值）
         var times=[];   //使用时间数组（实际用来盛放Y坐标值）
         $.ajax({
             type : "get",
             async : true,            //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
             url : "${ctx }/selectEveryDeviceCategoryTimes?aboutDay=7",    //请求发送到TestServlet处
             data : {},
             cache : false,
             dataType : "json",        //返回数据形式为json
             success : function(result) {
                 //请求成功时执行该函数内容，result即为服务器返回的json对象
            			
						if (result) {
							for (var i = 0; i < result.extend.success.length; i++) {
								names
										.push(result.extend.success[i].deviceCategoryName); //挨个取出设别类别并填入类别数组
							}
							for (var i = 0; i < result.extend.success.length; i++) {
								nums
										.push(result.extend.success[i].allDeviceAllUseTime); //挨个取出使用次数并填入使用次数数组
							}
							for (var i = 0; i < result.extend.success.length; i++) {
								times.push(result.extend.success[i].useNumber); //挨个取出使用次数并填入使用次数数组
							}
							myChart.hideLoading(); //隐藏加载动画
							myChart.setOption({ //加载数据图表
								xAxis : {
									data : names
								},
								series : [ {
									// 根据名字对应到相应的系列
									name : '设备使用次数',
									type : 'line',
									data : times
								}, {
									name : '设备使用时间',
									type : 'line',
									data : nums
								} ]
							});

						}

					},
					error : function(errorMsg) {
						//请求失败时执行该函数
						alert("图表请求数据失败!");
						myChart.hideLoading();
					}
				})

		var dom = document.getElementById("container");
		var myChart = echarts.init(dom);
		var app = {};
		option = null;
		option = {
			title : {
				text : '一周内设备的使用情况',
				left : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : '{a} <br/>{b} : {c}'
			},
			legend : {
				left : 'left',
				data : [ '设备使用次数', '设备使用时间' ]
			},
			toolbox : {
				show : true,
				feature : {
					dataZoom : {
						yAxisIndex : 'none'
					},
					dataView : {
						readOnly : false
					},
					magicType : {
						type : [ 'line', 'bar' ]
					},
					restore : {},
					saveAsImage : {}
				}
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'log',
				name : 'y'
			},

		};
		;
		if (option && typeof option === "object") {
			myChart.setOption(option, true);
		}
		
	});
	
	 var websocket = null;
	    //判断当前浏览器是否支持WebSocket
	    if ('WebSocket' in window) {
	        websocket = new WebSocket("ws://localhost:8080/Subscribe/websocket");
	    }
	    else {
	        alert('当前浏览器 Not support websocket')
	    }

	    //连接发生错误的回调方法
	    websocket.onerror = function () {
	        
	    };

	    //连接成功建立的回调方法
	    websocket.onopen = function () {
	    	setMessageInnerHTML("连接成功");
	    }

	    //接收到消息的回调方法
	    websocket.onmessage = function (event) {
	    	setMessageInnerHTML(event.data);
	    }
	    

	    //连接关闭的回调方法
	    websocket.onclose = function () {
	    }

	    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
	    window.onbeforeunload = function () {
	        closeWebSocket();
	    }

	    //将消息显示在网页上
	    function setMessageInnerHTML(innerHTML) {
	        console.log(innerHTML);
	        $.messager.show({
		    	title:'My Title',
		    	msg:innerHTML,
		    	timeout:10000,
		    	showType:'slide'
		    });
	    }

	    //关闭WebSocket连接
	    function closeWebSocket() {
	        websocket.close();
	    }

	    //发送消息
	    function send() {
	        var message = document.getElementById('text').value;
	        websocket.send(message);
	    }
	   

	/*双击关闭TAB选项卡*/
	/* function tabClose()
	 {
	   
	   $(".tabs-inner").dblclick(function(){
	     var subtitle = $(this).children("span").text();
	     $('#tabs').tabs('close',subtitle);
	   })
	  
	  $(".tabs-inner").bind('contextmenu',function(e){
	     $('#mm').menu('show', {
	       left: e.pageX,
	       top: e.pageY,
	     });
	      
	     var subtitle =$(this).children("span").text();
	     $('#mm').data("currtab",subtitle);
	      
	     return false;
	   });
	 } */
	/* //绑定右键菜单事件
	function tabCloseEven()
	{
	  //关闭当前
	  $('#mm-tabclose').click(function(){
	    var currtab_title = $('#mm').data("currtab");
	    $('#tabs').tabs('close',currtab_title);
	  })
	  //全部关闭
	  $('#mm-tabcloseall').click(function(){
	    $('.tabs-inner span').each(function(i,n){
	      var t = $(n).text();
	      $('#tabs').tabs('close',t);
	    });  
	  });
	  //关闭除当前之外的TAB
	  $('#mm-tabcloseother').click(function(){
	    var currtab_title = $('#mm').data("currtab");
	    $('.tabs-inner span').each(function(i,n){
	      var t = $(n).text();
	      if(t!=currtab_title)
	        $('#tabs').tabs('close',t);
	    });  
	  });
	
	  //关闭当前右侧的TAB
	  $('#mm-tabcloseright').click(function(){
	    var nextall = $('.tabs-selected').nextAll();
	    if(nextall.length==0){
	      //msgShow('系统提示','后边没有啦~~','error');
	      alert('后边没有啦~~');
	      return false;
	    }
	    nextall.each(function(i,n){
	      var t=$('a:eq(0) span',$(n)).text();
	      $('#tabs').tabs('close',t);
	    });
	    return false;
	  });
	  //关闭当前左侧的TAB
	  $('#mm-tabcloseleft').click(function(){
	    var prevall = $('.tabs-selected').prevAll();
	    if(prevall.length==0){
	      alert('到头了，前边没有啦~~');
	      return false;
	    }
	    prevall.each(function(i,n){
	      var t=$('a:eq(0) span',$(n)).text();
	      $('#tabs').tabs('close',t);
	    });
	    return false;
	  });
	} */
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',title:'header',split:true,noheader:true"  style="height:60px;background:#666;">
		<div class="logo">员工健身管理系统</div>
		<div class="logout">您好,admin  <a href="logout.jsp">退出</a></div>
	</div>
	<div data-options="region:'south',title:'footer',split:true,noheader:true" style="height:35px;line-height:30px;text-align:center;">
		&copy;2017 Powered by SSM and EasyUI.
	</div>
	<!-- <div id="mm" class="easyui-menu" style="width:150px;">
     <div id="mm-tabclose">关闭</div>
     <div id="mm-tabcloseall">全部关闭</div>
     <div id="mm-tabcloseother">除此之外全部关闭</div>
     <div class="menu-sep"></div>
     <div id="mm-tabcloseright">当前页右侧全部关闭</div>
     <div id="mm-tabcloseleft">当前页左侧全部关闭</div> -->
 </div>
	<div data-options="region:'west',title:'导航',split:true"  style="width:180px;">
		<div id="navMenu" class="easyui-accordion" data-options="fit:true,border:false">
			<div title="管理员管理" iconCls="icon-manager">
				<ul id="nav"></ul>
			</div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tabs">
			<div title="主页" style="padding:0 10px;display:block;">
			<div id="container" style="height: 100%"></div>
				
			</div>
		</div>
	</div>
	
</body>
</html>