package cn.com.cvit.nc.bean;

public class Organize {
    private String id;

    private String code;

    private String departtype;

    private String description;

    private String name;

    private String orders;

    private String pid;

    private String shortname;

    private String type;
    
    

    public Organize() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Organize(String id, String code, String departtype, String description, String name, String orders,
			String pid, String shortname, String type) {
		super();
		this.id = id;
		this.code = code;
		this.departtype = departtype;
		this.description = description;
		this.name = name;
		this.orders = orders;
		this.pid = pid;
		this.shortname = shortname;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Organize [id=" + id + ", code=" + code + ", departtype=" + departtype + ", description=" + description
				+ ", name=" + name + ", orders=" + orders + ", pid=" + pid + ", shortname=" + shortname + ", type="
				+ type + "]";
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getDeparttype() {
        return departtype;
    }

    public void setDeparttype(String departtype) {
        this.departtype = departtype == null ? null : departtype.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders == null ? null : orders.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname == null ? null : shortname.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
}