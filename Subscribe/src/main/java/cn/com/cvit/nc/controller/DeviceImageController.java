package cn.com.cvit.nc.controller;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.bean.DeviceImage;
import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.service.DeviceImageService;

@Controller
public class DeviceImageController {
	
	@Autowired
	private DeviceImageService deviceImageService;
	/*
	 * 查询设备图片-分页
	 */
	@RequestMapping(value="/selectDeviceImage")
	@ResponseBody
	public Msg select(@RequestParam(value = "pn", defaultValue = "1") Integer pn,@RequestParam(value = "nr", defaultValue = "10") Integer nr,String deviceId){
		PageHelper.startPage(pn, nr);//分页查询
		DeviceImage dev = new DeviceImage();
		dev.setDeviceClass(deviceId);
		List<DeviceImage> de = deviceImageService.getAll(dev);
		PageInfo<DeviceImage> page = new PageInfo<DeviceImage>(de, nr);
		return Msg.success().add("pageInfo", page);
	}
	/*
	 * 添加图片
	 */
	@RequestMapping(value="/insertDeviceImage")
	@ResponseBody
	public Msg insert(@RequestParam("device_id")String device_id,@RequestParam("fileurl")String fileurl){
		String uuid = UUID.randomUUID().toString().substring(0,31);//32位UUID
		DeviceImage de = new DeviceImage();
		de.setId(uuid);
		de.setDeviceId(device_id);
		/*de.setFileurl(fileurl);*/
		//String image = "Subscribe_image"+File.separator+fileurl.substring(12,fileurl.length());
		String image = "Subscribe_image"+File.separator+fileurl;
		de.setFileurl(image);
		if(deviceImageService.insertSelective(de) == 1)
			return Msg.success().add("successMsg", "添加图片成功!!!");
		else
			return Msg.fail().add("failMsg", "添加图片失败!!!");
	}
	
	/*
	 * 根据设备Id删除图片
	 */
	@RequestMapping(value="/deleteByDeviceId")
	@ResponseBody
	public Msg deleteByDeviceId(@RequestParam("device_id")String device_id){
		String[] id = device_id.split(",");
		if(deviceImageService.deleteByDeviceId(id) > 0)
			return Msg.success().add("successMsg", "删除图片成功!!!");
		else
			return Msg.fail().add("failMsg", "删除图片失败!!!");
	}

}
