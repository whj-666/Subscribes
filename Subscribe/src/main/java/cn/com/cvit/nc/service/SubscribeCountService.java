package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.SubscribeCount;
import cn.com.cvit.nc.bean.SubscribeCountParameter;
import cn.com.cvit.nc.dao.SubscribeCountMapper;

@Service
public class SubscribeCountService {
	@Autowired
	SubscribeCountMapper subscribeCountMapper;
	
	//所有设备总使用时长
 	public String selectAllDeviceAllUseTime(){
 		return subscribeCountMapper.selectAllDeviceAllUseTime();
 	}
 	//所有设备类别使用时长
 	public List<SubscribeCount> selectDeviceCategoryAllUseTime(){
 		return subscribeCountMapper.selectDeviceCategoryAllUseTime();
 	}
 	//所有设备类别使用次数
 	public List<SubscribeCount> selectDeviceCategoryAllUseNumber(){
 		return subscribeCountMapper.selectDeviceCategoryAllUseNumber();
 	}
 	//所有设备近*天预约信息
 	public List<SubscribeCount> selectAllDeviceSubscribeDataDay(String day){
 		return subscribeCountMapper.selectAllDeviceSubscribeDataDay(day);
 	}
 	//所有设备近*天预约信息
 	public List<SubscribeCount> selectAllDeviceSubscribeDataMonth(String month){
 		return subscribeCountMapper.selectAllDeviceSubscribeDataMonth(month);
 	}
 	//所有设备使用时间*--*分钟预约信息
 	public List<SubscribeCount> selectAllDeviceSubscribeMinute(String mintime,String maxtime){
 		return subscribeCountMapper.selectAllDeviceSubscribeMinute(mintime,maxtime);
 	}
 	//某台设备使用时间*--*分钟预约信息
 	public List<SubscribeCount> selectDeviceSubscribeMinuteByDeviceId(String mintime,String maxtime,String deviceid){
 		return subscribeCountMapper.selectDeviceSubscribeMinuteByDeviceId(mintime,maxtime,deviceid);
 	}
 	//每人的预约次数
 	public List<SubscribeCount> selectPersonAllUseNumber(){
 		return subscribeCountMapper.selectPersonAllUseNumber();
 	}
 	//每人的预约时长
 	public List<SubscribeCount> selectPersonAllUseTime(){
 		return subscribeCountMapper.selectPersonAllUseTime();
 	}
    //某一时间段的预约信息
 	public List<SubscribeCount> selectTimeToTime(String mintime,String maxtime){
 		return subscribeCountMapper.selectTimeToTime(mintime,maxtime);
 	}
 	//所有部门使用次数和时间
 	public List<SubscribeCount> selectOrganizeAllUseNumberAndTime(){
 		return subscribeCountMapper.selectOrganizeAllUseNumberAndTime();
 	}
}
