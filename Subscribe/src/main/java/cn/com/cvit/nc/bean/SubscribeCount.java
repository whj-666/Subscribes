package cn.com.cvit.nc.bean;

public class SubscribeCount {
	
	private String allDeviceAllUseTime;//设备使用总时长
	
	private String deviceCategoryName;//设备类别名称
	
	private String deviceName;//设备名称
	
	private String deviceCategoryAllUseTime;//单一类别使用总时长

	private String useNumber;//使用次数
	
	private String personName;//用户名
	
	private String deviceId;//设备Id
	
	private String deviceCategoryId;//设备类别Id
	
	private String personId;//用户Id
	
	private String personEmail;//用户Email
	
	private String organizeName;//部门名称
	
	private String organizeId;//部门ID
	
	private String organizeNameTime;//部门时间

	public String getAllDeviceAllUseTime() {
		return allDeviceAllUseTime;
	}

	public void setAllDeviceAllUseTime(String allDeviceAllUseTime) {
		this.allDeviceAllUseTime = allDeviceAllUseTime;
	}

	public String getDeviceCategoryName() {
		return deviceCategoryName;
	}

	public void setDeviceCategoryName(String deviceCategoryName) {
		this.deviceCategoryName = deviceCategoryName;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceCategoryAllUseTime() {
		return deviceCategoryAllUseTime;
	}

	public void setDeviceCategoryAllUseTime(String deviceCategoryAllUseTime) {
		this.deviceCategoryAllUseTime = deviceCategoryAllUseTime;
	}

	public String getUseNumber() {
		return useNumber;
	}

	public void setUseNumber(String useNumber) {
		this.useNumber = useNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getOrganizeName() {
		return organizeName;
	}

	public void setOrganizeName(String organizeName) {
		this.organizeName = organizeName;
	}

	public String getPersonEmail() {
		return personEmail;
	}

	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail;
	}

	public String getDeviceCategoryId() {
		return deviceCategoryId;
	}

	public void setDeviceCategoryId(String deviceCategoryId) {
		this.deviceCategoryId = deviceCategoryId;
	}

	public String getOrganizeNameTime() {
		return organizeNameTime;
	}

	public void setOrganizeNameTime(String organizeNameTime) {
		this.organizeNameTime = organizeNameTime;
	}

	public String getOrganizeId() {
		return organizeId;
	}

	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}

}
