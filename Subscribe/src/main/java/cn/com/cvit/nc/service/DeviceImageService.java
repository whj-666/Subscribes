package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.DeviceImage;
import cn.com.cvit.nc.dao.DeviceImageMapper;

@Service
public class DeviceImageService {
	
	@Autowired
	private DeviceImageMapper deviceImageMapper;
	/*
	 * 添加图片
	 */
	public int insertSelective(DeviceImage image){
		return deviceImageMapper.insertSelective(image);
	}
	/*
	 * 删除图片
	 */
	public int deleteByDeviceId(String[] deviceId){
		return deviceImageMapper.deleteByDeviceId(deviceId);
	}
	
	//查询图片
	
	
	public List<DeviceImage> getAll(DeviceImage dev) {
		
		return deviceImageMapper.selectByExample(dev);
	}
	

}
