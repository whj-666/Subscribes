package cn.com.cvit.nc.poi;

import java.beans.IntrospectionException;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.SubscribeCountParameter;

@Controller
public class ExcelController{
	@Autowired
	ExcelService excelService;
	//导出-所有设备类别使用时长
	@RequestMapping("/export")  
	public @ResponseBody void export(HttpServletRequest request, HttpServletResponse response) throws IOException, InvocationTargetException, ClassNotFoundException, IllegalAccessException, IntrospectionException, ParseException {  
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "所有设备类别使用时长"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.exportExcelInfo();  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}  
	//单一设备使用时长和次数
	@RequestMapping(value = "/excelEveryDeviceTimesAndNumber", method = RequestMethod.GET)
	public @ResponseBody void excelEveryDeviceTimesAndNumber(HttpServletRequest request, HttpServletResponse response,String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String deviceId) throws Exception {  
		//封装参数
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(deviceId!="" || !deviceId.equals("")){
			scp.setDeviceId(deviceId);
		}
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "设备使用时长及次数"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.excelEveryDeviceTimesAndNumber(scp);  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}
	//单一设备类别使用时长和次数
	@RequestMapping(value = "/excelEveryDeviceCategoryTimesAndNumber", method = RequestMethod.GET)
	public @ResponseBody void excelEveryDeviceCategoryTimesAndNumber(HttpServletRequest request, HttpServletResponse response,String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String deviceCatagoryId) throws Exception {  
		//封装参数
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(deviceCatagoryId!="" || !deviceCatagoryId.equals("")){
			scp.setDeviceCatagoryId(deviceCatagoryId);
		}
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "设备类别使用时长及次数"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.excelEveryDeviceCategoryTimesAndNumber(scp);  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}
	//用户使用时长及次数
	@RequestMapping(value = "/excelEveryUserTimesAndNumdber", method = RequestMethod.GET)
	public @ResponseBody void excelEveryUserTimesAndNumdber(HttpServletRequest request, HttpServletResponse response,String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String personId) throws Exception {  
		//封装参数
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(personId!="" || !personId.equals("")){
			scp.setPersonId(personId);
		}
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "用户使用时长及次数"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.excelEveryUserTimesAndNumdber(scp);  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}	
	//单一部门使用时长和次数
	@RequestMapping(value = "/excelEveryOrganizeTimesAndNumber", method = RequestMethod.GET)
	public @ResponseBody void excelEveryOrganizeTimesAndNumber(HttpServletRequest request, HttpServletResponse response,String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String organizeId) throws Exception {  
		//封装参数
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(organizeId!="" || !organizeId.equals("")){
			scp.setOrganizeId(organizeId);
		}
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "部门使用时长和次数"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.excelEveryOrganizeTimesAndNumber(scp);  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}	
	//预约数据统计
	@RequestMapping(value = "/excelSubscribeCountData", method = RequestMethod.GET)
	public @ResponseBody void excelSubscribeCountData(HttpServletRequest request, HttpServletResponse response,String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String organizeId,String deviceId,String deviceCatagoryId,String personId) throws Exception {  
		//封装参数
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(organizeId!="" || !organizeId.equals("")){
			scp.setOrganizeId(organizeId);
		}
		if(deviceId!="" || !deviceId.equals("")){
			scp.setDeviceId(deviceId);
		}
		if(deviceCatagoryId!="" || !deviceCatagoryId.equals("")){
			scp.setDeviceCatagoryId(deviceCatagoryId);
		}
		if(personId!="" || !personId.equals("")){
			scp.setPersonId(personId);
		}
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String subscribeCountDate = "预约数据统计"+df.format(day); 
	    if(subscribeCountDate!=""){  
	        response.reset(); //清除buffer缓存  
	        // 指定下载的文件名  
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String(subscribeCountDate.getBytes(),"iso-8859-1") +".xlsx");  
	        response.setContentType("application/vnd.ms-excel;charset=UTF-8");  
	        response.setHeader("Pragma", "no-cache");  
	        response.setHeader("Cache-Control", "no-cache");  
	        response.setDateHeader("Expires", 0);  
	        XSSFWorkbook workbook=null;  
	        //导出Excel对象 
	        workbook = excelService.excelSubscribeCountData(scp);  
	        OutputStream output;  
	        try {  
	            output = response.getOutputStream();  
	            BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);  
	            bufferedOutPut.flush();  
	            workbook.write(bufferedOutPut);  
	            bufferedOutPut.close();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }  
	}	

}
