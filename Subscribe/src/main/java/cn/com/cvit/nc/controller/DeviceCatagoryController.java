package cn.com.cvit.nc.controller;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.bean.DeviceCatagory;
import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.service.DeviceCatagoryService;
import cn.com.cvit.nc.service.DeviceService;

@Controller
public class DeviceCatagoryController {
	@Autowired
	DeviceCatagoryService deviceCatagoryService;
	
	@Autowired
	DeviceService deviceService;
	/**
	 * 查询所有设备类别
	 * */
	@RequestMapping("/device_catagoryService")
	@ResponseBody
	public Msg getDevice() {
		//List<DeviceCatagory> device = deviceCatagoryService.getAll();
		List<DeviceCatagory> device = deviceCatagoryService.getFlag();
		return Msg.success().add("device", device);
	}
	/**
	 * 根据flag查询**
	 * */
	@ResponseBody
	@RequestMapping("/device_catagoryFlag")
	public Msg getFlag(@RequestParam(value = "pn", defaultValue = "1") Integer pn,@RequestParam(value = "nr", defaultValue = "10") Integer nr){
		PageHelper.startPage(pn,nr);
		//List<DeviceCatagory> flags=deviceCatagoryService.getFlag();
		List<DeviceCatagory> flags=deviceCatagoryService.getFlag();
		PageInfo<DeviceCatagory> page = new PageInfo<DeviceCatagory>(flags, nr);
		return Msg.success().add("pageInfo", page);
		
	}
	/**
	 * 添加保存设备类别
	 * */
	@RequestMapping("/deviceADD")
	@ResponseBody
	public Msg saveDev(DeviceCatagory deviceCatagory){
		String uuid = UUID.randomUUID().toString();
		//String uuid="6";
		//deviceCatagory.setId(uuid);
		DeviceCatagory de = new DeviceCatagory();
		de.setId(uuid);
		de.setEquipmentFirstSpell(deviceCatagory.getEquipmentFirstSpell());
		de.setEquipmentNumber(deviceCatagory.getEquipmentNumber());
		String image = "Subscribe_image"+File.separator+deviceCatagory.getImages();
		de.setImages(image);
		de.setEquipmentName(deviceCatagory.getEquipmentName());
		de.setEquipmentSpell(deviceCatagory.getEquipmentSpell());
		deviceCatagoryService.saveDec(de);
		return Msg.success();
	}
	/**
	 * 根据id查询设备类别
	 * */
	@RequestMapping("/device")
	@ResponseBody
	public Msg getDevices(@RequestParam("id")String id){
		DeviceCatagory deviceCatagory=deviceCatagoryService.getDevices(id);
		return Msg.success().add("devices", deviceCatagory);
	}
	/**
	 * 更新
	 * */
	@ResponseBody
	@RequestMapping("/decUpdate")
	public Msg saveDevs(DeviceCatagory deviceCatagory){
		if(deviceCatagory.getImages() != null){
			deviceCatagory.setImages("Subscribe_image"+File.separator+deviceCatagory.getImages());
		}
		Device device = new Device();
		device.setDeviceClass(deviceCatagory.getId());
		device.setDevicePhoto(deviceCatagory.getImages());
		if(deviceCatagory.getImages() != null){
			deviceService.updateImages(device);
		}
		deviceCatagoryService.updateDevs(deviceCatagory);
		return Msg.success();
		
	}
	/**
	 * 批量删除
	 * */
	@ResponseBody
	@RequestMapping("/dev")
	public Msg deleteDevById(@RequestParam("id")String ids){
		String[] id = ids.split(",");
		String deviceType ="";
		int y =0,i;
		for(i = 0;i<id.length;i++){
			List<Device> list = deviceService.getDeviceCatagoryId(id[i]);
			if(list.size()>0){
				deviceType=deviceType+list.get(0).getDeviceType()+" ";
			}else{
				y++;
			}
		}
		if(y==i){
			deviceCatagoryService.deleteDev(id);
			return Msg.success();
		}else{
			return Msg.fail().add("fail", deviceType+"类别下有设备,请先删除设备");
		}
		
	}
//		if(deviceService.getDeviceCatagoryId(id).size()>0){
//			return Msg.fail().add("fail", "该设备类别下有设备,请先删除设备");
//		}else{
//			deviceCatagoryService.deleteDev(id);
//			return Msg.success();
//		}
	
	/**
	 * 分页查询
	 * */
	@RequestMapping("/devices")
	@ResponseBody
	public Msg getEmpsWithJson(@RequestParam(value = "pn", defaultValue = "1") Integer pn) {
		// 引入PageHelper分页插件
		// 在查询之前只需要调用，传入页码，以及每页的大小
		PageHelper.startPage(pn, 5);
		// startPage后面紧跟的这个查询就是一个分页查询
		List<DeviceCatagory> emps = deviceCatagoryService.getAll();
		// 使用pageInfo包装查询后的结果，只需要将pageInfo交给页面就行了。
		// 封装了详细的分页信息,包括有我们查询出来的数据，传入连续显示的页数
		PageInfo page = new PageInfo(emps, 5);
		return Msg.success().add("pageInfo", page);
	}
	/**
	 * 查询所有设备类别后台下拉列表分类用
	 * */
	@RequestMapping("/device_catagoryService1")
	@ResponseBody
	public List getDevice1() {
		List<DeviceCatagory> device = deviceCatagoryService.getFlag();
		return device;
	}

	

}
