package cn.com.cvit.nc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeSon;
import cn.com.cvit.nc.dao.SubscribeMapper;
import cn.com.cvit.nc.service.SubscribeService;
import cn.com.cvit.nc.service.SubscribeSonService;
import cn.com.cvit.nc.util.BaseParameter;

@Controller
public class SubscribeSonController {
	
	@Autowired
	private SubscribeSonService subscribeSonService;
	
	@Autowired
	SubscribeService subscribeService;
	
	@Autowired
	private SubscribeMapper subscribeMapper;
	
	@Autowired
	SubscribeController subscribeController;
	
	BaseParameter baseParameter =new BaseParameter();
	/*
     * 查询所有受邀信息
     */
	@RequestMapping(value="/selectSubscribeSonByExampleWithName",method=RequestMethod.GET)
	@ResponseBody
	public Msg selectByExampleWithName(){
		List<SubscribeSon> list = subscribeSonService.selectByExampleWithName();
		System.out.println(list);
		return Msg.success().add("selectByExampleWithName", list);
	}
	
	/*
     * 根据subscribe_id查询我的邀请
     */
	@RequestMapping(value="/selectSubscribeSonByEmailAndNameForSubscribeId",method=RequestMethod.GET)
	@ResponseBody
	public Msg selectBySubscribeId(@RequestParam("subscribe_user_email") String email,
			@RequestParam("subscribe_user_name") String name){
		List<Subscribe> list = subscribeService.getEmailAndNameAndTime(email, name);
		System.out.println("list长度:"+list.size());
		List list2 = new ArrayList<>();
		for(int i=0;i<list.size();i++){
			list2.add(subscribeSonService.selectBySubscribeId(list.get(i).getId()));
		}
		return Msg.success().add("selectBySubscribeId", list2);
	}
	
	/*
     * 根据subscribe_partner_id(email)和subscribe_user_name查询我的受邀
     */
	@RequestMapping(value="/selectSubscribeSonByEmailAndName",method=RequestMethod.GET)
	@ResponseBody
	public Msg selectByEmailAndName(@RequestParam("subscribe_user_email")String subscribe_email,@RequestParam("subscribe_user_name")String subscribe_user_name){
		List<SubscribeSon> list = subscribeSonService.selectByEmailAndName(subscribe_email,subscribe_user_name);
		return Msg.success().add("selectByEmailAndName", list);
	}
	
	/*
	 * 根据id查询我受邀的详细信息-带主表信息-带设备信息
	 */
	@RequestMapping(value="/selectBySubscribeSonId",method=RequestMethod.GET)
	@ResponseBody
	public Msg selectBySubscribeSonId(@RequestParam("id")String id){
		List<SubscribeSon> list = subscribeSonService.selectBySubscribeSonId(id);
		List<Subscribe> list2 = subscribeMapper.selectByPrimaryKeyWithDevice(list.get(0).getSubscribeId());
		return Msg.success().add("selectBySubscribeSonId", list).add("selectByPrimaryKeyWithDevice", list2);
	}
	
	/*
     * 批量插入数据----For循环list
     */
	@RequestMapping(value="/insertSubscribeSon",method=RequestMethod.GET)
	@ResponseBody
	public Msg insertSubscribeSon(SubscribeSon sub){
			if(subscribeSonService.insertSelective(sub)==1){
				return Msg.success().add("successMsg", "添加成功");
			}else
				return Msg.fail().add("failMsg", "添加失败");
	}

	/*
     * 修改
     * update状态状态1.未通过2通过3正常结束4撤销（撤销原因）5终止
     */
	@RequestMapping(value="/updateSubscribeSonByIdForStatus",method=RequestMethod.GET)
	@ResponseBody
	public Msg updateByIdForStatus(@RequestParam("id")String id,@RequestParam("subscribe_status")String subscribe_status,@RequestParam("subscribe_result")String subscribe_result){
		SubscribeSon sub = new SubscribeSon();
		sub.setId(id);
		sub.setSubscribeStatus(subscribe_status);
		sub.setSubscribeResult(subscribe_result);
		if(subscribeSonService.updateByPrimaryKeySelective(sub) == 1){
			return Msg.success().add("successMsg", "修改状态成功");
		}else
			return Msg.fail().add("failMsg", "修改状态失败");
	}
	
	/*
     * 修改
     * update受邀状态1、接受2、拒绝（拒绝原因）3已被邀请人请出
     */
	@RequestMapping(value="/updateSubscribeSonByIdForUserStatus",method=RequestMethod.GET)
	@ResponseBody
	public Msg updateByIdForUserStatus(@RequestParam("id")String id,@RequestParam("subscribe_user_status")String subscribe_user_status){
		SubscribeSon sub = new SubscribeSon();
		sub.setId(id);
		sub.setSubscribeUserStatus(subscribe_user_status);
		//System.out.println(subscribeSonService.updateByPrimaryKeySelective(sub));
		//消息推送2018-5-16 begin
		List<SubscribeSon> listson = subscribeSonService.selectBySubscribeSonId(id);
		List<Subscribe> listsub = subscribeService.getByPrimaryKeyWithDevice(listson.get(0).getSubscribeId());//获取预约信息、带设备信息--推送
		//消息推送2018-5-16 end
		String times = subscribeController.timeToTime(listsub.get(0).getSubscribeBeginTime(), listsub.get(0).getSubscribeEndTime());
		if(subscribeSonService.updateByPrimaryKeySelective(sub) == 1){
			//消息推送2018-5-16 begin
			if(subscribe_user_status.equals("1")){//接受邀请--推送给主邀人
				String content=listson.get(0).getSubscribeUserName()+"已同意您的健身邀请,简要信息:"+times+","+listsub.get(0).getDeviceType()+listsub.get(0).getDeviceNumber()+"运动,位置:"+listsub.get(0).getDevicePlace();
				String title="健身邀请接受通知";
				//String urlss="";
				String urlss = baseParameter.getPushUrlBegin()+listsub.get(0).getId()+baseParameter.getPushUrlEnd();// 业务系统跳转地址
				System.out.println("email:"+listsub.get(0).getSubscribeUserEmail()+"\n"+"信息内容:"+content+"\n"+"标题:"+title+"\n"+"业务系统跳转地址:"+urlss);
				subscribeController.pushMessage(listsub.get(0).getSubscribeUserEmail(), content, title, urlss);
			}else if(subscribe_user_status.equals("2")){//拒绝邀请--推送给主邀人、加原因
				String content=listson.get(0).getSubscribeUserName()+"已拒绝您的健身邀请,简要信息:"+times+","+listsub.get(0).getDeviceType()+listsub.get(0).getDeviceNumber()+"运动,位置:"+listsub.get(0).getDevicePlace();
				String title="健身邀请拒绝通知";
				//String urlss="";
				String urlss = baseParameter.getPushUrlBegin()+listsub.get(0).getId()+baseParameter.getPushUrlEnd();// 业务系统跳转地址
				System.out.println("email:"+listsub.get(0).getSubscribeUserEmail()+"\n"+"信息内容:"+content+"\n"+"标题:"+title+"\n"+"业务系统跳转地址:"+urlss);
				subscribeController.pushMessage(listsub.get(0).getSubscribeUserEmail(), content, title, urlss);
			}else if(subscribe_user_status.equals("3")){//被邀请人请出--推送给受邀人
//				String contentson=listsub.get(0).getSubscribeUserName()+"把你移除,简要信息:"+times+","+listsub.get(0).getDeviceType()+listsub.get(0).getDeviceNumber()+"运动,位置:"+listsub.get(0).getDevicePlace();
//				String titleson="健身邀请移除通知";
//				//String urlssson = baseParameter.getPushUrlSon3();
//				String urlssson = baseParameter.getPushUrlBegin()+id+baseParameter.getPushUrlEnd();// 业务系统跳转地址
//				System.out.println("email:"+listson.get(0).getSubscribePartnerId()+"\n"+"信息内容:"+contentson+"\n"+"标题:"+titleson+"\n"+"业务系统跳转地址:"+urlssson);
//				subscribeController.pushMessage(listsub.get(0).getSubscribeUserEmail(), contentson, titleson, urlssson);
				
				//2018-6-4修改
				//推受邀人
				String contentson=listsub.get(0).getSubscribeUserName()+"把你移除,简要信息:"+times+","+listsub.get(0).getDeviceType()+listsub.get(0).getDeviceNumber()+"运动,位置:"+listsub.get(0).getDevicePlace();
				String titleson="健身邀请移除通知";
				//String urlssson = baseParameter.getPushUrlBegin()+id+baseParameter.getPushUrlEnd();// 业务系统跳转地址
				String urlssson = baseParameter.getPushUrlSonBegin()+id+baseParameter.getPushUrlSonEnd();// 业务系统跳转地址
				System.out.println("email:"+listson.get(0).getSubscribePartnerId()+"\n"+"信息内容:"+contentson+"\n"+"标题:"+titleson+"\n"+"业务系统跳转地址:"+urlssson);
				subscribeController.pushMessage(listson.get(0).getSubscribePartnerId(), contentson, titleson, urlssson);
				//推主约人
				String contenta="您已移除"+listson.get(0).getSubscribeUserName()+",简要信息:"+times+","+listsub.get(0).getDeviceType()+listsub.get(0).getDeviceNumber()+"运动,位置:"+listsub.get(0).getDevicePlace();
				String titlea="健身邀请移除通知";
				String urlssa = baseParameter.getPushUrlBegin()+listson.get(0).getSubscribeId()+baseParameter.getPushUrlEnd();// 业务系统跳转地址
				System.out.println("email:"+listsub.get(0).getSubscribeUserEmail()+"\n"+"信息内容:"+contenta+"\n"+"标题:"+titlea+"\n"+"业务系统跳转地址:"+urlssa);
				subscribeController.pushMessage(listsub.get(0).getSubscribeUserEmail(), contenta, titlea, urlssa);
			}
			//消息推送2018-5-16 end
			return Msg.success().add("successMsg", "受邀状态修改成功");
		}else
			return Msg.fail().add("failMsg", "受邀状态修改失败");
	}
//	@RequestMapping(value="/updateSubscribeSonByIdForUserStatus",method=RequestMethod.GET)
//	@ResponseBody
//	public Msg updateByIdForUserStatus(@RequestParam("id")String id,@RequestParam("subscribe_user_status")String subscribe_user_status){
//		SubscribeSon sub = new SubscribeSon();
//		sub.setId(id);
//		sub.setSubscribeUserStatus(subscribe_user_status);
//		System.out.println(subscribeSonService.updateByPrimaryKeySelective(sub));
//		if(subscribeSonService.updateByPrimaryKeySelective(sub) == 1){
//			return Msg.success().add("successMsg", "受邀状态修改成功");
//		}else
//			return Msg.fail().add("failMsg", "受邀状态修改失败");
//	}
	/*
     * 根据设备ID和开始时间和结束时间查询
     */
	@RequestMapping(value="/getSubscribeByTime",method=RequestMethod.GET)
	@ResponseBody
	public  Msg getIdAndTime(@RequestParam("subscribe_begintime")String subscribe_begintime,
							@RequestParam("subscribe_endtime")String subscribe_endtime) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		List list = subscribeService.getTime(df.parse(subscribe_begintime),df.parse(subscribe_endtime));
		if(list.size()!=0){
			return Msg.fail().add("failMsg", list);
		}else{
			return Msg.success().add("successMsg", "时间合理!!!");
		}
	}
	/*
	 * update Flag值0.删除1.没删除
	 */
	@RequestMapping(value="/updateSubscribeSonByIdForUserFlag",method=RequestMethod.GET)
	@ResponseBody
	public Msg updateSubscribeSonByIdForUserFlag(@RequestParam("id")String id){
		SubscribeSon sub = new SubscribeSon();
		sub.setId(id);
		sub.setFlag(0);
		System.out.println(subscribeSonService.updateByPrimaryKeySelective(sub));
		if(subscribeSonService.updateByPrimaryKeySelective(sub) == 1){
			return Msg.success().add("successMsg", "受邀删除成功");
		}else
			return Msg.fail().add("failMsg", "受邀删除失败");
	}

}
