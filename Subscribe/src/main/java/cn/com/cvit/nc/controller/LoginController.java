package cn.com.cvit.nc.controller;




import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.util.DES;


@Controller
public class LoginController {
	
	
	
	
	//登陆解密
	@RequestMapping(value = "/login",method = RequestMethod.GET)
	@ResponseBody
	public Msg login(@RequestParam(value = "subscribe_user_email", defaultValue = "") String email){
		DES _des = new DES("FAWPORTAL");
		String user_code=email;
		String decryptResult = _des.decrypt(user_code);//解密
		return Msg.success().add("success", decryptResult);
		
	}
	

}
