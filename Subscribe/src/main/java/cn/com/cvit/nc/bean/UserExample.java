package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNull() {
            addCriterion("user_code is null");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNotNull() {
            addCriterion("user_code is not null");
            return (Criteria) this;
        }

        public Criteria andUserCodeEqualTo(String value) {
            addCriterion("user_code =", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotEqualTo(String value) {
            addCriterion("user_code <>", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThan(String value) {
            addCriterion("user_code >", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
            addCriterion("user_code >=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThan(String value) {
            addCriterion("user_code <", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThanOrEqualTo(String value) {
            addCriterion("user_code <=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLike(String value) {
            addCriterion("user_code like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotLike(String value) {
            addCriterion("user_code not like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeIn(List<String> values) {
            addCriterion("user_code in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotIn(List<String> values) {
            addCriterion("user_code not in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeBetween(String value1, String value2) {
            addCriterion("user_code between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotBetween(String value1, String value2) {
            addCriterion("user_code not between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNull() {
            addCriterion("user_password is null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNotNull() {
            addCriterion("user_password is not null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordEqualTo(String value) {
            addCriterion("user_password =", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotEqualTo(String value) {
            addCriterion("user_password <>", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThan(String value) {
            addCriterion("user_password >", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("user_password >=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThan(String value) {
            addCriterion("user_password <", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThanOrEqualTo(String value) {
            addCriterion("user_password <=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLike(String value) {
            addCriterion("user_password like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotLike(String value) {
            addCriterion("user_password not like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIn(List<String> values) {
            addCriterion("user_password in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotIn(List<String> values) {
            addCriterion("user_password not in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordBetween(String value1, String value2) {
            addCriterion("user_password between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotBetween(String value1, String value2) {
            addCriterion("user_password not between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserSexIsNull() {
            addCriterion("user_sex is null");
            return (Criteria) this;
        }

        public Criteria andUserSexIsNotNull() {
            addCriterion("user_sex is not null");
            return (Criteria) this;
        }

        public Criteria andUserSexEqualTo(Integer value) {
            addCriterion("user_sex =", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexNotEqualTo(Integer value) {
            addCriterion("user_sex <>", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexGreaterThan(Integer value) {
            addCriterion("user_sex >", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_sex >=", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexLessThan(Integer value) {
            addCriterion("user_sex <", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexLessThanOrEqualTo(Integer value) {
            addCriterion("user_sex <=", value, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexIn(List<Integer> values) {
            addCriterion("user_sex in", values, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexNotIn(List<Integer> values) {
            addCriterion("user_sex not in", values, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexBetween(Integer value1, Integer value2) {
            addCriterion("user_sex between", value1, value2, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserSexNotBetween(Integer value1, Integer value2) {
            addCriterion("user_sex not between", value1, value2, "userSex");
            return (Criteria) this;
        }

        public Criteria andUserEmailIsNull() {
            addCriterion("user_email is null");
            return (Criteria) this;
        }

        public Criteria andUserEmailIsNotNull() {
            addCriterion("user_email is not null");
            return (Criteria) this;
        }

        public Criteria andUserEmailEqualTo(String value) {
            addCriterion("user_email =", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotEqualTo(String value) {
            addCriterion("user_email <>", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailGreaterThan(String value) {
            addCriterion("user_email >", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailGreaterThanOrEqualTo(String value) {
            addCriterion("user_email >=", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLessThan(String value) {
            addCriterion("user_email <", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLessThanOrEqualTo(String value) {
            addCriterion("user_email <=", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLike(String value) {
            addCriterion("user_email like", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotLike(String value) {
            addCriterion("user_email not like", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailIn(List<String> values) {
            addCriterion("user_email in", values, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotIn(List<String> values) {
            addCriterion("user_email not in", values, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailBetween(String value1, String value2) {
            addCriterion("user_email between", value1, value2, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotBetween(String value1, String value2) {
            addCriterion("user_email not between", value1, value2, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneIsNull() {
            addCriterion("user_office_phone is null");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneIsNotNull() {
            addCriterion("user_office_phone is not null");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneEqualTo(String value) {
            addCriterion("user_office_phone =", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneNotEqualTo(String value) {
            addCriterion("user_office_phone <>", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneGreaterThan(String value) {
            addCriterion("user_office_phone >", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneGreaterThanOrEqualTo(String value) {
            addCriterion("user_office_phone >=", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneLessThan(String value) {
            addCriterion("user_office_phone <", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneLessThanOrEqualTo(String value) {
            addCriterion("user_office_phone <=", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneLike(String value) {
            addCriterion("user_office_phone like", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneNotLike(String value) {
            addCriterion("user_office_phone not like", value, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneIn(List<String> values) {
            addCriterion("user_office_phone in", values, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneNotIn(List<String> values) {
            addCriterion("user_office_phone not in", values, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneBetween(String value1, String value2) {
            addCriterion("user_office_phone between", value1, value2, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserOfficePhoneNotBetween(String value1, String value2) {
            addCriterion("user_office_phone not between", value1, value2, "userOfficePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneIsNull() {
            addCriterion("user_mobile_phone is null");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneIsNotNull() {
            addCriterion("user_mobile_phone is not null");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneEqualTo(String value) {
            addCriterion("user_mobile_phone =", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneNotEqualTo(String value) {
            addCriterion("user_mobile_phone <>", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneGreaterThan(String value) {
            addCriterion("user_mobile_phone >", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneGreaterThanOrEqualTo(String value) {
            addCriterion("user_mobile_phone >=", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneLessThan(String value) {
            addCriterion("user_mobile_phone <", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneLessThanOrEqualTo(String value) {
            addCriterion("user_mobile_phone <=", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneLike(String value) {
            addCriterion("user_mobile_phone like", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneNotLike(String value) {
            addCriterion("user_mobile_phone not like", value, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneIn(List<String> values) {
            addCriterion("user_mobile_phone in", values, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneNotIn(List<String> values) {
            addCriterion("user_mobile_phone not in", values, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneBetween(String value1, String value2) {
            addCriterion("user_mobile_phone between", value1, value2, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserMobilePhoneNotBetween(String value1, String value2) {
            addCriterion("user_mobile_phone not between", value1, value2, "userMobilePhone");
            return (Criteria) this;
        }

        public Criteria andUserTeamIsNull() {
            addCriterion("user_team is null");
            return (Criteria) this;
        }

        public Criteria andUserTeamIsNotNull() {
            addCriterion("user_team is not null");
            return (Criteria) this;
        }

        public Criteria andUserTeamEqualTo(String value) {
            addCriterion("user_team =", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamNotEqualTo(String value) {
            addCriterion("user_team <>", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamGreaterThan(String value) {
            addCriterion("user_team >", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamGreaterThanOrEqualTo(String value) {
            addCriterion("user_team >=", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamLessThan(String value) {
            addCriterion("user_team <", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamLessThanOrEqualTo(String value) {
            addCriterion("user_team <=", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamLike(String value) {
            addCriterion("user_team like", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamNotLike(String value) {
            addCriterion("user_team not like", value, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamIn(List<String> values) {
            addCriterion("user_team in", values, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamNotIn(List<String> values) {
            addCriterion("user_team not in", values, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamBetween(String value1, String value2) {
            addCriterion("user_team between", value1, value2, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserTeamNotBetween(String value1, String value2) {
            addCriterion("user_team not between", value1, value2, "userTeam");
            return (Criteria) this;
        }

        public Criteria andUserRoleIsNull() {
            addCriterion("user_role is null");
            return (Criteria) this;
        }

        public Criteria andUserRoleIsNotNull() {
            addCriterion("user_role is not null");
            return (Criteria) this;
        }

        public Criteria andUserRoleEqualTo(String value) {
            addCriterion("user_role =", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleNotEqualTo(String value) {
            addCriterion("user_role <>", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleGreaterThan(String value) {
            addCriterion("user_role >", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleGreaterThanOrEqualTo(String value) {
            addCriterion("user_role >=", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleLessThan(String value) {
            addCriterion("user_role <", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleLessThanOrEqualTo(String value) {
            addCriterion("user_role <=", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleLike(String value) {
            addCriterion("user_role like", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleNotLike(String value) {
            addCriterion("user_role not like", value, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleIn(List<String> values) {
            addCriterion("user_role in", values, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleNotIn(List<String> values) {
            addCriterion("user_role not in", values, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleBetween(String value1, String value2) {
            addCriterion("user_role between", value1, value2, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserRoleNotBetween(String value1, String value2) {
            addCriterion("user_role not between", value1, value2, "userRole");
            return (Criteria) this;
        }

        public Criteria andUserOrderIsNull() {
            addCriterion("user_order is null");
            return (Criteria) this;
        }

        public Criteria andUserOrderIsNotNull() {
            addCriterion("user_order is not null");
            return (Criteria) this;
        }

        public Criteria andUserOrderEqualTo(Integer value) {
            addCriterion("user_order =", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderNotEqualTo(Integer value) {
            addCriterion("user_order <>", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderGreaterThan(Integer value) {
            addCriterion("user_order >", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_order >=", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderLessThan(Integer value) {
            addCriterion("user_order <", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderLessThanOrEqualTo(Integer value) {
            addCriterion("user_order <=", value, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderIn(List<Integer> values) {
            addCriterion("user_order in", values, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderNotIn(List<Integer> values) {
            addCriterion("user_order not in", values, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderBetween(Integer value1, Integer value2) {
            addCriterion("user_order between", value1, value2, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("user_order not between", value1, value2, "userOrder");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateIsNull() {
            addCriterion("user_create_date is null");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateIsNotNull() {
            addCriterion("user_create_date is not null");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateEqualTo(Date value) {
            addCriterion("user_create_date =", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateNotEqualTo(Date value) {
            addCriterion("user_create_date <>", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateGreaterThan(Date value) {
            addCriterion("user_create_date >", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("user_create_date >=", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateLessThan(Date value) {
            addCriterion("user_create_date <", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("user_create_date <=", value, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateIn(List<Date> values) {
            addCriterion("user_create_date in", values, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateNotIn(List<Date> values) {
            addCriterion("user_create_date not in", values, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateBetween(Date value1, Date value2) {
            addCriterion("user_create_date between", value1, value2, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("user_create_date not between", value1, value2, "userCreateDate");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesIsNull() {
            addCriterion("user_modify_times is null");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesIsNotNull() {
            addCriterion("user_modify_times is not null");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesEqualTo(Integer value) {
            addCriterion("user_modify_times =", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesNotEqualTo(Integer value) {
            addCriterion("user_modify_times <>", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesGreaterThan(Integer value) {
            addCriterion("user_modify_times >", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_modify_times >=", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesLessThan(Integer value) {
            addCriterion("user_modify_times <", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesLessThanOrEqualTo(Integer value) {
            addCriterion("user_modify_times <=", value, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesIn(List<Integer> values) {
            addCriterion("user_modify_times in", values, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesNotIn(List<Integer> values) {
            addCriterion("user_modify_times not in", values, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesBetween(Integer value1, Integer value2) {
            addCriterion("user_modify_times between", value1, value2, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserModifyTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("user_modify_times not between", value1, value2, "userModifyTimes");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserIsNull() {
            addCriterion("user_leatest_modify_user is null");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserIsNotNull() {
            addCriterion("user_leatest_modify_user is not null");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserEqualTo(String value) {
            addCriterion("user_leatest_modify_user =", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserNotEqualTo(String value) {
            addCriterion("user_leatest_modify_user <>", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserGreaterThan(String value) {
            addCriterion("user_leatest_modify_user >", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserGreaterThanOrEqualTo(String value) {
            addCriterion("user_leatest_modify_user >=", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserLessThan(String value) {
            addCriterion("user_leatest_modify_user <", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserLessThanOrEqualTo(String value) {
            addCriterion("user_leatest_modify_user <=", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserLike(String value) {
            addCriterion("user_leatest_modify_user like", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserNotLike(String value) {
            addCriterion("user_leatest_modify_user not like", value, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserIn(List<String> values) {
            addCriterion("user_leatest_modify_user in", values, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserNotIn(List<String> values) {
            addCriterion("user_leatest_modify_user not in", values, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserBetween(String value1, String value2) {
            addCriterion("user_leatest_modify_user between", value1, value2, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserLeatestModifyUserNotBetween(String value1, String value2) {
            addCriterion("user_leatest_modify_user not between", value1, value2, "userLeatestModifyUser");
            return (Criteria) this;
        }

        public Criteria andUserStatusIsNull() {
            addCriterion("user_status is null");
            return (Criteria) this;
        }

        public Criteria andUserStatusIsNotNull() {
            addCriterion("user_status is not null");
            return (Criteria) this;
        }

        public Criteria andUserStatusEqualTo(Integer value) {
            addCriterion("user_status =", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusNotEqualTo(Integer value) {
            addCriterion("user_status <>", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusGreaterThan(Integer value) {
            addCriterion("user_status >", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_status >=", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusLessThan(Integer value) {
            addCriterion("user_status <", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusLessThanOrEqualTo(Integer value) {
            addCriterion("user_status <=", value, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusIn(List<Integer> values) {
            addCriterion("user_status in", values, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusNotIn(List<Integer> values) {
            addCriterion("user_status not in", values, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusBetween(Integer value1, Integer value2) {
            addCriterion("user_status between", value1, value2, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("user_status not between", value1, value2, "userStatus");
            return (Criteria) this;
        }

        public Criteria andUserPositionIsNull() {
            addCriterion("user_position is null");
            return (Criteria) this;
        }

        public Criteria andUserPositionIsNotNull() {
            addCriterion("user_position is not null");
            return (Criteria) this;
        }

        public Criteria andUserPositionEqualTo(String value) {
            addCriterion("user_position =", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionNotEqualTo(String value) {
            addCriterion("user_position <>", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionGreaterThan(String value) {
            addCriterion("user_position >", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionGreaterThanOrEqualTo(String value) {
            addCriterion("user_position >=", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionLessThan(String value) {
            addCriterion("user_position <", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionLessThanOrEqualTo(String value) {
            addCriterion("user_position <=", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionLike(String value) {
            addCriterion("user_position like", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionNotLike(String value) {
            addCriterion("user_position not like", value, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionIn(List<String> values) {
            addCriterion("user_position in", values, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionNotIn(List<String> values) {
            addCriterion("user_position not in", values, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionBetween(String value1, String value2) {
            addCriterion("user_position between", value1, value2, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserPositionNotBetween(String value1, String value2) {
            addCriterion("user_position not between", value1, value2, "userPosition");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentIsNull() {
            addCriterion("user_employment is null");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentIsNotNull() {
            addCriterion("user_employment is not null");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentEqualTo(String value) {
            addCriterion("user_employment =", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentNotEqualTo(String value) {
            addCriterion("user_employment <>", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentGreaterThan(String value) {
            addCriterion("user_employment >", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentGreaterThanOrEqualTo(String value) {
            addCriterion("user_employment >=", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentLessThan(String value) {
            addCriterion("user_employment <", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentLessThanOrEqualTo(String value) {
            addCriterion("user_employment <=", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentLike(String value) {
            addCriterion("user_employment like", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentNotLike(String value) {
            addCriterion("user_employment not like", value, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentIn(List<String> values) {
            addCriterion("user_employment in", values, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentNotIn(List<String> values) {
            addCriterion("user_employment not in", values, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentBetween(String value1, String value2) {
            addCriterion("user_employment between", value1, value2, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserEmploymentNotBetween(String value1, String value2) {
            addCriterion("user_employment not between", value1, value2, "userEmployment");
            return (Criteria) this;
        }

        public Criteria andUserPhotoIsNull() {
            addCriterion("user_photo is null");
            return (Criteria) this;
        }

        public Criteria andUserPhotoIsNotNull() {
            addCriterion("user_photo is not null");
            return (Criteria) this;
        }

        public Criteria andUserPhotoEqualTo(String value) {
            addCriterion("user_photo =", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoNotEqualTo(String value) {
            addCriterion("user_photo <>", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoGreaterThan(String value) {
            addCriterion("user_photo >", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoGreaterThanOrEqualTo(String value) {
            addCriterion("user_photo >=", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoLessThan(String value) {
            addCriterion("user_photo <", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoLessThanOrEqualTo(String value) {
            addCriterion("user_photo <=", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoLike(String value) {
            addCriterion("user_photo like", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoNotLike(String value) {
            addCriterion("user_photo not like", value, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoIn(List<String> values) {
            addCriterion("user_photo in", values, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoNotIn(List<String> values) {
            addCriterion("user_photo not in", values, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoBetween(String value1, String value2) {
            addCriterion("user_photo between", value1, value2, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserPhotoNotBetween(String value1, String value2) {
            addCriterion("user_photo not between", value1, value2, "userPhoto");
            return (Criteria) this;
        }

        public Criteria andUserWfroleIsNull() {
            addCriterion("user_wfrole is null");
            return (Criteria) this;
        }

        public Criteria andUserWfroleIsNotNull() {
            addCriterion("user_wfrole is not null");
            return (Criteria) this;
        }

        public Criteria andUserWfroleEqualTo(String value) {
            addCriterion("user_wfrole =", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleNotEqualTo(String value) {
            addCriterion("user_wfrole <>", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleGreaterThan(String value) {
            addCriterion("user_wfrole >", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleGreaterThanOrEqualTo(String value) {
            addCriterion("user_wfrole >=", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleLessThan(String value) {
            addCriterion("user_wfrole <", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleLessThanOrEqualTo(String value) {
            addCriterion("user_wfrole <=", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleLike(String value) {
            addCriterion("user_wfrole like", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleNotLike(String value) {
            addCriterion("user_wfrole not like", value, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleIn(List<String> values) {
            addCriterion("user_wfrole in", values, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleNotIn(List<String> values) {
            addCriterion("user_wfrole not in", values, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleBetween(String value1, String value2) {
            addCriterion("user_wfrole between", value1, value2, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfroleNotBetween(String value1, String value2) {
            addCriterion("user_wfrole not between", value1, value2, "userWfrole");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionIsNull() {
            addCriterion("user_wfposition is null");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionIsNotNull() {
            addCriterion("user_wfposition is not null");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionEqualTo(String value) {
            addCriterion("user_wfposition =", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionNotEqualTo(String value) {
            addCriterion("user_wfposition <>", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionGreaterThan(String value) {
            addCriterion("user_wfposition >", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionGreaterThanOrEqualTo(String value) {
            addCriterion("user_wfposition >=", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionLessThan(String value) {
            addCriterion("user_wfposition <", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionLessThanOrEqualTo(String value) {
            addCriterion("user_wfposition <=", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionLike(String value) {
            addCriterion("user_wfposition like", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionNotLike(String value) {
            addCriterion("user_wfposition not like", value, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionIn(List<String> values) {
            addCriterion("user_wfposition in", values, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionNotIn(List<String> values) {
            addCriterion("user_wfposition not in", values, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionBetween(String value1, String value2) {
            addCriterion("user_wfposition between", value1, value2, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserWfpositionNotBetween(String value1, String value2) {
            addCriterion("user_wfposition not between", value1, value2, "userWfposition");
            return (Criteria) this;
        }

        public Criteria andUserShortnameIsNull() {
            addCriterion("user_shortname is null");
            return (Criteria) this;
        }

        public Criteria andUserShortnameIsNotNull() {
            addCriterion("user_shortname is not null");
            return (Criteria) this;
        }

        public Criteria andUserShortnameEqualTo(String value) {
            addCriterion("user_shortname =", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameNotEqualTo(String value) {
            addCriterion("user_shortname <>", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameGreaterThan(String value) {
            addCriterion("user_shortname >", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameGreaterThanOrEqualTo(String value) {
            addCriterion("user_shortname >=", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameLessThan(String value) {
            addCriterion("user_shortname <", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameLessThanOrEqualTo(String value) {
            addCriterion("user_shortname <=", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameLike(String value) {
            addCriterion("user_shortname like", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameNotLike(String value) {
            addCriterion("user_shortname not like", value, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameIn(List<String> values) {
            addCriterion("user_shortname in", values, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameNotIn(List<String> values) {
            addCriterion("user_shortname not in", values, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameBetween(String value1, String value2) {
            addCriterion("user_shortname between", value1, value2, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserShortnameNotBetween(String value1, String value2) {
            addCriterion("user_shortname not between", value1, value2, "userShortname");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesIsNull() {
            addCriterion("user_pwd_change_times is null");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesIsNotNull() {
            addCriterion("user_pwd_change_times is not null");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesEqualTo(Integer value) {
            addCriterion("user_pwd_change_times =", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesNotEqualTo(Integer value) {
            addCriterion("user_pwd_change_times <>", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesGreaterThan(Integer value) {
            addCriterion("user_pwd_change_times >", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_pwd_change_times >=", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesLessThan(Integer value) {
            addCriterion("user_pwd_change_times <", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesLessThanOrEqualTo(Integer value) {
            addCriterion("user_pwd_change_times <=", value, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesIn(List<Integer> values) {
            addCriterion("user_pwd_change_times in", values, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesNotIn(List<Integer> values) {
            addCriterion("user_pwd_change_times not in", values, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesBetween(Integer value1, Integer value2) {
            addCriterion("user_pwd_change_times between", value1, value2, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("user_pwd_change_times not between", value1, value2, "userPwdChangeTimes");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateIsNull() {
            addCriterion("user_pwd_change_date is null");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateIsNotNull() {
            addCriterion("user_pwd_change_date is not null");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateEqualTo(Date value) {
            addCriterion("user_pwd_change_date =", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateNotEqualTo(Date value) {
            addCriterion("user_pwd_change_date <>", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateGreaterThan(Date value) {
            addCriterion("user_pwd_change_date >", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateGreaterThanOrEqualTo(Date value) {
            addCriterion("user_pwd_change_date >=", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateLessThan(Date value) {
            addCriterion("user_pwd_change_date <", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateLessThanOrEqualTo(Date value) {
            addCriterion("user_pwd_change_date <=", value, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateIn(List<Date> values) {
            addCriterion("user_pwd_change_date in", values, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateNotIn(List<Date> values) {
            addCriterion("user_pwd_change_date not in", values, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateBetween(Date value1, Date value2) {
            addCriterion("user_pwd_change_date between", value1, value2, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserPwdChangeDateNotBetween(Date value1, Date value2) {
            addCriterion("user_pwd_change_date not between", value1, value2, "userPwdChangeDate");
            return (Criteria) this;
        }

        public Criteria andUserTruenameIsNull() {
            addCriterion("user_truename is null");
            return (Criteria) this;
        }

        public Criteria andUserTruenameIsNotNull() {
            addCriterion("user_truename is not null");
            return (Criteria) this;
        }

        public Criteria andUserTruenameEqualTo(String value) {
            addCriterion("user_truename =", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameNotEqualTo(String value) {
            addCriterion("user_truename <>", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameGreaterThan(String value) {
            addCriterion("user_truename >", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameGreaterThanOrEqualTo(String value) {
            addCriterion("user_truename >=", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameLessThan(String value) {
            addCriterion("user_truename <", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameLessThanOrEqualTo(String value) {
            addCriterion("user_truename <=", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameLike(String value) {
            addCriterion("user_truename like", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameNotLike(String value) {
            addCriterion("user_truename not like", value, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameIn(List<String> values) {
            addCriterion("user_truename in", values, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameNotIn(List<String> values) {
            addCriterion("user_truename not in", values, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameBetween(String value1, String value2) {
            addCriterion("user_truename between", value1, value2, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserTruenameNotBetween(String value1, String value2) {
            addCriterion("user_truename not between", value1, value2, "userTruename");
            return (Criteria) this;
        }

        public Criteria andUserMailPathIsNull() {
            addCriterion("user_mail_path is null");
            return (Criteria) this;
        }

        public Criteria andUserMailPathIsNotNull() {
            addCriterion("user_mail_path is not null");
            return (Criteria) this;
        }

        public Criteria andUserMailPathEqualTo(String value) {
            addCriterion("user_mail_path =", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathNotEqualTo(String value) {
            addCriterion("user_mail_path <>", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathGreaterThan(String value) {
            addCriterion("user_mail_path >", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathGreaterThanOrEqualTo(String value) {
            addCriterion("user_mail_path >=", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathLessThan(String value) {
            addCriterion("user_mail_path <", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathLessThanOrEqualTo(String value) {
            addCriterion("user_mail_path <=", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathLike(String value) {
            addCriterion("user_mail_path like", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathNotLike(String value) {
            addCriterion("user_mail_path not like", value, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathIn(List<String> values) {
            addCriterion("user_mail_path in", values, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathNotIn(List<String> values) {
            addCriterion("user_mail_path not in", values, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathBetween(String value1, String value2) {
            addCriterion("user_mail_path between", value1, value2, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailPathNotBetween(String value1, String value2) {
            addCriterion("user_mail_path not between", value1, value2, "userMailPath");
            return (Criteria) this;
        }

        public Criteria andUserMailFileIsNull() {
            addCriterion("user_mail_file is null");
            return (Criteria) this;
        }

        public Criteria andUserMailFileIsNotNull() {
            addCriterion("user_mail_file is not null");
            return (Criteria) this;
        }

        public Criteria andUserMailFileEqualTo(String value) {
            addCriterion("user_mail_file =", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileNotEqualTo(String value) {
            addCriterion("user_mail_file <>", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileGreaterThan(String value) {
            addCriterion("user_mail_file >", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileGreaterThanOrEqualTo(String value) {
            addCriterion("user_mail_file >=", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileLessThan(String value) {
            addCriterion("user_mail_file <", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileLessThanOrEqualTo(String value) {
            addCriterion("user_mail_file <=", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileLike(String value) {
            addCriterion("user_mail_file like", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileNotLike(String value) {
            addCriterion("user_mail_file not like", value, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileIn(List<String> values) {
            addCriterion("user_mail_file in", values, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileNotIn(List<String> values) {
            addCriterion("user_mail_file not in", values, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileBetween(String value1, String value2) {
            addCriterion("user_mail_file between", value1, value2, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailFileNotBetween(String value1, String value2) {
            addCriterion("user_mail_file not between", value1, value2, "userMailFile");
            return (Criteria) this;
        }

        public Criteria andUserMailServerIsNull() {
            addCriterion("user_mail_server is null");
            return (Criteria) this;
        }

        public Criteria andUserMailServerIsNotNull() {
            addCriterion("user_mail_server is not null");
            return (Criteria) this;
        }

        public Criteria andUserMailServerEqualTo(String value) {
            addCriterion("user_mail_server =", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerNotEqualTo(String value) {
            addCriterion("user_mail_server <>", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerGreaterThan(String value) {
            addCriterion("user_mail_server >", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerGreaterThanOrEqualTo(String value) {
            addCriterion("user_mail_server >=", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerLessThan(String value) {
            addCriterion("user_mail_server <", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerLessThanOrEqualTo(String value) {
            addCriterion("user_mail_server <=", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerLike(String value) {
            addCriterion("user_mail_server like", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerNotLike(String value) {
            addCriterion("user_mail_server not like", value, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerIn(List<String> values) {
            addCriterion("user_mail_server in", values, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerNotIn(List<String> values) {
            addCriterion("user_mail_server not in", values, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerBetween(String value1, String value2) {
            addCriterion("user_mail_server between", value1, value2, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserMailServerNotBetween(String value1, String value2) {
            addCriterion("user_mail_server not between", value1, value2, "userMailServer");
            return (Criteria) this;
        }

        public Criteria andUserBackup1IsNull() {
            addCriterion("user_backup_1 is null");
            return (Criteria) this;
        }

        public Criteria andUserBackup1IsNotNull() {
            addCriterion("user_backup_1 is not null");
            return (Criteria) this;
        }

        public Criteria andUserBackup1EqualTo(String value) {
            addCriterion("user_backup_1 =", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1NotEqualTo(String value) {
            addCriterion("user_backup_1 <>", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1GreaterThan(String value) {
            addCriterion("user_backup_1 >", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1GreaterThanOrEqualTo(String value) {
            addCriterion("user_backup_1 >=", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1LessThan(String value) {
            addCriterion("user_backup_1 <", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1LessThanOrEqualTo(String value) {
            addCriterion("user_backup_1 <=", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1Like(String value) {
            addCriterion("user_backup_1 like", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1NotLike(String value) {
            addCriterion("user_backup_1 not like", value, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1In(List<String> values) {
            addCriterion("user_backup_1 in", values, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1NotIn(List<String> values) {
            addCriterion("user_backup_1 not in", values, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1Between(String value1, String value2) {
            addCriterion("user_backup_1 between", value1, value2, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup1NotBetween(String value1, String value2) {
            addCriterion("user_backup_1 not between", value1, value2, "userBackup1");
            return (Criteria) this;
        }

        public Criteria andUserBackup2IsNull() {
            addCriterion("user_backup_2 is null");
            return (Criteria) this;
        }

        public Criteria andUserBackup2IsNotNull() {
            addCriterion("user_backup_2 is not null");
            return (Criteria) this;
        }

        public Criteria andUserBackup2EqualTo(String value) {
            addCriterion("user_backup_2 =", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2NotEqualTo(String value) {
            addCriterion("user_backup_2 <>", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2GreaterThan(String value) {
            addCriterion("user_backup_2 >", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2GreaterThanOrEqualTo(String value) {
            addCriterion("user_backup_2 >=", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2LessThan(String value) {
            addCriterion("user_backup_2 <", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2LessThanOrEqualTo(String value) {
            addCriterion("user_backup_2 <=", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2Like(String value) {
            addCriterion("user_backup_2 like", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2NotLike(String value) {
            addCriterion("user_backup_2 not like", value, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2In(List<String> values) {
            addCriterion("user_backup_2 in", values, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2NotIn(List<String> values) {
            addCriterion("user_backup_2 not in", values, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2Between(String value1, String value2) {
            addCriterion("user_backup_2 between", value1, value2, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andUserBackup2NotBetween(String value1, String value2) {
            addCriterion("user_backup_2 not between", value1, value2, "userBackup2");
            return (Criteria) this;
        }

        public Criteria andSequenceIdIsNull() {
            addCriterion("sequence_id is null");
            return (Criteria) this;
        }

        public Criteria andSequenceIdIsNotNull() {
            addCriterion("sequence_id is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceIdEqualTo(Integer value) {
            addCriterion("sequence_id =", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdNotEqualTo(Integer value) {
            addCriterion("sequence_id <>", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdGreaterThan(Integer value) {
            addCriterion("sequence_id >", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sequence_id >=", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdLessThan(Integer value) {
            addCriterion("sequence_id <", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdLessThanOrEqualTo(Integer value) {
            addCriterion("sequence_id <=", value, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdIn(List<Integer> values) {
            addCriterion("sequence_id in", values, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdNotIn(List<Integer> values) {
            addCriterion("sequence_id not in", values, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdBetween(Integer value1, Integer value2) {
            addCriterion("sequence_id between", value1, value2, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andSequenceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sequence_id not between", value1, value2, "sequenceId");
            return (Criteria) this;
        }

        public Criteria andUserCompanyIsNull() {
            addCriterion("user_company is null");
            return (Criteria) this;
        }

        public Criteria andUserCompanyIsNotNull() {
            addCriterion("user_company is not null");
            return (Criteria) this;
        }

        public Criteria andUserCompanyEqualTo(String value) {
            addCriterion("user_company =", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyNotEqualTo(String value) {
            addCriterion("user_company <>", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyGreaterThan(String value) {
            addCriterion("user_company >", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("user_company >=", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyLessThan(String value) {
            addCriterion("user_company <", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyLessThanOrEqualTo(String value) {
            addCriterion("user_company <=", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyLike(String value) {
            addCriterion("user_company like", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyNotLike(String value) {
            addCriterion("user_company not like", value, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyIn(List<String> values) {
            addCriterion("user_company in", values, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyNotIn(List<String> values) {
            addCriterion("user_company not in", values, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyBetween(String value1, String value2) {
            addCriterion("user_company between", value1, value2, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserCompanyNotBetween(String value1, String value2) {
            addCriterion("user_company not between", value1, value2, "userCompany");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIsNull() {
            addCriterion("user_pinyin is null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIsNotNull() {
            addCriterion("user_pinyin is not null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinEqualTo(String value) {
            addCriterion("user_pinyin =", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotEqualTo(String value) {
            addCriterion("user_pinyin <>", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinGreaterThan(String value) {
            addCriterion("user_pinyin >", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinGreaterThanOrEqualTo(String value) {
            addCriterion("user_pinyin >=", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLessThan(String value) {
            addCriterion("user_pinyin <", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLessThanOrEqualTo(String value) {
            addCriterion("user_pinyin <=", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLike(String value) {
            addCriterion("user_pinyin like", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotLike(String value) {
            addCriterion("user_pinyin not like", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIn(List<String> values) {
            addCriterion("user_pinyin in", values, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotIn(List<String> values) {
            addCriterion("user_pinyin not in", values, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinBetween(String value1, String value2) {
            addCriterion("user_pinyin between", value1, value2, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotBetween(String value1, String value2) {
            addCriterion("user_pinyin not between", value1, value2, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersIsNull() {
            addCriterion("user_pinyin_letters is null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersIsNotNull() {
            addCriterion("user_pinyin_letters is not null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersEqualTo(String value) {
            addCriterion("user_pinyin_letters =", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersNotEqualTo(String value) {
            addCriterion("user_pinyin_letters <>", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersGreaterThan(String value) {
            addCriterion("user_pinyin_letters >", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersGreaterThanOrEqualTo(String value) {
            addCriterion("user_pinyin_letters >=", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersLessThan(String value) {
            addCriterion("user_pinyin_letters <", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersLessThanOrEqualTo(String value) {
            addCriterion("user_pinyin_letters <=", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersLike(String value) {
            addCriterion("user_pinyin_letters like", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersNotLike(String value) {
            addCriterion("user_pinyin_letters not like", value, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersIn(List<String> values) {
            addCriterion("user_pinyin_letters in", values, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersNotIn(List<String> values) {
            addCriterion("user_pinyin_letters not in", values, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersBetween(String value1, String value2) {
            addCriterion("user_pinyin_letters between", value1, value2, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLettersNotBetween(String value1, String value2) {
            addCriterion("user_pinyin_letters not between", value1, value2, "userPinyinLetters");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterIsNull() {
            addCriterion("user_pinyin_firstletter is null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterIsNotNull() {
            addCriterion("user_pinyin_firstletter is not null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterEqualTo(String value) {
            addCriterion("user_pinyin_firstletter =", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterNotEqualTo(String value) {
            addCriterion("user_pinyin_firstletter <>", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterGreaterThan(String value) {
            addCriterion("user_pinyin_firstletter >", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterGreaterThanOrEqualTo(String value) {
            addCriterion("user_pinyin_firstletter >=", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterLessThan(String value) {
            addCriterion("user_pinyin_firstletter <", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterLessThanOrEqualTo(String value) {
            addCriterion("user_pinyin_firstletter <=", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterLike(String value) {
            addCriterion("user_pinyin_firstletter like", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterNotLike(String value) {
            addCriterion("user_pinyin_firstletter not like", value, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterIn(List<String> values) {
            addCriterion("user_pinyin_firstletter in", values, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterNotIn(List<String> values) {
            addCriterion("user_pinyin_firstletter not in", values, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterBetween(String value1, String value2) {
            addCriterion("user_pinyin_firstletter between", value1, value2, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andUserPinyinFirstletterNotBetween(String value1, String value2) {
            addCriterion("user_pinyin_firstletter not between", value1, value2, "userPinyinFirstletter");
            return (Criteria) this;
        }

        public Criteria andImKeyIsNull() {
            addCriterion("im_key is null");
            return (Criteria) this;
        }

        public Criteria andImKeyIsNotNull() {
            addCriterion("im_key is not null");
            return (Criteria) this;
        }

        public Criteria andImKeyEqualTo(String value) {
            addCriterion("im_key =", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyNotEqualTo(String value) {
            addCriterion("im_key <>", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyGreaterThan(String value) {
            addCriterion("im_key >", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyGreaterThanOrEqualTo(String value) {
            addCriterion("im_key >=", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyLessThan(String value) {
            addCriterion("im_key <", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyLessThanOrEqualTo(String value) {
            addCriterion("im_key <=", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyLike(String value) {
            addCriterion("im_key like", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyNotLike(String value) {
            addCriterion("im_key not like", value, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyIn(List<String> values) {
            addCriterion("im_key in", values, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyNotIn(List<String> values) {
            addCriterion("im_key not in", values, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyBetween(String value1, String value2) {
            addCriterion("im_key between", value1, value2, "imKey");
            return (Criteria) this;
        }

        public Criteria andImKeyNotBetween(String value1, String value2) {
            addCriterion("im_key not between", value1, value2, "imKey");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNull() {
            addCriterion("id_number is null");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNotNull() {
            addCriterion("id_number is not null");
            return (Criteria) this;
        }

        public Criteria andIdNumberEqualTo(String value) {
            addCriterion("id_number =", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotEqualTo(String value) {
            addCriterion("id_number <>", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThan(String value) {
            addCriterion("id_number >", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThanOrEqualTo(String value) {
            addCriterion("id_number >=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThan(String value) {
            addCriterion("id_number <", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThanOrEqualTo(String value) {
            addCriterion("id_number <=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLike(String value) {
            addCriterion("id_number like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotLike(String value) {
            addCriterion("id_number not like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberIn(List<String> values) {
            addCriterion("id_number in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotIn(List<String> values) {
            addCriterion("id_number not in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberBetween(String value1, String value2) {
            addCriterion("id_number between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotBetween(String value1, String value2) {
            addCriterion("id_number not between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameIsNull() {
            addCriterion("myleader_name is null");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameIsNotNull() {
            addCriterion("myleader_name is not null");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameEqualTo(String value) {
            addCriterion("myleader_name =", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameNotEqualTo(String value) {
            addCriterion("myleader_name <>", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameGreaterThan(String value) {
            addCriterion("myleader_name >", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameGreaterThanOrEqualTo(String value) {
            addCriterion("myleader_name >=", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameLessThan(String value) {
            addCriterion("myleader_name <", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameLessThanOrEqualTo(String value) {
            addCriterion("myleader_name <=", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameLike(String value) {
            addCriterion("myleader_name like", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameNotLike(String value) {
            addCriterion("myleader_name not like", value, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameIn(List<String> values) {
            addCriterion("myleader_name in", values, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameNotIn(List<String> values) {
            addCriterion("myleader_name not in", values, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameBetween(String value1, String value2) {
            addCriterion("myleader_name between", value1, value2, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderNameNotBetween(String value1, String value2) {
            addCriterion("myleader_name not between", value1, value2, "myleaderName");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdIsNull() {
            addCriterion("myleader_id is null");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdIsNotNull() {
            addCriterion("myleader_id is not null");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdEqualTo(String value) {
            addCriterion("myleader_id =", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdNotEqualTo(String value) {
            addCriterion("myleader_id <>", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdGreaterThan(String value) {
            addCriterion("myleader_id >", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdGreaterThanOrEqualTo(String value) {
            addCriterion("myleader_id >=", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdLessThan(String value) {
            addCriterion("myleader_id <", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdLessThanOrEqualTo(String value) {
            addCriterion("myleader_id <=", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdLike(String value) {
            addCriterion("myleader_id like", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdNotLike(String value) {
            addCriterion("myleader_id not like", value, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdIn(List<String> values) {
            addCriterion("myleader_id in", values, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdNotIn(List<String> values) {
            addCriterion("myleader_id not in", values, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdBetween(String value1, String value2) {
            addCriterion("myleader_id between", value1, value2, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andMyleaderIdNotBetween(String value1, String value2) {
            addCriterion("myleader_id not between", value1, value2, "myleaderId");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelIsNull() {
            addCriterion("security_level is null");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelIsNotNull() {
            addCriterion("security_level is not null");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelEqualTo(Integer value) {
            addCriterion("security_level =", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelNotEqualTo(Integer value) {
            addCriterion("security_level <>", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelGreaterThan(Integer value) {
            addCriterion("security_level >", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("security_level >=", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelLessThan(Integer value) {
            addCriterion("security_level <", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelLessThanOrEqualTo(Integer value) {
            addCriterion("security_level <=", value, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelIn(List<Integer> values) {
            addCriterion("security_level in", values, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelNotIn(List<Integer> values) {
            addCriterion("security_level not in", values, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelBetween(Integer value1, Integer value2) {
            addCriterion("security_level between", value1, value2, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andSecurityLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("security_level not between", value1, value2, "securityLevel");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneIsNull() {
            addCriterion("user_other_phone is null");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneIsNotNull() {
            addCriterion("user_other_phone is not null");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneEqualTo(String value) {
            addCriterion("user_other_phone =", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneNotEqualTo(String value) {
            addCriterion("user_other_phone <>", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneGreaterThan(String value) {
            addCriterion("user_other_phone >", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("user_other_phone >=", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneLessThan(String value) {
            addCriterion("user_other_phone <", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneLessThanOrEqualTo(String value) {
            addCriterion("user_other_phone <=", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneLike(String value) {
            addCriterion("user_other_phone like", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneNotLike(String value) {
            addCriterion("user_other_phone not like", value, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneIn(List<String> values) {
            addCriterion("user_other_phone in", values, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneNotIn(List<String> values) {
            addCriterion("user_other_phone not in", values, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneBetween(String value1, String value2) {
            addCriterion("user_other_phone between", value1, value2, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserOtherPhoneNotBetween(String value1, String value2) {
            addCriterion("user_other_phone not between", value1, value2, "userOtherPhone");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayIsNull() {
            addCriterion("user_birthday is null");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayIsNotNull() {
            addCriterion("user_birthday is not null");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayEqualTo(Date value) {
            addCriterion("user_birthday =", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayNotEqualTo(Date value) {
            addCriterion("user_birthday <>", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayGreaterThan(Date value) {
            addCriterion("user_birthday >", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("user_birthday >=", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayLessThan(Date value) {
            addCriterion("user_birthday <", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("user_birthday <=", value, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayIn(List<Date> values) {
            addCriterion("user_birthday in", values, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayNotIn(List<Date> values) {
            addCriterion("user_birthday not in", values, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayBetween(Date value1, Date value2) {
            addCriterion("user_birthday between", value1, value2, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andUserBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("user_birthday not between", value1, value2, "userBirthday");
            return (Criteria) this;
        }

        public Criteria andHrEmplidIsNull() {
            addCriterion("hr_emplid is null");
            return (Criteria) this;
        }

        public Criteria andHrEmplidIsNotNull() {
            addCriterion("hr_emplid is not null");
            return (Criteria) this;
        }

        public Criteria andHrEmplidEqualTo(String value) {
            addCriterion("hr_emplid =", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidNotEqualTo(String value) {
            addCriterion("hr_emplid <>", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidGreaterThan(String value) {
            addCriterion("hr_emplid >", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidGreaterThanOrEqualTo(String value) {
            addCriterion("hr_emplid >=", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidLessThan(String value) {
            addCriterion("hr_emplid <", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidLessThanOrEqualTo(String value) {
            addCriterion("hr_emplid <=", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidLike(String value) {
            addCriterion("hr_emplid like", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidNotLike(String value) {
            addCriterion("hr_emplid not like", value, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidIn(List<String> values) {
            addCriterion("hr_emplid in", values, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidNotIn(List<String> values) {
            addCriterion("hr_emplid not in", values, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidBetween(String value1, String value2) {
            addCriterion("hr_emplid between", value1, value2, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrEmplidNotBetween(String value1, String value2) {
            addCriterion("hr_emplid not between", value1, value2, "hrEmplid");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnIsNull() {
            addCriterion("hr_admin_rank_chn is null");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnIsNotNull() {
            addCriterion("hr_admin_rank_chn is not null");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnEqualTo(String value) {
            addCriterion("hr_admin_rank_chn =", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnNotEqualTo(String value) {
            addCriterion("hr_admin_rank_chn <>", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnGreaterThan(String value) {
            addCriterion("hr_admin_rank_chn >", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnGreaterThanOrEqualTo(String value) {
            addCriterion("hr_admin_rank_chn >=", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnLessThan(String value) {
            addCriterion("hr_admin_rank_chn <", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnLessThanOrEqualTo(String value) {
            addCriterion("hr_admin_rank_chn <=", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnLike(String value) {
            addCriterion("hr_admin_rank_chn like", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnNotLike(String value) {
            addCriterion("hr_admin_rank_chn not like", value, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnIn(List<String> values) {
            addCriterion("hr_admin_rank_chn in", values, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnNotIn(List<String> values) {
            addCriterion("hr_admin_rank_chn not in", values, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnBetween(String value1, String value2) {
            addCriterion("hr_admin_rank_chn between", value1, value2, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrAdminRankChnNotBetween(String value1, String value2) {
            addCriterion("hr_admin_rank_chn not between", value1, value2, "hrAdminRankChn");
            return (Criteria) this;
        }

        public Criteria andHrDeptidIsNull() {
            addCriterion("hr_deptid is null");
            return (Criteria) this;
        }

        public Criteria andHrDeptidIsNotNull() {
            addCriterion("hr_deptid is not null");
            return (Criteria) this;
        }

        public Criteria andHrDeptidEqualTo(String value) {
            addCriterion("hr_deptid =", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidNotEqualTo(String value) {
            addCriterion("hr_deptid <>", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidGreaterThan(String value) {
            addCriterion("hr_deptid >", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidGreaterThanOrEqualTo(String value) {
            addCriterion("hr_deptid >=", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidLessThan(String value) {
            addCriterion("hr_deptid <", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidLessThanOrEqualTo(String value) {
            addCriterion("hr_deptid <=", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidLike(String value) {
            addCriterion("hr_deptid like", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidNotLike(String value) {
            addCriterion("hr_deptid not like", value, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidIn(List<String> values) {
            addCriterion("hr_deptid in", values, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidNotIn(List<String> values) {
            addCriterion("hr_deptid not in", values, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidBetween(String value1, String value2) {
            addCriterion("hr_deptid between", value1, value2, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrDeptidNotBetween(String value1, String value2) {
            addCriterion("hr_deptid not between", value1, value2, "hrDeptid");
            return (Criteria) this;
        }

        public Criteria andHrEmail1IsNull() {
            addCriterion("hr_email1 is null");
            return (Criteria) this;
        }

        public Criteria andHrEmail1IsNotNull() {
            addCriterion("hr_email1 is not null");
            return (Criteria) this;
        }

        public Criteria andHrEmail1EqualTo(String value) {
            addCriterion("hr_email1 =", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1NotEqualTo(String value) {
            addCriterion("hr_email1 <>", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1GreaterThan(String value) {
            addCriterion("hr_email1 >", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1GreaterThanOrEqualTo(String value) {
            addCriterion("hr_email1 >=", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1LessThan(String value) {
            addCriterion("hr_email1 <", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1LessThanOrEqualTo(String value) {
            addCriterion("hr_email1 <=", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1Like(String value) {
            addCriterion("hr_email1 like", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1NotLike(String value) {
            addCriterion("hr_email1 not like", value, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1In(List<String> values) {
            addCriterion("hr_email1 in", values, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1NotIn(List<String> values) {
            addCriterion("hr_email1 not in", values, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1Between(String value1, String value2) {
            addCriterion("hr_email1 between", value1, value2, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail1NotBetween(String value1, String value2) {
            addCriterion("hr_email1 not between", value1, value2, "hrEmail1");
            return (Criteria) this;
        }

        public Criteria andHrEmail2IsNull() {
            addCriterion("hr_email2 is null");
            return (Criteria) this;
        }

        public Criteria andHrEmail2IsNotNull() {
            addCriterion("hr_email2 is not null");
            return (Criteria) this;
        }

        public Criteria andHrEmail2EqualTo(String value) {
            addCriterion("hr_email2 =", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2NotEqualTo(String value) {
            addCriterion("hr_email2 <>", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2GreaterThan(String value) {
            addCriterion("hr_email2 >", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2GreaterThanOrEqualTo(String value) {
            addCriterion("hr_email2 >=", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2LessThan(String value) {
            addCriterion("hr_email2 <", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2LessThanOrEqualTo(String value) {
            addCriterion("hr_email2 <=", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2Like(String value) {
            addCriterion("hr_email2 like", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2NotLike(String value) {
            addCriterion("hr_email2 not like", value, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2In(List<String> values) {
            addCriterion("hr_email2 in", values, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2NotIn(List<String> values) {
            addCriterion("hr_email2 not in", values, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2Between(String value1, String value2) {
            addCriterion("hr_email2 between", value1, value2, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail2NotBetween(String value1, String value2) {
            addCriterion("hr_email2 not between", value1, value2, "hrEmail2");
            return (Criteria) this;
        }

        public Criteria andHrEmail3IsNull() {
            addCriterion("hr_email3 is null");
            return (Criteria) this;
        }

        public Criteria andHrEmail3IsNotNull() {
            addCriterion("hr_email3 is not null");
            return (Criteria) this;
        }

        public Criteria andHrEmail3EqualTo(String value) {
            addCriterion("hr_email3 =", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3NotEqualTo(String value) {
            addCriterion("hr_email3 <>", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3GreaterThan(String value) {
            addCriterion("hr_email3 >", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3GreaterThanOrEqualTo(String value) {
            addCriterion("hr_email3 >=", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3LessThan(String value) {
            addCriterion("hr_email3 <", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3LessThanOrEqualTo(String value) {
            addCriterion("hr_email3 <=", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3Like(String value) {
            addCriterion("hr_email3 like", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3NotLike(String value) {
            addCriterion("hr_email3 not like", value, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3In(List<String> values) {
            addCriterion("hr_email3 in", values, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3NotIn(List<String> values) {
            addCriterion("hr_email3 not in", values, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3Between(String value1, String value2) {
            addCriterion("hr_email3 between", value1, value2, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrEmail3NotBetween(String value1, String value2) {
            addCriterion("hr_email3 not between", value1, value2, "hrEmail3");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidIsNull() {
            addCriterion("hr_old_emoid is null");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidIsNotNull() {
            addCriterion("hr_old_emoid is not null");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidEqualTo(String value) {
            addCriterion("hr_old_emoid =", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidNotEqualTo(String value) {
            addCriterion("hr_old_emoid <>", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidGreaterThan(String value) {
            addCriterion("hr_old_emoid >", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidGreaterThanOrEqualTo(String value) {
            addCriterion("hr_old_emoid >=", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidLessThan(String value) {
            addCriterion("hr_old_emoid <", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidLessThanOrEqualTo(String value) {
            addCriterion("hr_old_emoid <=", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidLike(String value) {
            addCriterion("hr_old_emoid like", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidNotLike(String value) {
            addCriterion("hr_old_emoid not like", value, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidIn(List<String> values) {
            addCriterion("hr_old_emoid in", values, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidNotIn(List<String> values) {
            addCriterion("hr_old_emoid not in", values, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidBetween(String value1, String value2) {
            addCriterion("hr_old_emoid between", value1, value2, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrOldEmoidNotBetween(String value1, String value2) {
            addCriterion("hr_old_emoid not between", value1, value2, "hrOldEmoid");
            return (Criteria) this;
        }

        public Criteria andHrStatusIsNull() {
            addCriterion("hr_status is null");
            return (Criteria) this;
        }

        public Criteria andHrStatusIsNotNull() {
            addCriterion("hr_status is not null");
            return (Criteria) this;
        }

        public Criteria andHrStatusEqualTo(String value) {
            addCriterion("hr_status =", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusNotEqualTo(String value) {
            addCriterion("hr_status <>", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusGreaterThan(String value) {
            addCriterion("hr_status >", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusGreaterThanOrEqualTo(String value) {
            addCriterion("hr_status >=", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusLessThan(String value) {
            addCriterion("hr_status <", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusLessThanOrEqualTo(String value) {
            addCriterion("hr_status <=", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusLike(String value) {
            addCriterion("hr_status like", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusNotLike(String value) {
            addCriterion("hr_status not like", value, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusIn(List<String> values) {
            addCriterion("hr_status in", values, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusNotIn(List<String> values) {
            addCriterion("hr_status not in", values, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusBetween(String value1, String value2) {
            addCriterion("hr_status between", value1, value2, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andHrStatusNotBetween(String value1, String value2) {
            addCriterion("hr_status not between", value1, value2, "hrStatus");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckIsNull() {
            addCriterion("user_mobile_check is null");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckIsNotNull() {
            addCriterion("user_mobile_check is not null");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckEqualTo(Integer value) {
            addCriterion("user_mobile_check =", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckNotEqualTo(Integer value) {
            addCriterion("user_mobile_check <>", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckGreaterThan(Integer value) {
            addCriterion("user_mobile_check >", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_mobile_check >=", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckLessThan(Integer value) {
            addCriterion("user_mobile_check <", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckLessThanOrEqualTo(Integer value) {
            addCriterion("user_mobile_check <=", value, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckIn(List<Integer> values) {
            addCriterion("user_mobile_check in", values, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckNotIn(List<Integer> values) {
            addCriterion("user_mobile_check not in", values, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckBetween(Integer value1, Integer value2) {
            addCriterion("user_mobile_check between", value1, value2, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andUserMobileCheckNotBetween(Integer value1, Integer value2) {
            addCriterion("user_mobile_check not between", value1, value2, "userMobileCheck");
            return (Criteria) this;
        }

        public Criteria andIsAdminIsNull() {
            addCriterion("is_admin is null");
            return (Criteria) this;
        }

        public Criteria andIsAdminIsNotNull() {
            addCriterion("is_admin is not null");
            return (Criteria) this;
        }

        public Criteria andIsAdminEqualTo(Integer value) {
            addCriterion("is_admin =", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotEqualTo(Integer value) {
            addCriterion("is_admin <>", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminGreaterThan(Integer value) {
            addCriterion("is_admin >", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_admin >=", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminLessThan(Integer value) {
            addCriterion("is_admin <", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminLessThanOrEqualTo(Integer value) {
            addCriterion("is_admin <=", value, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminIn(List<Integer> values) {
            addCriterion("is_admin in", values, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotIn(List<Integer> values) {
            addCriterion("is_admin not in", values, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminBetween(Integer value1, Integer value2) {
            addCriterion("is_admin between", value1, value2, "isAdmin");
            return (Criteria) this;
        }

        public Criteria andIsAdminNotBetween(Integer value1, Integer value2) {
            addCriterion("is_admin not between", value1, value2, "isAdmin");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}