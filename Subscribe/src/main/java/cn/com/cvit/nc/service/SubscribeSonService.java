package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.SubscribeSon;
import cn.com.cvit.nc.dao.SubscribeSonMapper;

@Service
public class SubscribeSonService {
	
	@Autowired
	private SubscribeSonMapper subscribeSonMapper;

	/*
     * 查询所有受邀信息
     * List<SubscribeSon> selectByExampleWithName(SubscribeSonExample example);
     */
	public List<SubscribeSon> selectByExampleWithName(){
		return subscribeSonMapper.selectByExampleWithName(null);
	}
	/*
     * 根据subscribe_id查询我的邀请
     * List<SubscribeSon> selectBySubscribeId(String subscribe_id);
     */
	public List<SubscribeSon> selectBySubscribeId(String subscribe_id){
		return subscribeSonMapper.selectBySubscribeId(subscribe_id);
	}
	/*
     * 根据subscribe_id查询我的邀请--只查询subscribe_user_status=  0   1    2018-5-16--消息推送
     */
	public List<SubscribeSon> selectBySubscribeIdForPushMessage(String subscribe_id){
		return subscribeSonMapper.selectBySubscribeIdForPushMessage(subscribe_id);
	}
	//根据subscribe_id查询我的邀请-筛选出0待确定1接受2018-4-20
	public List<SubscribeSon> selectBySubscribeIdWebService(String subscribe_id){
		return subscribeSonMapper.selectBySubscribeIdWebService(subscribe_id);
	}
	/*
     * 根据subscribe_partner_id(email)和subscribe_user_name查询我的受邀
     * List<SubscribeSon> selectByEmailAndName(String email,String name);
     */
	public List<SubscribeSon> selectByEmailAndName(String email,String name){
		return subscribeSonMapper.selectByEmailAndName(email,name);
	}
	
	/*
	 * selectBySubscribeSonId
	 */
	public List<SubscribeSon> selectBySubscribeSonId(String id){
		return subscribeSonMapper.selectBySubscribeSonId(id);
	}
	/*
     * 插入一条数据
     * int insertSelective(SubscribeSon record);
     */
	public int insertSelective(SubscribeSon record){
		return subscribeSonMapper.insertSelective(record);
	}
	
	 /*
     * 修改
     * update状态1.正常结束2.撤销（撤销原因）
     * update受邀状态1、接受2、拒绝（拒绝原因）
     * int updateByPrimaryKeySelective(SubscribeSon record);
     */
	public int updateByPrimaryKeySelective(SubscribeSon record){
		return subscribeSonMapper.updateByPrimaryKeySelective(record);
	}
    //修改状态2018-5-16
	public int updateBySubscribeId(SubscribeSon record){
		return subscribeSonMapper.updateBySubscribeId(record);
	}
	//
	public int updateBySubscribeIdTwo(SubscribeSon record){
		return subscribeSonMapper.updateBySubscribeIdTwo(record);
	}
	/*
     * web、service提示是否有未处理受邀信息2018-4-12
     */
	public List<SubscribeSon> selectSubscribeSonStatusByEmailWebService(SubscribeSon record){
		return subscribeSonMapper.selectSubscribeSonStatusByEmailWebService(record);
	}
	
}
