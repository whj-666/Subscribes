package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeExample;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SubscribeMapper {
    long countByExample(SubscribeExample example);

    int deleteByExample(SubscribeExample example);

    int deleteByPrimaryKey(String id);

    int insert(Subscribe record);
    //看板
    List<Subscribe> selectSeeBoard(Subscribe subscribe);
    
    //
    List<Subscribe> selectBase(SubscribeExample example);
	//查询所有1
    List<Subscribe> selectByExample(SubscribeExample example);
    //查询所有Flag过滤
    List<Subscribe> selectByExampleAndFlag(SubscribeExample example);
    //根据工号、姓名、开始时间、结束时间查询预约信息（参数可以为空）flag过滤
    List<Subscribe> selectByExampleEmailOrNameOrTime(String email, String name,String begintime,String endtime,String deviceId);
	//根据工号和姓名查询，未过滤，后台用
	List<Subscribe> selectByExampleEmailAndName(String email, String name);
	//根据工号和姓名查询**已过虑，根据结束时间，过滤掉当前时间之前的预约信息
	List<Subscribe> selectByExampleEmailAndNameAndTime(String email, String name);
	//根据设备ID查询,status过滤
	List<Subscribe> selectByIdAndStatus(String id);
	//根据设备ID查询
	List<Subscribe> selectByExampleId(String id);
	//根据设备ID查询,查询 2通过的预约2018-5-3
	List<Subscribe> selectSubscribeIdByDeviceId(String id);
	//根据设备ID查询,查询 4撤销的预约2018-5-3
	List<Subscribe> selectSubscribeIdByDeviceIdTwo(String id);
	//根据类别ID查指定类别下的预约信息
	List<Subscribe> selectDeviceCatagorySubscribeAll(String deviceCatagoryId);
	//根据预约id查询***带设备信息
	List<Subscribe> selectByPrimaryKeyWithDevice(String id);
	//根据设备ID和时间查询
	List<Subscribe> selectByExampleIdAndTime(String id, Date begin,Date end);
	//根据时间查询
	List<Subscribe> selectByTime(Date begin,Date end);
	//web、service提示是否有进行中信息
	List<Subscribe> selectStatusByEmailWebService(Subscribe record);
	//web、service通过预约人email查询今天预约简略信息-推送2018-4-16
	List<Subscribe> selectMessageByEmailWebService(Subscribe record);
	//web、service查询今天预约简略信息-推送2018-5-15
	List<Subscribe> selectToDayMessageWebService();
	//根据时间查数据2018-5-16
	List<Subscribe> selectSubscribeForAll();
	//插入一条记录
    int insertSelective(Subscribe record);

    Subscribe selectByPrimaryKey(String id);
    int updateSubscribeStatus(String email,String name,String status);
    //根据email查询过时数据2018-5-17
    List<Subscribe> selectSubscribeByEmailForAfterTime(String email);
    //根据时间修改2018-5-16
    int updateSubscribeStatusAll();
    
    int updateByExampleSelective(@Param("record") Subscribe record, @Param("example") SubscribeExample example);

    int updateByExample(@Param("record") Subscribe record, @Param("example") SubscribeExample example);

    int updateByPrimaryKeySelective(Subscribe record);
    //假删除、批量
    int updateFlag(String [] id);
    
    //根据设备Id修改预约状态，原因等2018-5-3
    int updateByDeviceId(Subscribe record);
    //根据设备Id修改预约状态，原因等2018-5-3
    int updateByDeviceIdTwo(Subscribe record);

    int updateByPrimaryKey(Subscribe record);
}