package cn.com.cvit.nc.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.service.DeviceCatagoryService;
import cn.com.cvit.nc.service.DeviceService;
import cn.com.cvit.nc.service.SubscribeService;
import cn.com.cvit.nc.util.FileUpload;

/**
 * 处理设备有关请求
 * */
@Controller
public class DeviceController {
	
	@Autowired
	DeviceService  deviceService;
	
	@Autowired
	SubscribeController subscribeController;
	
	@Autowired
	SubscribeService subscribeService;
	
	@Autowired
	DeviceCatagoryService deviceCatagoryService;
	
	/**
	 * 查询所有设备（分页）
	 * @return
	 * */
	@RequestMapping("/getAllDevice")
	@ResponseBody
	public Msg getDevice(@RequestParam(value = "pn", defaultValue = "1") Integer pn,@RequestParam(value = "nr", defaultValue = "10") Integer nr){
		PageHelper.startPage(pn, nr);//分页查询
		List<Device> de = deviceService.getAll();
		PageInfo<Device> page = new PageInfo<Device>(de, nr);
		return Msg.success().add("pageInfo", page);
	}
	
	
	/**
	 * 查询所有Flag=1的设备（分页）
	 * @return
	 * */ 
	@RequestMapping("/getAllDeviceByFlag")
	@ResponseBody
	public Msg getDeviceByFlag(@RequestParam(value = "pn", defaultValue = "1") Integer pn,@RequestParam(value = "nr", defaultValue = "10") Integer nr){
		PageHelper.startPage(pn, nr);//分页查询
		int flag=1;
		List<Device> deviceFlag = deviceService.getDeviceFlag(flag);
		PageInfo<Device> page = new PageInfo<Device>(deviceFlag, nr);
		return Msg.success().add("pageInfo", page);
	}
	
	/**
	 * 查询所有Flag=1的设备（分页）
	 * @return
	 * */ 
	@RequestMapping("/getAllDeviceByFlag1")
	@ResponseBody
	public List getDeviceByFlag1(){
		int flag=1;
		List<Device> deviceFlag = deviceService.getDeviceFlag(flag);
		return deviceFlag;
	}
	
	/**
	 * 查询所有Flag=1的设备(前台)
	 * @return
	 * */ 
	@RequestMapping("/getAllDeviceByFlagQ")
	@ResponseBody
	public Msg getDeviceByFlagQ(){
		int flag=1;
		subscribeController.selectSubscribeForAll();
		List<Device> deviceFlag = deviceService.getDeviceFlag(flag);
		return Msg.success().add("DeviceFlag", deviceFlag);
	}
	
	
	/**
	 * 根据类别id查询所属设备信息
	 * 转成Json返回
	 * @return
	 */
	@RequestMapping("/getDeviceListByCatagoryId")
	@ResponseBody
	public Msg getDeviceListByCatagoryId(@RequestParam("CatagoryId")String id){
		List<Device> deviceC = deviceService.getDeviceCatagoryId(id);
		return Msg.success().add("deviceClass", deviceC);
	}
	@RequestMapping("/selectByDeviceClassIdOrNull")
	@ResponseBody
	public Msg selectByDeviceClassIdOrNull(@RequestParam(value = "pn", defaultValue = "1") Integer pn,@RequestParam(value = "nr", defaultValue = "10") Integer nr,@RequestParam(value = "id", defaultValue = "")String id){
		PageHelper.startPage(pn, nr);//分页查询
		Device de = new Device();
		de.setDeviceClass(id);
		List<Device> list = deviceService.selectByDeviceClassIdOrNull(de);
		PageInfo<Device> page = new PageInfo<Device>(list, nr);
		return Msg.success().add("pageInfo", page);
	}
	/**
	 * 根据类别id查询所属设备信息(后台管理)
	 * 转成Json返回
	 * @return
	 */
	@RequestMapping("/getDeviceListByCatagoryId1")
	@ResponseBody
	public List<Device> getDeviceListByCatagoryId1(@RequestParam("CatagoryId")String id){
		List<Device> deviceC = deviceService.getDeviceCatagoryId(id);
		return deviceC;
	}
	
	
	
	/**
	 * 根据设备id查询设备信息
	 * 转成Json返回
	 * @return
	 * */
	@RequestMapping("/getDeviceDataById")
	@ResponseBody
	public Msg getDeviceListById(@RequestParam("DeviceId")String id){
		Device deviceId = deviceService.getDevice(id);
		return Msg.success().add("deviceId", deviceId);
	}
	
	/**
	 * 根据设备id查询图片子表
	 * */
	@RequestMapping("/getImgListByDeviceId")
	@ResponseBody
	public Msg getImgListByDeviceId(@RequestParam("DeviceId")String id){
		List<Device> deviceI = deviceService.getImgDeviceId(id);
		return Msg.success().add("deviceImg", deviceI);
		
	}
	
//	/**
//	 * 根据设备id查询设备数据+图片子表
//	 * 转成Json返回
//	 * @return
//	 * */
//	@RequestMapping(value="/getDeviceandimgById/DeviceId={id}",method=RequestMethod.GET)
//	@ResponseBody
//	public Msg getDeviceandimgById(@PathVariable("id")String id){
//		Device deviceId = deviceService.getDeviceAndImg(id);
//		return Msg.success().add("deviceId", deviceId);
//	}
	/**
	 * 设备更新(假删除)
	 * */
	@ResponseBody
	@RequestMapping("/SaveDeviceById")
	public Msg UpdateDevice(Device device){
		if(subscribeService.getSubscribeByIdAndStatus(device.getId()).size()>0){
			return Msg.fail().add("fail", "您要删除的设备有未完成的预约");
		}else{
			deviceService.updateDevice(device);
			return Msg.success(); 
		}
	}
	//批量更新(假删除)
	@RequestMapping("/deleteDeviceByIds")
	@ResponseBody
	public Msg UpdateDevices(String ids){
		String[] id = ids.split(",");
		String device ="";
		int y =0,i;
		for(i=0;i<id.length;i++){
			List<Subscribe> list = subscribeService.getSubscribeByIdAndStatus(id[i]);
			if(list.size()>0){
				device=device+list.get(0).getDeviceType()+list.get(0).getDeviceNumber()+" ";
			}else{
				y++;
			}
		}
		if(y==i){
			deviceService.updateByDeviceIds(id);
			return Msg.success();
		}else{
			return Msg.fail().add("fail", device+"设备有未完成的预约");
		}
	}
	
	/**
	 * 设备更新
	 * */
	@ResponseBody
	@RequestMapping("/SaveDeviceById1")
	public Msg UpdateDevice1(Device device){
		String images = deviceCatagoryService.getDevices(device.getDeviceClass()).getImages();
		device.setDevicePhoto(images);
		if(device.getId().equals("")){
				String uuid = UUID.randomUUID().toString();//生成UUID插入到device中
				device.setId(uuid);//生成UUID
				device.setDeviceStatus("1");//设备状态
				device.setFlag(1);//状态值判断 
				deviceService.saveDevice(device);
				System.out.println("新增");
		}else{
			Device devices = deviceService.getDevice(device.getId());
			System.out.println("想要的 设备信息"+devices);
			deviceService.updateDevice(device);
				//修改预约信息状态2018-5-3
				if(device.getDeviceStatus().equals("1") || device.getDeviceStatus()=="1"){//1正常
					String subscribe_result = ".";
					String subscribe_status = "2";
					String subscribe_user_status = "0";
					try {
						subscribeController.updateSubscribeStatusByDeviceIdTwo(device.getId(), subscribe_result,subscribe_status,subscribe_user_status);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(device.getDeviceStatus().equals("2") || device.getDeviceStatus()=="2"){//使用中
					String subscribe_result = ".";
					String subscribe_status = "2";
					String subscribe_user_status = "0";
					try {
						subscribeController.updateSubscribeStatusByDeviceIdTwo(device.getId(), subscribe_result,subscribe_status,subscribe_user_status);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(device.getDeviceStatus().equals("3") || device.getDeviceStatus()=="3"){//3维修
					String subscribe_result = "管理员:设备已损坏,维修中...";
					String subscribe_status = "4";
					String subscribe_user_status = "3";
					try {
						subscribeController.updateSubscribeStatusByDeviceId(device.getId(), subscribe_result,subscribe_status,subscribe_user_status);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(device.getDeviceStatus().equals("4") || device.getDeviceStatus()=="4"){//4下线
					String subscribe_result = "管理员:设备已下线...";
					String subscribe_status = "4";
					String subscribe_user_status = "3";
					try {
						subscribeController.updateSubscribeStatusByDeviceId(device.getId(), subscribe_result,subscribe_status,subscribe_user_status);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			System.out.println("修改");
		}
		return Msg.success(); 
	}
	/**
	 * 批量添加设备
	 * */
	@RequestMapping("/AddDeviceMany")
	@ResponseBody
	public Msg SaveDeviceMany(Device device){
		int number = device.getNumber();//批量添加 （number=添加次数）
		for(int i=0;i<number;i++){
			String uuid = UUID.randomUUID().toString();//.substring(0,5);//生成UUID插入到device中
			device.setId(uuid);//生成UUID
			device.setDeviceNumber(uuid.substring(0,8));
			device.setDeviceStatus("1");//设备状态
			device.setFlag(1);//状态值判断
			deviceService.saveDevice(device);
		}
		return Msg.success();
	}
	
	/**
	 * 插入类别图片
	 * */
	@RequestMapping("/InsertCategoryImage")
	@ResponseBody
	public Msg InsertCategoryImage(Device device){
		//String image = "Subscribe_image"+File.separator+device.getDevicePhoto().substring(12,device.getDevicePhoto().length());
		String image = "Subscribe_image"+File.separator+device.getDevicePhoto();
		device.setDevicePhoto(image);
		deviceService.updateImage(device);
		return Msg.success();
	}
	
	
	
}
