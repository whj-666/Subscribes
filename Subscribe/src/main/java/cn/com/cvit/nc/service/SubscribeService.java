package cn.com.cvit.nc.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeSon;
import cn.com.cvit.nc.dao.SubscribeMapper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class SubscribeService {
	

	@Autowired
	private SubscribeMapper subscribeMapper;
	@Autowired
	DeviceService deviceService;
	@Autowired
	SubscribeSonService subscribeSonService;
	//看板2018-5-19
	public List<Subscribe> selectSeeBoard(Subscribe subscribe){
		return subscribeMapper.selectSeeBoard(subscribe);
	}

	//查询所有subscribe1
	public List<Subscribe> getAllSubscribe(){
		List<Subscribe> list = subscribeMapper.selectByExample(null);
		return list;
	}
	
	//查询基本预约信息
	public List<Subscribe> selectBase(){
		List<Subscribe> list = subscribeMapper.selectBase(null);
		return list;
	}
	// 查询所有subscribe Flag过滤
	public List<Subscribe> getSubscribeAllByFlag(){
		List<Subscribe> list = subscribeMapper.selectByExampleAndFlag(null);
		return list;
	}
	
	//根据工号、姓名、开始时间、结束时间查询预约信息（参数可以为空）flag过滤
	public List<Subscribe> getSubscribeByEmailOrNameOrBTimeOrETime(String email,String name,String begintime,String endtime,String deviceId){
		List<Subscribe> list = subscribeMapper.selectByExampleEmailOrNameOrTime(email, name, begintime, endtime,deviceId);
		return list;
	}
	
   //根据工号和姓名查询，未过滤，后台用
	public List<Subscribe> getEmailAndName(String email,String name){
		List<Subscribe> list = subscribeMapper.selectByExampleEmailAndName(email,name);
		return list;
	}
	
	//根据工号和姓名查询，已过滤，根据结束时间，过滤掉当前时间之前的预约信息
	public List<Subscribe> getEmailAndNameAndTime(String email,String name){
		List<Subscribe> list = subscribeMapper.selectByExampleEmailAndNameAndTime(email,name);
		return list;
	}
	
    //根据设备ID查询
	public List<Subscribe> getId(String id){
		List<Subscribe> list = subscribeMapper.selectByExampleId(id);
		return list;
	}
    //根据设备ID查询  查询 2通过的预约2018-5-3
	public List<Subscribe> selectSubscribeIdByDeviceId(String id){
		List<Subscribe> list = subscribeMapper.selectSubscribeIdByDeviceId(id);
		return list;
	}
    //根据设备ID查询 查询 4撤销的预约2018-5-3
	public List<Subscribe> selectSubscribeIdByDeviceIdTwo(String id){
		List<Subscribe> list = subscribeMapper.selectSubscribeIdByDeviceIdTwo(id);
		return list;
	}
	//根据类别ID查指定类别下的预约信息
	public List<Subscribe> selectDeviceCatagorySubscribeAll(String deviceCatagoryId){
		return subscribeMapper.selectDeviceCatagorySubscribeAll(deviceCatagoryId);
	}
	//根据设备ID查询，过滤
	public List<Subscribe> getSubscribeByIdAndStatus(String id){
		List<Subscribe> list = subscribeMapper.selectByIdAndStatus(id);
		return list;
	}
	
	//根据预约id查询***带设备信息
	public List<Subscribe> getByPrimaryKeyWithDevice(String id){
		List<Subscribe> list = subscribeMapper.selectByPrimaryKeyWithDevice(id);
		return list;
	}
	//插入一条数据
	public int insertSelective(Subscribe subscribe){
		return subscribeMapper.insertSelective(subscribe);
	}
    //根据设备ID和时间查询
    //getIdAndTime
	public List<Subscribe> getIdAndTime(String id,Date begin,Date end){
		List<Subscribe> list = subscribeMapper.selectByExampleIdAndTime(id,begin,end);
		return list;
	}
	/*
     * 根据设备ID和时间查询
     * getIdAndTime
     */
	public List<Subscribe> getIdAndTime1(Date begin,Date end){
		List<Subscribe> list = subscribeMapper.selectByTime(begin,end);
		return list;
	}
	/*
	 * 根据预约id修改状态***1未通过2通过3正常结束4撤销
	 * 根据预约id修改状态值***0删除1没删除
	 */
	public int updateStatusAndFlag(Subscribe record){
		return subscribeMapper.updateByPrimaryKeySelective(record);
	}
	//假删除、批量
	public int updateFlag(String [] id){
		return subscribeMapper.updateFlag(id);
	}
	//根据设备Id修改预约状态，原因等    2018-5-3
	public int updateByDeviceId(Subscribe record){
		return subscribeMapper.updateByDeviceId(record);
	}
	//根据设备Id修改预约状态，原因等    2018-5-3
	public int updateByDeviceIdTwo(Subscribe record){
		return subscribeMapper.updateByDeviceIdTwo(record);
	}
	
	/*
     * 根据时间查询
     * getIdAndTime1
     */
	public List<Subscribe> getTime(Date begin,Date end){
		List<Subscribe> list = subscribeMapper.selectByTime(begin,end);
		return list;
	}
	
	/*
     * 根据时间修改SubscribeStatus（时间过后，默认修改为‘3.正常结束’）
     * 根据email和name查询后，顺便调用
     */
	public int updateSubscribeStatus(String email,String name,String status){
		return subscribeMapper.updateSubscribeStatus(email,name,status);
	}
	//根据email查询过时数据2018-5-17
	public List<Subscribe> selectSubscribeByEmailForAfterTime(String email){
		return subscribeMapper.selectSubscribeByEmailForAfterTime(email);
	}
	//根据时间查数据2018-5-16
	public List<Subscribe> selectSubscribeForAll(){
		return subscribeMapper.selectSubscribeForAll();
	}
    //根据时间修改2018-5-16
	public int updateSubscribeStatusAll(){
		return subscribeMapper.updateSubscribeStatusAll();
	}
	//web、service提示是否有进行中信息
	public List<Subscribe> selectSubscribeStatusByEmailWebService(Subscribe record){
		List<Subscribe> list = subscribeMapper.selectStatusByEmailWebService(record);
		return list;
	}
	//web、service通过预约人email查询今天预约简略信息-推送
	public List<Subscribe> selectMessageByEmailWebService(Subscribe record){
		return subscribeMapper.selectMessageByEmailWebService(record);
	}
	//web、service通过预约人email查询今天预约简略信息-推送
	public List<Subscribe> selectToDayMessageWebService(){
		return subscribeMapper.selectToDayMessageWebService();
	}
	//转格式--list->json
	public JSONArray ProLogList2Json(List<Subscribe> list){
   	 JSONArray json = new JSONArray();
   	 System.out.println("预约数据："+list.toString());
        for(Subscribe sub : list){
            JSONObject jo = new JSONObject();
            jo.put("subscribeUserEmail", sub.getSubscribeUserEmail());
            jo.put("subscribeBeginTime", sub.getSubscribeBeginTime());
            jo.put("subscribeEndTime", sub.getSubscribeEndTime());
            jo.put("subscribeUserName", sub.getSubscribeUserName());
            jo.put("subscribeUserMobile", sub.getSubscribeUserMobile());
            jo.put("subscribeOrganizeName", sub.getSubscribeOrganizeName());
            //设备ID
            Device deviceData = deviceService.getDevice(sub.getSubscribeDeviceId());
            jo.put("deviceName", deviceData.getDeviceClass());
            jo.put("deviceNumber", deviceData.getDeviceNumber());
            jo.put("devicePlace", deviceData.getDevicePlace());
            jo.put("deviceType", deviceData.getDeviceType());
            //添加受邀信息
            List<SubscribeSon> son = subscribeSonService.selectBySubscribeIdWebService(sub.getId());
            JSONArray jsason = new JSONArray();
            for(SubscribeSon subson : son){
            	jsason.add(subson.getSubscribePartnerId());
            }
            jo.put("subscribeSonEmail", jsason);
            json.add(jo);
        }
        return json;
   }
}
