package cn.com.cvit.nc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.Organize;
import cn.com.cvit.nc.bean.Person;
import cn.com.cvit.nc.service.OrganizeService;
import cn.com.cvit.nc.service.PersonService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class OrganizeController {
	
	@Autowired
	OrganizeService organizeService;
	
	@Autowired
	PersonService personService;
	
	//根据id删除部门
	@RequestMapping(value = "/deleteOrganizeById",method = RequestMethod.GET)
	@ResponseBody
    public Msg deleteOrganizeById(String id){
    	if(organizeService.deleteByPrimaryKey(id)>0){
    		return Msg.success().add("success", "删除成功");
    	}else{
    		return Msg.fail().add("fail", "删除失败");
    	}
    }
    //删除所有部门
	@RequestMapping(value = "/deleteOrganizeAll",method = RequestMethod.GET)
	@ResponseBody
    public Msg deleteOrganizeAll(){
    	if(organizeService.deleteOrganizeAll()>0){
    		return Msg.success().add("success", "删除成功");
    	}else{
    		return Msg.fail().add("fail", "删除失败");
    	}
    }
    //新增部门
	@RequestMapping(value = "/insertOrganize",method = RequestMethod.GET)
	@ResponseBody
    public Msg insertOrganize(@RequestParam("jsonUsers")String jsonUsers){
		JSONArray json = JSONArray.fromObject(jsonUsers);
		Organize org = new Organize();
		if(jsonUsers != "" || !jsonUsers.equals("")){
			for(int i=0;i<json.size();i++){
				JSONObject jsonUser = json.getJSONObject(i);
				org.setCode(jsonUser.getString("code"));
				org.setDeparttype(jsonUser.getString("departType"));
				org.setDescription(jsonUser.getString("description"));
				org.setId(jsonUser.getString("id"));
				org.setName(jsonUser.getString("name"));
				org.setOrders(jsonUser.getString("orders"));
				org.setPid(jsonUser.getString("pid"));
				org.setShortname(jsonUser.getString("shortname"));
				org.setType(jsonUser.getString("type"));
				if(organizeService.insertSelective(org) == 1){
					Msg.success().add("success", "添加一个用户成功");
				}else{
					return Msg.fail().add("fail", "添加一个用户失败");
				}
			}
			return Msg.success().add("success", "批量添加成功");
		}else
			return Msg.fail().add("fail", "数据为空");
		}
	//新增部门按照webservice接口
	//新增部门
    public String insertOrganizeOne(String jsonUsers){
    	String str = "["+jsonUsers+"]";
		JSONArray json = JSONArray.fromObject(str);
		Organize org = new Organize();
		if(jsonUsers != "" || !jsonUsers.equals("")){
				JSONObject jsonUser = json.getJSONObject(0);
				if(jsonUser.getString("code")!="" || !jsonUser.getString("code").equals("")){
					org.setCode(jsonUser.getString("code"));
				}
				if(jsonUser.getString("departType")!="" || !jsonUser.getString("departType").equals("")){
					org.setDeparttype(jsonUser.getString("departType"));
				}
				if(jsonUser.getString("description")!="" || !jsonUser.getString("description").equals("")){
					org.setDescription(jsonUser.getString("description"));
				}
				if(jsonUser.getString("id")!="" || !jsonUser.getString("id").equals("")){
					org.setId(jsonUser.getString("id"));
				}
				if(jsonUser.getString("name")!="" || !jsonUser.getString("name").equals("")){
					org.setName(jsonUser.getString("name"));
				}
				if(jsonUser.getString("orders")!="" || !jsonUser.getString("orders").equals("")){
					org.setOrders(jsonUser.getString("orders"));
				}
				if(jsonUser.getString("pid")!="" || !jsonUser.getString("pid").equals("")){
					org.setPid(jsonUser.getString("pid"));
				}
				if(jsonUser.getString("shortname")!="" || !jsonUser.getString("shortname").equals("")){
					org.setShortname(jsonUser.getString("shortname"));
				}
				if(jsonUser.getString("type")!="" || !jsonUser.getString("type").equals("")){
					org.setType(jsonUser.getString("type"));
				}
				if(organizeService.selectByOrganizeId(org).size()>0){
					if(organizeService.updateByOrganizeId(org)>0){
						return "{\"code\":\"1\",\"msg\":\"更新成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"更新失败\"}";
					}
				}else{
					if(organizeService.insertSelective(org) == 1){
						return "{\"code\":\"1\",\"msg\":\"添加成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"添加失败\"}";
					}
				}
			}else{
				return "{\"code\":\"0\",\"msg\":\"数据为空\"}";
			}
		}
    //查询所有
	@RequestMapping(value = "/selectOrganizeAll",method = RequestMethod.GET)
	@ResponseBody
    public Msg selectOrganizeAll(){
		List<Organize> list = organizeService.selectByExample(null);
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "没有数据");
		}
    }
	//查询所有Three
		@RequestMapping(value = "/selectOrganizeAllThree",method = RequestMethod.GET)
		@ResponseBody
	    public Msg selectOrganizeAllThree(){
			List<Organize> list = organizeService.selectByExample(null);
			List<Map<String,String>> list2 = new ArrayList<Map<String,String>>();
			for(int i = 0;i<list.size();i++){
				list.get(i).getId();
				list.get(i).getName();
				list.get(i).getPid();
				Map<String,String> map = new HashMap<String,String>();
				map.put("id", list.get(i).getId());
				map.put("text", list.get(i).getName());
				map.put("pid", list.get(i).getPid());
				list2.add(map);
			}
			if(list2.size()>0){
				return Msg.success().add("success", list2);
			}else{
				return Msg.fail().add("fail", "没有数据");
			}
	    }
	
    //根据code编号查询
	@RequestMapping(value = "/selectByOrganizeCode",method = RequestMethod.GET)
	@ResponseBody
    public Msg selectByOrganizeCode(String organizeCode){
    	Organize org = new Organize();
    	org.setCode(organizeCode);
    	List<Organize> list = organizeService.selectByOrganizeCode(org);
    	if(list.size()>0){
    		return Msg.success().add("success", list);
    	}else{
    		return Msg.fail().add("fail", "没有此部门");
    	}
    }
	//根据Id查询
	@RequestMapping(value = "/selectByOrganizeId",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectByOrganizeId(String organizeId){
	    Organize org = new Organize();
	    org.setId(organizeId);
	    List<Organize> list = organizeService.selectByOrganizeId(org);
	    if(list.size()>0){
	    	return Msg.success().add("success", list);
	    }else{
	    	return Msg.fail().add("fail", "没有此部门");
	    }
	}
	//根据id修改
		@RequestMapping(value = "/updateByOrganizeId",method = RequestMethod.GET)
		@ResponseBody
	    public Msg updateByOrganizeId(Organize record){
			if(organizeService.updateByOrganizeId(record)>0){
				return Msg.success().add("success", "修改成功");
			}else{
				return Msg.fail().add("fail", "修改失败");
			}
	    }
	/*
	 * 根据PID查询
	 * personService.selectByUserTeam(userTeam);
	 */
	@RequestMapping(value = "/selectByOrganizePid",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectByOrganizePId(@RequestParam(value = "organizePId", defaultValue = "-99999")String organizePId){
	    List<Organize> list = organizeService.selectByOrganizePId(organizePId);
	    if(list.size()>0){//不是最小子节点
	    	List<Person> listPersonT = personService.selectByUserTeam(organizePId);
	    	return Msg.success().add("success",list).add("successPerson", listPersonT);
	    }else{//是最小子节点
	    	List<Person> listPerson = personService.selectByUserTeam(organizePId);
	    	if(listPerson.size()>0){
	    		return Msg.success().add("successPerson",listPerson);
	    	}else{
	    		return Msg.fail().add("fail", "此部门下没有用户!!!");
	    	}
	    	
	    }
	}
	
	/*
	 * 根据PID查询
	 * personService.selectByUserTeam(userTeam);
	 * 测试测试！！！！！！！！！
	 * -99999
	 * -1
	 * 8a86411b4e90ccc8014ea8e58dff00b5
	 * 8a05dcc45b1817cd015b556e602b0183
	 * 8a05dcc45b1817cd015b556fae590186
	 */
	@RequestMapping(value = "/selectByOrganizePidTest",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectByOrganizePIdTest(@RequestParam(value = "organizePId", defaultValue = "8a05dcc45b1817cd015b556fae590186")String organizePId){
	    List<Organize> list = organizeService.selectByOrganizePId(organizePId);
	    List<Organize> listOrganizeAll= new ArrayList<Organize>();
	    List<Person> listPersonAll= new ArrayList<Person>();
	    if(list.size()>0){
	    	organizeTree(list);
	 	    Persons(list);
	    }
	    return Msg.success().add("success", listPersonAll);
	}
	/*
	 * 部门
	 */
	public List<Organize> organizeTree(List<Organize> list){
		List<Organize> list2= new ArrayList<Organize>();
	    	for(int a=0;a<list.size();a++){
	    		list2.addAll(organizeService.selectByOrganizePId(list.get(a).getId()));
	    	}
	    	return list2;
	}
	/*
	 * 人员
	 */
	public List<Person> Persons(List<Organize> organizePIdList){
		List<Person> Persons= new ArrayList<Person>();
		for(int i = 0;i<organizePIdList.size();i++){
			Persons.addAll(personService.selectByUserTeam(organizePIdList.get(i).getId()));
		}
		return Persons;
	}

	
	
	
	
	
	
	

}