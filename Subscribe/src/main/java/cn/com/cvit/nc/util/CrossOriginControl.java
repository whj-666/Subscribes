package cn.com.cvit.nc.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrossOriginControl implements Filter{
    private boolean isCross = false;

    public void destroy() {
        // TODO Auto-generated method stub
        isCross = false;
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        // TODO Auto-generated method stub
        if(isCross){
            HttpServletRequest httpServletRequest = (HttpServletRequest)request;
            HttpServletResponse httpServletResponse = (HttpServletResponse)response;
            Thread current = Thread.currentThread();
            System.out.println(current.getId()); 
            System.out.println(current.getName());
            
    		SimpleDateFormat sf=new SimpleDateFormat("MM-dd hh:mm:ss");
    		System.out.println("〖Time:"+sf.format(new Date())+",  IP:"+httpServletRequest.getRemoteAddr()+",  Type:"+httpServletRequest.getMethod()+"〗");
    		System.out.println("*接口:"+httpServletRequest.getRequestURI());
    		System.out.println("-参数:"+httpServletRequest.getQueryString());
    		System.out.println("*---end---*"+"\n");
           // System.out.println("拦截请求: "+httpServletRequest.getServletPath());
            httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");  
          //  httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");  
          //  httpServletResponse.setHeader("Access-Control-Max-Age", "0");  
          //  httpServletResponse.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token");  
          //  httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");  
          //  httpServletResponse.setHeader("XDomainRequestAllowed","1");  
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
        String isCrossStr = filterConfig.getInitParameter("IsCross");
        isCross = isCrossStr.equals("true")?true:false;
     //   System.out.println(isCrossStr);
    }
/**
 * @Override
	public String intercept(ActionInvocation invocation) throws Exception {
		ActionContext ctx = invocation.getInvocationContext();
		HttpServletRequest req=ServletActionContext.getRequest();
		
		Thread current = Thread.currentThread();
        System.out.println(current.getId()); 
        System.out.println(current.getName());
        
		SimpleDateFormat sf=new SimpleDateFormat("MM-dd hh:mm:ss");
		System.out.println("〖Time:"+sf.format(new Date())+",  IP:"+req.getRemoteAddr()+",  Type:"+req.getMethod()+"〗");
		System.out.println("*接口:"+req.getRequestURI());
		System.out.println("-参数:"+req.getQueryString());
		System.out.println("*---end---*"+"\n");
		//记录后台日志
		//MyServletContextListener.pool.addTask(new LogServiceThread()) ;
		invocation.invoke();
		return null;
	}
 */
}
