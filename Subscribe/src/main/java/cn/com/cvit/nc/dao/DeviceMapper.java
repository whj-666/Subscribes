package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.bean.DeviceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DeviceMapper {
    long countByExample(DeviceExample example);

    int deleteByExample(DeviceExample example);

    int deleteByPrimaryKey(String id);

    //设备保存（添加UUID）
    int insert(Device record);

    int insertSelective(Device record);

    //查询所有
    List<Device> selectByExample(DeviceExample example);

    // 根据设备id查询设备数据
    Device selectByPrimaryKey(String id);
    
    //根据设备id查询图片子表
    List<Device> selectImgByDeviceId(String id);
    
    // 根据设备id查询设备数据+图片子表
    //Device selectByPrimaryKeyWithImg(String id);
    
    //按照设备类别id查询设备信息
    List<Device> selectByDeviceClass(String id);
    List<Device> selectByDeviceClassIdOrNull(Device device);
    //查询所有Flag=1的设备
    List<Device> selectByDeviceFlag(Integer flag);
    
    int updateByExampleSelective(@Param("record") Device record, @Param("example") DeviceExample example);

    int updateByExample(@Param("record") Device record, @Param("example") DeviceExample example);
    
    //设备更新
    int updateByPrimaryKeySelective(Device record);
    //批量更新-假删除
    int updateByDeviceIds(String [] ids);
    //类别图片更新
    int updateByCategory(Device record);
    
    //根据设备类别id修改图片
    int updateImages(Device record);

    int updateByPrimaryKey(Device record);
}