package cn.com.cvit.nc.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.User;
import cn.com.cvit.nc.service.UserService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	
	/*
	 * 查询所有
	 */
	@RequestMapping(value = "/selectByUserAll",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectByUserAll(){
		List<User> list = userService.selectByExample();
		return Msg.success().add("success", list);
	}
	/*
	 * 按照email和name查找name可为空
	 */
	@RequestMapping(value = "/selectByUserEmailAndName",method = RequestMethod.GET)
	@ResponseBody
	public Msg getEmailAndName(@RequestParam("email")String email,@RequestParam(value = "name",defaultValue = "")String name){
		List<User> list = userService.getEmailAndNameAndTime(email,name);
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else
			return Msg.fail().add("fail", "用户不存在");
	}
	/*
	 * 根据email更新、修改用户
	 */
	@RequestMapping(value = "/updateByUserEmail",method = RequestMethod.GET)
	@ResponseBody
	public Msg updateByUserEmail(User user){
		if(userService.updateByEmail(user)==1){
			return Msg.success().add("success", "更新成功");
		}else
			return Msg.fail().add("fail", "更新失败");
	}
	/*
	 * 删除用户
	 */
	@RequestMapping(value = "/deleteByUserUserId",method = RequestMethod.GET)
	@ResponseBody
	public Msg deleteByPrimaryKey(User user){
		if(userService.deleteByPrimaryKey(user)>0){
			return Msg.success().add("success", "删除成功");
		}else
			return Msg.fail().add("fail", "删除失败");
	}
	/*
	 * 清空用户表
	 */
	@RequestMapping(value = "/deleteUserAll",method = RequestMethod.GET)
	@ResponseBody
	public Msg deleteByAll(){
		int i=userService.deleteByAll();
		return Msg.success().add("success", "删除"+i+"个用户");
	}
	
	/*
	 * 批量添加用户
	 */
	@RequestMapping(value = "/insertUsers",method = RequestMethod.GET)
	@ResponseBody
	public Msg insertUsers(@RequestParam("jsonUsers")String jsonUsers) throws Exception{
		JSONArray json = JSONArray.fromObject(jsonUsers); // 传入字符串
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		SimpleDateFormat dfc = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式--生日
		User user = new User();
		if(jsonUsers != "" || !jsonUsers.equals("")){
		for(int i=0;i<json.size();i++){
			JSONObject jsonUser = json.getJSONObject(i);
			String uuid = UUID.randomUUID().toString().substring(0, 31);// 32位UUID
			user.setUserId(jsonUser.getString(uuid));
			user.setUserName(jsonUser.getString("userName")); 
			user.setUserCode(jsonUser.getString("userCode")); 
			user.setUserPassword(jsonUser.getString("userPassword")); 
			user.setUserSex(Integer.parseInt(jsonUser.getString("userSex"))); 
			user.setUserEmail(jsonUser.getString("userEmail")); 
			user.setUserOfficePhone(jsonUser.getString("userOfficePhone")); 
			user.setUserMobilePhone(jsonUser.getString("userMobilePhone")); 
			user.setUserTeam(jsonUser.getString("userTeam")); 
			user.setUserRole(jsonUser.getString("userRole")); 
			user.setUserOrder(Integer.parseInt(jsonUser.getString("userOrder"))); 
			user.setUserCreateDate(df.parse(jsonUser.getString("userCreateDate"))); 
			user.setUserModifyTimes(Integer.parseInt(jsonUser.getString("userModifyTimes"))); 
			user.setUserLeatestModifyUser(jsonUser.getString("userLeatestModifyUser")); 
			user.setUserStatus(Integer.parseInt(jsonUser.getString("userStatus"))); 
			user.setUserPosition(jsonUser.getString("userPosition")); 
			user.setUserEmployment(jsonUser.getString("userEmployment")); 
			user.setUserPhoto(jsonUser.getString("userPhoto")); 
			user.setUserWfrole(jsonUser.getString("userWfrole")); 
			user.setUserWfposition(jsonUser.getString("userWfposition")); 
			user.setUserShortname(jsonUser.getString("userShortname")); 
			user.setUserPwdChangeTimes(Integer.parseInt(jsonUser.getString("userPwdChangeTimes"))); 
			user.setUserPwdChangeDate(df.parse(jsonUser.getString("userPwdChangeDate"))); 
			user.setUserTruename(jsonUser.getString("userTruename")); 
			user.setUserMailPath(jsonUser.getString("userMailPath")); 
			user.setUserMailFile(jsonUser.getString("userMailFile")); 
			user.setUserMailServer(jsonUser.getString("userMailServer")); 
			user.setUserBackup1(jsonUser.getString("userBackup1")); 
			user.setUserBackup2(jsonUser.getString("userBackup2")); 
			user.setSequenceId(Integer.parseInt(jsonUser.getString("sequenceId"))); 
			user.setUserCompany(jsonUser.getString("userCompany")); 
			user.setUserPinyin(jsonUser.getString("userPinyin")); 
			user.setUserPinyinLetters(jsonUser.getString("userPinyinLetters")); 
			user.setUserPinyinFirstletter(jsonUser.getString("userPinyinFirstletter")); 
			user.setImKey(jsonUser.getString("imKey")); 
			user.setIdNumber(jsonUser.getString("idNumber")); 
			user.setMyleaderName(jsonUser.getString("myleaderName")); 
			user.setMyleaderId(jsonUser.getString("myleaderId")); 
			user.setSecurityLevel(Integer.parseInt(jsonUser.getString("securityLevel"))); 
			user.setUserOtherPhone(jsonUser.getString("userOtherPhone")); 
			user.setUserBirthday(dfc.parse(jsonUser.getString("userBirthday"))); 
			user.setHrEmplid(jsonUser.getString("hrEmplid")); 
			user.setHrAdminRankChn(jsonUser.getString("hrAdminRankChn")); 
			user.setHrDeptid(jsonUser.getString("hrDeptid")); 
			user.setHrEmail1(jsonUser.getString("hrEmail1")); 
			user.setHrEmail2(jsonUser.getString("hrEmail2")); 
			user.setHrEmail3(jsonUser.getString("hrEmail3")); 
			user.setHrOldEmoid(jsonUser.getString("hrOldEmoid")); 
			user.setHrStatus(jsonUser.getString("hrStatus")); 
			user.setUserMobileCheck(Integer.parseInt(jsonUser.getString("userMobileCheck"))); 
			user.setIsAdmin(Integer.parseInt(jsonUser.getString("isAdmin"))); 
			if(userService.insertSelective(user) == 1){
				Msg.success().add("successMsgOne", "添加一个用户成功");
			}else{
				return Msg.fail().add("failMsg", "添加一个用户失败");
			}
		}
		return Msg.success().add("successMsg", "批量添加成功");
		}else
			return Msg.fail().add("failMsg", "数据为空");
	}

	
	/*
	 * 批量插入测试
	 */
	@RequestMapping(value = "/usesInsertt",method = RequestMethod.GET)
	@ResponseBody
	public Msg insertUserss() throws Exception{
		String jsonUsers = "[{'userName':'邢玉聪','userSex':'1','userBirthday':'2017-12-22'},{'userName':'邢玉聪','userSex':'1','userBirthday':'2017-12-22'},{'userName':'邢玉聪','userSex':'1','userBirthday':'2017-12-22'}]";
		JSONArray json = JSONArray.fromObject(jsonUsers);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		User user = new User();
		if(jsonUsers != "" || !jsonUsers.equals("")){
			for(int i=0;i<json.size();i++){
				JSONObject jsonUser = json.getJSONObject(i);
				String uuid = UUID.randomUUID().toString().substring(0, 31);// 32位UUID
				user.setUserId(uuid);
				user.setUserName(jsonUser.getString("userName")); 
				user.setUserSex(Integer.parseInt(jsonUser.getString("userSex"))); 
				user.setUserBirthday(df.parse(jsonUser.getString("userBirthday")));
				System.out.println("姓名--"+jsonUser.getString("userName")+"性别--"+jsonUser.getString("userSex")+"生日--"+jsonUser.getString("userBirthday"));
				if(userService.insertSelective(user) == 1){
					Msg.success().add("successMsgOne", "添加一个用户成功");
				}else{
					return Msg.fail().add("failMsg", "添加一个用户失败");
				}
			}
			return Msg.success().add("successMsg", "批量添加成功");
		}else
			return Msg.fail().add("failMsg", "数据为空");
		}
	
	}
