package cn.com.cvit.nc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeCount;
import cn.com.cvit.nc.bean.SubscribeCountParameter;
import cn.com.cvit.nc.service.SubscribeCountAnalysisService;

@Controller
public class SubscribeCountAnalysisController {
	@Autowired
	SubscribeCountAnalysisService subscribeCountAnalysisService;

	//所有设备使用时长
	@RequestMapping(value = "/selectAllDeviceTimes", method = RequestMethod.GET)
	@ResponseBody
	public String selectAllDeviceTimes(String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		String i = subscribeCountAnalysisService.selectAllDeviceTimes(scp);
		System.out.println(i);
		return i;
	}
	
	//单一设备使用时长和次数
	@RequestMapping(value = "/selectEveryDeviceTimes", method = RequestMethod.GET)
	@ResponseBody
	public Msg selectEveryDeviceTimes(String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String deviceId){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(deviceId!="" || !deviceId.equals("")){
			scp.setDeviceId(deviceId);
		}
		List<SubscribeCount> list = subscribeCountAnalysisService.selectEveryDeviceTimes(scp);
		System.out.println(list.toString());
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "数据为空");
		}
	}
	//单一设备类别使用时长和次数
	@RequestMapping(value = "/selectEveryDeviceCategoryTimes", method = RequestMethod.GET)
	@ResponseBody
	public Msg selectEveryDeviceCategoryTimes(String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String deviceCatagoryId){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(deviceCatagoryId!="" || !deviceCatagoryId.equals("")){
			scp.setDeviceCatagoryId(deviceCatagoryId);
		}
		List<SubscribeCount> list = subscribeCountAnalysisService.selectEveryDeviceCategoryTimes(scp);
		System.out.println(list.toString());
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "数据为空");
		}
	}
	//单一用户使用时长和次数
	@RequestMapping(value = "/selectEveryUserTimes", method = RequestMethod.GET)
	@ResponseBody
	public Msg selectEveryUserTimes(String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String personId){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(personId!="" || !personId.equals("")){
			scp.setPersonId(personId);
		}
		List<SubscribeCount> list = subscribeCountAnalysisService.selectEveryUserTimes(scp);
		System.out.println(list.toString());
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "数据为空");
		}
	}
	//单一部门使用时长和次数
	@RequestMapping(value = "/selectEveryOrganizeTimes", method = RequestMethod.GET)
	@ResponseBody
	public Msg selectEveryOrganizeTimes(String minuteBegin,String minuteEnd,String year,String yearMonth,String yearMonthDay,String aboutDay,String aboutMonth,String max,String min,String organizeId){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(organizeId!="" || !organizeId.equals("")){
			scp.setOrganizeId(organizeId);
		}
		List<SubscribeCount> list = subscribeCountAnalysisService.selectEveryOrganizeTimes(scp);
		System.out.println(list.toString());
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "数据为空");
		}
	}
	
	//预约数据统计
	@RequestMapping(value = "/selectSubscribeCountData", method = RequestMethod.GET)
	@ResponseBody
	public Msg selectSubscribeCountData(String minuteBegin,
			String minuteEnd,
			String year,
			String yearMonth,
			String yearMonthDay,
			String aboutDay,
			String aboutMonth,
			String max,
			String min,
			String organizeId,
			String deviceId,
			String deviceCatagoryId,
			String personId){
		SubscribeCountParameter scp = new SubscribeCountParameter();
		if(minuteBegin!="" || !minuteBegin.equals("")){
			scp.setMinuteBegin(minuteBegin);
		}
		if(minuteEnd!="" || !minuteEnd.equals("")){
			scp.setMinuteEnd(minuteEnd);
		}
		if(year!="" || !year.equals("")){
			scp.setYear(year);
		}
		if(yearMonth!="" || !yearMonth.equals("")){
			scp.setYearMonth(yearMonth);
		}
		if(yearMonthDay!="" || !yearMonthDay.equals("")){
			scp.setYearMonthDay(yearMonthDay);
		}
		if(aboutDay!="" || !aboutDay.equals("")){
			scp.setAboutDay(aboutDay);
		}
		if(aboutMonth!="" || !aboutMonth.equals("")){
			scp.setAboutMonth(aboutMonth);
		}
		if(max!="" || !max.equals("")){
			scp.setMax(max);
		}
		if(min!="" || !min.equals("")){
			scp.setMin(min);
		}
		if(organizeId!="" || !organizeId.equals("")){
			scp.setOrganizeId(organizeId);
		}
		if(deviceId!="" || !deviceId.equals("")){
			scp.setDeviceId(deviceId);
		}
		if(deviceCatagoryId!="" || !deviceCatagoryId.equals("")){
			scp.setDeviceCatagoryId(deviceCatagoryId);
		}
		if(personId!="" || !personId.equals("")){
			scp.setPersonId(personId);
		}
		List<Subscribe> list = subscribeCountAnalysisService.selectSubscribeCountData(scp);
		System.out.println(list.toString());
		System.out.println("list长度 : "+list.size());
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "数据为空");
		}
		}
}
