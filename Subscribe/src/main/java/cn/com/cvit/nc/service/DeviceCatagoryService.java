package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.cvit.nc.bean.DeviceCatagory;
import cn.com.cvit.nc.dao.DeviceCatagoryMapper;

@Service
public class DeviceCatagoryService {
	@Autowired
	DeviceCatagoryMapper deviceCatagoryMapper;
	/**
	 * 查询所有设备类别
	 * */
	public List<DeviceCatagory> getAll() {
		return deviceCatagoryMapper.selectByExample(null);
	}
	/**
	 * 添加设备类别
	 * */
	public void saveDec(DeviceCatagory deviceCatagory) {
		deviceCatagoryMapper.insertSelective(deviceCatagory);
		
	}
	/**
	 * 根据id查询设备类别
	 * */
	public DeviceCatagory getDevices(String id) {
		// TODO Auto-generated method stub
		DeviceCatagory deviceCatagory=deviceCatagoryMapper.selectByPrimaryKey(id);
		return deviceCatagory;
	}
	/**
	 * 更新
	 * */
	public void updateDevs(DeviceCatagory deviceCatagory) {
		// TODO Auto-generated method stub
		deviceCatagoryMapper.updateByPrimaryKeySelective(deviceCatagory);
		
	}
	/**
	 * 删除
	 * */
	public void deleteDev(String [] id) {
		// TODO Auto-generated method stub
		deviceCatagoryMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 根据flag查询
	 * */
	public List<DeviceCatagory> getFlag() {
		// TODO Auto-generated method stub
		return deviceCatagoryMapper.selectByDeviceCatagoryFlag(null);
	}
	
	
}
