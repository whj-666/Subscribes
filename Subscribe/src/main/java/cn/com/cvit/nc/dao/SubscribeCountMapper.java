package cn.com.cvit.nc.dao;

import java.util.List;

import cn.com.cvit.nc.bean.SubscribeCount;
import cn.com.cvit.nc.bean.SubscribeCountParameter;

public interface SubscribeCountMapper {
	
	//所有设备总使用时长
    String selectAllDeviceAllUseTime();
    //所有设备类别使用时长
    List<SubscribeCount> selectDeviceCategoryAllUseTime();
    //所有设备类别使用次数
    List<SubscribeCount> selectDeviceCategoryAllUseNumber();
    //所有设备近*天预约信息
    List<SubscribeCount> selectAllDeviceSubscribeDataDay(String day);
    //所有设备近*天个月预约信息
    List<SubscribeCount> selectAllDeviceSubscribeDataMonth(String month);
    //所有设备使用时间*--*分钟预约信息
    List<SubscribeCount> selectAllDeviceSubscribeMinute(String mintime,String maxtime);
    //某台设备使用时间*--*分钟预约信息
    List<SubscribeCount> selectDeviceSubscribeMinuteByDeviceId(String mintime,String maxtime,String deviceid);
    //每人的预约次数
    List<SubscribeCount> selectPersonAllUseNumber();
    //每人的预约时长
    List<SubscribeCount> selectPersonAllUseTime();
    //某一时间段的预约信息
    List<SubscribeCount> selectTimeToTime(String mintime,String maxtime);
    //所有部门使用次数和时间
    List<SubscribeCount> selectOrganizeAllUseNumberAndTime();
    
    
}