package cn.com.cvit.nc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



import cn.com.cvit.nc.bean.Manager;
import cn.com.cvit.nc.service.ManagerService;
import cn.com.cvit.nc.util.MD5Util;
/**
 * 管理员登录ManagerController
 */
@Controller
@RequestMapping("/manager")
public class ManagerController {
	
	@Autowired
	ManagerService managerService;
	
	@Autowired
	HttpServletRequest request;
	
	@Autowired
	HttpServletResponse response;
	
	/**
	 * 验证管理员登录
	 */
	@RequestMapping("/islogin")
	@ResponseBody
	public Integer isLogin(@RequestParam("manager")String manager,@RequestParam("password")String password){
		String pwd = MD5Util.string2MD5(password);
		Integer id=managerService.isLogin(manager,pwd);
		HttpSession session = request.getSession();
		session.setAttribute("manager", manager);
		return id;
	}
	
	/**
	 * 管理员列表
	 */
	@RequestMapping("/datagrid")
	@ResponseBody
	public void dataGrid(){
		int page = Integer.parseInt(request.getParameter("page"));
        int rows = Integer.parseInt(request.getParameter("rows"));
        String jsonstr=managerService.getPageJson(page,rows);
		try {
			response.getWriter().write(jsonstr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增用户
	 */
	@RequestMapping("/add_manager")
	@ResponseBody
	public Map addManager(@RequestParam("manager")String manager,@RequestParam("password")String password){
		Manager m=new Manager();
		String pwd = MD5Util.string2MD5(password);
		m.setManager(manager);
		m.setPassword(pwd);
		managerService.addManager(m);
		Map map=new HashMap();
		map.put("result", true);
 	    map.put("message", "新增用户成功");
		return map;
	}
	
	/**
	 * 根据id查询用户
	 */
	@RequestMapping("/query_managerbyid")
	@ResponseBody
	public List<Manager> queryManagerById(@RequestParam("id")Integer id){
		List<Manager> list=managerService.queryManagerById(id);
		return list;
	}
	
	/**
	 * 修改用户
	 */
	@RequestMapping("/update_manager")
	@ResponseBody
	public Map updateManager(@RequestParam("id")Integer id,@RequestParam("manager")String manager,@RequestParam("password")String password){
		Manager m=new Manager();
		String pwd = MD5Util.string2MD5(password);
		m.setId(id);
		m.setManager(manager);
		m.setPassword(pwd);
		managerService.updateManager(m);
		Map map=new HashMap();
		map.put("result", true);
 	    map.put("message", "修改用户成功");
		return map;
	}
	
	/**
	 * 删除管理员
	 */
	@RequestMapping("/del_manager")
	@ResponseBody
	public Integer delIds(@RequestParam("ids")String ids){
		String[] id = ids.split(",");
		managerService.delIds(id);
		int result=id.length;
		return result;
	}
}
