package cn.com.cvit.nc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.com.cvit.nc.bean.Tree;
import cn.com.cvit.nc.service.TreeService;
/**
 * 导航栏TreeController
 */
@Controller
@RequestMapping("/tree")
public class TreeController {
	
	@Autowired
	TreeService treeService;
	
	@Autowired
	HttpServletRequest request;
	
	/**
	 * 获取导航栏数据
	 */
	@RequestMapping("/gettree")
	@ResponseBody
	public List<Tree> getTree(@RequestParam(value="id",defaultValue="0")Integer id){
		List list=treeService.getTree(id);
		return list;
	}
}
