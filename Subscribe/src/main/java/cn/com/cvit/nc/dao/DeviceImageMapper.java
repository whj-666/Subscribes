package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.DeviceImage;
import cn.com.cvit.nc.bean.DeviceImageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DeviceImageMapper {
    long countByExample(DeviceImageExample example);

    int deleteByExample(DeviceImageExample example);

    int deleteByPrimaryKey(String id);

    /*
     * 删除图片
     */
    int deleteByDeviceId(String[] id);
    
    int insert(DeviceImage record);
	/*
	 * 添加图片
	 */
    int insertSelective(DeviceImage record);

    List<DeviceImage> selectByExample(DeviceImage dev);

    DeviceImage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") DeviceImage record, @Param("example") DeviceImageExample example);

    int updateByExample(@Param("record") DeviceImage record, @Param("example") DeviceImageExample example);

    int updateByPrimaryKeySelective(DeviceImage record);

    int updateByPrimaryKey(DeviceImage record);
}