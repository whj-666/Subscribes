package cn.com.cvit.nc.bean;

import java.util.Date;

public class SubscribeSon extends SubscribeSonKey {
    private String subscribePartner;

    private String subscribePartnerId;

    private String subscribeUserName;

    private String subscribeUserStatus;

    private String subscribeUserMobile;

    private String subscribeRemark;

    private String subscribeStatus;

    private String subscribeResult;

    private Integer flag;

    private String subscribeUserStatusName;
    
    private String subscribeStatusName;
    
    private String flagName;
    //sub_son_org_name部门名称
    private String  subSonOrgName;
    
    private Date subscribeBeginTime;//开始时间*

    private Date subscribeEndTime;//结束时间*

    private String mainName;//预约人*
    
    private String statusName;//预约人*
    
//    private String deviceNumber;//设备编号*
//    
//    private String deviceType;//型号名称*
//    
//    private String devicePlace;//位置描述*

    
	public String getFlagName() {
		return flagName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public SubscribeSon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SubscribeSon(String subscribePartner, String subscribePartnerId, String subscribeUserName,
			String subscribeUserStatus, String subscribeUserMobile, String subscribeRemark, String subscribeStatus,
			String subscribeResult, Integer flag, String subscribeUserStatusName, String subscribeStatusName,
			String flagName, String subSonOrgName, Date subscribeBeginTime, Date subscribeEndTime, String mainName) {
		super();
		this.subscribePartner = subscribePartner;
		this.subscribePartnerId = subscribePartnerId;
		this.subscribeUserName = subscribeUserName;
		this.subscribeUserStatus = subscribeUserStatus;
		this.subscribeUserMobile = subscribeUserMobile;
		this.subscribeRemark = subscribeRemark;
		this.subscribeStatus = subscribeStatus;
		this.subscribeResult = subscribeResult;
		this.flag = flag;
		this.subscribeUserStatusName = subscribeUserStatusName;
		this.subscribeStatusName = subscribeStatusName;
		this.flagName = flagName;
		this.subSonOrgName = subSonOrgName;
		this.subscribeBeginTime = subscribeBeginTime;
		this.subscribeEndTime = subscribeEndTime;
		this.mainName = mainName;
	}

	@Override
	public String toString() {
		return "SubscribeSon [subscribePartner=" + subscribePartner + ", subscribePartnerId=" + subscribePartnerId
				+ ", subscribeUserName=" + subscribeUserName + ", subscribeUserStatus=" + subscribeUserStatus
				+ ", subscribeUserMobile=" + subscribeUserMobile + ", subscribeRemark=" + subscribeRemark
				+ ", subscribeStatus=" + subscribeStatus + ", subscribeResult=" + subscribeResult + ", flag=" + flag
				+ ", subscribeUserStatusName=" + subscribeUserStatusName + ", subscribeStatusName="
				+ subscribeStatusName + ", flagName=" + flagName + ", subSonOrgName=" + subSonOrgName
				+ ", subscribeBeginTime=" + subscribeBeginTime + ", subscribeEndTime=" + subscribeEndTime
				+ ", mainName=" + mainName + "]";
	}

	public String getSubSonOrgName() {
		return subSonOrgName;
	}

	public void setSubSonOrgName(String subSonOrgName) {
		this.subSonOrgName = subSonOrgName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getSubscribeUserStatusName() {
		return subscribeUserStatusName;
	}

	public void setSubscribeUserStatusName(String subscribeUserStatusName) {
		this.subscribeUserStatusName = subscribeUserStatusName;
	}

	public String getSubscribeStatusName() {
		return subscribeStatusName;
	}

	public void setSubscribeStatusName(String subscribeStatusName) {
		this.subscribeStatusName = subscribeStatusName;
	}

	public String getSubscribePartner() {
        return subscribePartner;
    }

    public void setSubscribePartner(String subscribePartner) {
        this.subscribePartner = subscribePartner == null ? null : subscribePartner.trim();
    }

    public String getSubscribePartnerId() {
        return subscribePartnerId;
    }

    public void setSubscribePartnerId(String subscribePartnerId) {
        this.subscribePartnerId = subscribePartnerId == null ? null : subscribePartnerId.trim();
    }

    public String getSubscribeUserName() {
        return subscribeUserName;
    }

    public void setSubscribeUserName(String subscribeUserName) {
        this.subscribeUserName = subscribeUserName == null ? null : subscribeUserName.trim();
    }

    public String getSubscribeUserStatus() {
        return subscribeUserStatus;
    }

    public void setSubscribeUserStatus(String subscribeUserStatus) {
        this.subscribeUserStatus = subscribeUserStatus == null ? null : subscribeUserStatus.trim();
    }

    public String getSubscribeUserMobile() {
        return subscribeUserMobile;
    }

    public void setSubscribeUserMobile(String subscribeUserMobile) {
        this.subscribeUserMobile = subscribeUserMobile == null ? null : subscribeUserMobile.trim();
    }

    public String getSubscribeRemark() {
        return subscribeRemark;
    }

    public void setSubscribeRemark(String subscribeRemark) {
        this.subscribeRemark = subscribeRemark == null ? null : subscribeRemark.trim();
    }

    public String getSubscribeStatus() {
        return subscribeStatus;
    }

    public void setSubscribeStatus(String subscribeStatus) {
        this.subscribeStatus = subscribeStatus == null ? null : subscribeStatus.trim();
    }

    public String getSubscribeResult() {
        return subscribeResult;
    }

    public void setSubscribeResult(String subscribeResult) {
        this.subscribeResult = subscribeResult == null ? null : subscribeResult.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

	public Date getSubscribeBeginTime() {
		return subscribeBeginTime;
	}

	public void setSubscribeBeginTime(Date subscribeBeginTime) {
		this.subscribeBeginTime = subscribeBeginTime;
	}

	public Date getSubscribeEndTime() {
		return subscribeEndTime;
	}

	public void setSubscribeEndTime(Date subscribeEndTime) {
		this.subscribeEndTime = subscribeEndTime;
	}

	public String getMainName() {
		return mainName;
	}

	public void setMainName(String mainName) {
		this.mainName = mainName;
	}
    
    
}