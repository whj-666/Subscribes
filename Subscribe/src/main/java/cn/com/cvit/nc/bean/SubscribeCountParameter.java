package cn.com.cvit.nc.bean;

public class SubscribeCountParameter {
	
	private String minuteBegin;//开始时间-分钟
	
	private String minuteEnd;//结束时间-分钟
	
	private String year;//年
	
	private String yearMonth;//年-月
	
	private String yearMonthDay;//年-月-日
	
	private String aboutDay;//近*天
	
	private String aboutMonth;//近*月
	
	private String max;//最大
	
	private String min;//最小
	
	private String deviceId;//设备Id

	private String deviceCatagoryId;//设备类别Id
	
	private String personId;//用户Id
	
	private String organizeId;//部门id
	

	public String getMinuteBegin() {
		return minuteBegin;
	}

	public void setMinuteBegin(String minuteBegin) {
		this.minuteBegin = minuteBegin;
	}

	public String getMinuteEnd() {
		return minuteEnd;
	}

	public void setMinuteEnd(String minuteEnd) {
		this.minuteEnd = minuteEnd;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	public String getYearMonthDay() {
		return yearMonthDay;
	}

	public void setYearMonthDay(String yearMonthDay) {
		this.yearMonthDay = yearMonthDay;
	}

	public String getAboutDay() {
		return aboutDay;
	}

	public void setAboutDay(String aboutDay) {
		this.aboutDay = aboutDay;
	}

	public String getAboutMonth() {
		return aboutMonth;
	}

	public void setAboutMonth(String aboutMonth) {
		this.aboutMonth = aboutMonth;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceCatagoryId() {
		return deviceCatagoryId;
	}

	public void setDeviceCatagoryId(String deviceCatagoryId) {
		this.deviceCatagoryId = deviceCatagoryId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getOrganizeId() {
		return organizeId;
	}

	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}

}
