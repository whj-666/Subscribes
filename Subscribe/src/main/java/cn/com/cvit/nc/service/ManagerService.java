package cn.com.cvit.nc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import cn.com.cvit.nc.bean.Manager;
import cn.com.cvit.nc.dao.ManagerMapper;
/**
 * 管理员登录ManagerService
 */
@Service
public class ManagerService {
	
	@Autowired
	ManagerMapper managerMapper;
	
	/**
	 * 验证管理员登录
	 */
	public Integer isLogin(String manager, String password) {
		return managerMapper.isLogin(manager,password);
	}
	
	/**
	 * 管理员列表
	 */
	public String getPageJson(int page, int rows) {
		String rtn="{'total':0,'rows':[]}";
		int total=managerMapper.getTotal();
		int startPage=(page-1)*rows;
		if(total>0){
			List<Manager> list=managerMapper.getPageList(startPage,rows);
			//将List集合转为Json集合
			String json_list=JSONArray.toJSONString(list);
			//转义字符返回复合类型的json字符串
			rtn = "{\"total\":"+total+",\"rows\":"+json_list+"}";
		}
		return rtn;
	}
	
	/**
	 * 新增用户
	 * @param rowData
	 */
	public void addManager(Manager manager) {
		managerMapper.insert(manager);
	}
	
	/**
	 * 根据id查询用户
	 */
	public List<Manager> queryManagerById(Integer id) {
		return managerMapper.queryManagerById(id);
	}
	
	
	/**
	 * 修改用户
	 * @param rowData
	 */
	public Integer updateManager(Manager rowData) {
		Integer key = managerMapper.updateByPrimaryKey(rowData);
		return key;
	}
	
	/**
	 * 删除管理员
	 * @param id
	 */
	public void delIds(String[] id) {
		managerMapper.delIds(id);
	}

	
	
}
