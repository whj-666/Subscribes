package cn.com.cvit.nc.bean;

import java.util.Date;

public class Subscribe {
    private String id;

    private String subscribeUserId;

    private String subscribeUserEmail;

    private String subscribeDeviceId;

    private Date subscribeBeginTime;

    private Date subscribeEndTime;

    private Date subscribeBeginTimes;

    private Date subscribeEndTimes;

    private Date subscribeCreatetime;

    private Date subscribeBackouttime;

    private String subscribeUserName;

    private String subscribeUserMobile;

    private String subscribeRemark;

    private String subscribeStatus;

    private String subscribeResult;
    
    private String subscribeOrganizeName;//部门名称

    private Integer flag;
    
    private String name;
    
    private String flagName;
    
    private String subscribeStatusName;//预约信息状态
    
    private String deviceNumber;//设备编号

    private String deviceBrand;//品牌

    private String deviceType;//型号名称

    private String deviceClass;//类别

    private String devicePlace;//位置描述

    private String deviceStatus;//设备状态（1正常2使用中3维修4下线）
    
    private String organizeName;//部门名称2018-3-16  统计
    
    private String deviceRemark;//设备备注

	/*
     * 查询设备信息
     */
    private Device device;
    //看板2018-5-21
    private String day;
    private String btime;
    private String etime;
    private String ids;
    private String sfyy;
    
    
	public String getDeviceRemark() {
		return deviceRemark;
	}
	public void setDeviceRemark(String deviceRemark) {
		this.deviceRemark = deviceRemark;
	}
	public String getSfyy() {
		return sfyy;
	}
	public void setSfyy(String sfyy) {
		this.sfyy = sfyy;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getBtime() {
		return btime;
	}
	public void setBtime(String btime) {
		this.btime = btime;
	}
	public String getEtime() {
		return etime;
	}
	public void setEtime(String etime) {
		this.etime = etime;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}

	
	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Subscribe() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Subscribe(String id, String subscribeUserId, String subscribeUserEmail, String subscribeDeviceId,
			Date subscribeBeginTime, Date subscribeEndTime, Date subscribeBeginTimes, Date subscribeEndTimes,
			Date subscribeCreatetime, Date subscribeBackouttime, String subscribeUserName, String subscribeUserMobile,
			String subscribeRemark, String subscribeStatus, String subscribeResult, String subscribeOrganizeName,
			Integer flag, String name, String flagName, String subscribeStatusName, String deviceNumber,
			String deviceBrand, String deviceType, String deviceClass, String devicePlace, String deviceStatus,
			Device device) {
		super();
		this.id = id;
		this.subscribeUserId = subscribeUserId;
		this.subscribeUserEmail = subscribeUserEmail;
		this.subscribeDeviceId = subscribeDeviceId;
		this.subscribeBeginTime = subscribeBeginTime;
		this.subscribeEndTime = subscribeEndTime;
		this.subscribeBeginTimes = subscribeBeginTimes;
		this.subscribeEndTimes = subscribeEndTimes;
		this.subscribeCreatetime = subscribeCreatetime;
		this.subscribeBackouttime = subscribeBackouttime;
		this.subscribeUserName = subscribeUserName;
		this.subscribeUserMobile = subscribeUserMobile;
		this.subscribeRemark = subscribeRemark;
		this.subscribeStatus = subscribeStatus;
		this.subscribeResult = subscribeResult;
		this.subscribeOrganizeName = subscribeOrganizeName;
		this.flag = flag;
		this.name = name;
		this.flagName = flagName;
		this.subscribeStatusName = subscribeStatusName;
		this.deviceNumber = deviceNumber;
		this.deviceBrand = deviceBrand;
		this.deviceType = deviceType;
		this.deviceClass = deviceClass;
		this.devicePlace = devicePlace;
		this.deviceStatus = deviceStatus;
		this.device = device;
	}

	@Override
	public String toString() {
		return "Subscribe [id=" + id + ", subscribeUserId=" + subscribeUserId + ", subscribeUserEmail="
				+ subscribeUserEmail + ", subscribeDeviceId=" + subscribeDeviceId + ", subscribeBeginTime="
				+ subscribeBeginTime + ", subscribeEndTime=" + subscribeEndTime + ", subscribeBeginTimes="
				+ subscribeBeginTimes + ", subscribeEndTimes=" + subscribeEndTimes + ", subscribeCreatetime="
				+ subscribeCreatetime + ", subscribeBackouttime=" + subscribeBackouttime + ", subscribeUserName="
				+ subscribeUserName + ", subscribeUserMobile=" + subscribeUserMobile + ", subscribeRemark="
				+ subscribeRemark + ", subscribeStatus=" + subscribeStatus + ", subscribeResult=" + subscribeResult
				+ ", subscribeOrganizeName=" + subscribeOrganizeName + ", flag=" + flag + ", name=" + name
				+ ", flagName=" + flagName + ", subscribeStatusName=" + subscribeStatusName + ", deviceNumber="
				+ deviceNumber + ", deviceBrand=" + deviceBrand + ", deviceType=" + deviceType + ", deviceClass="
				+ deviceClass + ", devicePlace=" + devicePlace + ", deviceStatus=" + deviceStatus + ", device=" + device
				+ "]";
	}

	public String getSubscribeOrganizeName() {
		return subscribeOrganizeName;
	}

	public void setSubscribeOrganizeName(String subscribeOrganizeName) {
		this.subscribeOrganizeName = subscribeOrganizeName;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSubscribeUserId() {
        return subscribeUserId;
    }

    public void setSubscribeUserId(String subscribeUserId) {
        this.subscribeUserId = subscribeUserId == null ? null : subscribeUserId.trim();
    }

  

    public String getSubscribeUserEmail() {
		return subscribeUserEmail;
	}

	public void setSubscribeUserEmail(String subscribeUserEmail) {
		this.subscribeUserEmail = subscribeUserEmail;
	}

	public String getSubscribeDeviceId() {
        return subscribeDeviceId;
    }

    public void setSubscribeDeviceId(String subscribeDeviceId) {
        this.subscribeDeviceId = subscribeDeviceId == null ? null : subscribeDeviceId.trim();
    }

    public Date getSubscribeBeginTime() {
        return subscribeBeginTime;
    }

    public void setSubscribeBeginTime(Date subscribeBeginTime) {
        this.subscribeBeginTime = subscribeBeginTime;
    }

    public Date getSubscribeEndTime() {
        return subscribeEndTime;
    }

    public void setSubscribeEndTime(Date subscribeEndTime) {
        this.subscribeEndTime = subscribeEndTime;
    }

    public Date getSubscribeBeginTimes() {
        return subscribeBeginTimes;
    }

    public void setSubscribeBeginTimes(Date subscribeBeginTimes) {
        this.subscribeBeginTimes = subscribeBeginTimes;
    }

    public Date getSubscribeEndTimes() {
        return subscribeEndTimes;
    }

    public void setSubscribeEndTimes(Date subscribeEndTimes) {
        this.subscribeEndTimes = subscribeEndTimes;
    }

    public Date getSubscribeCreatetime() {
        return subscribeCreatetime;
    }

    public void setSubscribeCreatetime(Date subscribeCreatetime) {
        this.subscribeCreatetime = subscribeCreatetime;
    }

    public Date getSubscribeBackouttime() {
        return subscribeBackouttime;
    }

    public void setSubscribeBackouttime(Date subscribeBackouttime) {
        this.subscribeBackouttime = subscribeBackouttime;
    }

    public String getSubscribeUserName() {
        return subscribeUserName;
    }

    public void setSubscribeUserName(String subscribeUserName) {
        this.subscribeUserName = subscribeUserName == null ? null : subscribeUserName.trim();
    }

    public String getSubscribeUserMobile() {
        return subscribeUserMobile;
    }

    public void setSubscribeUserMobile(String subscribeUserMobile) {
        this.subscribeUserMobile = subscribeUserMobile == null ? null : subscribeUserMobile.trim();
    }

    public String getSubscribeRemark() {
        return subscribeRemark;
    }

    public void setSubscribeRemark(String subscribeRemark) {
        this.subscribeRemark = subscribeRemark == null ? null : subscribeRemark.trim();
    }

    public String getSubscribeStatus() {
        return subscribeStatus;
    }

    public void setSubscribeStatus(String subscribeStatus) {
        this.subscribeStatus = subscribeStatus == null ? null : subscribeStatus.trim();
    }

    public String getSubscribeResult() {
        return subscribeResult;
    }

    public void setSubscribeResult(String subscribeResult) {
        this.subscribeResult = subscribeResult == null ? null : subscribeResult.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getSubscribeStatusName() {
		return subscribeStatusName;
	}

	public void setSubscribeStatusName(String subscribeStatusName) {
		this.subscribeStatusName = subscribeStatusName;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceClass() {
		return deviceClass;
	}

	public void setDeviceClass(String deviceClass) {
		this.deviceClass = deviceClass;
	}

	public String getDevicePlace() {
		return devicePlace;
	}

	public void setDevicePlace(String devicePlace) {
		this.devicePlace = devicePlace;
	}

	public String getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getOrganizeName() {
		return organizeName;
	}

	public void setOrganizeName(String organizeName) {
		this.organizeName = organizeName;
	}
    
    
}