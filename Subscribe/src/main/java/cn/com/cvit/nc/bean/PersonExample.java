package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.List;

public class PersonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PersonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andIdnumberIsNull() {
            addCriterion("IDNumber is null");
            return (Criteria) this;
        }

        public Criteria andIdnumberIsNotNull() {
            addCriterion("IDNumber is not null");
            return (Criteria) this;
        }

        public Criteria andIdnumberEqualTo(String value) {
            addCriterion("IDNumber =", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberNotEqualTo(String value) {
            addCriterion("IDNumber <>", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberGreaterThan(String value) {
            addCriterion("IDNumber >", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberGreaterThanOrEqualTo(String value) {
            addCriterion("IDNumber >=", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberLessThan(String value) {
            addCriterion("IDNumber <", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberLessThanOrEqualTo(String value) {
            addCriterion("IDNumber <=", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberLike(String value) {
            addCriterion("IDNumber like", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberNotLike(String value) {
            addCriterion("IDNumber not like", value, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberIn(List<String> values) {
            addCriterion("IDNumber in", values, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberNotIn(List<String> values) {
            addCriterion("IDNumber not in", values, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberBetween(String value1, String value2) {
            addCriterion("IDNumber between", value1, value2, "idnumber");
            return (Criteria) this;
        }

        public Criteria andIdnumberNotBetween(String value1, String value2) {
            addCriterion("IDNumber not between", value1, value2, "idnumber");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andHremplidIsNull() {
            addCriterion("hrEmplid is null");
            return (Criteria) this;
        }

        public Criteria andHremplidIsNotNull() {
            addCriterion("hrEmplid is not null");
            return (Criteria) this;
        }

        public Criteria andHremplidEqualTo(Integer value) {
            addCriterion("hrEmplid =", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidNotEqualTo(Integer value) {
            addCriterion("hrEmplid <>", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidGreaterThan(Integer value) {
            addCriterion("hrEmplid >", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidGreaterThanOrEqualTo(Integer value) {
            addCriterion("hrEmplid >=", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidLessThan(Integer value) {
            addCriterion("hrEmplid <", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidLessThanOrEqualTo(Integer value) {
            addCriterion("hrEmplid <=", value, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidIn(List<Integer> values) {
            addCriterion("hrEmplid in", values, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidNotIn(List<Integer> values) {
            addCriterion("hrEmplid not in", values, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidBetween(Integer value1, Integer value2) {
            addCriterion("hrEmplid between", value1, value2, "hremplid");
            return (Criteria) this;
        }

        public Criteria andHremplidNotBetween(Integer value1, Integer value2) {
            addCriterion("hrEmplid not between", value1, value2, "hremplid");
            return (Criteria) this;
        }

        public Criteria andKeytalentsIsNull() {
            addCriterion("keyTalents is null");
            return (Criteria) this;
        }

        public Criteria andKeytalentsIsNotNull() {
            addCriterion("keyTalents is not null");
            return (Criteria) this;
        }

        public Criteria andKeytalentsEqualTo(String value) {
            addCriterion("keyTalents =", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsNotEqualTo(String value) {
            addCriterion("keyTalents <>", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsGreaterThan(String value) {
            addCriterion("keyTalents >", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsGreaterThanOrEqualTo(String value) {
            addCriterion("keyTalents >=", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsLessThan(String value) {
            addCriterion("keyTalents <", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsLessThanOrEqualTo(String value) {
            addCriterion("keyTalents <=", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsLike(String value) {
            addCriterion("keyTalents like", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsNotLike(String value) {
            addCriterion("keyTalents not like", value, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsIn(List<String> values) {
            addCriterion("keyTalents in", values, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsNotIn(List<String> values) {
            addCriterion("keyTalents not in", values, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsBetween(String value1, String value2) {
            addCriterion("keyTalents between", value1, value2, "keytalents");
            return (Criteria) this;
        }

        public Criteria andKeytalentsNotBetween(String value1, String value2) {
            addCriterion("keyTalents not between", value1, value2, "keytalents");
            return (Criteria) this;
        }

        public Criteria andLockstateIsNull() {
            addCriterion("lockState is null");
            return (Criteria) this;
        }

        public Criteria andLockstateIsNotNull() {
            addCriterion("lockState is not null");
            return (Criteria) this;
        }

        public Criteria andLockstateEqualTo(String value) {
            addCriterion("lockState =", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateNotEqualTo(String value) {
            addCriterion("lockState <>", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateGreaterThan(String value) {
            addCriterion("lockState >", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateGreaterThanOrEqualTo(String value) {
            addCriterion("lockState >=", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateLessThan(String value) {
            addCriterion("lockState <", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateLessThanOrEqualTo(String value) {
            addCriterion("lockState <=", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateLike(String value) {
            addCriterion("lockState like", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateNotLike(String value) {
            addCriterion("lockState not like", value, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateIn(List<String> values) {
            addCriterion("lockState in", values, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateNotIn(List<String> values) {
            addCriterion("lockState not in", values, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateBetween(String value1, String value2) {
            addCriterion("lockState between", value1, value2, "lockstate");
            return (Criteria) this;
        }

        public Criteria andLockstateNotBetween(String value1, String value2) {
            addCriterion("lockState not between", value1, value2, "lockstate");
            return (Criteria) this;
        }

        public Criteria andMobilephoneIsNull() {
            addCriterion("mobilePhone is null");
            return (Criteria) this;
        }

        public Criteria andMobilephoneIsNotNull() {
            addCriterion("mobilePhone is not null");
            return (Criteria) this;
        }

        public Criteria andMobilephoneEqualTo(String value) {
            addCriterion("mobilePhone =", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneNotEqualTo(String value) {
            addCriterion("mobilePhone <>", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneGreaterThan(String value) {
            addCriterion("mobilePhone >", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneGreaterThanOrEqualTo(String value) {
            addCriterion("mobilePhone >=", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneLessThan(String value) {
            addCriterion("mobilePhone <", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneLessThanOrEqualTo(String value) {
            addCriterion("mobilePhone <=", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneLike(String value) {
            addCriterion("mobilePhone like", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneNotLike(String value) {
            addCriterion("mobilePhone not like", value, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneIn(List<String> values) {
            addCriterion("mobilePhone in", values, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneNotIn(List<String> values) {
            addCriterion("mobilePhone not in", values, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneBetween(String value1, String value2) {
            addCriterion("mobilePhone between", value1, value2, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andMobilephoneNotBetween(String value1, String value2) {
            addCriterion("mobilePhone not between", value1, value2, "mobilephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneIsNull() {
            addCriterion("officePhone is null");
            return (Criteria) this;
        }

        public Criteria andOfficephoneIsNotNull() {
            addCriterion("officePhone is not null");
            return (Criteria) this;
        }

        public Criteria andOfficephoneEqualTo(String value) {
            addCriterion("officePhone =", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneNotEqualTo(String value) {
            addCriterion("officePhone <>", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneGreaterThan(String value) {
            addCriterion("officePhone >", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneGreaterThanOrEqualTo(String value) {
            addCriterion("officePhone >=", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneLessThan(String value) {
            addCriterion("officePhone <", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneLessThanOrEqualTo(String value) {
            addCriterion("officePhone <=", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneLike(String value) {
            addCriterion("officePhone like", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneNotLike(String value) {
            addCriterion("officePhone not like", value, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneIn(List<String> values) {
            addCriterion("officePhone in", values, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneNotIn(List<String> values) {
            addCriterion("officePhone not in", values, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneBetween(String value1, String value2) {
            addCriterion("officePhone between", value1, value2, "officephone");
            return (Criteria) this;
        }

        public Criteria andOfficephoneNotBetween(String value1, String value2) {
            addCriterion("officePhone not between", value1, value2, "officephone");
            return (Criteria) this;
        }

        public Criteria andOldcompanyIsNull() {
            addCriterion("oldCompany is null");
            return (Criteria) this;
        }

        public Criteria andOldcompanyIsNotNull() {
            addCriterion("oldCompany is not null");
            return (Criteria) this;
        }

        public Criteria andOldcompanyEqualTo(String value) {
            addCriterion("oldCompany =", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyNotEqualTo(String value) {
            addCriterion("oldCompany <>", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyGreaterThan(String value) {
            addCriterion("oldCompany >", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyGreaterThanOrEqualTo(String value) {
            addCriterion("oldCompany >=", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyLessThan(String value) {
            addCriterion("oldCompany <", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyLessThanOrEqualTo(String value) {
            addCriterion("oldCompany <=", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyLike(String value) {
            addCriterion("oldCompany like", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyNotLike(String value) {
            addCriterion("oldCompany not like", value, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyIn(List<String> values) {
            addCriterion("oldCompany in", values, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyNotIn(List<String> values) {
            addCriterion("oldCompany not in", values, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyBetween(String value1, String value2) {
            addCriterion("oldCompany between", value1, value2, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOldcompanyNotBetween(String value1, String value2) {
            addCriterion("oldCompany not between", value1, value2, "oldcompany");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentIsNull() {
            addCriterion("oldDepartment is null");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentIsNotNull() {
            addCriterion("oldDepartment is not null");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentEqualTo(String value) {
            addCriterion("oldDepartment =", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentNotEqualTo(String value) {
            addCriterion("oldDepartment <>", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentGreaterThan(String value) {
            addCriterion("oldDepartment >", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentGreaterThanOrEqualTo(String value) {
            addCriterion("oldDepartment >=", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentLessThan(String value) {
            addCriterion("oldDepartment <", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentLessThanOrEqualTo(String value) {
            addCriterion("oldDepartment <=", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentLike(String value) {
            addCriterion("oldDepartment like", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentNotLike(String value) {
            addCriterion("oldDepartment not like", value, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentIn(List<String> values) {
            addCriterion("oldDepartment in", values, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentNotIn(List<String> values) {
            addCriterion("oldDepartment not in", values, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentBetween(String value1, String value2) {
            addCriterion("oldDepartment between", value1, value2, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOlddepartmentNotBetween(String value1, String value2) {
            addCriterion("oldDepartment not between", value1, value2, "olddepartment");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameIsNull() {
            addCriterion("organizeAllName is null");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameIsNotNull() {
            addCriterion("organizeAllName is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameEqualTo(String value) {
            addCriterion("organizeAllName =", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameNotEqualTo(String value) {
            addCriterion("organizeAllName <>", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameGreaterThan(String value) {
            addCriterion("organizeAllName >", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameGreaterThanOrEqualTo(String value) {
            addCriterion("organizeAllName >=", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameLessThan(String value) {
            addCriterion("organizeAllName <", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameLessThanOrEqualTo(String value) {
            addCriterion("organizeAllName <=", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameLike(String value) {
            addCriterion("organizeAllName like", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameNotLike(String value) {
            addCriterion("organizeAllName not like", value, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameIn(List<String> values) {
            addCriterion("organizeAllName in", values, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameNotIn(List<String> values) {
            addCriterion("organizeAllName not in", values, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameBetween(String value1, String value2) {
            addCriterion("organizeAllName between", value1, value2, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizeallnameNotBetween(String value1, String value2) {
            addCriterion("organizeAllName not between", value1, value2, "organizeallname");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeIsNull() {
            addCriterion("organizeCode is null");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeIsNotNull() {
            addCriterion("organizeCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeEqualTo(String value) {
            addCriterion("organizeCode =", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeNotEqualTo(String value) {
            addCriterion("organizeCode <>", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeGreaterThan(String value) {
            addCriterion("organizeCode >", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeGreaterThanOrEqualTo(String value) {
            addCriterion("organizeCode >=", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeLessThan(String value) {
            addCriterion("organizeCode <", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeLessThanOrEqualTo(String value) {
            addCriterion("organizeCode <=", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeLike(String value) {
            addCriterion("organizeCode like", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeNotLike(String value) {
            addCriterion("organizeCode not like", value, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeIn(List<String> values) {
            addCriterion("organizeCode in", values, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeNotIn(List<String> values) {
            addCriterion("organizeCode not in", values, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeBetween(String value1, String value2) {
            addCriterion("organizeCode between", value1, value2, "organizecode");
            return (Criteria) this;
        }

        public Criteria andOrganizecodeNotBetween(String value1, String value2) {
            addCriterion("organizeCode not between", value1, value2, "organizecode");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusIsNull() {
            addCriterion("politicsStatus is null");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusIsNotNull() {
            addCriterion("politicsStatus is not null");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusEqualTo(String value) {
            addCriterion("politicsStatus =", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusNotEqualTo(String value) {
            addCriterion("politicsStatus <>", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusGreaterThan(String value) {
            addCriterion("politicsStatus >", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusGreaterThanOrEqualTo(String value) {
            addCriterion("politicsStatus >=", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusLessThan(String value) {
            addCriterion("politicsStatus <", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusLessThanOrEqualTo(String value) {
            addCriterion("politicsStatus <=", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusLike(String value) {
            addCriterion("politicsStatus like", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusNotLike(String value) {
            addCriterion("politicsStatus not like", value, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusIn(List<String> values) {
            addCriterion("politicsStatus in", values, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusNotIn(List<String> values) {
            addCriterion("politicsStatus not in", values, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusBetween(String value1, String value2) {
            addCriterion("politicsStatus between", value1, value2, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andPoliticsstatusNotBetween(String value1, String value2) {
            addCriterion("politicsStatus not between", value1, value2, "politicsstatus");
            return (Criteria) this;
        }

        public Criteria andSalaryownerIsNull() {
            addCriterion("salaryOwner is null");
            return (Criteria) this;
        }

        public Criteria andSalaryownerIsNotNull() {
            addCriterion("salaryOwner is not null");
            return (Criteria) this;
        }

        public Criteria andSalaryownerEqualTo(String value) {
            addCriterion("salaryOwner =", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerNotEqualTo(String value) {
            addCriterion("salaryOwner <>", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerGreaterThan(String value) {
            addCriterion("salaryOwner >", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerGreaterThanOrEqualTo(String value) {
            addCriterion("salaryOwner >=", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerLessThan(String value) {
            addCriterion("salaryOwner <", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerLessThanOrEqualTo(String value) {
            addCriterion("salaryOwner <=", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerLike(String value) {
            addCriterion("salaryOwner like", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerNotLike(String value) {
            addCriterion("salaryOwner not like", value, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerIn(List<String> values) {
            addCriterion("salaryOwner in", values, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerNotIn(List<String> values) {
            addCriterion("salaryOwner not in", values, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerBetween(String value1, String value2) {
            addCriterion("salaryOwner between", value1, value2, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSalaryownerNotBetween(String value1, String value2) {
            addCriterion("salaryOwner not between", value1, value2, "salaryowner");
            return (Criteria) this;
        }

        public Criteria andSequenceidIsNull() {
            addCriterion("sequenceId is null");
            return (Criteria) this;
        }

        public Criteria andSequenceidIsNotNull() {
            addCriterion("sequenceId is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceidEqualTo(Integer value) {
            addCriterion("sequenceId =", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidNotEqualTo(Integer value) {
            addCriterion("sequenceId <>", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidGreaterThan(Integer value) {
            addCriterion("sequenceId >", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidGreaterThanOrEqualTo(Integer value) {
            addCriterion("sequenceId >=", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidLessThan(Integer value) {
            addCriterion("sequenceId <", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidLessThanOrEqualTo(Integer value) {
            addCriterion("sequenceId <=", value, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidIn(List<Integer> values) {
            addCriterion("sequenceId in", values, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidNotIn(List<Integer> values) {
            addCriterion("sequenceId not in", values, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidBetween(Integer value1, Integer value2) {
            addCriterion("sequenceId between", value1, value2, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andSequenceidNotBetween(Integer value1, Integer value2) {
            addCriterion("sequenceId not between", value1, value2, "sequenceid");
            return (Criteria) this;
        }

        public Criteria andShortnamelcIsNull() {
            addCriterion("shortNameLC is null");
            return (Criteria) this;
        }

        public Criteria andShortnamelcIsNotNull() {
            addCriterion("shortNameLC is not null");
            return (Criteria) this;
        }

        public Criteria andShortnamelcEqualTo(String value) {
            addCriterion("shortNameLC =", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcNotEqualTo(String value) {
            addCriterion("shortNameLC <>", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcGreaterThan(String value) {
            addCriterion("shortNameLC >", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcGreaterThanOrEqualTo(String value) {
            addCriterion("shortNameLC >=", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcLessThan(String value) {
            addCriterion("shortNameLC <", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcLessThanOrEqualTo(String value) {
            addCriterion("shortNameLC <=", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcLike(String value) {
            addCriterion("shortNameLC like", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcNotLike(String value) {
            addCriterion("shortNameLC not like", value, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcIn(List<String> values) {
            addCriterion("shortNameLC in", values, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcNotIn(List<String> values) {
            addCriterion("shortNameLC not in", values, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcBetween(String value1, String value2) {
            addCriterion("shortNameLC between", value1, value2, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnamelcNotBetween(String value1, String value2) {
            addCriterion("shortNameLC not between", value1, value2, "shortnamelc");
            return (Criteria) this;
        }

        public Criteria andShortnameucIsNull() {
            addCriterion("shortNameUC is null");
            return (Criteria) this;
        }

        public Criteria andShortnameucIsNotNull() {
            addCriterion("shortNameUC is not null");
            return (Criteria) this;
        }

        public Criteria andShortnameucEqualTo(String value) {
            addCriterion("shortNameUC =", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucNotEqualTo(String value) {
            addCriterion("shortNameUC <>", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucGreaterThan(String value) {
            addCriterion("shortNameUC >", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucGreaterThanOrEqualTo(String value) {
            addCriterion("shortNameUC >=", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucLessThan(String value) {
            addCriterion("shortNameUC <", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucLessThanOrEqualTo(String value) {
            addCriterion("shortNameUC <=", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucLike(String value) {
            addCriterion("shortNameUC like", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucNotLike(String value) {
            addCriterion("shortNameUC not like", value, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucIn(List<String> values) {
            addCriterion("shortNameUC in", values, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucNotIn(List<String> values) {
            addCriterion("shortNameUC not in", values, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucBetween(String value1, String value2) {
            addCriterion("shortNameUC between", value1, value2, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andShortnameucNotBetween(String value1, String value2) {
            addCriterion("shortNameUC not between", value1, value2, "shortnameuc");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationIsNull() {
            addCriterion("trainingSituation is null");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationIsNotNull() {
            addCriterion("trainingSituation is not null");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationEqualTo(String value) {
            addCriterion("trainingSituation =", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationNotEqualTo(String value) {
            addCriterion("trainingSituation <>", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationGreaterThan(String value) {
            addCriterion("trainingSituation >", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationGreaterThanOrEqualTo(String value) {
            addCriterion("trainingSituation >=", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationLessThan(String value) {
            addCriterion("trainingSituation <", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationLessThanOrEqualTo(String value) {
            addCriterion("trainingSituation <=", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationLike(String value) {
            addCriterion("trainingSituation like", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationNotLike(String value) {
            addCriterion("trainingSituation not like", value, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationIn(List<String> values) {
            addCriterion("trainingSituation in", values, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationNotIn(List<String> values) {
            addCriterion("trainingSituation not in", values, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationBetween(String value1, String value2) {
            addCriterion("trainingSituation between", value1, value2, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andTrainingsituationNotBetween(String value1, String value2) {
            addCriterion("trainingSituation not between", value1, value2, "trainingsituation");
            return (Criteria) this;
        }

        public Criteria andUseraddressIsNull() {
            addCriterion("userAddress is null");
            return (Criteria) this;
        }

        public Criteria andUseraddressIsNotNull() {
            addCriterion("userAddress is not null");
            return (Criteria) this;
        }

        public Criteria andUseraddressEqualTo(String value) {
            addCriterion("userAddress =", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressNotEqualTo(String value) {
            addCriterion("userAddress <>", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressGreaterThan(String value) {
            addCriterion("userAddress >", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressGreaterThanOrEqualTo(String value) {
            addCriterion("userAddress >=", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressLessThan(String value) {
            addCriterion("userAddress <", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressLessThanOrEqualTo(String value) {
            addCriterion("userAddress <=", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressLike(String value) {
            addCriterion("userAddress like", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressNotLike(String value) {
            addCriterion("userAddress not like", value, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressIn(List<String> values) {
            addCriterion("userAddress in", values, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressNotIn(List<String> values) {
            addCriterion("userAddress not in", values, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressBetween(String value1, String value2) {
            addCriterion("userAddress between", value1, value2, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUseraddressNotBetween(String value1, String value2) {
            addCriterion("userAddress not between", value1, value2, "useraddress");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayIsNull() {
            addCriterion("userBirthday is null");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayIsNotNull() {
            addCriterion("userBirthday is not null");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayEqualTo(String value) {
            addCriterion("userBirthday =", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayNotEqualTo(String value) {
            addCriterion("userBirthday <>", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayGreaterThan(String value) {
            addCriterion("userBirthday >", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayGreaterThanOrEqualTo(String value) {
            addCriterion("userBirthday >=", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayLessThan(String value) {
            addCriterion("userBirthday <", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayLessThanOrEqualTo(String value) {
            addCriterion("userBirthday <=", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayLike(String value) {
            addCriterion("userBirthday like", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayNotLike(String value) {
            addCriterion("userBirthday not like", value, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayIn(List<String> values) {
            addCriterion("userBirthday in", values, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayNotIn(List<String> values) {
            addCriterion("userBirthday not in", values, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayBetween(String value1, String value2) {
            addCriterion("userBirthday between", value1, value2, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUserbirthdayNotBetween(String value1, String value2) {
            addCriterion("userBirthday not between", value1, value2, "userbirthday");
            return (Criteria) this;
        }

        public Criteria andUsercategoryIsNull() {
            addCriterion("userCategory is null");
            return (Criteria) this;
        }

        public Criteria andUsercategoryIsNotNull() {
            addCriterion("userCategory is not null");
            return (Criteria) this;
        }

        public Criteria andUsercategoryEqualTo(String value) {
            addCriterion("userCategory =", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryNotEqualTo(String value) {
            addCriterion("userCategory <>", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryGreaterThan(String value) {
            addCriterion("userCategory >", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryGreaterThanOrEqualTo(String value) {
            addCriterion("userCategory >=", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryLessThan(String value) {
            addCriterion("userCategory <", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryLessThanOrEqualTo(String value) {
            addCriterion("userCategory <=", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryLike(String value) {
            addCriterion("userCategory like", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryNotLike(String value) {
            addCriterion("userCategory not like", value, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryIn(List<String> values) {
            addCriterion("userCategory in", values, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryNotIn(List<String> values) {
            addCriterion("userCategory not in", values, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryBetween(String value1, String value2) {
            addCriterion("userCategory between", value1, value2, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercategoryNotBetween(String value1, String value2) {
            addCriterion("userCategory not between", value1, value2, "usercategory");
            return (Criteria) this;
        }

        public Criteria andUsercodeIsNull() {
            addCriterion("userCode is null");
            return (Criteria) this;
        }

        public Criteria andUsercodeIsNotNull() {
            addCriterion("userCode is not null");
            return (Criteria) this;
        }

        public Criteria andUsercodeEqualTo(String value) {
            addCriterion("userCode =", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeNotEqualTo(String value) {
            addCriterion("userCode <>", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeGreaterThan(String value) {
            addCriterion("userCode >", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeGreaterThanOrEqualTo(String value) {
            addCriterion("userCode >=", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeLessThan(String value) {
            addCriterion("userCode <", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeLessThanOrEqualTo(String value) {
            addCriterion("userCode <=", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeLike(String value) {
            addCriterion("userCode like", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeNotLike(String value) {
            addCriterion("userCode not like", value, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeIn(List<String> values) {
            addCriterion("userCode in", values, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeNotIn(List<String> values) {
            addCriterion("userCode not in", values, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeBetween(String value1, String value2) {
            addCriterion("userCode between", value1, value2, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercodeNotBetween(String value1, String value2) {
            addCriterion("userCode not between", value1, value2, "usercode");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidIsNull() {
            addCriterion("userCompanyId is null");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidIsNotNull() {
            addCriterion("userCompanyId is not null");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidEqualTo(String value) {
            addCriterion("userCompanyId =", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidNotEqualTo(String value) {
            addCriterion("userCompanyId <>", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidGreaterThan(String value) {
            addCriterion("userCompanyId >", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidGreaterThanOrEqualTo(String value) {
            addCriterion("userCompanyId >=", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidLessThan(String value) {
            addCriterion("userCompanyId <", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidLessThanOrEqualTo(String value) {
            addCriterion("userCompanyId <=", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidLike(String value) {
            addCriterion("userCompanyId like", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidNotLike(String value) {
            addCriterion("userCompanyId not like", value, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidIn(List<String> values) {
            addCriterion("userCompanyId in", values, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidNotIn(List<String> values) {
            addCriterion("userCompanyId not in", values, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidBetween(String value1, String value2) {
            addCriterion("userCompanyId between", value1, value2, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUsercompanyidNotBetween(String value1, String value2) {
            addCriterion("userCompanyId not between", value1, value2, "usercompanyid");
            return (Criteria) this;
        }

        public Criteria andUserdutyIsNull() {
            addCriterion("userDuty is null");
            return (Criteria) this;
        }

        public Criteria andUserdutyIsNotNull() {
            addCriterion("userDuty is not null");
            return (Criteria) this;
        }

        public Criteria andUserdutyEqualTo(String value) {
            addCriterion("userDuty =", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyNotEqualTo(String value) {
            addCriterion("userDuty <>", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyGreaterThan(String value) {
            addCriterion("userDuty >", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyGreaterThanOrEqualTo(String value) {
            addCriterion("userDuty >=", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyLessThan(String value) {
            addCriterion("userDuty <", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyLessThanOrEqualTo(String value) {
            addCriterion("userDuty <=", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyLike(String value) {
            addCriterion("userDuty like", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyNotLike(String value) {
            addCriterion("userDuty not like", value, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyIn(List<String> values) {
            addCriterion("userDuty in", values, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyNotIn(List<String> values) {
            addCriterion("userDuty not in", values, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyBetween(String value1, String value2) {
            addCriterion("userDuty between", value1, value2, "userduty");
            return (Criteria) this;
        }

        public Criteria andUserdutyNotBetween(String value1, String value2) {
            addCriterion("userDuty not between", value1, value2, "userduty");
            return (Criteria) this;
        }

        public Criteria andUseremploymentIsNull() {
            addCriterion("userEmployment is null");
            return (Criteria) this;
        }

        public Criteria andUseremploymentIsNotNull() {
            addCriterion("userEmployment is not null");
            return (Criteria) this;
        }

        public Criteria andUseremploymentEqualTo(String value) {
            addCriterion("userEmployment =", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentNotEqualTo(String value) {
            addCriterion("userEmployment <>", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentGreaterThan(String value) {
            addCriterion("userEmployment >", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentGreaterThanOrEqualTo(String value) {
            addCriterion("userEmployment >=", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentLessThan(String value) {
            addCriterion("userEmployment <", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentLessThanOrEqualTo(String value) {
            addCriterion("userEmployment <=", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentLike(String value) {
            addCriterion("userEmployment like", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentNotLike(String value) {
            addCriterion("userEmployment not like", value, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentIn(List<String> values) {
            addCriterion("userEmployment in", values, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentNotIn(List<String> values) {
            addCriterion("userEmployment not in", values, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentBetween(String value1, String value2) {
            addCriterion("userEmployment between", value1, value2, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUseremploymentNotBetween(String value1, String value2) {
            addCriterion("userEmployment not between", value1, value2, "useremployment");
            return (Criteria) this;
        }

        public Criteria andUserentrydateIsNull() {
            addCriterion("userEntryDate is null");
            return (Criteria) this;
        }

        public Criteria andUserentrydateIsNotNull() {
            addCriterion("userEntryDate is not null");
            return (Criteria) this;
        }

        public Criteria andUserentrydateEqualTo(String value) {
            addCriterion("userEntryDate =", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateNotEqualTo(String value) {
            addCriterion("userEntryDate <>", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateGreaterThan(String value) {
            addCriterion("userEntryDate >", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateGreaterThanOrEqualTo(String value) {
            addCriterion("userEntryDate >=", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateLessThan(String value) {
            addCriterion("userEntryDate <", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateLessThanOrEqualTo(String value) {
            addCriterion("userEntryDate <=", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateLike(String value) {
            addCriterion("userEntryDate like", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateNotLike(String value) {
            addCriterion("userEntryDate not like", value, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateIn(List<String> values) {
            addCriterion("userEntryDate in", values, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateNotIn(List<String> values) {
            addCriterion("userEntryDate not in", values, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateBetween(String value1, String value2) {
            addCriterion("userEntryDate between", value1, value2, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUserentrydateNotBetween(String value1, String value2) {
            addCriterion("userEntryDate not between", value1, value2, "userentrydate");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("userName =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("userName <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("userName >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("userName <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("userName like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("userName not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("userName in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("userName not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernationIsNull() {
            addCriterion("userNation is null");
            return (Criteria) this;
        }

        public Criteria andUsernationIsNotNull() {
            addCriterion("userNation is not null");
            return (Criteria) this;
        }

        public Criteria andUsernationEqualTo(String value) {
            addCriterion("userNation =", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationNotEqualTo(String value) {
            addCriterion("userNation <>", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationGreaterThan(String value) {
            addCriterion("userNation >", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationGreaterThanOrEqualTo(String value) {
            addCriterion("userNation >=", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationLessThan(String value) {
            addCriterion("userNation <", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationLessThanOrEqualTo(String value) {
            addCriterion("userNation <=", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationLike(String value) {
            addCriterion("userNation like", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationNotLike(String value) {
            addCriterion("userNation not like", value, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationIn(List<String> values) {
            addCriterion("userNation in", values, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationNotIn(List<String> values) {
            addCriterion("userNation not in", values, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationBetween(String value1, String value2) {
            addCriterion("userNation between", value1, value2, "usernation");
            return (Criteria) this;
        }

        public Criteria andUsernationNotBetween(String value1, String value2) {
            addCriterion("userNation not between", value1, value2, "usernation");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressIsNull() {
            addCriterion("userOfficeAddress is null");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressIsNotNull() {
            addCriterion("userOfficeAddress is not null");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressEqualTo(String value) {
            addCriterion("userOfficeAddress =", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressNotEqualTo(String value) {
            addCriterion("userOfficeAddress <>", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressGreaterThan(String value) {
            addCriterion("userOfficeAddress >", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressGreaterThanOrEqualTo(String value) {
            addCriterion("userOfficeAddress >=", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressLessThan(String value) {
            addCriterion("userOfficeAddress <", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressLessThanOrEqualTo(String value) {
            addCriterion("userOfficeAddress <=", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressLike(String value) {
            addCriterion("userOfficeAddress like", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressNotLike(String value) {
            addCriterion("userOfficeAddress not like", value, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressIn(List<String> values) {
            addCriterion("userOfficeAddress in", values, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressNotIn(List<String> values) {
            addCriterion("userOfficeAddress not in", values, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressBetween(String value1, String value2) {
            addCriterion("userOfficeAddress between", value1, value2, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserofficeaddressNotBetween(String value1, String value2) {
            addCriterion("userOfficeAddress not between", value1, value2, "userofficeaddress");
            return (Criteria) this;
        }

        public Criteria andUserpasswordIsNull() {
            addCriterion("userPassword is null");
            return (Criteria) this;
        }

        public Criteria andUserpasswordIsNotNull() {
            addCriterion("userPassword is not null");
            return (Criteria) this;
        }

        public Criteria andUserpasswordEqualTo(String value) {
            addCriterion("userPassword =", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordNotEqualTo(String value) {
            addCriterion("userPassword <>", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordGreaterThan(String value) {
            addCriterion("userPassword >", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordGreaterThanOrEqualTo(String value) {
            addCriterion("userPassword >=", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordLessThan(String value) {
            addCriterion("userPassword <", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordLessThanOrEqualTo(String value) {
            addCriterion("userPassword <=", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordLike(String value) {
            addCriterion("userPassword like", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordNotLike(String value) {
            addCriterion("userPassword not like", value, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordIn(List<String> values) {
            addCriterion("userPassword in", values, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordNotIn(List<String> values) {
            addCriterion("userPassword not in", values, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordBetween(String value1, String value2) {
            addCriterion("userPassword between", value1, value2, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserpasswordNotBetween(String value1, String value2) {
            addCriterion("userPassword not between", value1, value2, "userpassword");
            return (Criteria) this;
        }

        public Criteria andUserphotoIsNull() {
            addCriterion("userPhoto is null");
            return (Criteria) this;
        }

        public Criteria andUserphotoIsNotNull() {
            addCriterion("userPhoto is not null");
            return (Criteria) this;
        }

        public Criteria andUserphotoEqualTo(String value) {
            addCriterion("userPhoto =", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoNotEqualTo(String value) {
            addCriterion("userPhoto <>", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoGreaterThan(String value) {
            addCriterion("userPhoto >", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoGreaterThanOrEqualTo(String value) {
            addCriterion("userPhoto >=", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoLessThan(String value) {
            addCriterion("userPhoto <", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoLessThanOrEqualTo(String value) {
            addCriterion("userPhoto <=", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoLike(String value) {
            addCriterion("userPhoto like", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoNotLike(String value) {
            addCriterion("userPhoto not like", value, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoIn(List<String> values) {
            addCriterion("userPhoto in", values, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoNotIn(List<String> values) {
            addCriterion("userPhoto not in", values, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoBetween(String value1, String value2) {
            addCriterion("userPhoto between", value1, value2, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserphotoNotBetween(String value1, String value2) {
            addCriterion("userPhoto not between", value1, value2, "userphoto");
            return (Criteria) this;
        }

        public Criteria andUserpositionalIsNull() {
            addCriterion("userPositional is null");
            return (Criteria) this;
        }

        public Criteria andUserpositionalIsNotNull() {
            addCriterion("userPositional is not null");
            return (Criteria) this;
        }

        public Criteria andUserpositionalEqualTo(String value) {
            addCriterion("userPositional =", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalNotEqualTo(String value) {
            addCriterion("userPositional <>", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalGreaterThan(String value) {
            addCriterion("userPositional >", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalGreaterThanOrEqualTo(String value) {
            addCriterion("userPositional >=", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalLessThan(String value) {
            addCriterion("userPositional <", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalLessThanOrEqualTo(String value) {
            addCriterion("userPositional <=", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalLike(String value) {
            addCriterion("userPositional like", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalNotLike(String value) {
            addCriterion("userPositional not like", value, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalIn(List<String> values) {
            addCriterion("userPositional in", values, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalNotIn(List<String> values) {
            addCriterion("userPositional not in", values, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalBetween(String value1, String value2) {
            addCriterion("userPositional between", value1, value2, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserpositionalNotBetween(String value1, String value2) {
            addCriterion("userPositional not between", value1, value2, "userpositional");
            return (Criteria) this;
        }

        public Criteria andUserrankIsNull() {
            addCriterion("userRank is null");
            return (Criteria) this;
        }

        public Criteria andUserrankIsNotNull() {
            addCriterion("userRank is not null");
            return (Criteria) this;
        }

        public Criteria andUserrankEqualTo(Integer value) {
            addCriterion("userRank =", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankNotEqualTo(Integer value) {
            addCriterion("userRank <>", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankGreaterThan(Integer value) {
            addCriterion("userRank >", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankGreaterThanOrEqualTo(Integer value) {
            addCriterion("userRank >=", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankLessThan(Integer value) {
            addCriterion("userRank <", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankLessThanOrEqualTo(Integer value) {
            addCriterion("userRank <=", value, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankIn(List<Integer> values) {
            addCriterion("userRank in", values, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankNotIn(List<Integer> values) {
            addCriterion("userRank not in", values, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankBetween(Integer value1, Integer value2) {
            addCriterion("userRank between", value1, value2, "userrank");
            return (Criteria) this;
        }

        public Criteria andUserrankNotBetween(Integer value1, Integer value2) {
            addCriterion("userRank not between", value1, value2, "userrank");
            return (Criteria) this;
        }

        public Criteria andUsersexIsNull() {
            addCriterion("userSex is null");
            return (Criteria) this;
        }

        public Criteria andUsersexIsNotNull() {
            addCriterion("userSex is not null");
            return (Criteria) this;
        }

        public Criteria andUsersexEqualTo(String value) {
            addCriterion("userSex =", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexNotEqualTo(String value) {
            addCriterion("userSex <>", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexGreaterThan(String value) {
            addCriterion("userSex >", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexGreaterThanOrEqualTo(String value) {
            addCriterion("userSex >=", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexLessThan(String value) {
            addCriterion("userSex <", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexLessThanOrEqualTo(String value) {
            addCriterion("userSex <=", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexLike(String value) {
            addCriterion("userSex like", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexNotLike(String value) {
            addCriterion("userSex not like", value, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexIn(List<String> values) {
            addCriterion("userSex in", values, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexNotIn(List<String> values) {
            addCriterion("userSex not in", values, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexBetween(String value1, String value2) {
            addCriterion("userSex between", value1, value2, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsersexNotBetween(String value1, String value2) {
            addCriterion("userSex not between", value1, value2, "usersex");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeIsNull() {
            addCriterion("userShoesSize is null");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeIsNotNull() {
            addCriterion("userShoesSize is not null");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeEqualTo(String value) {
            addCriterion("userShoesSize =", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeNotEqualTo(String value) {
            addCriterion("userShoesSize <>", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeGreaterThan(String value) {
            addCriterion("userShoesSize >", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeGreaterThanOrEqualTo(String value) {
            addCriterion("userShoesSize >=", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeLessThan(String value) {
            addCriterion("userShoesSize <", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeLessThanOrEqualTo(String value) {
            addCriterion("userShoesSize <=", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeLike(String value) {
            addCriterion("userShoesSize like", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeNotLike(String value) {
            addCriterion("userShoesSize not like", value, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeIn(List<String> values) {
            addCriterion("userShoesSize in", values, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeNotIn(List<String> values) {
            addCriterion("userShoesSize not in", values, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeBetween(String value1, String value2) {
            addCriterion("userShoesSize between", value1, value2, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUsershoessizeNotBetween(String value1, String value2) {
            addCriterion("userShoesSize not between", value1, value2, "usershoessize");
            return (Criteria) this;
        }

        public Criteria andUserstatureIsNull() {
            addCriterion("userStature is null");
            return (Criteria) this;
        }

        public Criteria andUserstatureIsNotNull() {
            addCriterion("userStature is not null");
            return (Criteria) this;
        }

        public Criteria andUserstatureEqualTo(String value) {
            addCriterion("userStature =", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureNotEqualTo(String value) {
            addCriterion("userStature <>", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureGreaterThan(String value) {
            addCriterion("userStature >", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureGreaterThanOrEqualTo(String value) {
            addCriterion("userStature >=", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureLessThan(String value) {
            addCriterion("userStature <", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureLessThanOrEqualTo(String value) {
            addCriterion("userStature <=", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureLike(String value) {
            addCriterion("userStature like", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureNotLike(String value) {
            addCriterion("userStature not like", value, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureIn(List<String> values) {
            addCriterion("userStature in", values, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureNotIn(List<String> values) {
            addCriterion("userStature not in", values, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureBetween(String value1, String value2) {
            addCriterion("userStature between", value1, value2, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserstatureNotBetween(String value1, String value2) {
            addCriterion("userStature not between", value1, value2, "userstature");
            return (Criteria) this;
        }

        public Criteria andUserteamIsNull() {
            addCriterion("userTeam is null");
            return (Criteria) this;
        }

        public Criteria andUserteamIsNotNull() {
            addCriterion("userTeam is not null");
            return (Criteria) this;
        }

        public Criteria andUserteamEqualTo(String value) {
            addCriterion("userTeam =", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamNotEqualTo(String value) {
            addCriterion("userTeam <>", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamGreaterThan(String value) {
            addCriterion("userTeam >", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamGreaterThanOrEqualTo(String value) {
            addCriterion("userTeam >=", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamLessThan(String value) {
            addCriterion("userTeam <", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamLessThanOrEqualTo(String value) {
            addCriterion("userTeam <=", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamLike(String value) {
            addCriterion("userTeam like", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamNotLike(String value) {
            addCriterion("userTeam not like", value, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamIn(List<String> values) {
            addCriterion("userTeam in", values, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamNotIn(List<String> values) {
            addCriterion("userTeam not in", values, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamBetween(String value1, String value2) {
            addCriterion("userTeam between", value1, value2, "userteam");
            return (Criteria) this;
        }

        public Criteria andUserteamNotBetween(String value1, String value2) {
            addCriterion("userTeam not between", value1, value2, "userteam");
            return (Criteria) this;
        }

        public Criteria andUsertruenameIsNull() {
            addCriterion("userTrueName is null");
            return (Criteria) this;
        }

        public Criteria andUsertruenameIsNotNull() {
            addCriterion("userTrueName is not null");
            return (Criteria) this;
        }

        public Criteria andUsertruenameEqualTo(String value) {
            addCriterion("userTrueName =", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameNotEqualTo(String value) {
            addCriterion("userTrueName <>", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameGreaterThan(String value) {
            addCriterion("userTrueName >", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameGreaterThanOrEqualTo(String value) {
            addCriterion("userTrueName >=", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameLessThan(String value) {
            addCriterion("userTrueName <", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameLessThanOrEqualTo(String value) {
            addCriterion("userTrueName <=", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameLike(String value) {
            addCriterion("userTrueName like", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameNotLike(String value) {
            addCriterion("userTrueName not like", value, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameIn(List<String> values) {
            addCriterion("userTrueName in", values, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameNotIn(List<String> values) {
            addCriterion("userTrueName not in", values, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameBetween(String value1, String value2) {
            addCriterion("userTrueName between", value1, value2, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUsertruenameNotBetween(String value1, String value2) {
            addCriterion("userTrueName not between", value1, value2, "usertruename");
            return (Criteria) this;
        }

        public Criteria andUserweightIsNull() {
            addCriterion("userWeight is null");
            return (Criteria) this;
        }

        public Criteria andUserweightIsNotNull() {
            addCriterion("userWeight is not null");
            return (Criteria) this;
        }

        public Criteria andUserweightEqualTo(String value) {
            addCriterion("userWeight =", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightNotEqualTo(String value) {
            addCriterion("userWeight <>", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightGreaterThan(String value) {
            addCriterion("userWeight >", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightGreaterThanOrEqualTo(String value) {
            addCriterion("userWeight >=", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightLessThan(String value) {
            addCriterion("userWeight <", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightLessThanOrEqualTo(String value) {
            addCriterion("userWeight <=", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightLike(String value) {
            addCriterion("userWeight like", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightNotLike(String value) {
            addCriterion("userWeight not like", value, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightIn(List<String> values) {
            addCriterion("userWeight in", values, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightNotIn(List<String> values) {
            addCriterion("userWeight not in", values, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightBetween(String value1, String value2) {
            addCriterion("userWeight between", value1, value2, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserweightNotBetween(String value1, String value2) {
            addCriterion("userWeight not between", value1, value2, "userweight");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionIsNull() {
            addCriterion("userWfpostion is null");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionIsNotNull() {
            addCriterion("userWfpostion is not null");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionEqualTo(String value) {
            addCriterion("userWfpostion =", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionNotEqualTo(String value) {
            addCriterion("userWfpostion <>", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionGreaterThan(String value) {
            addCriterion("userWfpostion >", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionGreaterThanOrEqualTo(String value) {
            addCriterion("userWfpostion >=", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionLessThan(String value) {
            addCriterion("userWfpostion <", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionLessThanOrEqualTo(String value) {
            addCriterion("userWfpostion <=", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionLike(String value) {
            addCriterion("userWfpostion like", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionNotLike(String value) {
            addCriterion("userWfpostion not like", value, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionIn(List<String> values) {
            addCriterion("userWfpostion in", values, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionNotIn(List<String> values) {
            addCriterion("userWfpostion not in", values, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionBetween(String value1, String value2) {
            addCriterion("userWfpostion between", value1, value2, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andUserwfpostionNotBetween(String value1, String value2) {
            addCriterion("userWfpostion not between", value1, value2, "userwfpostion");
            return (Criteria) this;
        }

        public Criteria andIsadminIsNull() {
            addCriterion("isAdmin is null");
            return (Criteria) this;
        }

        public Criteria andIsadminIsNotNull() {
            addCriterion("isAdmin is not null");
            return (Criteria) this;
        }

        public Criteria andIsadminEqualTo(Integer value) {
            addCriterion("isAdmin =", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminNotEqualTo(Integer value) {
            addCriterion("isAdmin <>", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminGreaterThan(Integer value) {
            addCriterion("isAdmin >", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminGreaterThanOrEqualTo(Integer value) {
            addCriterion("isAdmin >=", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminLessThan(Integer value) {
            addCriterion("isAdmin <", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminLessThanOrEqualTo(Integer value) {
            addCriterion("isAdmin <=", value, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminIn(List<Integer> values) {
            addCriterion("isAdmin in", values, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminNotIn(List<Integer> values) {
            addCriterion("isAdmin not in", values, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminBetween(Integer value1, Integer value2) {
            addCriterion("isAdmin between", value1, value2, "isadmin");
            return (Criteria) this;
        }

        public Criteria andIsadminNotBetween(Integer value1, Integer value2) {
            addCriterion("isAdmin not between", value1, value2, "isadmin");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}