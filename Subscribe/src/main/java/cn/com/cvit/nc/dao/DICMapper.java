package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.DIC;
import cn.com.cvit.nc.bean.DICExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DICMapper {
    long countByExample(DICExample example);

    int deleteByExample(DICExample example);

    int deleteByPrimaryKey(String id);

    int insert(DIC record);

    int insertSelective(DIC record);

    List<DIC> selectByExample(DICExample example);

    DIC selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") DIC record, @Param("example") DICExample example);

    int updateByExample(@Param("record") DIC record, @Param("example") DICExample example);

    int updateByPrimaryKeySelective(DIC record);

    int updateByPrimaryKey(DIC record);
}