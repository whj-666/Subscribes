package cn.com.cvit.nc.bean;

import org.springframework.web.multipart.MultipartFile;

/**
 * 设备
 * */
public class Device {
    private String id;

    private Integer deviceNo;//设备序号

    private String deviceNumber;//设备编号

    private String deviceBrand;//品牌

    private String deviceType;//型号名称

    private String deviceClass;//类别

    private String devicePhoto;//图片地址id

    private String devicePlace;//位置描述

    private String deviceStatus;//设备状态（1正常2使用中3维修4下线）

    private Integer flag;//状态值判断（0删除1没删除）

    private String deviceRemark;//备注
    
    private Integer number;//批量添加个数
    
	private String name;//查询设备信息时带出设备状态名

	private String equipmentName;//查询设备信息时带出设备的类别名
	
	private MultipartFile imagetop;
	
	private String deviceStatusName;//设备状态名称（1正常2使用中3维修4下线）
	private String flagName;//状态值判断（0删除1没删除）
    
	public Device() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Device(String id, Integer deviceNo, String deviceNumber, String deviceBrand, String deviceType,
			String deviceClass, String devicePhoto, String devicePlace, String deviceStatus, Integer flag,
			String deviceRemark,String name ,String equipmentName,MultipartFile imagetop) {
		super();
		this.id = id;
		this.deviceNo = deviceNo;
		this.deviceNumber = deviceNumber;
		this.deviceBrand = deviceBrand;
		this.deviceType = deviceType;
		this.deviceClass = deviceClass;
		this.devicePhoto = devicePhoto;
		this.devicePlace = devicePlace;
		this.deviceStatus = deviceStatus;
		this.flag = flag;
		this.deviceRemark = deviceRemark;
		this.name=name;
		this.equipmentName=equipmentName;
		this.imagetop = imagetop;
	}



	@Override
	public String toString() {
		return "Device [id=" + id + ", deviceNo=" + deviceNo + ", deviceNumber=" + deviceNumber + ", deviceBrand="
				+ deviceBrand + ", deviceType=" + deviceType + ", deviceClass=" + deviceClass + ", devicePhoto="
				+ devicePhoto + ", devicePlace=" + devicePlace + ", deviceStatus=" + deviceStatus + ", flag=" + flag
				+ ", deviceRemark=" + deviceRemark + "]";
	}

	

	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getDeviceStatusName() {
		return deviceStatusName;
	}

	public void setDeviceStatusName(String deviceStatusName) {
		this.deviceStatusName = deviceStatusName;
	}

	public MultipartFile getImagetop() {
		return imagetop;
	}

	public void setImagetop(MultipartFile imagetop) {
		this.imagetop = imagetop;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(Integer deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber == null ? null : deviceNumber.trim();
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand == null ? null : deviceBrand.trim();
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType == null ? null : deviceType.trim();
    }

    public String getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(String deviceClass) {
        this.deviceClass = deviceClass == null ? null : deviceClass.trim();
    }

    public String getDevicePhoto() {
        return devicePhoto;
    }

    public void setDevicePhoto(String devicePhoto) {
        this.devicePhoto = devicePhoto == null ? null : devicePhoto.trim();
    }

    public String getDevicePlace() {
        return devicePlace;
    }

    public void setDevicePlace(String devicePlace) {
        this.devicePlace = devicePlace == null ? null : devicePlace.trim();
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus == null ? null : deviceStatus.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getDeviceRemark() {
        return deviceRemark;
    }

    public void setDeviceRemark(String deviceRemark) {
        this.deviceRemark = deviceRemark == null ? null : deviceRemark.trim();
    }
}