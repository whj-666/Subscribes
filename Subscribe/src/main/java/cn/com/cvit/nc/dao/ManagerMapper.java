package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Manager;
import cn.com.cvit.nc.bean.ManagerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ManagerMapper {
    long countByExample(ManagerExample example);

    int deleteByExample(ManagerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Manager record);

    int insertSelective(Manager record);

    List<Manager> selectByExample(ManagerExample example);

    Manager selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Manager record, @Param("example") ManagerExample example);

    int updateByExample(@Param("record") Manager record, @Param("example") ManagerExample example);

    int updateByPrimaryKeySelective(Manager record);

    int updateByPrimaryKey(Manager record);
    
    //验证管理员登录
   	Integer isLogin(String manager, String password);
   	
    //管理员列表
   	int getTotal();
	List<Manager> getPageList(int startPage, int rows);
	
   	//删除管理员
	void delIds(String[] id);
	
	//根据id查询用户
	List<Manager> queryManagerById(Integer id);
	
}