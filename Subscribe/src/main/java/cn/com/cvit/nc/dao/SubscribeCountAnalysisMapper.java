package cn.com.cvit.nc.dao;


import java.util.List;

import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeCount;
import cn.com.cvit.nc.bean.SubscribeCountParameter;

public interface SubscribeCountAnalysisMapper {
	
	//所有设备使用时长
	String selectAllDeviceTimes(SubscribeCountParameter parameter);
	//单一设备使用时长
	List<SubscribeCount> selectEveryDeviceTimes(SubscribeCountParameter parameter);
	//单一设备类别的使用时长和次数
	List<SubscribeCount> selectEveryDeviceCategoryTimes(SubscribeCountParameter parameter);
	//单一用户的使用时长和次数
	List<SubscribeCount> selectEveryUserTimes(SubscribeCountParameter parameter);
	//单一部门的使用时长和次数
	List<SubscribeCount> selectEveryOrganizeTimes(SubscribeCountParameter parameter);
	//预约数据
	List<Subscribe> selectSubscribeCountData(SubscribeCountParameter parameter);
}