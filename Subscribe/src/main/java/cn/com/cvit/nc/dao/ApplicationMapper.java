package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Application;
import cn.com.cvit.nc.bean.ApplicationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationMapper {
    long countByExample(ApplicationExample example);

    int deleteByExample(ApplicationExample example);

    int deleteByPrimaryKey(String id);

    int insert(Application record);
    //添加
    int insertSelective(Application record);
    //查询
    List<Application> selectApplication(Application record);
    //按Email查询
    List<Application> selectApplicationByEmail(Application record);
    
    List<Application> selectByExample(ApplicationExample example);

    Application selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Application record, @Param("example") ApplicationExample example);

    int updateByExample(@Param("record") Application record, @Param("example") ApplicationExample example);

    int updateByPrimaryKeySelective(Application record);

    int updateByPrimaryKey(Application record);
}