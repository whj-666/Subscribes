package cn.com.cvit.nc.test;

import java.awt.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.controller.SubscribeController;
import cn.com.cvit.nc.dao.SubscribeMapper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class SubscribeTest {
	@Autowired
	SubscribeMapper subscribeMapper;
	
	@Autowired
	SqlSession sqlSession;
	
	@Test
	public void test() throws Exception{
		SubscribeMapper mapper = sqlSession.getMapper(SubscribeMapper.class);
		Date d=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
		System.out.println(df.format(new Date(d.getTime()+3*24*60*60*1000)));//三天后时间
		//mapper.insertSelective(new Subscribe("1002","000003","012345","2",df.format(new Date()),df.format(new Date(d.getTime()+3*24*60*60*1000)),df.format(new Date()),df.format(new Date(d.getTime()+3*24*60*60*1000)),"邢玉聪","13756590049","备注","2",null,1));
		//mapper.insertSelective(new Subscribe("1002","000003","012345","2",df.parse(df.format(new Date())),df.parse(df.format(new Date(d.getTime()+3*24*60*60*1000))),null,null,null,null,"邢玉聪","13756590049","备注","2",null,1));
	}
	
	@Test
	public void testTime(){
		SubscribeMapper mapper = sqlSession.getMapper(SubscribeMapper.class);

		System.out.println(mapper.selectByExampleId("1"));
	}

	
}
