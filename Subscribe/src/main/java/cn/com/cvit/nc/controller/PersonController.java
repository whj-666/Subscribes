package cn.com.cvit.nc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.bean.Person;
import cn.com.cvit.nc.service.PersonService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class PersonController {
	@Autowired
	PersonService personService;
	
	//根据id删除用户
	@RequestMapping(value = "/deletePersonById",method = RequestMethod.GET)
	@ResponseBody
    public Msg deletePersonById(@RequestParam("id")String id){
		if(personService.deleteByPrimaryKey(id)>0){
			return Msg.success().add("success", "删除成功");
    	}else{
    		return Msg.fail().add("fail", "删除失败");
    	}
    }
    //删除所有用户
	@RequestMapping(value = "/deletePersonAll",method = RequestMethod.GET)
	@ResponseBody
    public Msg deletePersonAll(){
		if(personService.deletePersonAll()>0){
			return Msg.success().add("success", "删除成功");
    	}else{
    		return Msg.fail().add("fail", "删除失败");
    	}
    }
    //新增用户
	@RequestMapping(value = "/insertPerson",method = RequestMethod.GET)
	@ResponseBody
    public Msg insertPerson(@RequestParam("jsonUsers")String jsonUsers){
		JSONArray json = JSONArray.fromObject(jsonUsers);
		Person per = new Person();
		if(jsonUsers != "" || !jsonUsers.equals("")){
			for(int i=0;i<json.size();i++){
				JSONObject jsonUser = json.getJSONObject(i);
				per.setUserid(jsonUser.getString("userId"));
				per.setIdnumber(jsonUser.getString("IDNumber"));
				per.setEmail(jsonUser.getString("email"));
				per.setHremplid(Integer.valueOf(jsonUser.getString("hrEmplid")));
				per.setKeytalents(jsonUser.getString("keyTalents"));
				per.setLockstate(jsonUser.getString("lockState"));
				per.setMobilephone(jsonUser.getString("mobilePhone"));
				per.setOfficephone(jsonUser.getString("officePhone"));
				per.setOldcompany(jsonUser.getString("oldCompany"));
				per.setOlddepartment(jsonUser.getString("oldDepartment"));
				per.setOrganizeallname(jsonUser.getString("organizeAllName"));
				per.setOrganizecode(jsonUser.getString("organizeCode"));
				per.setPoliticsstatus(jsonUser.getString("politicsStatus"));
				per.setSalaryowner(jsonUser.getString("salaryOwner"));
				per.setSequenceid(Integer.valueOf(jsonUser.getString("sequenceId")));
				per.setShortnamelc(jsonUser.getString("shortNameLC"));
				per.setShortnameuc(jsonUser.getString("shortNameUC"));
				per.setTrainingsituation(jsonUser.getString("trainingSituation"));
				per.setUseraddress(jsonUser.getString("userAddress"));
				per.setUserbirthday(jsonUser.getString("userBirthday"));
				per.setUsercategory(jsonUser.getString("userCategory"));
				per.setUsercode(jsonUser.getString("userCode"));
				per.setUsercompanyid(jsonUser.getString("userCompanyId"));
				per.setUserduty(jsonUser.getString("userDuty"));
				per.setUseremployment(jsonUser.getString("userEmployment"));
				per.setUserentrydate(jsonUser.getString("userEntryDate"));
				per.setUsername(jsonUser.getString("userName"));
				per.setUsernation(jsonUser.getString("userNation"));
				per.setUserofficeaddress(jsonUser.getString("userOfficeAddress"));
				per.setUserpassword(jsonUser.getString("userPassword"));
				per.setUserphoto(jsonUser.getString("userPhoto"));
				per.setUserpositional(jsonUser.getString("userPositional"));
				per.setUserrank(Integer.valueOf(jsonUser.getString("userRank")));
				per.setUsersex(jsonUser.getString("userSex"));
				per.setUsershoessize(jsonUser.getString("userShoesSize"));
				per.setUserstature(jsonUser.getString("userStature"));
				per.setUserteam(jsonUser.getString("userTeam"));
				per.setUsertruename(jsonUser.getString("userTrueName"));
				per.setUserweight(jsonUser.getString("userWeight"));
				per.setUserwfpostion(jsonUser.getString("userWfpostion"));
				per.setIsadmin(0);
				if(personService.insertSelective(per) == 1){
					Msg.success().add("success", "添加一个用户成功");
				}else{
					return Msg.fail().add("fail", "添加一个用户失败");
				}
			}
			return Msg.success().add("success", "批量添加成功");
		}else
			return Msg.fail().add("fail", "数据为空");
    }
	//新增用户按照webservice接口
	public String insertPersonOne(String jsonUsers){
		String str = "["+jsonUsers+"]";
		JSONArray json = JSONArray.fromObject(str);
		Person per = new Person();
		if(jsonUsers != "" || !jsonUsers.equals("")){
				JSONObject jsonUser = json.getJSONObject(0);
				if(jsonUser.getString("email")!="" || !jsonUser.getString("email").equals("")){
					per.setEmail(jsonUser.getString("email"));
				}
				if(jsonUser.getString("userId")!="" || !jsonUser.getString("userId").equals("")){
					per.setUserid(jsonUser.getString("userId"));
				}
				if(jsonUser.getString("IDNumber")!="" || !jsonUser.getString("IDNumber").equals("")){
					per.setIdnumber(jsonUser.getString("IDNumber"));
				}
				if(jsonUser.getString("hrEmplid")!="" || !jsonUser.getString("hrEmplid").equals("")){
					per.setHremplid(Integer.valueOf(jsonUser.getString("hrEmplid")));
				}
				if(jsonUser.getString("keyTalents")!="" || !jsonUser.getString("keyTalents").equals("")){
					per.setKeytalents(jsonUser.getString("keyTalents"));
				}
				if(jsonUser.getString("lockState")!="" || !jsonUser.getString("lockState").equals("")){
					per.setLockstate(jsonUser.getString("lockState"));
				}
				if(jsonUser.getString("mobilePhone")!="" || !jsonUser.getString("mobilePhone").equals("")){
					per.setMobilephone(jsonUser.getString("mobilePhone"));
				}
				if(jsonUser.getString("officePhone")!="" || !jsonUser.getString("officePhone").equals("")){
					per.setOfficephone(jsonUser.getString("officePhone"));
				}
				if(jsonUser.getString("oldCompany")!="" || !jsonUser.getString("oldCompany").equals("")){
					per.setOldcompany(jsonUser.getString("oldCompany"));
				}
				if(jsonUser.getString("oldDepartment")!="" || !jsonUser.getString("oldDepartment").equals("")){
					per.setOlddepartment(jsonUser.getString("oldDepartment"));
				}
				if(jsonUser.getString("organizeAllName")!="" || !jsonUser.getString("organizeAllName").equals("")){
					per.setOrganizeallname(jsonUser.getString("organizeAllName"));
				}
				if(jsonUser.getString("organizeCode")!="" || !jsonUser.getString("organizeCode").equals("")){
					per.setOrganizecode(jsonUser.getString("organizeCode"));
				}
				if(jsonUser.getString("politicsStatus")!="" || !jsonUser.getString("politicsStatus").equals("")){
					per.setPoliticsstatus(jsonUser.getString("politicsStatus"));
				}
				if(jsonUser.getString("salaryOwner")!="" || !jsonUser.getString("salaryOwner").equals("")){
					per.setSalaryowner(jsonUser.getString("salaryOwner"));
				}
				if(jsonUser.getString("sequenceId")!="" || !jsonUser.getString("sequenceId").equals("")){
					per.setSequenceid(Integer.valueOf(jsonUser.getString("sequenceId")));
				}
				if(jsonUser.getString("shortNameLC")!="" || !jsonUser.getString("shortNameLC").equals("")){
					per.setShortnamelc(jsonUser.getString("shortNameLC"));
				}
				if(jsonUser.getString("shortNameUC")!="" || !jsonUser.getString("shortNameUC").equals("")){
					per.setShortnameuc(jsonUser.getString("shortNameUC"));
				}
				if(jsonUser.getString("trainingSituation")!="" || !jsonUser.getString("trainingSituation").equals("")){
					per.setTrainingsituation(jsonUser.getString("trainingSituation"));
				}
				if(jsonUser.getString("userAddress")!="" || !jsonUser.getString("userAddress").equals("")){
					per.setUseraddress(jsonUser.getString("userAddress"));
				}
				if(jsonUser.getString("userBirthday")!="" || !jsonUser.getString("userBirthday").equals("")){
					per.setUserbirthday(jsonUser.getString("userBirthday"));
				}
				if(jsonUser.getString("userCategory")!="" || !jsonUser.getString("userCategory").equals("")){
					per.setUsercategory(jsonUser.getString("userCategory"));
				}
				if(jsonUser.getString("userCode")!="" || !jsonUser.getString("userCode").equals("")){
					per.setUsercode(jsonUser.getString("userCode"));
				}
				if(jsonUser.getString("userCompanyId")!="" || !jsonUser.getString("userCompanyId").equals("")){
					per.setUsercompanyid(jsonUser.getString("userCompanyId"));
				}
				if(jsonUser.getString("userDuty")!="" || !jsonUser.getString("userDuty").equals("")){
					per.setUserduty(jsonUser.getString("userDuty"));
				}
				if(jsonUser.getString("userEmployment")!="" || !jsonUser.getString("userEmployment").equals("")){
					per.setUseremployment(jsonUser.getString("userEmployment"));
				}
				if(jsonUser.getString("userEntryDate")!="" || !jsonUser.getString("userEntryDate").equals("")){
					per.setUserentrydate(jsonUser.getString("userEntryDate"));
				}
				if(jsonUser.getString("userName")!="" || !jsonUser.getString("userName").equals("")){
					per.setUsername(jsonUser.getString("userName"));
				}
				if(jsonUser.getString("userNation")!="" || !jsonUser.getString("userNation").equals("")){
					per.setUsernation(jsonUser.getString("userNation"));
				}
				if(jsonUser.getString("userOfficeAddress")!="" || !jsonUser.getString("userOfficeAddress").equals("")){
					per.setUserofficeaddress(jsonUser.getString("userOfficeAddress"));
				}
				if(jsonUser.getString("userPassword")!="" || !jsonUser.getString("userPassword").equals("")){
					per.setUserpassword(jsonUser.getString("userPassword"));
				}
				if(jsonUser.getString("userPhoto")!="" || !jsonUser.getString("userPhoto").equals("")){
					per.setUserphoto(jsonUser.getString("userPhoto"));
				}
				if(jsonUser.getString("userPositional")!="" || !jsonUser.getString("userPositional").equals("")){
					per.setUserpositional(jsonUser.getString("userPositional"));
				}
				if(jsonUser.getString("userSex")!="" || !jsonUser.getString("userSex").equals("")){
					per.setUsersex(jsonUser.getString("userSex"));
				}
				if(jsonUser.getString("userShoesSize")!="" || !jsonUser.getString("userShoesSize").equals("")){
					per.setUsershoessize(jsonUser.getString("userShoesSize"));
				}
				if(jsonUser.getString("userStature")!="" || !jsonUser.getString("userStature").equals("")){
					per.setUserstature(jsonUser.getString("userStature"));
				}
				if(jsonUser.getString("userTeam")!="" || !jsonUser.getString("userTeam").equals("")){
					per.setUserteam(jsonUser.getString("userTeam"));
				}
				if(jsonUser.getString("userTrueName")!="" || !jsonUser.getString("userTrueName").equals("")){
					per.setUsertruename(jsonUser.getString("userTrueName"));
				}
				if(jsonUser.getString("userWeight")!="" || !jsonUser.getString("userWeight").equals("")){
					per.setUserweight(jsonUser.getString("userWeight"));
				}
				if(jsonUser.getString("userWfpostion")!="" || !jsonUser.getString("userWfpostion").equals("")){
					per.setUserwfpostion(jsonUser.getString("userWfpostion"));
				}
				per.setIsadmin(0);
				if(personService.selectByPersonUserId(per).size()>0){
					if(personService.updateByPersonId(per)>0){
						return "{\"code\":\"1\",\"msg\":\"更新成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"更新失败\"}";
					}
				}else{
					if(personService.insertSelective(per) == 1){
						return "{\"code\":\"1\",\"msg\":\"添加成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"添加失败\"}";
					}
				}
		}else
			return "{\"code\":\"0\",\"msg\":\"数据为空\"}";
  }
    //查询所有
	@RequestMapping(value = "/selectPersonAll",method = RequestMethod.GET)
	@ResponseBody
    public Msg selectPersonAll(){
		List<Person> list = personService.selectByExample();
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "没有数据");
		}
    }
    //根据Email查询
	@RequestMapping(value = "/selectByPersonEmail",method = RequestMethod.GET)
	@ResponseBody
    public Msg selectByPersonEmail(String email){
		Person per = new Person();
		per.setEmail(email);
		List<Person> list = personService.selectByPersonEmail(per);
		if(list.size()>0){
    		return Msg.success().add("success", list);
    	}else{
    		return Msg.fail().add("fail", "没有此用户");
    	}
    }
	//根据name模糊查询
	@RequestMapping(value = "/selectByPersonTrueName",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectByPersonTrueName(String trueName){
		Person per = new Person();
		per.setUsertruename(trueName);
		List<Person> list = personService.selectByPersonTrueName(per);
		if(list.size()>0){
	    	return Msg.success().add("success", list);
	    }else{
	    	return Msg.fail().add("fail", "没有此用户");
	    }
	}
	//根据Email和password查询--登录用
		@RequestMapping(value = "/selectByPersonEmailAndPassword",method = RequestMethod.GET)
		@ResponseBody
	    public Msg selectByPersonEmailAndPassword(String email,String password){
			Person per = new Person();
			per.setEmail(email);
			per.setUserpassword(password);
			List<Person> list = personService.selectByPersonEmail(per);
			if(list.size()>0){
				List<Person> list2 = personService.selectByPersonEmailAndPassword(per);
				if(list2.size()>0){
					return Msg.success().add("success", list2);
				}
				return Msg.fail().add("fail", "密码错误");
	    	}else{
	    		return Msg.fail().add("fail", "Email错误");
	    	}
	    }
	//根据部门Id查询
		@RequestMapping(value = "/selectByUserTeam",method = RequestMethod.GET)
		@ResponseBody
	    public Msg selectByUserTeam(String userTeam){
			List<Person> list = personService.selectByUserTeam(userTeam);
			if(list.size()>0){
	    		return Msg.success().add("success", list);
	    	}else{
	    		return Msg.fail().add("fail", "没有此用户");
	    	}
	    }
    //根据id修改
	@RequestMapping(value = "/updateByPersonId",method = RequestMethod.GET)
	@ResponseBody
    public Msg updateByPersonId(Person record){
		if(personService.updateByPersonId(record)>0){
			return Msg.success().add("success", "修改成功");
		}else{
			return Msg.fail().add("fail", "修改失败");
		}
    }

}
