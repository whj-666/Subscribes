package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.DeviceCatagory;
import cn.com.cvit.nc.bean.DeviceCatagoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DeviceCatagoryMapper {
    long countByExample(DeviceCatagoryExample example);

    int deleteByExample(DeviceCatagoryExample example);

    //删除
    int deleteByPrimaryKey(String [] id);

    int insert(DeviceCatagory record);

    //添加保存设备类别
    int insertSelective(DeviceCatagory record);

    
	//查询所有设备类别
    List<DeviceCatagory> selectByExample(DeviceCatagoryExample example);
    //根据flag查询
    List<DeviceCatagory> selectByDeviceCatagoryFlag(DeviceCatagoryExample example);

    //根据id查询设备类别
    DeviceCatagory selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") DeviceCatagory record, @Param("example") DeviceCatagoryExample example);

    int updateByExample(@Param("record") DeviceCatagory record, @Param("example") DeviceCatagoryExample example);

    //更新
    int updateByPrimaryKeySelective(DeviceCatagory record);

    int updateByPrimaryKey(DeviceCatagory record);
}