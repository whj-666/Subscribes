package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.List;

public class SubscribeSonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SubscribeSonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdIsNull() {
            addCriterion("subscribe_id is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdIsNotNull() {
            addCriterion("subscribe_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdEqualTo(String value) {
            addCriterion("subscribe_id =", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdNotEqualTo(String value) {
            addCriterion("subscribe_id <>", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdGreaterThan(String value) {
            addCriterion("subscribe_id >", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_id >=", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdLessThan(String value) {
            addCriterion("subscribe_id <", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdLessThanOrEqualTo(String value) {
            addCriterion("subscribe_id <=", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdLike(String value) {
            addCriterion("subscribe_id like", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdNotLike(String value) {
            addCriterion("subscribe_id not like", value, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdIn(List<String> values) {
            addCriterion("subscribe_id in", values, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdNotIn(List<String> values) {
            addCriterion("subscribe_id not in", values, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdBetween(String value1, String value2) {
            addCriterion("subscribe_id between", value1, value2, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribeIdNotBetween(String value1, String value2) {
            addCriterion("subscribe_id not between", value1, value2, "subscribeId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIsNull() {
            addCriterion("subscribe_partner is null");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIsNotNull() {
            addCriterion("subscribe_partner is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerEqualTo(String value) {
            addCriterion("subscribe_partner =", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerNotEqualTo(String value) {
            addCriterion("subscribe_partner <>", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerGreaterThan(String value) {
            addCriterion("subscribe_partner >", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_partner >=", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerLessThan(String value) {
            addCriterion("subscribe_partner <", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerLessThanOrEqualTo(String value) {
            addCriterion("subscribe_partner <=", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerLike(String value) {
            addCriterion("subscribe_partner like", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerNotLike(String value) {
            addCriterion("subscribe_partner not like", value, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIn(List<String> values) {
            addCriterion("subscribe_partner in", values, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerNotIn(List<String> values) {
            addCriterion("subscribe_partner not in", values, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerBetween(String value1, String value2) {
            addCriterion("subscribe_partner between", value1, value2, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerNotBetween(String value1, String value2) {
            addCriterion("subscribe_partner not between", value1, value2, "subscribePartner");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdIsNull() {
            addCriterion("subscribe_partner_id is null");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdIsNotNull() {
            addCriterion("subscribe_partner_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdEqualTo(String value) {
            addCriterion("subscribe_partner_id =", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdNotEqualTo(String value) {
            addCriterion("subscribe_partner_id <>", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdGreaterThan(String value) {
            addCriterion("subscribe_partner_id >", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_partner_id >=", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdLessThan(String value) {
            addCriterion("subscribe_partner_id <", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdLessThanOrEqualTo(String value) {
            addCriterion("subscribe_partner_id <=", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdLike(String value) {
            addCriterion("subscribe_partner_id like", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdNotLike(String value) {
            addCriterion("subscribe_partner_id not like", value, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdIn(List<String> values) {
            addCriterion("subscribe_partner_id in", values, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdNotIn(List<String> values) {
            addCriterion("subscribe_partner_id not in", values, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdBetween(String value1, String value2) {
            addCriterion("subscribe_partner_id between", value1, value2, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribePartnerIdNotBetween(String value1, String value2) {
            addCriterion("subscribe_partner_id not between", value1, value2, "subscribePartnerId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIsNull() {
            addCriterion("subscribe_user_name is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIsNotNull() {
            addCriterion("subscribe_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameEqualTo(String value) {
            addCriterion("subscribe_user_name =", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotEqualTo(String value) {
            addCriterion("subscribe_user_name <>", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameGreaterThan(String value) {
            addCriterion("subscribe_user_name >", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_name >=", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLessThan(String value) {
            addCriterion("subscribe_user_name <", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_name <=", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLike(String value) {
            addCriterion("subscribe_user_name like", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotLike(String value) {
            addCriterion("subscribe_user_name not like", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIn(List<String> values) {
            addCriterion("subscribe_user_name in", values, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotIn(List<String> values) {
            addCriterion("subscribe_user_name not in", values, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameBetween(String value1, String value2) {
            addCriterion("subscribe_user_name between", value1, value2, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_name not between", value1, value2, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusIsNull() {
            addCriterion("subscribe_user_status is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusIsNotNull() {
            addCriterion("subscribe_user_status is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusEqualTo(String value) {
            addCriterion("subscribe_user_status =", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusNotEqualTo(String value) {
            addCriterion("subscribe_user_status <>", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusGreaterThan(String value) {
            addCriterion("subscribe_user_status >", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_status >=", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusLessThan(String value) {
            addCriterion("subscribe_user_status <", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_status <=", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusLike(String value) {
            addCriterion("subscribe_user_status like", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusNotLike(String value) {
            addCriterion("subscribe_user_status not like", value, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusIn(List<String> values) {
            addCriterion("subscribe_user_status in", values, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusNotIn(List<String> values) {
            addCriterion("subscribe_user_status not in", values, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusBetween(String value1, String value2) {
            addCriterion("subscribe_user_status between", value1, value2, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserStatusNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_status not between", value1, value2, "subscribeUserStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIsNull() {
            addCriterion("subscribe_user_mobile is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIsNotNull() {
            addCriterion("subscribe_user_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileEqualTo(String value) {
            addCriterion("subscribe_user_mobile =", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotEqualTo(String value) {
            addCriterion("subscribe_user_mobile <>", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileGreaterThan(String value) {
            addCriterion("subscribe_user_mobile >", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_mobile >=", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLessThan(String value) {
            addCriterion("subscribe_user_mobile <", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_mobile <=", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLike(String value) {
            addCriterion("subscribe_user_mobile like", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotLike(String value) {
            addCriterion("subscribe_user_mobile not like", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIn(List<String> values) {
            addCriterion("subscribe_user_mobile in", values, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotIn(List<String> values) {
            addCriterion("subscribe_user_mobile not in", values, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileBetween(String value1, String value2) {
            addCriterion("subscribe_user_mobile between", value1, value2, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_mobile not between", value1, value2, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIsNull() {
            addCriterion("subscribe_remark is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIsNotNull() {
            addCriterion("subscribe_remark is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkEqualTo(String value) {
            addCriterion("subscribe_remark =", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotEqualTo(String value) {
            addCriterion("subscribe_remark <>", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkGreaterThan(String value) {
            addCriterion("subscribe_remark >", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_remark >=", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLessThan(String value) {
            addCriterion("subscribe_remark <", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLessThanOrEqualTo(String value) {
            addCriterion("subscribe_remark <=", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLike(String value) {
            addCriterion("subscribe_remark like", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotLike(String value) {
            addCriterion("subscribe_remark not like", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIn(List<String> values) {
            addCriterion("subscribe_remark in", values, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotIn(List<String> values) {
            addCriterion("subscribe_remark not in", values, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkBetween(String value1, String value2) {
            addCriterion("subscribe_remark between", value1, value2, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotBetween(String value1, String value2) {
            addCriterion("subscribe_remark not between", value1, value2, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIsNull() {
            addCriterion("subscribe_status is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIsNotNull() {
            addCriterion("subscribe_status is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusEqualTo(String value) {
            addCriterion("subscribe_status =", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotEqualTo(String value) {
            addCriterion("subscribe_status <>", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusGreaterThan(String value) {
            addCriterion("subscribe_status >", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_status >=", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLessThan(String value) {
            addCriterion("subscribe_status <", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLessThanOrEqualTo(String value) {
            addCriterion("subscribe_status <=", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLike(String value) {
            addCriterion("subscribe_status like", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotLike(String value) {
            addCriterion("subscribe_status not like", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIn(List<String> values) {
            addCriterion("subscribe_status in", values, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotIn(List<String> values) {
            addCriterion("subscribe_status not in", values, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusBetween(String value1, String value2) {
            addCriterion("subscribe_status between", value1, value2, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotBetween(String value1, String value2) {
            addCriterion("subscribe_status not between", value1, value2, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIsNull() {
            addCriterion("subscribe_result is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIsNotNull() {
            addCriterion("subscribe_result is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultEqualTo(String value) {
            addCriterion("subscribe_result =", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotEqualTo(String value) {
            addCriterion("subscribe_result <>", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultGreaterThan(String value) {
            addCriterion("subscribe_result >", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_result >=", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLessThan(String value) {
            addCriterion("subscribe_result <", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLessThanOrEqualTo(String value) {
            addCriterion("subscribe_result <=", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLike(String value) {
            addCriterion("subscribe_result like", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotLike(String value) {
            addCriterion("subscribe_result not like", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIn(List<String> values) {
            addCriterion("subscribe_result in", values, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotIn(List<String> values) {
            addCriterion("subscribe_result not in", values, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultBetween(String value1, String value2) {
            addCriterion("subscribe_result between", value1, value2, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotBetween(String value1, String value2) {
            addCriterion("subscribe_result not between", value1, value2, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(Integer value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(Integer value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(Integer value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(Integer value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(Integer value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<Integer> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<Integer> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(Integer value1, Integer value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}