package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.User;
import cn.com.cvit.nc.bean.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    /*
	 * 根据id删除用户
	 */
    int deleteByPrimaryKey(User user);

    int insert(User record);

    /*
	 * 根据email更新、修改用户
	 */
    int updateByEmail(User user);
    
	/*
	 * 清空用户表
	 */
    int deleteByAll();
    /*
     * 新增用户
     */
    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(String userId);
    
    List<User> selectByNamePrimaryKey(String email,String name);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}