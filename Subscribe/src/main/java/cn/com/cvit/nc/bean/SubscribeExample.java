package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubscribeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SubscribeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdIsNull() {
            addCriterion("subscribe_user_id is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdIsNotNull() {
            addCriterion("subscribe_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdEqualTo(String value) {
            addCriterion("subscribe_user_id =", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdNotEqualTo(String value) {
            addCriterion("subscribe_user_id <>", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdGreaterThan(String value) {
            addCriterion("subscribe_user_id >", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_id >=", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdLessThan(String value) {
            addCriterion("subscribe_user_id <", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_id <=", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdLike(String value) {
            addCriterion("subscribe_user_id like", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdNotLike(String value) {
            addCriterion("subscribe_user_id not like", value, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdIn(List<String> values) {
            addCriterion("subscribe_user_id in", values, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdNotIn(List<String> values) {
            addCriterion("subscribe_user_id not in", values, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdBetween(String value1, String value2) {
            addCriterion("subscribe_user_id between", value1, value2, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserIdNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_id not between", value1, value2, "subscribeUserId");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeIsNull() {
            addCriterion("subscribe_user_code is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeIsNotNull() {
            addCriterion("subscribe_user_code is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeEqualTo(String value) {
            addCriterion("subscribe_user_code =", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeNotEqualTo(String value) {
            addCriterion("subscribe_user_code <>", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeGreaterThan(String value) {
            addCriterion("subscribe_user_code >", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_code >=", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeLessThan(String value) {
            addCriterion("subscribe_user_code <", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_code <=", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeLike(String value) {
            addCriterion("subscribe_user_code like", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeNotLike(String value) {
            addCriterion("subscribe_user_code not like", value, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeIn(List<String> values) {
            addCriterion("subscribe_user_code in", values, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeNotIn(List<String> values) {
            addCriterion("subscribe_user_code not in", values, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeBetween(String value1, String value2) {
            addCriterion("subscribe_user_code between", value1, value2, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserCodeNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_code not between", value1, value2, "subscribeUserCode");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdIsNull() {
            addCriterion("subscribe_device_id is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdIsNotNull() {
            addCriterion("subscribe_device_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdEqualTo(String value) {
            addCriterion("subscribe_device_id =", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdNotEqualTo(String value) {
            addCriterion("subscribe_device_id <>", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdGreaterThan(String value) {
            addCriterion("subscribe_device_id >", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_device_id >=", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdLessThan(String value) {
            addCriterion("subscribe_device_id <", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdLessThanOrEqualTo(String value) {
            addCriterion("subscribe_device_id <=", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdLike(String value) {
            addCriterion("subscribe_device_id like", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdNotLike(String value) {
            addCriterion("subscribe_device_id not like", value, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdIn(List<String> values) {
            addCriterion("subscribe_device_id in", values, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdNotIn(List<String> values) {
            addCriterion("subscribe_device_id not in", values, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdBetween(String value1, String value2) {
            addCriterion("subscribe_device_id between", value1, value2, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeDeviceIdNotBetween(String value1, String value2) {
            addCriterion("subscribe_device_id not between", value1, value2, "subscribeDeviceId");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeIsNull() {
            addCriterion("subscribe_begin_time is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeIsNotNull() {
            addCriterion("subscribe_begin_time is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeEqualTo(Date value) {
            addCriterion("subscribe_begin_time =", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeNotEqualTo(Date value) {
            addCriterion("subscribe_begin_time <>", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeGreaterThan(Date value) {
            addCriterion("subscribe_begin_time >", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("subscribe_begin_time >=", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeLessThan(Date value) {
            addCriterion("subscribe_begin_time <", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeLessThanOrEqualTo(Date value) {
            addCriterion("subscribe_begin_time <=", value, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeIn(List<Date> values) {
            addCriterion("subscribe_begin_time in", values, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeNotIn(List<Date> values) {
            addCriterion("subscribe_begin_time not in", values, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeBetween(Date value1, Date value2) {
            addCriterion("subscribe_begin_time between", value1, value2, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimeNotBetween(Date value1, Date value2) {
            addCriterion("subscribe_begin_time not between", value1, value2, "subscribeBeginTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeIsNull() {
            addCriterion("subscribe_end_time is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeIsNotNull() {
            addCriterion("subscribe_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeEqualTo(Date value) {
            addCriterion("subscribe_end_time =", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeNotEqualTo(Date value) {
            addCriterion("subscribe_end_time <>", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeGreaterThan(Date value) {
            addCriterion("subscribe_end_time >", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("subscribe_end_time >=", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeLessThan(Date value) {
            addCriterion("subscribe_end_time <", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("subscribe_end_time <=", value, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeIn(List<Date> values) {
            addCriterion("subscribe_end_time in", values, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeNotIn(List<Date> values) {
            addCriterion("subscribe_end_time not in", values, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeBetween(Date value1, Date value2) {
            addCriterion("subscribe_end_time between", value1, value2, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("subscribe_end_time not between", value1, value2, "subscribeEndTime");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesIsNull() {
            addCriterion("subscribe_begin_times is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesIsNotNull() {
            addCriterion("subscribe_begin_times is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesEqualTo(Date value) {
            addCriterion("subscribe_begin_times =", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesNotEqualTo(Date value) {
            addCriterion("subscribe_begin_times <>", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesGreaterThan(Date value) {
            addCriterion("subscribe_begin_times >", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesGreaterThanOrEqualTo(Date value) {
            addCriterion("subscribe_begin_times >=", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesLessThan(Date value) {
            addCriterion("subscribe_begin_times <", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesLessThanOrEqualTo(Date value) {
            addCriterion("subscribe_begin_times <=", value, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesIn(List<Date> values) {
            addCriterion("subscribe_begin_times in", values, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesNotIn(List<Date> values) {
            addCriterion("subscribe_begin_times not in", values, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesBetween(Date value1, Date value2) {
            addCriterion("subscribe_begin_times between", value1, value2, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeBeginTimesNotBetween(Date value1, Date value2) {
            addCriterion("subscribe_begin_times not between", value1, value2, "subscribeBeginTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesIsNull() {
            addCriterion("subscribe_end_times is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesIsNotNull() {
            addCriterion("subscribe_end_times is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesEqualTo(Date value) {
            addCriterion("subscribe_end_times =", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesNotEqualTo(Date value) {
            addCriterion("subscribe_end_times <>", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesGreaterThan(Date value) {
            addCriterion("subscribe_end_times >", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesGreaterThanOrEqualTo(Date value) {
            addCriterion("subscribe_end_times >=", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesLessThan(Date value) {
            addCriterion("subscribe_end_times <", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesLessThanOrEqualTo(Date value) {
            addCriterion("subscribe_end_times <=", value, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesIn(List<Date> values) {
            addCriterion("subscribe_end_times in", values, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesNotIn(List<Date> values) {
            addCriterion("subscribe_end_times not in", values, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesBetween(Date value1, Date value2) {
            addCriterion("subscribe_end_times between", value1, value2, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeEndTimesNotBetween(Date value1, Date value2) {
            addCriterion("subscribe_end_times not between", value1, value2, "subscribeEndTimes");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIsNull() {
            addCriterion("subscribe_user_name is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIsNotNull() {
            addCriterion("subscribe_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameEqualTo(String value) {
            addCriterion("subscribe_user_name =", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotEqualTo(String value) {
            addCriterion("subscribe_user_name <>", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameGreaterThan(String value) {
            addCriterion("subscribe_user_name >", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_name >=", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLessThan(String value) {
            addCriterion("subscribe_user_name <", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_name <=", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameLike(String value) {
            addCriterion("subscribe_user_name like", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotLike(String value) {
            addCriterion("subscribe_user_name not like", value, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameIn(List<String> values) {
            addCriterion("subscribe_user_name in", values, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotIn(List<String> values) {
            addCriterion("subscribe_user_name not in", values, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameBetween(String value1, String value2) {
            addCriterion("subscribe_user_name between", value1, value2, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserNameNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_name not between", value1, value2, "subscribeUserName");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIsNull() {
            addCriterion("subscribe_user_mobile is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIsNotNull() {
            addCriterion("subscribe_user_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileEqualTo(String value) {
            addCriterion("subscribe_user_mobile =", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotEqualTo(String value) {
            addCriterion("subscribe_user_mobile <>", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileGreaterThan(String value) {
            addCriterion("subscribe_user_mobile >", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_user_mobile >=", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLessThan(String value) {
            addCriterion("subscribe_user_mobile <", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLessThanOrEqualTo(String value) {
            addCriterion("subscribe_user_mobile <=", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileLike(String value) {
            addCriterion("subscribe_user_mobile like", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotLike(String value) {
            addCriterion("subscribe_user_mobile not like", value, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileIn(List<String> values) {
            addCriterion("subscribe_user_mobile in", values, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotIn(List<String> values) {
            addCriterion("subscribe_user_mobile not in", values, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileBetween(String value1, String value2) {
            addCriterion("subscribe_user_mobile between", value1, value2, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeUserMobileNotBetween(String value1, String value2) {
            addCriterion("subscribe_user_mobile not between", value1, value2, "subscribeUserMobile");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIsNull() {
            addCriterion("subscribe_remark is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIsNotNull() {
            addCriterion("subscribe_remark is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkEqualTo(String value) {
            addCriterion("subscribe_remark =", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotEqualTo(String value) {
            addCriterion("subscribe_remark <>", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkGreaterThan(String value) {
            addCriterion("subscribe_remark >", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_remark >=", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLessThan(String value) {
            addCriterion("subscribe_remark <", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLessThanOrEqualTo(String value) {
            addCriterion("subscribe_remark <=", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkLike(String value) {
            addCriterion("subscribe_remark like", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotLike(String value) {
            addCriterion("subscribe_remark not like", value, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkIn(List<String> values) {
            addCriterion("subscribe_remark in", values, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotIn(List<String> values) {
            addCriterion("subscribe_remark not in", values, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkBetween(String value1, String value2) {
            addCriterion("subscribe_remark between", value1, value2, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeRemarkNotBetween(String value1, String value2) {
            addCriterion("subscribe_remark not between", value1, value2, "subscribeRemark");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIsNull() {
            addCriterion("subscribe_status is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIsNotNull() {
            addCriterion("subscribe_status is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusEqualTo(String value) {
            addCriterion("subscribe_status =", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotEqualTo(String value) {
            addCriterion("subscribe_status <>", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusGreaterThan(String value) {
            addCriterion("subscribe_status >", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_status >=", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLessThan(String value) {
            addCriterion("subscribe_status <", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLessThanOrEqualTo(String value) {
            addCriterion("subscribe_status <=", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusLike(String value) {
            addCriterion("subscribe_status like", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotLike(String value) {
            addCriterion("subscribe_status not like", value, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusIn(List<String> values) {
            addCriterion("subscribe_status in", values, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotIn(List<String> values) {
            addCriterion("subscribe_status not in", values, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusBetween(String value1, String value2) {
            addCriterion("subscribe_status between", value1, value2, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeStatusNotBetween(String value1, String value2) {
            addCriterion("subscribe_status not between", value1, value2, "subscribeStatus");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIsNull() {
            addCriterion("subscribe_result is null");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIsNotNull() {
            addCriterion("subscribe_result is not null");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultEqualTo(String value) {
            addCriterion("subscribe_result =", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotEqualTo(String value) {
            addCriterion("subscribe_result <>", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultGreaterThan(String value) {
            addCriterion("subscribe_result >", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultGreaterThanOrEqualTo(String value) {
            addCriterion("subscribe_result >=", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLessThan(String value) {
            addCriterion("subscribe_result <", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLessThanOrEqualTo(String value) {
            addCriterion("subscribe_result <=", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultLike(String value) {
            addCriterion("subscribe_result like", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotLike(String value) {
            addCriterion("subscribe_result not like", value, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultIn(List<String> values) {
            addCriterion("subscribe_result in", values, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotIn(List<String> values) {
            addCriterion("subscribe_result not in", values, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultBetween(String value1, String value2) {
            addCriterion("subscribe_result between", value1, value2, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andSubscribeResultNotBetween(String value1, String value2) {
            addCriterion("subscribe_result not between", value1, value2, "subscribeResult");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(Integer value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(Integer value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(Integer value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(Integer value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(Integer value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<Integer> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<Integer> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(Integer value1, Integer value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}