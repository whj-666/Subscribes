package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.Organize;
import cn.com.cvit.nc.bean.OrganizeExample;
import cn.com.cvit.nc.dao.OrganizeMapper;

@Service
public class OrganizeService {
	@Autowired
	private OrganizeMapper organizeMapper;
	
	//根据id删除部门
    public int deleteByPrimaryKey(String id){
		return organizeMapper.deleteByPrimaryKey(id);
    }
    //删除所有部门
    public int deleteOrganizeAll(){
		return organizeMapper.deleteOrganizeAll();
    }
    //新增部门
    public int insertSelective(Organize record){
		return organizeMapper.insertSelective(record);
    }
    //查询所有
    public List<Organize> selectByExample(OrganizeExample example){
		return organizeMapper.selectByExample(example);
    }
    //根据code编号查询
    public List<Organize> selectByOrganizeCode(Organize record){
		return organizeMapper.selectByOrganizeCode(record);
    }
    //根据ID查询
    public List<Organize> selectByOrganizeId(Organize record){
		return organizeMapper.selectByOrganizeId(record);
    }
    //根据PID查询
    public List<Organize> selectByOrganizePId(String pid){
		return organizeMapper.selectByOrganizePId(pid);
    }
    //根据id修改
    public int updateByOrganizeId(Organize record){
		return organizeMapper.updateByOrganizeId(record);
    }
	
	
}
