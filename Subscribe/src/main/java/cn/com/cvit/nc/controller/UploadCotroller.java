package cn.com.cvit.nc.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("upload")
public class UploadCotroller {
	
	private final String IMAGE_UPLOAD_PATH = "f:\\"+File.separator+"wl"+File.separator+"temp";
	
	@RequestMapping("/openAddView.do")
	public ModelAndView openAddView() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("addView");
		return mv;
	}
	
	@RequestMapping("/image.do")
	public void upload(@RequestParam("fileList") MultipartFile fileList,HttpServletResponse resp,HttpServletRequest request){
			try {
				String dir = request.getSession().getServletContext().getRealPath("/");
				StringBuilder delete = new StringBuilder(dir.substring(0, dir.indexOf("Subscribe")));
				String filePath = "";
				filePath = delete+"Subscribe"+File.separator+"Subscribe_image"+File.separator+fileList.getOriginalFilename();
				File file = new File(filePath);
				fileList.transferTo(file);
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write(filePath+";");
				System.out.println(filePath);
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	//自动更换名称
	private String getUniqueFileName(String originalFileName) {
		String ext = originalFileName.substring(originalFileName.lastIndexOf("."),originalFileName.length());
		String originalPrefixFileName = originalFileName.substring(0,originalFileName.lastIndexOf("."));
		UUID uuid = UUID.randomUUID();
		String uniqueFileName = originalPrefixFileName+uuid+ext;
		return uniqueFileName;
	}
}
