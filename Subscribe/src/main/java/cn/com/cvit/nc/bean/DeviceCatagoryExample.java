package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.List;

public class DeviceCatagoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DeviceCatagoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberIsNull() {
            addCriterion("equipment_number is null");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberIsNotNull() {
            addCriterion("equipment_number is not null");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberEqualTo(Integer value) {
            addCriterion("equipment_number =", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberNotEqualTo(Integer value) {
            addCriterion("equipment_number <>", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberGreaterThan(Integer value) {
            addCriterion("equipment_number >", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("equipment_number >=", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberLessThan(Integer value) {
            addCriterion("equipment_number <", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberLessThanOrEqualTo(Integer value) {
            addCriterion("equipment_number <=", value, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberIn(List<Integer> values) {
            addCriterion("equipment_number in", values, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberNotIn(List<Integer> values) {
            addCriterion("equipment_number not in", values, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberBetween(Integer value1, Integer value2) {
            addCriterion("equipment_number between", value1, value2, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("equipment_number not between", value1, value2, "equipmentNumber");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameIsNull() {
            addCriterion("equipment_name is null");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameIsNotNull() {
            addCriterion("equipment_name is not null");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameEqualTo(String value) {
            addCriterion("equipment_name =", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameNotEqualTo(String value) {
            addCriterion("equipment_name <>", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameGreaterThan(String value) {
            addCriterion("equipment_name >", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameGreaterThanOrEqualTo(String value) {
            addCriterion("equipment_name >=", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameLessThan(String value) {
            addCriterion("equipment_name <", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameLessThanOrEqualTo(String value) {
            addCriterion("equipment_name <=", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameLike(String value) {
            addCriterion("equipment_name like", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameNotLike(String value) {
            addCriterion("equipment_name not like", value, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameIn(List<String> values) {
            addCriterion("equipment_name in", values, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameNotIn(List<String> values) {
            addCriterion("equipment_name not in", values, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameBetween(String value1, String value2) {
            addCriterion("equipment_name between", value1, value2, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentNameNotBetween(String value1, String value2) {
            addCriterion("equipment_name not between", value1, value2, "equipmentName");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellIsNull() {
            addCriterion("equipment_spell is null");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellIsNotNull() {
            addCriterion("equipment_spell is not null");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellEqualTo(String value) {
            addCriterion("equipment_spell =", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellNotEqualTo(String value) {
            addCriterion("equipment_spell <>", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellGreaterThan(String value) {
            addCriterion("equipment_spell >", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellGreaterThanOrEqualTo(String value) {
            addCriterion("equipment_spell >=", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellLessThan(String value) {
            addCriterion("equipment_spell <", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellLessThanOrEqualTo(String value) {
            addCriterion("equipment_spell <=", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellLike(String value) {
            addCriterion("equipment_spell like", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellNotLike(String value) {
            addCriterion("equipment_spell not like", value, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellIn(List<String> values) {
            addCriterion("equipment_spell in", values, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellNotIn(List<String> values) {
            addCriterion("equipment_spell not in", values, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellBetween(String value1, String value2) {
            addCriterion("equipment_spell between", value1, value2, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentSpellNotBetween(String value1, String value2) {
            addCriterion("equipment_spell not between", value1, value2, "equipmentSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellIsNull() {
            addCriterion("equipment_first_spell is null");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellIsNotNull() {
            addCriterion("equipment_first_spell is not null");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellEqualTo(String value) {
            addCriterion("equipment_first_spell =", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellNotEqualTo(String value) {
            addCriterion("equipment_first_spell <>", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellGreaterThan(String value) {
            addCriterion("equipment_first_spell >", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellGreaterThanOrEqualTo(String value) {
            addCriterion("equipment_first_spell >=", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellLessThan(String value) {
            addCriterion("equipment_first_spell <", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellLessThanOrEqualTo(String value) {
            addCriterion("equipment_first_spell <=", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellLike(String value) {
            addCriterion("equipment_first_spell like", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellNotLike(String value) {
            addCriterion("equipment_first_spell not like", value, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellIn(List<String> values) {
            addCriterion("equipment_first_spell in", values, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellNotIn(List<String> values) {
            addCriterion("equipment_first_spell not in", values, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellBetween(String value1, String value2) {
            addCriterion("equipment_first_spell between", value1, value2, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andEquipmentFirstSpellNotBetween(String value1, String value2) {
            addCriterion("equipment_first_spell not between", value1, value2, "equipmentFirstSpell");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(Integer value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(Integer value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(Integer value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(Integer value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(Integer value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<Integer> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<Integer> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(Integer value1, Integer value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}