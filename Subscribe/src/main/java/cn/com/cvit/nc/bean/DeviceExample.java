package cn.com.cvit.nc.bean;

import java.util.ArrayList;
import java.util.List;

public class DeviceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DeviceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDeviceNoIsNull() {
            addCriterion("device_no is null");
            return (Criteria) this;
        }

        public Criteria andDeviceNoIsNotNull() {
            addCriterion("device_no is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceNoEqualTo(Integer value) {
            addCriterion("device_no =", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoNotEqualTo(Integer value) {
            addCriterion("device_no <>", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoGreaterThan(Integer value) {
            addCriterion("device_no >", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("device_no >=", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoLessThan(Integer value) {
            addCriterion("device_no <", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoLessThanOrEqualTo(Integer value) {
            addCriterion("device_no <=", value, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoIn(List<Integer> values) {
            addCriterion("device_no in", values, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoNotIn(List<Integer> values) {
            addCriterion("device_no not in", values, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoBetween(Integer value1, Integer value2) {
            addCriterion("device_no between", value1, value2, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNoNotBetween(Integer value1, Integer value2) {
            addCriterion("device_no not between", value1, value2, "deviceNo");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberIsNull() {
            addCriterion("device_number is null");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberIsNotNull() {
            addCriterion("device_number is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberEqualTo(String value) {
            addCriterion("device_number =", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberNotEqualTo(String value) {
            addCriterion("device_number <>", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberGreaterThan(String value) {
            addCriterion("device_number >", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberGreaterThanOrEqualTo(String value) {
            addCriterion("device_number >=", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberLessThan(String value) {
            addCriterion("device_number <", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberLessThanOrEqualTo(String value) {
            addCriterion("device_number <=", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberLike(String value) {
            addCriterion("device_number like", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberNotLike(String value) {
            addCriterion("device_number not like", value, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberIn(List<String> values) {
            addCriterion("device_number in", values, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberNotIn(List<String> values) {
            addCriterion("device_number not in", values, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberBetween(String value1, String value2) {
            addCriterion("device_number between", value1, value2, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceNumberNotBetween(String value1, String value2) {
            addCriterion("device_number not between", value1, value2, "deviceNumber");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIsNull() {
            addCriterion("device_brand is null");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIsNotNull() {
            addCriterion("device_brand is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandEqualTo(String value) {
            addCriterion("device_brand =", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotEqualTo(String value) {
            addCriterion("device_brand <>", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandGreaterThan(String value) {
            addCriterion("device_brand >", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandGreaterThanOrEqualTo(String value) {
            addCriterion("device_brand >=", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLessThan(String value) {
            addCriterion("device_brand <", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLessThanOrEqualTo(String value) {
            addCriterion("device_brand <=", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLike(String value) {
            addCriterion("device_brand like", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotLike(String value) {
            addCriterion("device_brand not like", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIn(List<String> values) {
            addCriterion("device_brand in", values, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotIn(List<String> values) {
            addCriterion("device_brand not in", values, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandBetween(String value1, String value2) {
            addCriterion("device_brand between", value1, value2, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotBetween(String value1, String value2) {
            addCriterion("device_brand not between", value1, value2, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNull() {
            addCriterion("device_type is null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNotNull() {
            addCriterion("device_type is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeEqualTo(String value) {
            addCriterion("device_type =", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotEqualTo(String value) {
            addCriterion("device_type <>", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThan(String value) {
            addCriterion("device_type >", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThanOrEqualTo(String value) {
            addCriterion("device_type >=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThan(String value) {
            addCriterion("device_type <", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThanOrEqualTo(String value) {
            addCriterion("device_type <=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLike(String value) {
            addCriterion("device_type like", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotLike(String value) {
            addCriterion("device_type not like", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIn(List<String> values) {
            addCriterion("device_type in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotIn(List<String> values) {
            addCriterion("device_type not in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeBetween(String value1, String value2) {
            addCriterion("device_type between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotBetween(String value1, String value2) {
            addCriterion("device_type not between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceClassIsNull() {
            addCriterion("device_class is null");
            return (Criteria) this;
        }

        public Criteria andDeviceClassIsNotNull() {
            addCriterion("device_class is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceClassEqualTo(String value) {
            addCriterion("device_class =", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassNotEqualTo(String value) {
            addCriterion("device_class <>", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassGreaterThan(String value) {
            addCriterion("device_class >", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassGreaterThanOrEqualTo(String value) {
            addCriterion("device_class >=", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassLessThan(String value) {
            addCriterion("device_class <", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassLessThanOrEqualTo(String value) {
            addCriterion("device_class <=", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassLike(String value) {
            addCriterion("device_class like", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassNotLike(String value) {
            addCriterion("device_class not like", value, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassIn(List<String> values) {
            addCriterion("device_class in", values, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassNotIn(List<String> values) {
            addCriterion("device_class not in", values, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassBetween(String value1, String value2) {
            addCriterion("device_class between", value1, value2, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDeviceClassNotBetween(String value1, String value2) {
            addCriterion("device_class not between", value1, value2, "deviceClass");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoIsNull() {
            addCriterion("device_photo is null");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoIsNotNull() {
            addCriterion("device_photo is not null");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoEqualTo(String value) {
            addCriterion("device_photo =", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoNotEqualTo(String value) {
            addCriterion("device_photo <>", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoGreaterThan(String value) {
            addCriterion("device_photo >", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoGreaterThanOrEqualTo(String value) {
            addCriterion("device_photo >=", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoLessThan(String value) {
            addCriterion("device_photo <", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoLessThanOrEqualTo(String value) {
            addCriterion("device_photo <=", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoLike(String value) {
            addCriterion("device_photo like", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoNotLike(String value) {
            addCriterion("device_photo not like", value, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoIn(List<String> values) {
            addCriterion("device_photo in", values, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoNotIn(List<String> values) {
            addCriterion("device_photo not in", values, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoBetween(String value1, String value2) {
            addCriterion("device_photo between", value1, value2, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePhotoNotBetween(String value1, String value2) {
            addCriterion("device_photo not between", value1, value2, "devicePhoto");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceIsNull() {
            addCriterion("device_place is null");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceIsNotNull() {
            addCriterion("device_place is not null");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceEqualTo(String value) {
            addCriterion("device_place =", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceNotEqualTo(String value) {
            addCriterion("device_place <>", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceGreaterThan(String value) {
            addCriterion("device_place >", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceGreaterThanOrEqualTo(String value) {
            addCriterion("device_place >=", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceLessThan(String value) {
            addCriterion("device_place <", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceLessThanOrEqualTo(String value) {
            addCriterion("device_place <=", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceLike(String value) {
            addCriterion("device_place like", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceNotLike(String value) {
            addCriterion("device_place not like", value, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceIn(List<String> values) {
            addCriterion("device_place in", values, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceNotIn(List<String> values) {
            addCriterion("device_place not in", values, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceBetween(String value1, String value2) {
            addCriterion("device_place between", value1, value2, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDevicePlaceNotBetween(String value1, String value2) {
            addCriterion("device_place not between", value1, value2, "devicePlace");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusIsNull() {
            addCriterion("device_status is null");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusIsNotNull() {
            addCriterion("device_status is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusEqualTo(String value) {
            addCriterion("device_status =", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusNotEqualTo(String value) {
            addCriterion("device_status <>", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusGreaterThan(String value) {
            addCriterion("device_status >", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusGreaterThanOrEqualTo(String value) {
            addCriterion("device_status >=", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusLessThan(String value) {
            addCriterion("device_status <", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusLessThanOrEqualTo(String value) {
            addCriterion("device_status <=", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusLike(String value) {
            addCriterion("device_status like", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusNotLike(String value) {
            addCriterion("device_status not like", value, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusIn(List<String> values) {
            addCriterion("device_status in", values, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusNotIn(List<String> values) {
            addCriterion("device_status not in", values, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusBetween(String value1, String value2) {
            addCriterion("device_status between", value1, value2, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andDeviceStatusNotBetween(String value1, String value2) {
            addCriterion("device_status not between", value1, value2, "deviceStatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(Integer value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(Integer value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(Integer value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(Integer value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(Integer value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<Integer> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<Integer> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(Integer value1, Integer value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkIsNull() {
            addCriterion("device_remark is null");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkIsNotNull() {
            addCriterion("device_remark is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkEqualTo(String value) {
            addCriterion("device_remark =", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkNotEqualTo(String value) {
            addCriterion("device_remark <>", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkGreaterThan(String value) {
            addCriterion("device_remark >", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("device_remark >=", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkLessThan(String value) {
            addCriterion("device_remark <", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkLessThanOrEqualTo(String value) {
            addCriterion("device_remark <=", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkLike(String value) {
            addCriterion("device_remark like", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkNotLike(String value) {
            addCriterion("device_remark not like", value, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkIn(List<String> values) {
            addCriterion("device_remark in", values, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkNotIn(List<String> values) {
            addCriterion("device_remark not in", values, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkBetween(String value1, String value2) {
            addCriterion("device_remark between", value1, value2, "deviceRemark");
            return (Criteria) this;
        }

        public Criteria andDeviceRemarkNotBetween(String value1, String value2) {
            addCriterion("device_remark not between", value1, value2, "deviceRemark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}