package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.cvit.nc.bean.Device;
import cn.com.cvit.nc.dao.DeviceMapper;

@Service
public class DeviceService {
	
	@Autowired
	DeviceMapper deviceMapper;
	/**
	 * 查询所有设备信息
	 * @return
	 * */
	public List<Device> getAll(){
		return deviceMapper.selectByExample(null);
	}
	
	/**
	 * 查询所有Flag=1的设备
	 * @return
	 */
	public List<Device> getDeviceFlag(Integer flag){
		return deviceMapper.selectByDeviceFlag(flag);
	}
	
	/**
	 * 按照设备类别id查询设备信息
	 * @return
	 */
	public List<Device> getDeviceCatagoryId(String id) {
		List<Device> deviceC = deviceMapper.selectByDeviceClass(id);
		return deviceC;
	}
	public List<Device> selectByDeviceClassIdOrNull(Device device){
		return deviceMapper.selectByDeviceClassIdOrNull(device);
	}
	/**
	 * 根据设备id查询设备数据
	 * */
	public Device getDevice(String id) {
		Device deviceId = deviceMapper.selectByPrimaryKey(id);
		return deviceId;
	}
	
	/**
	 * 根据设备id查询图片子表
	 * */
	public List<Device> getImgDeviceId(String id) {
		List<Device> deviceI = deviceMapper.selectImgByDeviceId(id);
		return deviceI;
	}
	
//	/**
//	 * 根据设备id查询设备数据+图片子表
//	 * */
//	public Device getDeviceAndImg(String id) {
//		Device deviceId = deviceMapper.selectByPrimaryKeyWithImg(id);
//		System.out.println(deviceId);
//		return deviceId;
//	}
	
	/**
	 * 设备更新
	 * @param Device
	 * */
	public void updateDevice(Device device) {
		deviceMapper.updateByPrimaryKeySelective(device);
	}
	//更新设备图片
	public int updateImages(Device record){
		return deviceMapper.updateImages(record);
	}
	//批量更新-假删除
	public void updateByDeviceIds(String [] ids) {
		deviceMapper.updateByDeviceIds(ids);
	}
	/**
	 * 类别图片更新
	 * @param Device
	 * */
	public void updateImage(Device device) {
		deviceMapper.updateByCategory(device);
	}

	/**
	 * 设备保存
	 * @param Device
	 * */
	public void saveDevice(Device devices) {
		deviceMapper.insert(devices);
	}
}
