package cn.com.cvit.nc.util;

import java.util.Random;

public class Utils {
	
	/**
	 * 取随机产生(6个字符)
	 * 
	 * @return
	 */
	public static String createRandString() {
		// 生成随机类
		Random random = new Random();
		String sRand = "";
		String[] bytes = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", 
				"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", 
				"Y", "Z","a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", 
				"m", "b", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", 
				"Y", "Z" };
		for (int i = 0; i < 5; i++) {
			String rand = bytes[random.nextInt(36)];
			sRand += rand;
		}
		return sRand;
	} 

}
