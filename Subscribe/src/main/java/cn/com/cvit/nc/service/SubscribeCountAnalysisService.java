package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeCount;
import cn.com.cvit.nc.bean.SubscribeCountParameter;
import cn.com.cvit.nc.dao.SubscribeCountAnalysisMapper;

@Service
public class SubscribeCountAnalysisService {
	@Autowired
	SubscribeCountAnalysisMapper subscribeCountAnalysisMapper;
	
	//所有设备使用时长
	public String selectAllDeviceTimes(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectAllDeviceTimes(parameter);
	}
	//单一设备使用时长
	public List<SubscribeCount> selectEveryDeviceTimes(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectEveryDeviceTimes(parameter);
	}
	//单一设备类别的使用时长和次数
	public List<SubscribeCount> selectEveryDeviceCategoryTimes(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectEveryDeviceCategoryTimes(parameter);
	}
	//单一设备类别的使用时长和次数
	public List<SubscribeCount> selectEveryUserTimes(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectEveryUserTimes(parameter);
	}
	//单一部门的使用时长和次数
	public List<SubscribeCount> selectEveryOrganizeTimes(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectEveryOrganizeTimes(parameter);
	}
	//预约数据
	public List<Subscribe> selectSubscribeCountData(SubscribeCountParameter parameter){
		return subscribeCountAnalysisMapper.selectSubscribeCountData(parameter);
	}
	
	
}
