package cn.com.cvit.nc.bean;

import java.util.Date;

public class User {
    private String userId;

    private String userName;

    private String userCode;

    private String userPassword;

    private Integer userSex;

    private String userEmail;

    private String userOfficePhone;

    private String userMobilePhone;

    private String userTeam;

    private String userRole;

    private Integer userOrder;

    private Date userCreateDate;

    private Integer userModifyTimes;

    private String userLeatestModifyUser;

    private Integer userStatus;

    private String userPosition;

    private String userEmployment;

    private String userPhoto;

    private String userWfrole;

    private String userWfposition;

    private String userShortname;

    private Integer userPwdChangeTimes;

    private Date userPwdChangeDate;

    private String userTruename;

    private String userMailPath;

    private String userMailFile;

    private String userMailServer;

    private String userBackup1;

    private String userBackup2;

    private Integer sequenceId;

    private String userCompany;

    private String userPinyin;

    private String userPinyinLetters;

    private String userPinyinFirstletter;

    private String imKey;

    private String idNumber;

    private String myleaderName;

    private String myleaderId;

    private Integer securityLevel;

    private String userOtherPhone;

    private Date userBirthday;

    private String hrEmplid;

    private String hrAdminRankChn;

    private String hrDeptid;

    private String hrEmail1;

    private String hrEmail2;

    private String hrEmail3;

    private String hrOldEmoid;

    private String hrStatus;

    private Integer userMobileCheck;

    private Integer isAdmin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode == null ? null : userCode.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public Integer getUserSex() {
        return userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public String getUserOfficePhone() {
        return userOfficePhone;
    }

    public void setUserOfficePhone(String userOfficePhone) {
        this.userOfficePhone = userOfficePhone == null ? null : userOfficePhone.trim();
    }

    public String getUserMobilePhone() {
        return userMobilePhone;
    }

    public void setUserMobilePhone(String userMobilePhone) {
        this.userMobilePhone = userMobilePhone == null ? null : userMobilePhone.trim();
    }

    public String getUserTeam() {
        return userTeam;
    }

    public void setUserTeam(String userTeam) {
        this.userTeam = userTeam == null ? null : userTeam.trim();
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole == null ? null : userRole.trim();
    }

    public Integer getUserOrder() {
        return userOrder;
    }

    public void setUserOrder(Integer userOrder) {
        this.userOrder = userOrder;
    }

    public Date getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(Date userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public Integer getUserModifyTimes() {
        return userModifyTimes;
    }

    public void setUserModifyTimes(Integer userModifyTimes) {
        this.userModifyTimes = userModifyTimes;
    }

    public String getUserLeatestModifyUser() {
        return userLeatestModifyUser;
    }

    public void setUserLeatestModifyUser(String userLeatestModifyUser) {
        this.userLeatestModifyUser = userLeatestModifyUser == null ? null : userLeatestModifyUser.trim();
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition == null ? null : userPosition.trim();
    }

    public String getUserEmployment() {
        return userEmployment;
    }

    public void setUserEmployment(String userEmployment) {
        this.userEmployment = userEmployment == null ? null : userEmployment.trim();
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto == null ? null : userPhoto.trim();
    }

    public String getUserWfrole() {
        return userWfrole;
    }

    public void setUserWfrole(String userWfrole) {
        this.userWfrole = userWfrole == null ? null : userWfrole.trim();
    }

    public String getUserWfposition() {
        return userWfposition;
    }

    public void setUserWfposition(String userWfposition) {
        this.userWfposition = userWfposition == null ? null : userWfposition.trim();
    }

    public String getUserShortname() {
        return userShortname;
    }

    public void setUserShortname(String userShortname) {
        this.userShortname = userShortname == null ? null : userShortname.trim();
    }

    public Integer getUserPwdChangeTimes() {
        return userPwdChangeTimes;
    }

    public void setUserPwdChangeTimes(Integer userPwdChangeTimes) {
        this.userPwdChangeTimes = userPwdChangeTimes;
    }

    public Date getUserPwdChangeDate() {
        return userPwdChangeDate;
    }

    public void setUserPwdChangeDate(Date userPwdChangeDate) {
        this.userPwdChangeDate = userPwdChangeDate;
    }

    public String getUserTruename() {
        return userTruename;
    }

    public void setUserTruename(String userTruename) {
        this.userTruename = userTruename == null ? null : userTruename.trim();
    }

    public String getUserMailPath() {
        return userMailPath;
    }

    public void setUserMailPath(String userMailPath) {
        this.userMailPath = userMailPath == null ? null : userMailPath.trim();
    }

    public String getUserMailFile() {
        return userMailFile;
    }

    public void setUserMailFile(String userMailFile) {
        this.userMailFile = userMailFile == null ? null : userMailFile.trim();
    }

    public String getUserMailServer() {
        return userMailServer;
    }

    public void setUserMailServer(String userMailServer) {
        this.userMailServer = userMailServer == null ? null : userMailServer.trim();
    }

    public String getUserBackup1() {
        return userBackup1;
    }

    public void setUserBackup1(String userBackup1) {
        this.userBackup1 = userBackup1 == null ? null : userBackup1.trim();
    }

    public String getUserBackup2() {
        return userBackup2;
    }

    public void setUserBackup2(String userBackup2) {
        this.userBackup2 = userBackup2 == null ? null : userBackup2.trim();
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany == null ? null : userCompany.trim();
    }

    public String getUserPinyin() {
        return userPinyin;
    }

    public void setUserPinyin(String userPinyin) {
        this.userPinyin = userPinyin == null ? null : userPinyin.trim();
    }

    public String getUserPinyinLetters() {
        return userPinyinLetters;
    }

    public void setUserPinyinLetters(String userPinyinLetters) {
        this.userPinyinLetters = userPinyinLetters == null ? null : userPinyinLetters.trim();
    }

    public String getUserPinyinFirstletter() {
        return userPinyinFirstletter;
    }

    public void setUserPinyinFirstletter(String userPinyinFirstletter) {
        this.userPinyinFirstletter = userPinyinFirstletter == null ? null : userPinyinFirstletter.trim();
    }

    public String getImKey() {
        return imKey;
    }

    public void setImKey(String imKey) {
        this.imKey = imKey == null ? null : imKey.trim();
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    public String getMyleaderName() {
        return myleaderName;
    }

    public void setMyleaderName(String myleaderName) {
        this.myleaderName = myleaderName == null ? null : myleaderName.trim();
    }

    public String getMyleaderId() {
        return myleaderId;
    }

    public void setMyleaderId(String myleaderId) {
        this.myleaderId = myleaderId == null ? null : myleaderId.trim();
    }

    public Integer getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(Integer securityLevel) {
        this.securityLevel = securityLevel;
    }

    public String getUserOtherPhone() {
        return userOtherPhone;
    }

    public void setUserOtherPhone(String userOtherPhone) {
        this.userOtherPhone = userOtherPhone == null ? null : userOtherPhone.trim();
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getHrEmplid() {
        return hrEmplid;
    }

    public void setHrEmplid(String hrEmplid) {
        this.hrEmplid = hrEmplid == null ? null : hrEmplid.trim();
    }

    public String getHrAdminRankChn() {
        return hrAdminRankChn;
    }

    public void setHrAdminRankChn(String hrAdminRankChn) {
        this.hrAdminRankChn = hrAdminRankChn == null ? null : hrAdminRankChn.trim();
    }

    public String getHrDeptid() {
        return hrDeptid;
    }

    public void setHrDeptid(String hrDeptid) {
        this.hrDeptid = hrDeptid == null ? null : hrDeptid.trim();
    }

    public String getHrEmail1() {
        return hrEmail1;
    }

    public void setHrEmail1(String hrEmail1) {
        this.hrEmail1 = hrEmail1 == null ? null : hrEmail1.trim();
    }

    public String getHrEmail2() {
        return hrEmail2;
    }

    public void setHrEmail2(String hrEmail2) {
        this.hrEmail2 = hrEmail2 == null ? null : hrEmail2.trim();
    }

    public String getHrEmail3() {
        return hrEmail3;
    }

    public void setHrEmail3(String hrEmail3) {
        this.hrEmail3 = hrEmail3 == null ? null : hrEmail3.trim();
    }

    public String getHrOldEmoid() {
        return hrOldEmoid;
    }

    public void setHrOldEmoid(String hrOldEmoid) {
        this.hrOldEmoid = hrOldEmoid == null ? null : hrOldEmoid.trim();
    }

    public String getHrStatus() {
        return hrStatus;
    }

    public void setHrStatus(String hrStatus) {
        this.hrStatus = hrStatus == null ? null : hrStatus.trim();
    }

    public Integer getUserMobileCheck() {
        return userMobileCheck;
    }

    public void setUserMobileCheck(Integer userMobileCheck) {
        this.userMobileCheck = userMobileCheck;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }
}