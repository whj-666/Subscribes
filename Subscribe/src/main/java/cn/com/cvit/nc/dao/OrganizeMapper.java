package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Organize;
import cn.com.cvit.nc.bean.OrganizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrganizeMapper {
    long countByExample(OrganizeExample example);
    
    //根据id删除部门
    int deleteByPrimaryKey(String id);
    //删除所有部门
    int deleteOrganizeAll();
    //新增部门
    int insertSelective(Organize record);
    //查询所有
    List<Organize> selectByExample(OrganizeExample example);
    //根据code编号查询
    List<Organize> selectByOrganizeCode(Organize record);
    //根据Id查询
    List<Organize> selectByOrganizeId(Organize record);
    //根据PId查询
    List<Organize> selectByOrganizePId(String pid);
    //根据id修改
    int updateByOrganizeId(Organize record);
    
    
    int deleteByExample(OrganizeExample example);
    
    int insert(Organize record);
    
    Organize selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Organize record, @Param("example") OrganizeExample example);

    int updateByExample(@Param("record") Organize record, @Param("example") OrganizeExample example);

    int updateByPrimaryKeySelective(Organize record);

    int updateByPrimaryKey(Organize record);
}