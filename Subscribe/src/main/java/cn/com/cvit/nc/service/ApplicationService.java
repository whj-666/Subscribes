package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.Application;
import cn.com.cvit.nc.bean.ApplicationExample;
import cn.com.cvit.nc.dao.ApplicationMapper;

@Service
public class ApplicationService {
	
	@Autowired
	ApplicationMapper applicationMapper;
	//添加入会申请
	public int insertSelective(Application record){
		return applicationMapper.insertSelective(record);
	}
	//查询所有
	public List<Application> selectApplication(Application record){
		return applicationMapper.selectApplication(record);
	}	
	//按Email查询
	public List<Application> selectApplicationByEmail(Application record){
		return applicationMapper.selectApplicationByEmail(record);
	}
}
