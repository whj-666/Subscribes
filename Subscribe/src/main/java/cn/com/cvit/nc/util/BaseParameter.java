package cn.com.cvit.nc.util;

public class BaseParameter {
	//http://mobile.faw.com.cn:8089/jszx/app/html/
	//消息推送-预约url头
	final String pushUrlBegin="http://mobile.faw.com.cn:8089/jszx/app/html/OrderDetailed.html?OrderId=";
	//final String pushUrlBegin="http://fawyy.sunnyit.cn:8222/Subscribe/app/html/OrderDetailed.html?OrderId=";
	//消息推送-预约url尾11
	final String pushUrlEnd="&Pageflage=MyOrderPage";
	//消息推送-受邀url
	//http://mobile.faw.com.cn:8089/jszx/app/html/InviteDetailed.html?OrderId=e148520d-c1f7-451a-9499-cba653c&Pageflage=MyInvitePage
	final String pushUrlSonBegin = "http://mobile.faw.com.cn:8089/jszx/app/html/InviteDetailed.html?OrderId=";
	final String pushUrlSonEnd = "&Pageflage=MyInvitePage";
	final String pushUrlSon0="http://mobile.faw.com.cn:8089/jszx/app/html/MyInviteDetailed.html?state=0&flagePage=MyInvite";//待处理
	final String pushUrlSon1="http://mobile.faw.com.cn:8089/jszx/app/html/MyInviteDetailed.html?state=1&flagePage=MyInvite";//已接受
	final String pushUrlSon2="http://mobile.faw.com.cn:8089/jszx/app/html/MyInviteDetailed.html?state=2&flagePage=MyInvite";//已拒绝
	final String pushUrlSon3="http://mobile.faw.com.cn:8089/jszx/app/html/MyInviteDetailed.html?state=3&flagePage=MyInvite";//被撤销
	
	public String getPushUrlBegin() {
		return pushUrlBegin;
	}
	public String getPushUrlEnd() {
		return pushUrlEnd;
	}
	public String getPushUrlSon0() {
		return pushUrlSon0;
	}
	public String getPushUrlSon1() {
		return pushUrlSon1;
	}
	public String getPushUrlSon2() {
		return pushUrlSon2;
	}
	public String getPushUrlSon3() {
		return pushUrlSon3;
	}
	public String getPushUrlSonBegin() {
		return pushUrlSonBegin;
	}
	public String getPushUrlSonEnd() {
		return pushUrlSonEnd;
	}
	
	



}
