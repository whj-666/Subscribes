package cn.com.cvit.nc.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.cvit.nc.bean.Application;
import cn.com.cvit.nc.bean.ApplicationExample;
import cn.com.cvit.nc.bean.Msg;
import cn.com.cvit.nc.service.ApplicationService;
import cn.com.cvit.nc.service.PersonService;

@Controller
public class ApplicationController {
	
	@Autowired
	ApplicationService applicationService;
	@Autowired
	PersonService personService;
	
	//添加入会申请
	@RequestMapping(value = "/insertApplication",method = RequestMethod.GET)
	@ResponseBody
	public Msg insertSelective(String staffidno, String name,String gender,String cardtype,String cardno,String national,String birthday,String qqwechat,String address,String email,String phone,String phone1,String phone2,String department,String isdisease,String ishurt,String issport,String goal) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		String uuid = UUID.randomUUID().toString().substring(0, 31);// 32位UUID
		Application app = new Application();
		app.setId(uuid);
		app.setStaffidno(staffidno);
		app.setName(name);
		app.setGender(gender);
		app.setCardtype(cardtype);
		app.setCardno(cardno);
		app.setNational(national);
		app.setBirthday(birthday);
		app.setQqwechat(qqwechat);
		app.setAddress(address);
		app.setEmail(email);
		app.setPhone(phone);
		app.setPhone1(phone1);
		app.setPhone2(phone2);
		app.setDepartment(department);
		app.setIsdisease(isdisease);
		app.setIshurt(ishurt);
		app.setIssport(issport);
		app.setGoal(goal);
		app.setCreatetime(df.parse(df.format(new Date())));
		if(applicationService.insertSelective(app)==1){
			System.out.println(email);
			if(personService.updateIsApplicationByEmail(email)==1){
				return Msg.success().add("success", "提交申请成功");
			}else{
				return Msg.fail().add("fail", "入会申请状态修改失败");
			}
		}else{
			return Msg.fail().add("fail", "提交申请失败");
		}
	}
	//查询所有入会申请
	@RequestMapping(value = "/selectApplicationAll",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectApplicationAll(){
		List<Application> list = applicationService.selectApplication(null);
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "入会数据为空");
		}
	}
	//按email查询入会申请
	@RequestMapping(value = "/selectApplicationByEmail",method = RequestMethod.GET)
	@ResponseBody
	public Msg selectApplicationByEmail(String email){
		Application app = new Application();
		app.setEmail(email);
		List<Application> list = applicationService.selectApplicationByEmail(app);
		if(list.size()>0){
			return Msg.success().add("success", list);
		}else{
			return Msg.fail().add("fail", "入会数据为空");
		}
	}

}
