package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.Person;
import cn.com.cvit.nc.dao.PersonMapper;

@Service
public class PersonService {
	@Autowired
	PersonMapper personMapper;
	
	//根据id删除部门
    public int deleteByPrimaryKey(String id){
    	return personMapper.deleteByPrimaryKey(id);
    }
    //删除所有部门
    public int deletePersonAll(){
		return personMapper.deletePersonAll();
    }
    //新增部门
    public int insertSelective(Person record){
		return personMapper.insertSelective(record);
    }
    //查询所有
    public List<Person> selectByExample(){
		return personMapper.selectByExample(null);
    }
    //根据Email查询
    public List<Person> selectByPersonEmail(Person record){
		return personMapper.selectByPersonEmail(record);
    }
    //根据name查询
    public List<Person> selectByPersonTrueName(Person record){
		return personMapper.selectByPersonTrueName(record);
    }
    //根据Email和password查询
    public List<Person> selectByPersonEmailAndPassword(Person record){
		return personMapper.selectByPersonEmailAndPassword(record);
    }
    //根据部门Id查询
    public List<Person> selectByUserTeam(String record){
		return personMapper.selectByUserTeam(record);
    }
    //根据Id查询
    public List<Person> selectByPersonUserId(Person record){
		return personMapper.selectByPersonUserId(record);
    }
    //根据id修改
    public int updateByPersonId(Person record){
		return personMapper.updateByPersonId(record);
    }
    // 根据email查询并修改isapplication=1
    public int updateIsApplicationByEmail(String email){
    	return personMapper.updateIsApplicationByEmail(email);
    }

}
