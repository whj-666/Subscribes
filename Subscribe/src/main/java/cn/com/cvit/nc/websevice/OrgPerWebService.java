package cn.com.cvit.nc.websevice;

import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import cn.com.cvit.nc.bean.Organize;
import cn.com.cvit.nc.bean.Person;
import cn.com.cvit.nc.bean.Subscribe;
import cn.com.cvit.nc.bean.SubscribeSon;
import cn.com.cvit.nc.service.OrganizeService;
import cn.com.cvit.nc.service.PersonService;
import cn.com.cvit.nc.service.SubscribeService;
import cn.com.cvit.nc.service.SubscribeSonService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WebService
public class OrgPerWebService extends SpringBeanAutowiringSupport{
	
	@Autowired
	OrganizeService organizeService;
	
	@Autowired
	PersonService personService;
	
	@Autowired
	SubscribeService subscribeService;
	
	@Autowired
	SubscribeSonService subscribeSonService;
	
	//通过预约人email查询今天预约简略信息-推送
	public String selectMessageByEmailWebService(String email){
		Subscribe sub = new Subscribe();
		sub.setSubscribeUserEmail(email);
		List<Subscribe> list = subscribeService.selectMessageByEmailWebService(sub);
		String message = subscribeService.ProLogList2Json(list).toString();
		return "{\"message\":"+message+",\"number\":"+list.size()+"}";
	}
	//提示未受理信息条数
	public String selectStatusByEmailWebService(String email){
		Subscribe sub = new Subscribe();
		sub.setSubscribeUserEmail(email);
		List<Subscribe> list = subscribeService.selectSubscribeStatusByEmailWebService(sub);
		int subnum = (list.size());//预约信息-进行中-条数
		
		SubscribeSon subson = new SubscribeSon();
		subson.setSubscribePartnerId(email);
		List<SubscribeSon> list2 = subscribeSonService.selectSubscribeSonStatusByEmailWebService(subson);
		int subsonnum = list2.size();//受邀信息-待处理-条数
		return "{\"status\":"+subnum+",\"statusson\":"+subsonnum+"}";
	}
	
	/*-----------------部  门------------------------*/
    //新增部门
    public String insertOrganize(String jsonUsers){
    	String str = "["+jsonUsers+"]";
		JSONArray json = JSONArray.fromObject(str);
		Organize org = new Organize();
		if(jsonUsers != "" || !jsonUsers.equals("")){
				JSONObject jsonUser = json.getJSONObject(0);
				if(jsonUser.getString("code")!="" || !jsonUser.getString("code").equals("")){
					org.setCode(jsonUser.getString("code"));
				}
				if(jsonUser.getString("departType")!="" || !jsonUser.getString("departType").equals("")){
					org.setDeparttype(jsonUser.getString("departType"));
				}
				if(jsonUser.getString("description")!="" || !jsonUser.getString("description").equals("")){
					org.setDescription(jsonUser.getString("description"));
				}
				if(jsonUser.getString("id")!="" || !jsonUser.getString("id").equals("")){
					org.setId(jsonUser.getString("id"));
				}
				if(jsonUser.getString("name")!="" || !jsonUser.getString("name").equals("")){
					org.setName(jsonUser.getString("name"));
				}
				if(jsonUser.getString("orders")!="" || !jsonUser.getString("orders").equals("")){
					org.setOrders(jsonUser.getString("orders"));
				}
				if(jsonUser.getString("pid")!="" || !jsonUser.getString("pid").equals("")){
					org.setPid(jsonUser.getString("pid"));
				}
				if(jsonUser.getString("shortname")!="" || !jsonUser.getString("shortname").equals("")){
					org.setShortname(jsonUser.getString("shortname"));
				}
				if(jsonUser.getString("type")!="" || !jsonUser.getString("type").equals("")){
					org.setType(jsonUser.getString("type"));
				}
				if(organizeService.selectByOrganizeId(org).size()>0){
					if(organizeService.updateByOrganizeId(org)>0){
						return "{\"code\":\"1\",\"msg\":\"更新成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"更新失败\"}";
					}
				}else{
					if(organizeService.insertSelective(org) == 1){
						return "{\"code\":\"1\",\"msg\":\"添加成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"添加失败\"}";
					}
				}
			}else{
				return "{\"code\":\"0\",\"msg\":\"数据为空\"}";
			}
		}
	/*-------------------用   户------------------------*/
    //新增用户
    public String insertPerson(String jsonUsers){
		String str = "["+jsonUsers+"]";
		JSONArray json = JSONArray.fromObject(str);
		Person per = new Person();
		if(jsonUsers != "" || !jsonUsers.equals("")){
				JSONObject jsonUser = json.getJSONObject(0);
				if(jsonUser.getString("email")!="" || !jsonUser.getString("email").equals("")){
					per.setEmail(jsonUser.getString("email"));
				}
				if(jsonUser.getString("userId")!="" || !jsonUser.getString("userId").equals("")){
					per.setUserid(jsonUser.getString("userId"));
				}
				if(jsonUser.getString("IDNumber")!="" || !jsonUser.getString("IDNumber").equals("")){
					per.setIdnumber(jsonUser.getString("IDNumber"));
				}
				if(jsonUser.getString("hrEmplid")!="" || !jsonUser.getString("hrEmplid").equals("")){
					per.setHremplid(Integer.valueOf(jsonUser.getString("hrEmplid")));
				}
				if(jsonUser.getString("keyTalents")!="" || !jsonUser.getString("keyTalents").equals("")){
					per.setKeytalents(jsonUser.getString("keyTalents"));
				}
				if(jsonUser.getString("lockState")!="" || !jsonUser.getString("lockState").equals("")){
					per.setLockstate(jsonUser.getString("lockState"));
				}
				if(jsonUser.getString("mobilePhone")!="" || !jsonUser.getString("mobilePhone").equals("")){
					per.setMobilephone(jsonUser.getString("mobilePhone"));
				}
				if(jsonUser.getString("officePhone")!="" || !jsonUser.getString("officePhone").equals("")){
					per.setOfficephone(jsonUser.getString("officePhone"));
				}
				if(jsonUser.getString("oldCompany")!="" || !jsonUser.getString("oldCompany").equals("")){
					per.setOldcompany(jsonUser.getString("oldCompany"));
				}
				if(jsonUser.getString("oldDepartment")!="" || !jsonUser.getString("oldDepartment").equals("")){
					per.setOlddepartment(jsonUser.getString("oldDepartment"));
				}
				if(jsonUser.getString("organizeAllName")!="" || !jsonUser.getString("organizeAllName").equals("")){
					per.setOrganizeallname(jsonUser.getString("organizeAllName"));
				}
				if(jsonUser.getString("organizeCode")!="" || !jsonUser.getString("organizeCode").equals("")){
					per.setOrganizecode(jsonUser.getString("organizeCode"));
				}
				if(jsonUser.getString("politicsStatus")!="" || !jsonUser.getString("politicsStatus").equals("")){
					per.setPoliticsstatus(jsonUser.getString("politicsStatus"));
				}
				if(jsonUser.getString("salaryOwner")!="" || !jsonUser.getString("salaryOwner").equals("")){
					per.setSalaryowner(jsonUser.getString("salaryOwner"));
				}
				if(jsonUser.getString("sequenceId")!="" || !jsonUser.getString("sequenceId").equals("")){
					per.setSequenceid(Integer.valueOf(jsonUser.getString("sequenceId")));
				}
				if(jsonUser.getString("shortNameLC")!="" || !jsonUser.getString("shortNameLC").equals("")){
					per.setShortnamelc(jsonUser.getString("shortNameLC"));
				}
				if(jsonUser.getString("shortNameUC")!="" || !jsonUser.getString("shortNameUC").equals("")){
					per.setShortnameuc(jsonUser.getString("shortNameUC"));
				}
				if(jsonUser.getString("trainingSituation")!="" || !jsonUser.getString("trainingSituation").equals("")){
					per.setTrainingsituation(jsonUser.getString("trainingSituation"));
				}
				if(jsonUser.getString("userAddress")!="" || !jsonUser.getString("userAddress").equals("")){
					per.setUseraddress(jsonUser.getString("userAddress"));
				}
				if(jsonUser.getString("userBirthday")!="" || !jsonUser.getString("userBirthday").equals("")){
					per.setUserbirthday(jsonUser.getString("userBirthday"));
				}
				if(jsonUser.getString("userCategory")!="" || !jsonUser.getString("userCategory").equals("")){
					per.setUsercategory(jsonUser.getString("userCategory"));
				}
				if(jsonUser.getString("userCode")!="" || !jsonUser.getString("userCode").equals("")){
					per.setUsercode(jsonUser.getString("userCode"));
				}
				if(jsonUser.getString("userCompanyId")!="" || !jsonUser.getString("userCompanyId").equals("")){
					per.setUsercompanyid(jsonUser.getString("userCompanyId"));
				}
				if(jsonUser.getString("userDuty")!="" || !jsonUser.getString("userDuty").equals("")){
					per.setUserduty(jsonUser.getString("userDuty"));
				}
				if(jsonUser.getString("userEmployment")!="" || !jsonUser.getString("userEmployment").equals("")){
					per.setUseremployment(jsonUser.getString("userEmployment"));
				}
				if(jsonUser.getString("userEntryDate")!="" || !jsonUser.getString("userEntryDate").equals("")){
					per.setUserentrydate(jsonUser.getString("userEntryDate"));
				}
				if(jsonUser.getString("userName")!="" || !jsonUser.getString("userName").equals("")){
					per.setUsername(jsonUser.getString("userName"));
				}
				if(jsonUser.getString("userNation")!="" || !jsonUser.getString("userNation").equals("")){
					per.setUsernation(jsonUser.getString("userNation"));
				}
				if(jsonUser.getString("userOfficeAddress")!="" || !jsonUser.getString("userOfficeAddress").equals("")){
					per.setUserofficeaddress(jsonUser.getString("userOfficeAddress"));
				}
				if(jsonUser.getString("userPassword")!="" || !jsonUser.getString("userPassword").equals("")){
					per.setUserpassword(jsonUser.getString("userPassword"));
				}
				if(jsonUser.getString("userPhoto")!="" || !jsonUser.getString("userPhoto").equals("")){
					per.setUserphoto(jsonUser.getString("userPhoto"));
				}
				if(jsonUser.getString("userPositional")!="" || !jsonUser.getString("userPositional").equals("")){
					per.setUserpositional(jsonUser.getString("userPositional"));
				}
				if(jsonUser.getString("userSex")!="" || !jsonUser.getString("userSex").equals("")){
					per.setUsersex(jsonUser.getString("userSex"));
				}
				if(jsonUser.getString("userShoesSize")!="" || !jsonUser.getString("userShoesSize").equals("")){
					per.setUsershoessize(jsonUser.getString("userShoesSize"));
				}
				if(jsonUser.getString("userStature")!="" || !jsonUser.getString("userStature").equals("")){
					per.setUserstature(jsonUser.getString("userStature"));
				}
				if(jsonUser.getString("userTeam")!="" || !jsonUser.getString("userTeam").equals("")){
					per.setUserteam(jsonUser.getString("userTeam"));
				}
				if(jsonUser.getString("userTrueName")!="" || !jsonUser.getString("userTrueName").equals("")){
					per.setUsertruename(jsonUser.getString("userTrueName"));
				}
				if(jsonUser.getString("userWeight")!="" || !jsonUser.getString("userWeight").equals("")){
					per.setUserweight(jsonUser.getString("userWeight"));
				}
				if(jsonUser.getString("userWfpostion")!="" || !jsonUser.getString("userWfpostion").equals("")){
					per.setUserwfpostion(jsonUser.getString("userWfpostion"));
				}
				per.setIsadmin(0);
				if(personService.selectByPersonUserId(per).size()>0){
					if(personService.updateByPersonId(per)>0){
						return "{\"code\":\"1\",\"msg\":\"更新成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"更新失败\"}";
					}
				}else{
					if(personService.insertSelective(per) == 1){
						return "{\"code\":\"1\",\"msg\":\"添加成功\"}";
					}else{
						return "{\"code\":\"0\",\"msg\":\"添加失败\"}";
					}
				}
		}else
			return "{\"code\":\"0\",\"msg\":\"数据为空\"}";
  }
    
    
}
