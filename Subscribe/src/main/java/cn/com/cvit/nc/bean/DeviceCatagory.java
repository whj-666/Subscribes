package cn.com.cvit.nc.bean;

public class DeviceCatagory {
    private String id;

    private Integer equipmentNumber;

    private String equipmentName;

    private String equipmentSpell;

    private String equipmentFirstSpell;

    private Integer flag;
    
    private String images;//图片

    public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getEquipmentNumber() {
        return equipmentNumber;
    }

    public void setEquipmentNumber(Integer equipmentNumber) {
        this.equipmentNumber = equipmentNumber;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName == null ? null : equipmentName.trim();
    }

    public String getEquipmentSpell() {
        return equipmentSpell;
    }

    public void setEquipmentSpell(String equipmentSpell) {
        this.equipmentSpell = equipmentSpell == null ? null : equipmentSpell.trim();
    }

    public String getEquipmentFirstSpell() {
        return equipmentFirstSpell;
    }

    public void setEquipmentFirstSpell(String equipmentFirstSpell) {
        this.equipmentFirstSpell = equipmentFirstSpell == null ? null : equipmentFirstSpell.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}