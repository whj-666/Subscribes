package cn.com.cvit.nc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.cvit.nc.dao.TreeMapper;
/**
 * 导航栏TreeService
 */
@Service
public class TreeService {
	
	@Autowired
	TreeMapper treeMapper;
	
	/**
	 * 获取导航栏数据
	 * @param id 
	 */
	public List getTree(Integer id) {
		return treeMapper.getTree(id);
	}
}
