package cn.com.cvit.nc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.cvit.nc.bean.User;
import cn.com.cvit.nc.bean.UserExample;
import cn.com.cvit.nc.dao.UserMapper;

@Service
public class UserService {
	

	@Autowired
	private UserMapper userMapper;
	
	/*
	 * 查询所有
	 */
	public List<User> selectByExample(){
		List<User> list = userMapper.selectByExample(null);
		return list;
	}
	/*
	 * 按照email和name查找name可为空
	 */
	public List<User> getEmailAndNameAndTime(String email,String name){
		List<User> list = userMapper.selectByNamePrimaryKey(email,name);
		return list;
	}
	/*
	 * 添加新用户
	 */
	public int insertSelective(User user){
		return userMapper.insertSelective(user);
	}
	
	/*
	 * 根据email更新、修改用户
	 */
	public int updateByEmail(User user){
		return userMapper.updateByEmail(user);
	}
	/*
	 * 删除用户
	 */
	public int deleteByPrimaryKey(User user){
		return userMapper.deleteByPrimaryKey(user);
	}
	/*
	 * 清空用户表
	 */
	public int deleteByAll(){
		return userMapper.deleteByAll();
	}
	
}
