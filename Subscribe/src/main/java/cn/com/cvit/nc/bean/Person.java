package cn.com.cvit.nc.bean;

public class Person {
    private String userid;

    private String idnumber;

    private String email;

    private Integer hremplid;

    private String keytalents;

    private String lockstate;

    private String mobilephone;

    private String officephone;

    private String oldcompany;

    private String olddepartment;

    private String organizeallname;

    private String organizecode;

    private String politicsstatus;

    private String salaryowner;

    private Integer sequenceid;

    private String shortnamelc;

    private String shortnameuc;

    private String trainingsituation;

    private String useraddress;

    private String userbirthday;

    private String usercategory;

    private String usercode;

    private String usercompanyid;

    private String userduty;

    private String useremployment;

    private String userentrydate;

    private String username;

    private String usernation;

    private String userofficeaddress;

    private String userpassword;

    private String userphoto;

    private String userpositional;

    private Integer userrank;

    private String usersex;

    private String usershoessize;

    private String userstature;

    private String userteam;

    private String usertruename;

    private String userweight;

    private String userwfpostion;

    private Integer isadmin;

    private Integer isapplication;//是否填写健身入会申请2018-4-11

	public Integer getIsapplication() {
		return isapplication;
	}

	public void setIsapplication(Integer isapplication) {
		this.isapplication = isapplication;
	}

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(String userid, String idnumber, String email, Integer hremplid, String keytalents, String lockstate,
			String mobilephone, String officephone, String oldcompany, String olddepartment, String organizeallname,
			String organizecode, String politicsstatus, String salaryowner, Integer sequenceid, String shortnamelc,
			String shortnameuc, String trainingsituation, String useraddress, String userbirthday, String usercategory,
			String usercode, String usercompanyid, String userduty, String useremployment, String userentrydate,
			String username, String usernation, String userofficeaddress, String userpassword, String userphoto,
			String userpositional, Integer userrank, String usersex, String usershoessize, String userstature,
			String userteam, String usertruename, String userweight, String userwfpostion, Integer isadmin) {
		super();
		this.userid = userid;
		this.idnumber = idnumber;
		this.email = email;
		this.hremplid = hremplid;
		this.keytalents = keytalents;
		this.lockstate = lockstate;
		this.mobilephone = mobilephone;
		this.officephone = officephone;
		this.oldcompany = oldcompany;
		this.olddepartment = olddepartment;
		this.organizeallname = organizeallname;
		this.organizecode = organizecode;
		this.politicsstatus = politicsstatus;
		this.salaryowner = salaryowner;
		this.sequenceid = sequenceid;
		this.shortnamelc = shortnamelc;
		this.shortnameuc = shortnameuc;
		this.trainingsituation = trainingsituation;
		this.useraddress = useraddress;
		this.userbirthday = userbirthday;
		this.usercategory = usercategory;
		this.usercode = usercode;
		this.usercompanyid = usercompanyid;
		this.userduty = userduty;
		this.useremployment = useremployment;
		this.userentrydate = userentrydate;
		this.username = username;
		this.usernation = usernation;
		this.userofficeaddress = userofficeaddress;
		this.userpassword = userpassword;
		this.userphoto = userphoto;
		this.userpositional = userpositional;
		this.userrank = userrank;
		this.usersex = usersex;
		this.usershoessize = usershoessize;
		this.userstature = userstature;
		this.userteam = userteam;
		this.usertruename = usertruename;
		this.userweight = userweight;
		this.userwfpostion = userwfpostion;
		this.isadmin = isadmin;
	}

	@Override
	public String toString() {
		return "Person [userid=" + userid + ", idnumber=" + idnumber + ", email=" + email + ", hremplid=" + hremplid
				+ ", keytalents=" + keytalents + ", lockstate=" + lockstate + ", mobilephone=" + mobilephone
				+ ", officephone=" + officephone + ", oldcompany=" + oldcompany + ", olddepartment=" + olddepartment
				+ ", organizeallname=" + organizeallname + ", organizecode=" + organizecode + ", politicsstatus="
				+ politicsstatus + ", salaryowner=" + salaryowner + ", sequenceid=" + sequenceid + ", shortnamelc="
				+ shortnamelc + ", shortnameuc=" + shortnameuc + ", trainingsituation=" + trainingsituation
				+ ", useraddress=" + useraddress + ", userbirthday=" + userbirthday + ", usercategory=" + usercategory
				+ ", usercode=" + usercode + ", usercompanyid=" + usercompanyid + ", userduty=" + userduty
				+ ", useremployment=" + useremployment + ", userentrydate=" + userentrydate + ", username=" + username
				+ ", usernation=" + usernation + ", userofficeaddress=" + userofficeaddress + ", userpassword="
				+ userpassword + ", userphoto=" + userphoto + ", userpositional=" + userpositional + ", userrank="
				+ userrank + ", usersex=" + usersex + ", usershoessize=" + usershoessize + ", userstature="
				+ userstature + ", userteam=" + userteam + ", usertruename=" + usertruename + ", userweight="
				+ userweight + ", userwfpostion=" + userwfpostion + ", isadmin=" + isadmin + "]";
	}

	public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber == null ? null : idnumber.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getHremplid() {
        return hremplid;
    }

    public void setHremplid(Integer hremplid) {
        this.hremplid = hremplid;
    }

    public String getKeytalents() {
        return keytalents;
    }

    public void setKeytalents(String keytalents) {
        this.keytalents = keytalents == null ? null : keytalents.trim();
    }

    public String getLockstate() {
        return lockstate;
    }

    public void setLockstate(String lockstate) {
        this.lockstate = lockstate == null ? null : lockstate.trim();
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone == null ? null : mobilephone.trim();
    }

    public String getOfficephone() {
        return officephone;
    }

    public void setOfficephone(String officephone) {
        this.officephone = officephone == null ? null : officephone.trim();
    }

    public String getOldcompany() {
        return oldcompany;
    }

    public void setOldcompany(String oldcompany) {
        this.oldcompany = oldcompany == null ? null : oldcompany.trim();
    }

    public String getOlddepartment() {
        return olddepartment;
    }

    public void setOlddepartment(String olddepartment) {
        this.olddepartment = olddepartment == null ? null : olddepartment.trim();
    }

    public String getOrganizeallname() {
        return organizeallname;
    }

    public void setOrganizeallname(String organizeallname) {
        this.organizeallname = organizeallname == null ? null : organizeallname.trim();
    }

    public String getOrganizecode() {
        return organizecode;
    }

    public void setOrganizecode(String organizecode) {
        this.organizecode = organizecode == null ? null : organizecode.trim();
    }

    public String getPoliticsstatus() {
        return politicsstatus;
    }

    public void setPoliticsstatus(String politicsstatus) {
        this.politicsstatus = politicsstatus == null ? null : politicsstatus.trim();
    }

    public String getSalaryowner() {
        return salaryowner;
    }

    public void setSalaryowner(String salaryowner) {
        this.salaryowner = salaryowner == null ? null : salaryowner.trim();
    }

    public Integer getSequenceid() {
        return sequenceid;
    }

    public void setSequenceid(Integer sequenceid) {
        this.sequenceid = sequenceid;
    }

    public String getShortnamelc() {
        return shortnamelc;
    }

    public void setShortnamelc(String shortnamelc) {
        this.shortnamelc = shortnamelc == null ? null : shortnamelc.trim();
    }

    public String getShortnameuc() {
        return shortnameuc;
    }

    public void setShortnameuc(String shortnameuc) {
        this.shortnameuc = shortnameuc == null ? null : shortnameuc.trim();
    }

    public String getTrainingsituation() {
        return trainingsituation;
    }

    public void setTrainingsituation(String trainingsituation) {
        this.trainingsituation = trainingsituation == null ? null : trainingsituation.trim();
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress == null ? null : useraddress.trim();
    }

    public String getUserbirthday() {
        return userbirthday;
    }

    public void setUserbirthday(String userbirthday) {
        this.userbirthday = userbirthday == null ? null : userbirthday.trim();
    }

    public String getUsercategory() {
        return usercategory;
    }

    public void setUsercategory(String usercategory) {
        this.usercategory = usercategory == null ? null : usercategory.trim();
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode == null ? null : usercode.trim();
    }

    public String getUsercompanyid() {
        return usercompanyid;
    }

    public void setUsercompanyid(String usercompanyid) {
        this.usercompanyid = usercompanyid == null ? null : usercompanyid.trim();
    }

    public String getUserduty() {
        return userduty;
    }

    public void setUserduty(String userduty) {
        this.userduty = userduty == null ? null : userduty.trim();
    }

    public String getUseremployment() {
        return useremployment;
    }

    public void setUseremployment(String useremployment) {
        this.useremployment = useremployment == null ? null : useremployment.trim();
    }

    public String getUserentrydate() {
        return userentrydate;
    }

    public void setUserentrydate(String userentrydate) {
        this.userentrydate = userentrydate == null ? null : userentrydate.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUsernation() {
        return usernation;
    }

    public void setUsernation(String usernation) {
        this.usernation = usernation == null ? null : usernation.trim();
    }

    public String getUserofficeaddress() {
        return userofficeaddress;
    }

    public void setUserofficeaddress(String userofficeaddress) {
        this.userofficeaddress = userofficeaddress == null ? null : userofficeaddress.trim();
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword == null ? null : userpassword.trim();
    }

    public String getUserphoto() {
        return userphoto;
    }

    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto == null ? null : userphoto.trim();
    }

    public String getUserpositional() {
        return userpositional;
    }

    public void setUserpositional(String userpositional) {
        this.userpositional = userpositional == null ? null : userpositional.trim();
    }

    public Integer getUserrank() {
        return userrank;
    }

    public void setUserrank(Integer userrank) {
        this.userrank = userrank;
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex == null ? null : usersex.trim();
    }

    public String getUsershoessize() {
        return usershoessize;
    }

    public void setUsershoessize(String usershoessize) {
        this.usershoessize = usershoessize == null ? null : usershoessize.trim();
    }

    public String getUserstature() {
        return userstature;
    }

    public void setUserstature(String userstature) {
        this.userstature = userstature == null ? null : userstature.trim();
    }

    public String getUserteam() {
        return userteam;
    }

    public void setUserteam(String userteam) {
        this.userteam = userteam == null ? null : userteam.trim();
    }

    public String getUsertruename() {
        return usertruename;
    }

    public void setUsertruename(String usertruename) {
        this.usertruename = usertruename == null ? null : usertruename.trim();
    }

    public String getUserweight() {
        return userweight;
    }

    public void setUserweight(String userweight) {
        this.userweight = userweight == null ? null : userweight.trim();
    }

    public String getUserwfpostion() {
        return userwfpostion;
    }

    public void setUserwfpostion(String userwfpostion) {
        this.userwfpostion = userwfpostion == null ? null : userwfpostion.trim();
    }

    public Integer getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(Integer isadmin) {
        this.isadmin = isadmin;
    }
}