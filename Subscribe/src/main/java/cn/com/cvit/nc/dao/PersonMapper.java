package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.Person;
import cn.com.cvit.nc.bean.PersonExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PersonMapper {
    long countByExample(PersonExample example);

    
    //根据id删除部门
    int deleteByPrimaryKey(String id);
    //删除所有部门
    int deletePersonAll();
    //新增部门
    int insertSelective(Person record);
    //查询所有
    List<Person> selectByExample(PersonExample example);
    //根据Email查询
    List<Person> selectByPersonEmail(Person record);
    //根据name查询
    List<Person> selectByPersonTrueName(Person record);
    //根据Email和password查询
    List<Person> selectByPersonEmailAndPassword(Person record);
    //根据Email编号查询
    List<Person> selectByPersonName(Person record);
    //根据部门ID查询
    List<Person> selectByUserTeam(String userTeam);
    //根据Id查询
    List<Person> selectByPersonUserId(Person record);
    //根据id修改
    int updateByPersonId(Person record);
    // 根据email查询并修改isapplication=1
    int updateIsApplicationByEmail(String email);
    
    int deleteByExample(PersonExample example);

    int insert(Person record);

    Person selectByPrimaryKey(String userid);

    int updateByExampleSelective(@Param("record") Person record, @Param("example") PersonExample example);

    int updateByExample(@Param("record") Person record, @Param("example") PersonExample example);

    int updateByPrimaryKeySelective(Person record);

    int updateByPrimaryKey(Person record);
}