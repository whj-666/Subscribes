package cn.com.cvit.nc.dao;

import cn.com.cvit.nc.bean.SubscribeSon;
import cn.com.cvit.nc.bean.SubscribeSonExample;
import cn.com.cvit.nc.bean.SubscribeSonKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SubscribeSonMapper {
    long countByExample(SubscribeSonExample example);

    int deleteByExample(SubscribeSonExample example);

    int deleteByPrimaryKey(SubscribeSonKey key);

    int insert(SubscribeSon record);

    //插入一条数据
    int insertSelective(SubscribeSon record);

    //查询所有受邀信息
    List<SubscribeSon> selectByExampleWithName(SubscribeSonExample example);
    
    //根据subscribe_id查询我的邀请
    List<SubscribeSon> selectBySubscribeId(String subscribe_id);
    //根据subscribe_id查询我的邀请--2018-5-16--消息推送
    List<SubscribeSon> selectBySubscribeIdForPushMessage(String subscribe_id);
    
    //根据subscribe_id查询我的邀请-筛选出0待确定1接受2018-4-20
    List<SubscribeSon> selectBySubscribeIdWebService(String subscribe_id);
    
    //根据subscribe_partner_id和subscribe_user_name查询我的受邀
    List<SubscribeSon> selectByEmailAndNameWith(String email,String name);
    
    // 根据id查询子表信息
    List<SubscribeSon> selectBySubscribeSonId(String id);
    
    // 根据subscribe_partner_id和subscribe_user_name查询我的受邀
    List<SubscribeSon> selectByEmailAndName(String email,String name);
    
    // web、service提示是否有未处理受邀信息2018-4-12
    List<SubscribeSon> selectSubscribeSonStatusByEmailWebService(SubscribeSon record);
    
    List<SubscribeSon> selectByExample(SubscribeSonExample example);

    SubscribeSon selectByPrimaryKey(SubscribeSonKey key);

    int updateByExampleSelective(@Param("record") SubscribeSon record, @Param("example") SubscribeSonExample example);

    int updateByExample(@Param("record") SubscribeSon record, @Param("example") SubscribeSonExample example);
    /*
     * 修改
     * update状态1.正常结束2.撤销（撤销原因）
     * update受邀状态1、接受2、拒绝（拒绝原因）
     */
    int updateByPrimaryKeySelective(SubscribeSon record);
    //修改状态2018-5-16
    int updateBySubscribeId(SubscribeSon record);
    int updateBySubscribeIdTwo(SubscribeSon record);
    
    int updateByPrimaryKey(SubscribeSon record);
}