package cn.com.cvit.nc.bean;

import java.util.Date;

public class Application {
    private String id;

    private String staffidno;

    private String name;

    private String gender;

    private String cardtype;

    private String cardno;

    private String national;

    private String birthday;

    private String qqwechat;

    private String address;

    private String email;

    private String phone;

    private String phone1;

    private String phone2;

    private String department;

    private String isdisease;

    private String ishurt;

    private String issport;

    private String goal;

    private Date createtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getStaffidno() {
        return staffidno;
    }

    public void setStaffidno(String staffidno) {
        this.staffidno = staffidno == null ? null : staffidno.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype == null ? null : cardtype.trim();
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno == null ? null : cardno.trim();
    }

    public String getNational() {
        return national;
    }

    public void setNational(String national) {
        this.national = national == null ? null : national.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public String getQqwechat() {
        return qqwechat;
    }

    public void setQqwechat(String qqwechat) {
        this.qqwechat = qqwechat == null ? null : qqwechat.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1 == null ? null : phone1.trim();
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2 == null ? null : phone2.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getIsdisease() {
        return isdisease;
    }

    public void setIsdisease(String isdisease) {
        this.isdisease = isdisease == null ? null : isdisease.trim();
    }

    public String getIshurt() {
        return ishurt;
    }

    public void setIshurt(String ishurt) {
        this.ishurt = ishurt == null ? null : ishurt.trim();
    }

    public String getIssport() {
        return issport;
    }

    public void setIssport(String issport) {
        this.issport = issport == null ? null : issport.trim();
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal == null ? null : goal.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}